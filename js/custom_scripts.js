var customJS;
jQuery(document).ready(function($) {
    customJS = {
        common: {
            commonJS: function() {
                $('.menu-icon .hamburger').on('click', function() {
                    $('.hamburger-menu-wrapper').css('display', 'flex');
                });
                $('.hamburger-menu-wrapper .menu-close').on('click', function() {
                    $('.hamburger-menu-wrapper').fadeOut();
                });
                var $contents = $('.hamburger-nav li ul').hide();
                $('.hamburger-nav li span').on('click', function() {
                    var getThis = $(this);
                    var $current = $(this).next().stop(true, true).slideToggle(500, function() {
                        getAccordion();
                    });
                    $contents.not($current).slideUp(600, function() {
                        getAccordion();
                    });

                    function getAccordion() {
                        $('.hamburger-nav li span').removeClass('open');
                        if ($(getThis).closest('li').find('ul').is(':visible')) {
                            $(getThis).addClass('open');
                        } else {
                            $('.hamburger-nav li span').removeClass('open');
                        }
                    }
                });
                $(".tab").click(function() {
                    if (!$(this).hasClass('active')) {
                        $(".tab.active").removeClass("active");
                        $(this).addClass("active");
                    }
                });
                $(document).ready(function() {
                    $('#dropdown-content').hide()
                });
                $('#dropdown').on('click', function() {
                    $('#dropdown-content').show(500)
                });
                $(document).mouseup(function(e) {
                    var popup = $("#dropdown-content");
                    if (!$('#dropdown').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
                        popup.hide(500);
                    }
                    $('#dropdown').on('click', function() {
                        $('#dropdown-content').stop(true, true).show(500);
                    });
                    $('body').on('click', ' .add-address', function() {
                        $('.career-modal').css('display', 'flex');
                    });
                    $('body').on('click', ' .account-logincheck', function() {
                        $('.career-login').css('display', 'flex');
                    });
                    $('body').on('click', '.modal-wrapper .btnCloseModal', function() {
                        $(this).closest('.modal-wrapper').fadeOut();
                    });
                });
            },
            html5Tags: function() {
                document.createElement('header');
                document.createElement('section');
                document.createElement('nav');
                document.createElement('footer');
                document.createElement('menu');
                document.createElement('hgroup');
                document.createElement('article');
                document.createElement('aside');
                document.createElement('details');
                document.createElement('figure');
                document.createElement('time');
                document.createElement('mark');
            }
        } //end commonJS
    };
    customJS.common.commonJS();
    customJS.common.html5Tags();
});