<?php $__env->startSection('title', 'Roles List'); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid mt-5">
    <div class="row  align-items-center justify-content-between mb-5">
        <h6 class="h6 mb-0 dashboard-title">Roles List:</h6>
        <div class="add-search">
            <a href="<?php echo e(route('admin.roles.create')); ?>" class="btn btn-primary btn-purple" title="Add New">Add New</a>
            <form class="d-sm-inline-block form-inline navbar-search" action="" method="post"
                enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search By Name"
                        aria-label="Search" aria-describedby="basic-addon2" name="search">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit"> Search </button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>
<div class="container-fluid table-register">
    <div class="row ">
        <table class="table table-registerd-users ">
            <thead>
                <tr>
                    <th scope="col">Sl.No</th>
                    <th scope="col">Roles</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($roles[0])): ?> <?php $i=1; ?>
                <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($i); ?></td>
                    <td><?php echo e($values->name); ?></td>
                    <td class="action-buttons">
                        <a href="<?php echo e(route('admin.roles.show',$values->id)); ?>" class="btn preview">
                            <img src="<?php echo e(asset('backend/assets/images/preview.png')); ?>" alt="preview" />
                        </a>

                        <a href="<?php echo e(route('admin.roles.edit',$values->id)); ?>" type="button" class="btn edit">
                            <img src="<?php echo e(asset('backend/assets/images/edit.png')); ?>" alt="preview" />
                        </a>

                        <form action="<?php echo e(route('admin.roles.destroy', $values->id)); ?>" method="post">
                            <?php echo e(method_field('DELETE')); ?>

                            <?php echo e(csrf_field()); ?>

                            <button type="submit" class="btn trash"
                                onclick="return confirm('Are you sure you want to delete?')">
                                <img src="<?php echo e(asset('backend/assets/images/trash.png')); ?>" alt="preview" />
                            </button>
                        </form>
                    </td>
                </tr>
                <?php $i++; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </tbody>
        </table>
        <ul class="pagination d-flex mt-3 mb-3">
            <?php echo e($roles->links()); ?>

        </ul>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
    $(function() {
        $('.delete-model').on('click', function(){
            var target_id = $(this).data('id');
            var delete_route = '<?php echo e(route('admin.roles.destroy')); ?>'+'/'+target_id;
            var delete_form = $('#delete-form');

            delete_form.attr("action", delete_route);
        });

        $(document).on('click', '.modal-dismiss', function (e) {
            var delete_form = $('#delete-form');
            delete_form.removeAttr("action");
        });

    });
    $(document).ready(function() {

        $('#selectall').click(function(event) { //on click
            if(this.checked) { // check select status
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"
                });
            }else{
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"
                });
            }
        });

      /*Remove All*/
        $( "#removeall" ).click(function() {


            var selectedVal = new Array();
                $('input[name="check"]:checked').each(function() {
            selectedVal.push(this.value);
            });


            var values= selectedVal;

            var valC=selectedVal.length;
                var url='<?php echo e(route('admin.roles.destroy')); ?>';
            if(valC != 0){
            if(confirm('Are you sure you want to delete?')){
                $.ajax({
                type: "DELETE",
                    url: url+'/'+values,

                    success: function(data){

                        location.reload();
                        }

                });
            }
            else{
                //alert("fg");
            }
            }else{
            $('#msg').html("Please select at least one record").show().delay(600000000).hide('slow');
            }
        });
      /*End*/

    });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/roles/list.blade.php ENDPATH**/ ?>