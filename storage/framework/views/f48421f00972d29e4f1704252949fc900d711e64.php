<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
        <?php if(isset(Auth::user()->image)): ?>
                             <img src="<?php echo e(asset(Auth::user()->image)); ?>" class="user-circle" alt="User Image">
                         <?php else: ?>
                             <img src="<?php echo e(asset('images/default-user.jpg')); ?>" alt="" class="user-circle">
                         <?php endif; ?>
        </div>
        <div class="pull-left info">
            <p><?php if(isset(Auth::user()->display_name)): ?> <?php echo e(Auth::user()->display_name); ?><?php endif; ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
         <li class="<?php echo e(Request::is('dashboard') ? 'active' : ''); ?>" ><a href="<?php echo e(url('/dashboard')); ?>"  ><i class="fa fa-bars" style="color:#2E8B57"></i> <span>Dashboard</span></a></li>

        <!-- <li class="<?php echo e(Request::is('articles') ? 'active' : ''); ?>" ><a href="<?php echo e(url('/articles')); ?>"  ><i class="fa fa-newspaper" style="color:#004225"></i> <span>Articles</span></a></li> -->
        <?php if(Auth::user()->role_id == 1): ?>
           
            <li class="<?php echo e(Request::is('dashboard/users') ? 'active' : ''); ?>" ><a   href="<?php echo e(url('/dashboard/users')); ?>" ><i class="fa fa-users" style="color:#2E8B57"></i> <span>Users</span></a></li>
             <li class="<?php echo e(Request::is('dashboard/shops') ? 'active' : ''); ?>" ><a   href="<?php echo e(url('/dashboard/shops')); ?>" ><i class="fa fa-industry" style="color:#2E8B57"></i> <span>Shops</span></a></li>
             
        <?php endif; ?>  
        <?php if(Auth::user()->role_id == 1): ?>
          <li class="<?php echo e(Request::is('dashboard/categories') ? 'active' : ''); ?>" ><a   href="<?php echo e(url('/dashboard/categories')); ?>" ><i class="fa fa-utensils" style="color:#2E8B57"></i> <span>Categories</span></a></li>
          <?php else: ?>
          <li class="<?php echo e(Request::is('dashboard/shopcategories') ? 'active' : ''); ?>" ><a   href="<?php echo e(url('/dashboard/shopcategories')); ?>" >
            <i class="fa fa-utensils" style="color:#2E8B57"></i> <span>Categories</span></a></li>
          <?php endif; ?> 
        <li class="<?php echo e(Request::is('dashboard/orders') ? 'active' : ''); ?>" ><a   href="<?php echo e(url('/dashboard/orders')); ?>" ><i class="fa fa-shopping-cart" style="color:#2E8B57"></i> <span>Orders</span></a></li> 
        <li class="<?php echo e(Request::is('dashboard/products') ? 'active' : ''); ?>" ><a   href="<?php echo e(url('/dashboard/products')); ?>" ><i class="fa fa-tags" style="color:#2E8B57"></i> <span>Products</span></a></li>
        <li class="<?php echo e(Request::is('dashboard/addons') ? 'active' : ''); ?>" ><a   href="<?php echo e(url('/dashboard/addons')); ?>" ><i class="fa fa-puzzle-piece" style="color:#2E8B57"></i> <span>Addons</span></a></li>
         <li class="<?php echo e(Request::is('dashboard/offers') ? 'active' : ''); ?>" ><a   href="<?php echo e(url('/dashboard/offers')); ?>" ><i class="fa fa-gift" style="color:#2E8B57"></i> <span>Offers</span></a></li>
         <li class="<?php echo e(Request::is('dashboard/coupons') ? 'active' : ''); ?>" ><a   href="<?php echo e(url('/dashboard/coupons')); ?>" ><i class="fa fa-envelope-open" style="color:#2E8B57"></i> <span>Coupons</span></a></li>
         <?php if(Auth::user()->role_id == 1): ?>
          <li class="<?php echo e(Request::is('dashboard/areas') ? 'active' : ''); ?>" ><a   href="<?php echo e(url('/dashboard/areas')); ?>" ><i class="fa fa-area-chart" style="color:#2E8B57"></i> <span>Delivery Areas</span></a></li>
          <?php else: ?>
          <li class="<?php echo e(Request::is('dashboard/shopareas') ? 'active' : ''); ?>" ><a   href="<?php echo e(url('/dashboard/shopareas')); ?>" ><i class="fa fa-area-chart" style="color:#2E8B57"></i> <span>Delivery Areas</span></a></li>
          <?php endif; ?> 
        <!-- <li class="<?php echo e(Request::is('dining') ? 'active' : ''); ?> treeview" ><a href="<?php echo e(url('/dining')); ?>"  ><i class="fa fa-hotel" style="color:#dd4b39"></i> <span>Dining</span><i class="fa fa-angle-left pull-right"></i></a>
             <ul class="treeview-menu menu-open">
                <li class="<?php echo e(Request::is('dining') ? 'active' : ''); ?>" ><a href="<?php echo e(url('/dining')); ?>"  ><i class="fa fa-utensils"></i> <span>Dining</span></a></li>
                <li class="<?php echo e(Request::is('dining/category') ? 'active' : ''); ?>" ><a href="<?php echo e(url('/dining/category')); ?>"  ><i class="fa fa-th"></i> <span>Dining Category</span></a></li>
                <li class="<?php echo e(Request::is('dining/healthymenu') ? 'active' : ''); ?>" ><a href="<?php echo e(url('/dining/healthymenu')); ?>"  ><i class="fa fa-th"></i> <span>Healthy Menu</span></a></li>
            </ul>
        </li> -->
        <!-- <li class="<?php echo e(Request::is('notifications') ? 'active' : ''); ?>" ><a href="<?php echo e(url('/notifications')); ?>"  ><i class="fa fa-bell" style="color:#dd4b39"></i> <span>Notifications</span></a></li> -->
        <?php if(Auth::check()): ?>
        <li><a href="<?php echo e(url('/logout')); ?>" onclick="fnLogout()"  ><i class="fa fa-sign-out-alt text-red" style="color:#2E8B57 !important"></i></span>Logout</a></li>
        <?php endif; ?>
    </ul>
</section><?php /**PATH /var/www/html/laraveltest/resources/views/layouts/portions/mainSidebar.blade.php ENDPATH**/ ?>