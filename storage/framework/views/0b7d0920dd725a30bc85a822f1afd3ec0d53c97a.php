<?php $__env->startSection('title', 'Event Attendees'); ?>
<?php $__env->startSection('content'); ?>

<div class="edit-event-wrapper">
    <div class="container-fluid mt-3 mb-4">


        <div class="row ">
            <div class=" col-lg-6 mb-2 ">
                <h2 class="h2 mb-0 "><?php echo e($events->name); ?> Sessions</h2>
            </div>

        </div>
    </div>
    <div class="container-fluid edit-events event-sections">
        <?php if(count($sessions)>0 ): ?>
        <div class="accordion mb-5" id="accordionExample">
            <?php $__currentLoopData = $sessions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $session): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="card">
                <div class="card-header" id="heading<?php echo e($session->id); ?>">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#collapse<?php echo e($session->id); ?>" aria-expanded="false" aria-controls="collapse<?php echo e($session->id); ?>">
                            <?php echo e($session->name); ?>

                        </button>
                    </h2>
                </div>

                <div id="collapse<?php echo e($session->id); ?>" class="collapse" aria-labelledby="heading<?php echo e($session->id); ?>" data-parent="#accordionExample"
                    style="">
                    <div class="card-body">

                        <dl class="dl-horizontal d-flex ">
                            <dt>Name:</dt>
                            <dd><?php echo e($session->name); ?></dd>
                        </dl>
                        <dl class="dl-horizontal d-flex ">
                            <dt>Venu:</dt>
                            <dd><?php echo e($session->venue); ?></dd>
                        </dl>
                        <dl class="dl-horizontal d-flex ">
                            <dt>Time:</dt>
                            <dd><?php echo e($session->start_time); ?></dd>
                        </dl>
                        <dl class="dl-horizontal d-flex ">
                            <dt>Type:</dt>
                            <dd>Public</dd>
                        </dl>
                        <dl class="dl-horizontal d-flex ">
                            <dt>Session Attendees:</dt>
                        </dl>

                        <div class="row">
                            <table class="table table-striped" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Category</th>
                                        <th>Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $attendees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($session->id == $values->event_sessions_id): ?>
                                    <tr>
                                        <td><?php echo e($loop->iteration); ?></td>
                                        <td><?php echo e($values->category); ?></td>
                                        <td><?php echo e($values->username); ?></td>

                                    </tr>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="row justify-content-end">
                            <ul class=" pagination d-flex mt-3 mb-3 ">
                                <?php echo e($attendees->links()); ?>

                            </ul>
                        </div>


                    </div>
                </div>


            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <?php else: ?>
        <p>No records found</p>
        <?php endif; ?>
        <div class="container-fluid mt-5">
            <div class=" d-flex">
                <div class=" col-sm-4 mt-3 mb-5">
                    <button type="submit" class="btn btn-default">
                        <a href="<?php echo e(url()->previous()); ?>">Back</a>
                    </button>
                </div>
            </div>
        </div>






    </div>

</div>

</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/sessions/show.blade.php ENDPATH**/ ?>