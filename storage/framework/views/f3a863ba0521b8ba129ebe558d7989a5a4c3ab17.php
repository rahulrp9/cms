<?php $__env->startSection('content'); ?>
<!-- muliselect plugin's CSS  -->
<link href="<?php echo e(asset('assets/plugins/jasny-bootstrap/css/jasny-bootstrap.css')); ?>" rel="stylesheet" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <div class="col-md-12 ">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?php if(isset($offer)): ?>Delivery Area Updation <?php else: ?> Delivery Area Creation <?php endif; ?></small> </h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
                  <a href="<?php echo e(URL::to('dashboard/areas')); ?>"><button title="List" data-toggle="tooltip" class="btn btn-default" type="button">
                          <i class="fa fa-list"></i></button>
                  </a>
                
                  <?php if(isset($offer)): ?>
                        <a href="<?php echo e(URL::to('dashboard/areas/create')); ?>"><button title="New" data-toggle="tooltip" class="btn btn-default" type="button">
                                <i class="fa fa-plus"></i></button>
                        </a>
                        <a href="<?php echo e(URL::to('dashboard/areas/'.$offer->id)); ?>"><button title="More" data-toggle="tooltip" class="btn btn-default" type="button">
                                <i class="fa fa-eye"></i></button>
                        </a>

                  <?php endif; ?>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
                <div class="flash_messages">
                </div>
                <form id="formSpace" action="<?php echo e(URL::to('dashboard/areas')); ?><?php if(isset($area)): ?>/<?php echo e($area->id); ?> <?php endif; ?>" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <?php echo csrf_field(); ?>

                    <?php if(isset($area)): ?>
                        <?php echo e(method_field('PUT')); ?>

                        <input type="hidden" name="service_id" value="<?php echo e($area->id); ?>"/>
                    <?php endif; ?>
                    <!-- First Name-->
                    <div class="form-group <?php echo e($errors->has('delivery_area') ? ' has-error' : ''); ?>">
                        <label for="delivery_area" class="col-sm-3 control-label required">Delivery Area Name</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="delivery_area" id="delivery_area" value='<?php if(isset($area)): ?><?php echo e($area->delivery_area); ?><?php endif; ?>' class="form-control service_eng" required="" placeholder="Enter Offer Name">
                                <?php if($errors->has('delivery_area')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('delivery_area')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    

                        
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button id="btn-submit" type="submit" class="btn btn-default checkbtn">
                                <i class="fa fa-plus"></i> Save
                            </button>
                            <button id="btn-submit" type="button" class="btn btn-default" onclick="window.history.back()">
                                <i class="fa fa-history"></i> Cancel
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script type="text/javascript">
     $('.space_img').change(function(event) {
        var fileSize = this.files[0].size;
        var maxAllowedSize = 2000000;
        if(fileSize > maxAllowedSize){
            $('.errorimg').html('Please upload a smaller file');
           $('.space_img').val('');
        }
        else{
            $('.errorimg').html('');
            if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.oldpreview').fadeOut();
                        $('.imagePreview').attr('src', e.target.result);
                        $('.imagePreview').fadeIn();
                    }
                    reader.readAsDataURL(this.files[0]);
                }
        }

    });

    /*$('.checkbtn').click(function(){

        var name = $('.service_eng').val();
        $.ajax({
                    type: 'get',
                    url: '<?php echo url('/');?>/checkservice',
                    data: 'servicename='+name,
                    datatype: "json",
                    async: false,
                    cache: false,
                    timeout: 30000,
                        success: function (response) {
                             
                        }
            });
    }); */
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/laraveltest/resources/views//area/form.blade.php ENDPATH**/ ?>