<div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <?php
    $year = date("Y");
     $next = $year + 1;
    ?>
    <strong>Copyright &copy;<?php echo e($year); ?>-<?php echo e($next); ?> <a href="">FoodApp</a>.</strong> All rights
    reserved.<?php /**PATH /var/www/html/laraveltest/resources/views/layouts/portions/mainFooter.blade.php ENDPATH**/ ?>