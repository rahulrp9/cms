<?php if(isset($updating) && $updating): ?>
<?php $__env->startSection('title', 'Update Roles'); ?>
<?php else: ?>
<?php $__env->startSection('title','Add Roles'); ?>
<?php endif; ?>
<?php $__env->startSection('content'); ?>
<div class="edit-event-wrapper">
    <div class="container-fluid mt-3 mb-4">
        <div class="row">
            <div class=" mb-4">
                <h6 class="h6 mb-0 dashboard-title">Roles Form:</h6>
            </div>
        </div>
    </div>
    <div class="container-fluid edit-events">
        <div class="row">
            <?php if(isset($updating) && $updating): ?>
            <form class="form-horizontal form-datetimepicker col-sm-6"
                action="<?php echo e(route('admin.roles.update', $roles->id)); ?>" method="post" enctype="multipart/form-data">
                <?php echo e(method_field('PUT')); ?> <?php else: ?>
                <form class="form-horizontal form-datetimepicker col-sm-6" action="<?php echo e(route('admin.roles.store')); ?>"
                    method="post" enctype="multipart/form-data">
                    <?php endif; ?> <?php echo e(csrf_field()); ?>

                    <div class="form-group d-flex">
                        <label class="control-label col-sm-3" for="name">Roles Name:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name" placeholder="Roles Name *" maxlength="60"
                                name="name" value="<?php echo e(isset($roles) ? $roles->name : old('name')); ?>" <?php if(@isset($show)): ?>
                                readonly <?php endif; ?>>

                            <?php if($errors->has('name')): ?>
                            <label for="name" class="error">
                                <?php echo e($errors->first('name')); ?>

                            </label>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <label class="control-label col-sm-3" for="name">Permissions :</label>
                        <div class="col-sm-9 venue-text">
                            <div class=" input-group venue">
                                <select name="permission[]" id="permission" class="form-control select2"
                                    multiple="multiple" <?php if(@isset($show)): ?> readonly <?php endif; ?>>
                                    <?php if(isset($updating) && $updating): ?>
                                    <?php $__currentLoopData = $permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $permissions): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($id); ?>"
                                        <?php echo e((in_array($id, old('permissions', [])) || isset($roles) && $roles->permissions()->pluck('name', 'id')->contains($id)) ? 'selected' : ''); ?>>
                                        <?php echo e($permissions); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                    <?php $__currentLoopData = $permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $permissions): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($id); ?>"
                                        <?php echo e((in_array($id, old('permission', [])) || isset($role) && $role->permissions->contains($id)) ? 'selected' : ''); ?>>
                                        <?php echo e($permissions); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </select>

                            </div>
                            <?php if($errors->has('permission')): ?>
                            <label for="permission" class="error"><?php echo e($errors->first('permission')); ?></label>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="container-fluid mt-5">
                        <div class=" d-flex">
                            <div class=" col-sm-4 mt-3 mb-5">
                                <?php if(isset($show)): ?>
                                <?php if($show): ?>
                                <button type="submit" class="btn btn-default">
                                    <a href="<?php echo e(route('admin.roles.index')); ?>">Back</a>
                                </button>
                                <?php endif; ?>
                                <?php else: ?>
                                <button type="submit" class="btn btn-default btn-purple mr-2">Submit</button>
                                <button type="submit" class="btn btn-default">
                                    <a href="<?php echo e(route('admin.roles.index')); ?>">Cancel</a>
                                </button>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </form>
        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/roles/create.blade.php ENDPATH**/ ?>