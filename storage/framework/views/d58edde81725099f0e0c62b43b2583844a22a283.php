<?php $__env->startSection('title', 'Event Attendees'); ?>
<?php $__env->startSection('content'); ?>
<div class="edit-event-wrapper">
    <div class="container-fluid mt-3 mb-4">
        <div class="row justify-content-between">
            <div class=" col-lg-6 mb-4 ">
                <h2 class="h2 mb-0 ">Sessions Details</h2>
            </div>
        </div>
    </div>
    <div class="container-fluid edit-events ">

        <div class="row ">
            <div class="col-lg-12 ">

                <dl class="dl-horizontal d-flex ">
                    <dt>Status:</dt>
                    <dd><span class="label label-primary ">
                            <?php if($sessions->status == 1): ?>
                            Active
                            <?php else: ?>
                            Inactive
                            <?php endif; ?>
                        </span>
                    </dd>
                </dl>
            </div>

            <div class="col-lg-5 ">
                <dl class="dl-horizontal d-flex ">

                    <dt>Event Name:</dt>
                    <dd><?php echo e($sessions->name); ?></dd>
                </dl>
                <dl class="dl-horizontal d-flex ">
                    <dt>Venue:</dt>
                    <dd> <?php echo e($sessions->venue); ?></dd>
                </dl>
                <dl class="dl-horizontal d-flex ">
                    <dt>Description:</dt>
                    <p> <?php echo e($sessions->description); ?> </p>
                </dl>
            </div>
            <div class="col-lg-7 " id="cluster_info ">
                <dl class="dl-horizontal d-flex ">

                    <dt>Date:</dt>
                    <dd> <?php echo e($sessions->from_date); ?></dd>
                </dl>
                <dl class="dl-horizontal d-flex ">
                    <dt> Time:</dt>
                    <dd><?php echo e($sessions->start_time); ?></dd>
                </dl>
                <dl class="dl-horizontal d-flex ">
                    <dt>Type:</dt>
                    <dd>
                        <?php if($sessions->type == 1): ?>
                        Private
                        <?php else: ?>
                        Public
                        <?php endif; ?>
                    </dd>

                </dl>
            </div>
        </div>



        <div class="row">
            <table class="table table-striped" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Category</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $attendees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($loop->iteration); ?></td>
                        <td><?php echo e($values->category); ?></td>
                        <td><?php echo e($values->username); ?></td>

                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>

        <div class="row justify-content-end">
            <ul class=" pagination d-flex mt-3 mb-3 ">
                <?php echo e($attendees->links()); ?>

            </ul>
        </div>
        <div class="container-fluid mt-5">
            <div class=" d-flex">
                <div class=" col-sm-4 mt-3 mb-5">
                    <button type="submit" class="btn btn-default" title="Back">
                        <a href="<?php echo e(url()->previous()); ?>">Back</a>
                    </button>
                </div>
            </div>
        </div>

    </div>

</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/sessionattendees/show.blade.php ENDPATH**/ ?>