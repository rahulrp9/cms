<?php $__env->startSection('title', 'Country List'); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid mt-5">
    <div class="row  align-items-center justify-content-between mb-5">
        <h6 class="h6 mb-0 dashboard-title">Country List:</h6>
        <div class="add-search">
            <a href="<?php echo e(route('admin.country.create')); ?>" class="btn btn-primary btn-purple" title="Add New">Add New</a>
            <form class="d-sm-inline-block form-inline navbar-search" action="<?php echo e(route('admin.country.search')); ?>"
                  method="post" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search By Name"
                        aria-label="Search" aria-describedby="basic-addon2" name="search">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit"> Search </button>
                    </div>
                </div>
            </form>
        </div>
        
    </div>
</div>
<div class="container-fluid table-register">
    <div class="row ">
        <table class="table table-registerd-users ">
            <thead>
                <tr>
                    <th scope="col">Sl.No</th>
                    <th>Name En</th>
                    <th>Name Ar</th>
                    <th>Country code</th>
                    <th>ISD code</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($country)): ?>
                <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($key+1); ?></td>
                    <td><?php echo e($value->name); ?></td>
                    <td><?php echo e($value->name_ar); ?></td>
                    <td><?php echo e($value->country_code); ?></td>
                    <td><?php echo e($value->isd_code); ?></td>
                    <td>
                       
                            <span class="badge badge-info">
                                <?php if($value->status==1): ?>
                                   Active
                                <?php else: ?>
                                   Inactive
                                <?php endif; ?>
                            </span>
                       
                    </td>
                    <td class="action-buttons">
                        <a href="<?php echo e(route('admin.country.show',$value->id)); ?>" class="btn preview">
                            <img src="<?php echo e(asset('backend/assets/images/preview.png')); ?>" alt="preview" /> 
                        </a>

                        <a href="<?php echo e(route('admin.country.edit',$value->id)); ?>" type="button" class="btn edit">
                            <img src="<?php echo e(asset('backend/assets/images/edit.png')); ?>" alt="preview" /> 
                        </a>

                        <form action="<?php echo e(route('admin.country.destroy', $value->id)); ?>" method="post">
                        <?php echo e(method_field('DELETE')); ?>

                        <?php echo e(csrf_field()); ?>

                            <button type="submit" class="btn trash" onclick="return confirm('Are you sure you want to delete?')">
                                <img src="<?php echo e(asset('backend/assets/images/trash.png')); ?>" alt="preview" /> 
                            </button>
                        </form>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                <tr>
                    <td colspan="7" > No contents!!!!!</td>
                </tr>
              <?php endif; ?>
            </tbody>
        </table>
        <ul class="pagination d-flex mt-3 mb-3">
            <?php echo e($country->links()); ?>

        </ul>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/country/list.blade.php ENDPATH**/ ?>