<?php $__env->startSection('title', 'Event Attendees'); ?>
<?php $__env->startSection('content'); ?>
<style type="text/css">
  .imgfitclass {
    width: 300px;
    height: 200px;
    max-width: 300px;
    max-height: 200px;
    overflow: hidden;
  }

  .imgfitclass img {
    /*min-height: 300px;*/
    width: 100%;
    /*max-width: 300px;*/
  }

  #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
  }

  .img-profile {
    cursor: pointer;
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
  }

  .img-profile:hover {
    opacity: 0.7;
  }

  /* The Modal (background) */
  .modal {
    display: none;
    /* Hidden by default */
    position: fixed;
    /* Stay in place */
    z-index: 1;
    /* Sit on top */
    padding-top: 100px;
    /* Location of the box */
    left: 0;
    top: 0;
    width: 100%;
    /* Full width */
    height: 100%;
    /* Full height */
    overflow: auto;
    /* Enable scroll if needed */
    background-color: rgb(0, 0, 0);
    /* Fallback color */
    background-color: rgba(0, 0, 0, 0.9);
    /* Black w/ opacity */
  }

  /* Modal Content (Image) */
  .modal-content {
    margin: auto;
    display: block;
    width: 100%;
    max-width: 700px;
  }

  /* Caption of Modal Image (Image Text) - Same Width as the Image */
  #caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
  }

  /* Add Animation - Zoom in the Modal */
  .modal-content,
  #caption {
    animation-name: zoom;
    animation-duration: 0.6s;
  }

  @keyframes  zoom {
    from {
      transform: scale(0)
    }

    to {
      transform: scale(1)
    }
  }

  /* The Close Button */
  .close {
    position: absolute;
    top: 15px;
    right: 35px;
    /*color: #f1f1f1;*/
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
  }

  .close:hover,
  .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
  }
  .modal-backdrop.show {
    opacity: 0;
}
.modal-backdrop{
  position: inherit;
}
</style>
<div class="edit-event-wrapper">
  <div class="container-fluid mt-3 mb-4" style="margin-bottom: 12px !important;">
    <div class="row justify-content-between">


    </div>
  </div>

  <div class="container-fluid edit-events ">
    <div>
      <input type="hidden" name="" class="hidevnt" value="<?php echo e($event->id); ?>">
      <h2><span class="status">
          <?php if($event->status == 1): ?>
          Active
          <?php else: ?>
          Inactive
          <?php endif; ?>
        </span>
        <?php echo e($event->name); ?>

      </h2>
      <ul class="eventInfoV1">
        <li>
          <span>Date</span>
          <?php echo e(date('d - m - Y', strtotime($event->from_date))); ?>

        </li>
        <li>
          <span>Time</span>
          <?php echo e($event->start_time); ?> - <?php echo e($event->end_time); ?>

        </li>
        <li>
          <span>Seat Capacity</span>
          <?php if($event->seat_limit > 0): ?><?php echo e($event->seat_limit); ?><?php else: ?> Unlimited <?php endif; ?>
        </li>
        <li>
          <span>Type</span>
          <?php if($event->type == 1): ?>
          Private
          <?php else: ?>
          Public
          <?php endif; ?>
        </li>
      </ul>

      <ul class="tabCtrlV1 tabs tabui">
        <li class="active"><a data-toggle="tab" href="#tabV1Cont"> Event Details</a></li>
        <li class=""><a data-toggle="tab" href="#tabV2Cont">Sponosrs / Exhibitors</a></li>
        <li class=""><a data-toggle="tab" href="#tabV3Cont">Sessions</a></li>
        <li class=""><a data-toggle="tab" href="#tabV4Cont">Attendees</a></li>
      </ul>

      <div class="tabV1">
        <div class="tabV1Cont tab-pane tabshw" id="tabV1Cont">
          <div class="leftSection">
            <p>
              <span>Venue</span>
              <?php echo e($event->venue); ?>

            </p>
            <p>
              <span>Room</span>
              <?php echo e($event->room); ?>

            </p>
            <p>
              <span>Hall</span>
              <?php echo e($event->hall); ?>

            </p>
            <p>
              <span>Floor</span>
              <?php echo e($event->hall); ?>

            </p>
            <p>
              <span>Street</span>
              <?php echo e($event->street); ?>

            </p>
            <p>
              <span>Hall</span>
              <?php echo e($event->hall); ?>

            </p>
            <p>
              <span>City</span>
              <?php echo e($event->city->name); ?>

            </p>
            <p>
              <span>Country</span>
              <?php echo e($event->country->name); ?>

            </p>
            <p>
              <span>Address</span>
              <?php echo e($event->address); ?>

            </p>
          </div>
          <div class="rightSection">
            <ul>
              <li>
                <span>Event Banner</span>
                <figure class="img-profile " id="myImg"
                  style="background-image: url('<?php echo e(asset('images/events/'.$event->image)); ?>');"
                  data-src="<?php echo e(asset('images/events/'.$event->image)); ?>"></figure>
                <!-- <img class="img-profile " id="myImg" src="<?php echo e(asset('images/events/'.$event->image)); ?>" alt="Event Banner" width="154" height="154">  -->
                <div id="myModal" class="modal">
                  <span class="close">&times;</span>
                  <img class="modal-content" id="img01">
                  <div id="caption"></div>
                </div>
                <!-- -->
              </li>
              <li>
                <span>Event Icon</span>
                <figure class="img-profile " id="myImg1"
                  style="background-image: url('<?php echo e(asset('images/events/'.$event->icon)); ?>');"
                  data-src="<?php echo e(asset('images/events/'.$event->icon)); ?>"></figure>
                <div id="myModal1" class="modal">
                  <span class="close close1">&times;</span>
                  <img class="modal-content" id="img011">
                  <div id="caption1"></div>
                </div>
              </li>
              <li>
                <span>Event Itinerary</span>
                <figure class="img-profile " id="myImg2"
                  style="background-image: url('<?php echo e(asset('images/events/'.$event->itinerary)); ?>');"
                  data-src="<?php echo e(asset('images/events/'.$event->itinerary)); ?>"></figure>
                <div id="myModal2" class="modal">
                  <span class="close close2">&times;</span>
                  <img class="modal-content" id="img012">
                  <div id="caption2"></div>
                </div>
              </li>
              <li>
                <span>Floor Plan</span>
                <figure class="img-profile " id="myImg3"
                  style="background-image: url('<?php echo e(asset('images/events/'.$event->floor_plan)); ?>');"
                  data-src="<?php echo e(asset('images/events/'.$event->floor_plan)); ?>"></figure>
                <div id="myModal3" class="modal">
                  <span class="close close3">&times;</span>
                  <img class="modal-content" id="img013">
                  <div id="caption3"></div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="tabV2Cont tab-pane tabshw" id="tabV2Cont">
          <a href="<?php echo e(route('admin.events.sponsors',$event->id)); ?>" class="pull-right" style="margin-right: 15px;"> + Sponsors</a>
          <a href="<?php echo e(route('admin.events.exhibitors',$event->id)); ?>" class="pull-right" style="margin-right: 15px;"> + Exhibitors</a>
          <table class="listV1" cellspacing="0" cellpadding="0" style="width: 100%;">
            <thead style="background: #bfe0f4;">
              <tr>
                <th><span>Sl.No</span></th>
                <th><span>Name</span></th>
                <th><span>Status</span></th>
                <th><span>Category</span></th>
                <!-- <th><span>Actions</span></th> -->
              </tr>
            </thead>
            <tbody>
              <?php if(count($attendees_sponsors) > 0): ?>

              <?php $__currentLoopData = $attendees_sponsors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                <td><?php echo e($loop->iteration); ?></td>
                <td><?php echo e($values->username); ?></td>
                <td>
                  <?php if($values->status): ?>
                  <span title="Active">Active</span>
                  <?php else: ?>
                  <span title="Inactive">Inactive</span>
                  <?php endif; ?>
                  <!-- <a href="<?php echo e(route('admin.attendees.status',$values->id)); ?>">
                            <?php if($values->status): ?>
                            <button type="button" title="Active">Active</button>
                            <?php else: ?>
                            <button type="button" title="Inactive">Inactive</button>
                            <?php endif; ?>
                          </a> -->
                </td>
                <!-- <td class="action-buttons">
                        <a href="<?php echo e(route('admin.attendees.show',$values->id)); ?>" class="btn preview">
                            <img src="<?php echo e(asset('backend/assets/images/preview.png')); ?>" alt="preview" />
                        </a>

                        <a href="<?php echo e(route('admin.attendees.edit',$values->id)); ?>" type="button" class="btn edit">
                            <img src="<?php echo e(asset('backend/assets/images/edit.png')); ?>" alt="preview" />
                        </a>
                        <form action="<?php echo e(route('admin.attendees.destroy', $values->id)); ?>" method="post">
                            <?php echo e(method_field('DELETE')); ?>

                            <?php echo e(csrf_field()); ?>

                            <button type="submit" class="btn trash"
                                onclick="return confirm('Are you sure you want to delete?')">
                                <img src="<?php echo e(asset('backend/assets/images/trash.png')); ?>" alt="preview" />
                            </button>
                        </form>
                      </td> -->
                <td><?php echo e($values->category); ?></td>
              </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php else: ?>
              <tr>
                <td colspan="4" style="text-align: center !important;"> No Records</td>
              </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>

        <div class="tabV3Cont tab-pane tabshw" id="tabV3Cont">
          <a href="<?php echo e(route('admin.events.sessions',$event->id)); ?>" class="pull-right"> + Sessions</a>
          <?php if(count($sessions)>0 ): ?>
          <?php $__currentLoopData = $sessions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $session): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="session clicksession">
            <h4><?php echo e($session->name); ?></h4>

            <div class="secCont">
              <p>
                <span>Venue</span>
                <?php echo e($session->venue); ?>

              </p>
              <p>
                <span>Time</span>
                <?php echo e($session->start_time); ?>

              </p>
              <p>
                <span>Type</span>
                Public
              </p>
              <p>
                <span>Venue</span>
                <?php echo e($session->venue); ?>

              </p>


              <table class="listV1" cellspacing="0" cellpadding="0" style="width: 100%;">
                <thead style="background: #bfe0f4;">
                  <tr>
                    <th>#</th>
                    <th>Category</th>
                    <th>Name</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $sessions_attendees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                    <td><?php echo e($loop->iteration); ?></td>
                    <td><?php echo e($values->category); ?></td>
                    <td><?php echo e($values->username); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
          </div>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <?php else: ?>
          <p>No records found</p>
          <?php endif; ?>


        </div>


        <div class="tabV4Cont tab-pane tabshw" id="tabV4Cont">
          <a href="<?php echo e(route('admin.events.attendees',$event->id)); ?>" class="pull-right"> + Attendees</a>
          <table class="listV1" cellspacing="0" cellpadding="0" style="width: 100%;">
            <thead style="background: #bfe0f4;">
              <tr>
                <th><span>Sl.No</span></th>
                <th><span>Name</span></th>
                <th><span>Status</span></th>
                <th><span>Category</span></th>
                <th><span>Tag</span></th>
                <!-- <th><span>Actions</span></th> -->
              </tr>
            </thead>
            <tbody>
              <?php if($attendees): ?>
              <?php $__currentLoopData = $attendees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                <td><?php echo e($loop->iteration); ?></td>
                <td><?php echo e($values->username); ?></td>
                <td>
                  <?php if($values->status): ?>
                  <span title="Active">Active</span>
                  <?php else: ?>
                  <span title="Inactive">Inactive</span>
                  <?php endif; ?>
                  <!-- <a href="<?php echo e(route('admin.attendees.status',$values->id)); ?>">
                            <?php if($values->status): ?>
                            <button type="button" title="Active">Active</button>
                            <?php else: ?>
                            <button type="button" title="Inactive">Inactive</button>
                            <?php endif; ?>
                          </a> -->
                </td>
                <!-- <td class="action-buttons">
                        <a href="<?php echo e(route('admin.attendees.show',$values->id)); ?>" class="btn preview">
                            <img src="<?php echo e(asset('backend/assets/images/preview.png')); ?>" alt="preview" />
                        </a>

                        <a href="<?php echo e(route('admin.attendees.edit',$values->id)); ?>" type="button" class="btn edit">
                            <img src="<?php echo e(asset('backend/assets/images/edit.png')); ?>" alt="preview" />
                        </a>
                        <form action="<?php echo e(route('admin.attendees.destroy', $values->id)); ?>" method="post">
                            <?php echo e(method_field('DELETE')); ?>

                            <?php echo e(csrf_field()); ?>

                            <button type="submit" class="btn trash"
                                onclick="return confirm('Are you sure you want to delete?')">
                                <img src="<?php echo e(asset('backend/assets/images/trash.png')); ?>" alt="preview" />
                            </button>
                        </form>
                      </td> -->
                <td><?php echo e($values->category); ?></td>
                <td><button class="shwtag btn btn-info" type="button" style="background-color: #17a2b8;" data-uid="<?php echo e($values->user_id); ?>">Show Tag</button></td>
              </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php else: ?>
              <tr>
                <td colspan="4" style="text-align: center !important;"> No Records</td>
              </tr>
              <?php endif; ?>
            </tbody>
          </table>
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body modaltempbody">
                 
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Print</button>
                </div>
              </div>
            </div>
          </div>
        </div>


      </div>
    </div>




  </div>

</div>

<script type="text/javascript">
  var modal = document.getElementById("myModal");
        var img = document.getElementById("myImg");
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function(){
          modal.style.display = "block";
          var ss= $(this).attr('data-src');
          modalImg.src = ss;
          captionText.innerHTML = this.alt;
        }
        var span = document.getElementsByClassName("close")[0];
        span.onclick = function() {
          modal.style.display = "none";
        }
        var modal1 = document.getElementById("myModal1");
        var img = document.getElementById("myImg1");
        var modalImg1 = document.getElementById("img011");
        var captionText = document.getElementById("caption1");
        img.onclick = function(){
          modal1.style.display = "block";
          var ss= $(this).attr('data-src');
          modalImg1.src = ss;
          captionText.innerHTML = this.alt;
        }
        var span = document.getElementsByClassName("close1")[0];
        span.onclick = function() {
          modal1.style.display = "none";
        }
        var modal = document.getElementById("myModal2");
        var img = document.getElementById("myImg2");
        var modalImg = document.getElementById("img012");
        var captionText = document.getElementById("caption2");
        img.onclick = function(){
          modal.style.display = "block";
          var ss= $(this).attr('data-src');
          modalImg.src = ss;
          captionText.innerHTML = this.alt;
        }
        var span = document.getElementsByClassName("close2")[0];
        span.onclick = function() {
          modal.style.display = "none";
        }
        var modal = document.getElementById("myModal3");
        var img = document.getElementById("myImg3");
        var modalImg = document.getElementById("img013");
        var captionText = document.getElementById("caption3");
        img.onclick = function(){
          modal.style.display = "block";
          var ss= $(this).attr('data-src');
          modalImg.src = ss;
          captionText.innerHTML = this.alt;
        }
        var span = document.getElementsByClassName("close3")[0];
        span.onclick = function() {
          modal.style.display = "none";
        } 
        $(document).ready(function(){
          $("#tabV2Cont").hide();
          $("#tabV3Cont").hide();
          $("#tabV4Cont").hide();
          $(".tabs").on('click', 'a', function(e){
           e.preventDefault();
           $('.tabshw').hide();
           $($(this).attr("href")).show();
         });
          $(".tabui li").on("click", function() {
            $(".tabui li").removeClass("active");
            $(this).addClass("active");
          }); 

          $(".clicksession").click(function(){
            $('.clicksession.selected').not(this).removeClass('selected');
            $(this).toggleClass('selected');
          })

          $('.shwtag').click(function(){

              var event_id = $('.hidevnt').val();
              var uid = $(this).attr("data-uid");
              $.ajax({

                    url: '<?php echo e(route('admin.eventtemplate')); ?>',
                    datatype: "json",
                    data: { _token: "<?php echo e(csrf_token()); ?>",event_id:event_id,uid:uid},
                    type: 'post',
                    success: function (response) {
                      if(response.templateValue!=''){

                        $('.modaltempbody').html(response.templateValue);
                        $('.modaltempbody .evntusrname').html(response.uname);
                        $('.modaltempbody .evntusrdesig').html(response.designation);
                        $('.modaltempbody .eventuserpass').html(response.pass);
                        $(".modaltempbody .evntusersrclogo").attr("src",response.uimage);
                        //$(".modaltempbody .evntsrclogo").attr("src",response.logo);
                        $('#exampleModal').modal();
                      }
                    },
                    error: function (response) {
                        alert("Something Went Wrong!");
                    }
              });
          });  
        }); 

</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/events/show.blade.php ENDPATH**/ ?>