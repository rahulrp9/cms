<?php $__env->startSection('content'); ?>
<style type="text/css">
    .box .row{
         margin-bottom: 15px;
    }
</style>
    <div class="row">
    <div class="col-md-12">
            <div class="col-md-6">
                <h1 class="box-title">Shop Details  </h1>
            </div>    
    <div class="pull-right box-tools">
                    <a href="<?php echo e(URL::to('dashboard/shops')); ?>">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-default" type="button" data-original-title="Service List">
                            <i class="fa fa-list"></i>
                        </button>
                    </a>
     <?php if(isset($service)): ?>
                    <a href="<?php echo e(URL::to('dashboard/shops/create')); ?>">
                        <button title="" data-toggle="tooltip" class="btn btn-default" type="button" data-original-title="Add Service">
                            <i class="fa fa-plus"></i>
                        </button>
                    </a>
   
                    <a href="<?php echo e(URL::to('dashboard/shops/'.$service->id.'/edit')); ?>">
                        <button title="" data-toggle="tooltip" class="btn btn-default" type="button" data-original-title="Edit Service">
                            <i class="fa fa-edit"></i>
                        </button>
                    </a>
    <?php endif; ?>               
                </div>
    </div>            
    </div>
        <div class="box">
            <div class="box-body pad" id="tab_1">
                <div class="row">
                    <label class="col-sm-3 control-label">Shop English</label>
                    <div class="col-sm-6">
                        <?php echo e($shop->name); ?>

                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Email</label>
                    <div class="col-sm-3">
                        <?php echo e($shop->email); ?>

                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Phone</label>
                    <div class="col-sm-3">
                        <?php echo e($shop->phone); ?>

                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Address</label>
                    <div class="col-sm-3">
                        <?php echo e($shop->address); ?>

                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Street</label>
                    <div class="col-sm-3">
                        <?php echo e($shop->street); ?>

                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Latitude</label>
                    <div class="col-sm-3">
                        <?php echo e($shop->latitude); ?>

                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Longitude</label>
                    <div class="col-sm-3">
                        <?php echo e($shop->longitude); ?>

                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Image</label>
                    <div class="col-sm-3">
                        <?php if(!empty($shop->image)): ?>
                                    <a href="javascript:void(0)" class="pop" imgsrc="<?php echo e(asset($shop->space_image)); ?>"><img src=" <?php echo e(asset($shop->image)); ?>" class="img-responsive img-rounded" width="150" height="1150" /></a>
                        <?php else: ?>
                            <img src="<?php echo e(asset('assets/images/no-image.png')); ?>" />
                        <?php endif; ?>
                    </div>
                </div>
              
        </div>
    </div>
<div class="contentHolderV1">
    <h2>Product Details</h2>
    <div class="tableHolder">
    <table style="width: 100%;" class="tableV1" cellpadding="0" cellspacing="0">
        <thead class="tableHeader">
            <tr>
                <td>Product Number</td>
                <td>Product Name</td>
                <td>Product Price</td>

            </tr>
        </thead>
        <?php if(count($products)): ?>
            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($product->id); ?></td>
                    <td><?php echo e($product->name); ?></td>
                    <td><?php echo e($product->price); ?></td>
                </tr>   
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </table>
    </div>

</div>         
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/laraveltest/resources/views//shops/more.blade.php ENDPATH**/ ?>