<?php $__env->startSection('title', 'Dashboard'); ?>
<?php $__env->startSection('content'); ?>
<!-- <script src="https://cdn.ckeditor.com/4.14.1/full-all/ckeditor.js"></script> -->

<script type="text/javascript">
   
$(window).on('load', function (){ 
     CKEDITOR.replace( 'long_desc',{
         extraPlugins: 'sourcedialog'
     } );

/*$('#file').on('click', function () {
$('.hiddendiv').html('');
var html = CKEDITOR.instances.long_desc.getData();
$('.hiddendiv').html(html);
$('.hiddendiv').find('.maindiv').css("background-image", "url(http://eventeam.zoondia.org/backend/assets/images/id-photo.png)");  

var newhtml = $('.hiddendiv').html();
CKEDITOR.instances.long_desc.setData(newhtml);
}); */   

$("#file").on("change", function() {    
                        var file_data = $('#file').prop('files')[0];
                        var form_data = new FormData();
                        form_data.append('file', file_data);
                        $.ajax({
                            url: '<?php echo e(route('admin.image.testpageimage')); ?>',
                            dataType: 'text', // what to expect back from the server
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                              var html = CKEDITOR.instances.long_desc.getData();
                              $('.hiddendiv').html('');
                            var html = CKEDITOR.instances.long_desc.getData();
                            $('.hiddendiv').html(html);
                            var url = "http://eventeam.zoondia.org/backend/tagimg/"+response;
                            $('.hiddendiv').find('.maindiv').css("background-image", "url('"+url+"')");  

                            var newhtml = $('.hiddendiv').html();
                            CKEDITOR.instances.long_desc.setData(newhtml);
                            },
                            error: function (response) {
                               alert("qq");
                            }
                        });
                    });
     
/*tinymce.init({
  selector: 'textarea#long_desc',
  plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
  imagetools_cors_hosts: ['picsum.photos'],
  menubar: 'file edit view insert format tools table help',
  toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
  toolbar_sticky: true,
  autosave_ask_before_unload: true,
  autosave_interval: "30s",
  autosave_prefix: "{path}{query}-{id}-",
  autosave_restore_when_empty: false,
  autosave_retention: "2m",
  image_advtab: true,
  content_css: '//www.tiny.cloud/css/codepen.min.css',
  link_list: [
    { title: 'My page 1', value: 'http://www.tinymce.com' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_list: [
    { title: 'My page 1', value: 'http://www.tinymce.com' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_class_list: [
    { title: 'None', value: '' },
    { title: 'Some class', value: 'class-name' }
  ],
  importcss_append: true,
  file_picker_callback: function (callback, value, meta) {
     Provide file and text for the link dialog 
    if (meta.filetype === 'file') {
      callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
    }

    if (meta.filetype === 'image') {
      callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
    }

    if (meta.filetype === 'media') {
      callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
    }
  },
  templates: [
        { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
    { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
    { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
  ],
  template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
  template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
  height: 600,
  image_caption: true,
  quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
  noneditable_noneditable_class: "mceNonEditable",
  toolbar_mode: 'sliding',
  contextmenu: "link image imagetools table",
 });*/



});
</script>
<!-- Main content -->
<!-- <div class="containerc" style="width: 25%;grid-template-columns: 300px 300px 300px;grid-gap: 50px;justify-content: center;align-items: center;height: 100vh;background-color: #f5f5f5;">
  <div class="cardc" style="background-color: #222831;height: 20rem;border-radius: 5px;display: flex;flex-direction: column;align-items: center;box-shadow: rgba(0, 0, 0, 0.7);color: white;">
    <img src="http://eventeam.zoondia.org/backend/assets/images/avatar.png" alt="Person" class="card__image" style="height: 160px;width: 160px;border-radius: 50%;border: 5px solid #272133;margin-top: 20px;box-shadow: 0 10px 50px rgba(235, 25, 110, 1);">
    <p class="card__name" style="margin-top: 15px;font-size: 1.5em;">Lily-Grace Colley</p>
    <div class="grid-container">

      <div class="grid-child-posts">
        156 Post
      </div>

      <div class="grid-child-followers">
        1012 Likes
      </div>

    </div>

  </div>
  
  
</div> -->

<div class="row">
    <div class="col-md-6">
        <div class="hiddendiv" style="display: none;"></div>
         <form method='post' action=''>
          
       <textarea id='long_desc' name='long_desc' >
        <div style="background-color: #2b2b2b" class="tablediv">
            <div width="500" height="800" style="background-color: : #2b2b2b; visibility: visible; background-image: url('http://eventeam.zoondia.org/backend/assets/images/bg-tag.png'); background-repeat: no-repeat; background-size: cover;" class="maindiv">


    <div class="id-logo" style="margin-top: 53px;">
        <img src="http://eventeam.zoondia.org/backend/assets/images/id-logo.png" alt="Eventeam" style="margin:auto;">
    </div>

    <div>
        <img src="http://eventeam.zoondia.org/backend/assets/images/id-photo.png" alt="Eventeam" style="margin:auto;border-radius:50%; border: solid 16px #fff;">
    </div>

    <div align="center">
        <h1 style="color: #ffffff;font-size: 35px; text-align: center;
     font-weight: 700;margin-top: 18px; margin-bottom: 0;">Sophie Lewis</h1>
        <h2 style="color: #ec386f;font-size: 28px; text-align: center;
        font-weight: 500;  margin-bottom:36px;margin-top: 0px;">Creative Manager</h2>
    </div>
</div>
            </div>  
       </textarea><br>
       <input type="file" name="file" class="file" id="file">
       <!-- <input type="submit" name="submit" value="Submit"> -->
    </form>
    </div>
</div>    
<!-- /.content -->

<?php $__env->stopSection(); ?>


    
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/testpage.blade.php ENDPATH**/ ?>