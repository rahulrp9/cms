<?php if(isset($updating) && $updating): ?>
<?php $__env->startSection('title', 'Update Users'); ?>
<?php else: ?>
<?php $__env->startSection('title','Add Users'); ?>
<?php endif; ?>
<?php $__env->startSection('content'); ?>
<div class="edit-event-wrapper">
    <div class="container-fluid mt-3 mb-4">
        <div class="row">
            <div class=" mb-4">
                <h6 class="h6 mb-0 dashboard-title">Add Form:</h6>
            </div>
        </div>
    </div>
    <div class="container-fluid edit-events">
        <div class="row">
            <?php if(isset($updating) && $updating): ?>
            <form class="form-horizontal form-datetimepicker col-sm-6"
                action="<?php echo e(route('admin.scanningUsers.update', $scanningUser->id)); ?>" method="post" enctype="multipart/form-data">
                <?php echo e(method_field('PUT')); ?> <?php else: ?>
                <form class="form-horizontal form-datetimepicker col-sm-6" action="<?php echo e(route('admin.scanningUsers.store')); ?>"
                    method="post" enctype="multipart/form-data">
                    <?php endif; ?> <?php echo e(csrf_field()); ?>

                    <div class="form-group d-flex">
                        <label class="control-label col-sm-3" for="name">Name <em>*</em>:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name" placeholder="Enter Name" maxlength="60"
                                name="name" value="<?php echo e(isset($scanningUser) ? $scanningUser->name : old('name')); ?>" <?php if(@isset($show)): ?>
                                readonly <?php endif; ?>>

                            <?php if($errors->has('name')): ?>
                            <label for="name" class="error">
                                <?php echo e($errors->first('name')); ?>

                            </label>
                            <?php endif; ?>
                        </div>
                    </div>
                    

                    <div class="form-group d-flex">
                        <label class="control-label col-sm-3" for="name">Email <em>*</em>:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="email" placeholder="Enter Email" maxlength="60"
                                name="email" value="<?php echo e(isset($scanningUser) ? $scanningUser->email : old('email')); ?>"
                                <?php if(@isset($show)): ?> readonly <?php endif; ?>>

                            <?php if($errors->has('email')): ?>
                            <label for="email" class="error">
                                <?php echo e($errors->first('email')); ?>

                            </label>
                            <?php endif; ?>
                        </div>
                    </div>


                    <div class="form-group d-flex" >
                        <label class="control-label col-sm-3" for="name">Password <em>*</em>:</label>
                        <div class="col-sm-9 com" >
                           
                            <input type="password" class="form-control " id="pwd" placeholder="Enter Password" maxlength="60"
                                name="password"  value="<?php echo e(isset($scanningUser) ? $scanningUser->password : old('password')); ?>"
                                 <?php if(@isset($view)): ?> readonly <?php endif; ?>>

                            <?php if($errors->has('password')): ?>
                            <label for="password" class="error">
                                <?php echo e($errors->first('password')); ?>

                            </label>
                            <?php endif; ?>
                        </div>
                       
                    </div>
                    <div class="form-group d-flex">
                        <label class="control-label col-sm-3" for="name">Select Event <em>*</em>:</label>
                        <div class="col-sm-9">
                            <select name="events" class="form-control">
                                
                                <?php if($events): ?>
                                    <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($event->id); ?>"><?php echo e($event->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </select>

                            <?php if($errors->has('username')): ?>
                            <label for="username" class="error">
                                <?php echo e($errors->first('username')); ?>

                            </label>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if(isset($view)): ?>
                    <div class="form-group d-flex">
                        <button type="button" id="showfield" class="btn btn-default btn-purple" >Change Password</button>
                    </div>
                    <?php endif; ?>   
                    

                  
                    <div class="container-fluid mt-5">
                        <div class=" d-flex">
                            <div class=" col-sm-4 mt-3 mb-5">
                                <?php if(isset($show)): ?>
                                <?php if($show): ?>
                                <button type="submit" class="btn btn-default">
                                    <a href="<?php echo e(route('admin.scanningUsers.index')); ?>">Back</a>
                                </button>
                                <?php endif; ?>

                                <?php else: ?>
                                <button type="submit" class="btn btn-default btn-purple mr-2">Save</button>
                                <button type="submit" class="btn btn-default">
                                    <a href="<?php echo e(route('admin.scanningUsers.index')); ?>">Cancel</a>
                                </button>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </form>
        </div>
    </div>
    
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('scripts'); ?>
    <script>

        $(document).ready(function(){

            $("#showfield").click(function()
            {
                $('#pwd').val('');
                $('input').prop('readonly',false);
            });
        })

    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/scanningUsers/create.blade.php ENDPATH**/ ?>