<ul>
<?php $__currentLoopData = $childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
 <?php  
                if ($child->status == 1) {
                                $cstatus =  "Active";
                                $class   = "active";
                            } else {
                                  $cstatus = "Inactive";
                                  $class   = "inactive";
                            }
               ?>
                <li><a href="javascript:void(0);"><?php echo e($child->name); ?><i><img src="<?php echo url('/');?><?php echo e($child->image); ?>"></i></a><div  class="actions"><span class="status <?php echo e($class); ?>"><?php echo e($cstatus); ?></span><a href="<?php echo e(URL::to('dashboard/viewcat', ['id' => ($child->id)])); ?>" class="add"><i class="fa fa-eye"></i></a><a href="<?php echo e(URL::to('dashboard/delcat', ['id' => ($child->id)])); ?>" class="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a><a href="<?php echo e(URL::to('dashboard/editcat', ['id' => ($child->id)])); ?>" class="edit"><i class="fa fa-edit"></i></a></div>
                  <?php if(count($child->childs)): ?>
                                    <?php echo $__env->make('admin/manageChild',['childs' => $child->childs], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                                <?php endif; ?>

                </li>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </ul>
              

<?php /**PATH /var/www/html/laraveltest/resources/views/categories/manageChild.blade.php ENDPATH**/ ?>