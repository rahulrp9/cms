<?php $__env->startSection('content'); ?>
<!-- muliselect plugin's CSS  -->
<link href="<?php echo e(asset('assets/plugins/jasny-bootstrap/css/jasny-bootstrap.css')); ?>" rel="stylesheet" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<style type="text/css">
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
        color: black;
    }
</style>
    <div class="col-md-12 ">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?php if(isset($product)): ?>Product Updation <?php else: ?> Add Product <?php endif; ?></small> </h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
                  <a href="<?php echo e(URL::to('dashboard/products')); ?>"><button title="List" data-toggle="tooltip" class="btn btn-default" type="button">
                          <i class="fa fa-list"></i></button>
                  </a>
                
                  <?php if(isset($product)): ?>
                        <a href="<?php echo e(URL::to('dashboard/products/create')); ?>"><button title="New" data-toggle="tooltip" class="btn btn-default" type="button">
                                <i class="fa fa-plus"></i></button>
                        </a>
                        <a href="<?php echo e(URL::to('dashboard/products/'.$product->id)); ?>"><button title="More" data-toggle="tooltip" class="btn btn-default" type="button">
                                <i class="fa fa-eye"></i></button>
                        </a>

                  <?php endif; ?>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
                <div class="flash_messages">
                </div>
                <form id="formSpace" action="<?php echo e(URL::to('dashboard/products')); ?><?php if(isset($product)): ?>/<?php echo e($product->id); ?> <?php endif; ?>" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <?php echo csrf_field(); ?>

                    <?php if(isset($product)): ?>
                        <?php echo e(method_field('PUT')); ?>

                        <input type="hidden" name="service_id" value="<?php echo e($product->id); ?>"/>
                    <?php endif; ?>
                    <!-- First Name-->
                    <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                        <label for="name" class="col-sm-3 control-label required">Product Name</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="name" id="name" value='<?php if(isset($product)): ?><?php echo e($product->name); ?><?php endif; ?>' class="form-control service_eng" required="" placeholder="Enter Product Name">
                                <?php if($errors->has('name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?php echo e($errors->has('value') ? ' has-error' : ''); ?>">
                        <label for="value" class="col-sm-3 control-label required">Product Price</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="value" id="value" value='<?php if(isset($product)): ?><?php echo e($product->value); ?><?php endif; ?>' class="form-control service_eng" required="" placeholder="Enter Product Value">
                                <?php if($errors->has('value')): ?>value
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('value')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?php echo e($errors->has('code') ? ' has-error' : ''); ?>">
                        <label for="code" class="col-sm-3 control-label required">Product Category</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <select name="catgeory" id="SPECIAL">
                                  <option>Please Select</div>
                                    <?php if(count($mainCategory)): ?>
                                        <?php $__currentLoopData = $mainCategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $main): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <optgroup label="<?php echo e($main['name']); ?>">
                                                <?php if(count($main['sub'])): ?>
                                                    <?php $__currentLoopData = $main['sub']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option data-img="" value="<?php echo e($sub->id); ?>"><?php echo e($sub->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                                <?php endif; ?>    
     
                                            </optgroup>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                    <?php endif; ?>    

                                   </select>
                                <?php if($errors->has('code')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('code')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?php echo e($errors->has('isveg') ? ' has-error' : ''); ?>">
                        <label for="isveg" class="col-sm-3 control-label required">Veg or Non Veg</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                 <input type="radio" id="isveg" name="isveg" value="0">
                                <label for="male">Non Veg</label><br>
                                <input type="radio" id="isveg" name="isveg" value="1">
                                <label for="female">Veg</label><br>

                                <?php if($errors->has('isveg')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('isveg')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?php echo e($errors->has('type') ? ' has-error' : ''); ?>">
                        <label for="type" class="col-sm-3 control-label required">Product Customizable</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="checkbox" id="vehicle1" name="type" value="2">
                                <label for="vehicle1"> Customizable</label><br>
                                <?php if($errors->has('type')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('type')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group <?php echo e($errors->has('type') ? ' has-error' : ''); ?>">
                        <label for="type" class="col-sm-3 control-label required">Is Addon</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="checkbox" id="vehicle1" name="isaddon" value="1">
                                <label for="vehicle1"> Addon</label><br>
                                <?php if($errors->has('type')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('type')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div> -->


                   
                    <!-- SpaceImage-->
                
                        <div class="form-group">
                                <label for="title_ar" class="col-sm-3 control-label required">Product Image</label>
                                <div class="imgUploadHolder col-sm-6">
                                    <?php if(isset($product->image) && $product->image != ''): ?>
                                    <input type="hidden" name="lasthidimg" value="<?php echo e($product->image); ?>" class="lasthidimg">
                                    <?php endif; ?>
                                    <div class="col-lg-3">
                                   <input type="file" class="space_img " name="servicelogo" id="space_img"   <?php if(isset($product->image) && $product->image != ''): ?>
                                   value="<?php echo e($product->image); ?>" <?php else: ?> required <?php endif; ?>>
                                  <label class="errorimg" for="imageInput" style="color: red"></label>
                                    <div style="margin-top: 10px;">
                                    <img src="" class="imagePreview" width="150" height="150" style="display: none;">
                                    <?php if(isset($product->image) && $product->image != ''): ?>
                                   <img src="<?php echo url('/');?><?php echo e($product->image); ?>" width="150" height="150" class="oldpreview">
                                   
                                   <?php endif; ?>
                                   </div>
                                   </div>

                                </div>
                            </div>
                       <div class="custprdt" style="display: none;">     
                        <div class="form-group <?php echo e($errors->has('value') ? ' has-error' : ''); ?>">
                        <label for="value" class="col-sm-3 control-label required">Select Addons</label>
                        <div class="col-sm-9">
                                <div class="input-group-prepend"></div>
                               <select name="addons[]" multiple="" class="select2" style="width: 150px;">
                                <option value="">Select Addon</option>
                                <?php if($addons): ?>
                                    <?php $__currentLoopData = $addons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $addon): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($addon->id); ?>"><?php echo e($addon->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                               </select>
                        </div>
                    </div>    
                        <div class="form-group <?php echo e($errors->has('value') ? ' has-error' : ''); ?>">
                        <label for="value" class="col-sm-3 control-label required">Customizable Product options</label>
                        <div class="col-sm-9">
                            <div class="">
                                <div class="input-group-prepend"></div>
                                <!-- <input type="text" name="size[]" id="value" value='<?php if(isset($product)): ?><?php echo e($product->value); ?><?php endif; ?>' class="form-control service_eng"  placeholder="Enter Product Size" style="float: left;width: 30%"> -->
                                <select class="form-control service_eng" name="size[]" style="float: left;width: 30%">
                                    <?php if($sizegroups): ?>
                                         <option value="" selected="">Select option</option>
                                        <?php $__currentLoopData = $sizegroups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sizegroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($sizegroup->id); ?>"><?php echo e($sizegroup->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>

                                </select>
                                <input type="text" name="sizeval[]" id="value" value='<?php if(isset($product)): ?><?php echo e($product->value); ?><?php endif; ?>' class="form-control service_eng"  placeholder="Enter Product Value" style="float: left;width: 30%;margin-left: 10px;">
                                <?php if($errors->has('value')): ?>value
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('value')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group <?php echo e($errors->has('value') ? ' has-error' : ''); ?>">
                        <label for="value" class="col-sm-3 control-label required">Customizable Product options</label>
                        <div class="col-sm-9">
                                <div class="input-group-prepend"></div>
                                <select class="form-control service_eng" name="size[]" style="float: left;width: 30%">
                                    <?php if($sizegroups): ?>
                                        <option value="" selected="">Select option</option>
                                        <?php $__currentLoopData = $sizegroups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sizegroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($sizegroup->id); ?>"><?php echo e($sizegroup->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>

                                </select>
                                <input type="text" name="sizeval[]" id="value" value='<?php if(isset($product)): ?><?php echo e($product->value); ?><?php endif; ?>' class="form-control service_eng"  placeholder="Enter Product Value" style="float: left;width: 30%;margin-left: 10px;">
                                <?php if($errors->has('value')): ?>value
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('value')); ?></strong>
                                    </span>
                                <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo e($errors->has('value') ? ' has-error' : ''); ?>">
                        <label for="value" class="col-sm-3 control-label required">Customizable Product options</label>
                        <div class="col-sm-9">
                            <div class="">
                                <div class="input-group-prepend"></div>
                                <select class="form-control service_eng" name="size[]" style="float: left;width: 30%">
                                    <?php if($sizegroups): ?>
                                        <option value="" selected="">Select option</option>
                                        <?php $__currentLoopData = $sizegroups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sizegroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($sizegroup->id); ?>"><?php echo e($sizegroup->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>

                                </select>
                                <input type="text" name="sizeval[]" id="value" value='<?php if(isset($product)): ?><?php echo e($product->value); ?><?php endif; ?>' class="form-control service_eng"  placeholder="Enter Product Value" style="float: left;width: 30%;margin-left: 10px;">
                                <?php if($errors->has('value')): ?>value
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('value')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button id="btn-submit" type="submit" class="btn btn-default checkbtn">
                                <i class="fa fa-plus"></i> Save
                            </button>
                            <button id="btn-submit" type="button" class="btn btn-default" onclick="window.history.back()">
                                <i class="fa fa-history"></i> Cancel
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script type="text/javascript">
     $('.space_img').change(function(event) {
        var fileSize = this.files[0].size;
        var maxAllowedSize = 2000000;
        if(fileSize > maxAllowedSize){
            $('.errorimg').html('Please upload a smaller file');
           $('.space_img').val('');
        }
        else{
            $('.errorimg').html('');
            if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.oldpreview').fadeOut();
                        $('.imagePreview').attr('src', e.target.result);
                        $('.imagePreview').fadeIn();
                    }
                    reader.readAsDataURL(this.files[0]);
                }
        }

    });
     //custprdt
      $('#vehicle1').change(function() {
        if(this.checked) {
            $('.custprdt').fadeIn();
        }
        else{
            $('.custprdt').fadeOut();
        }        
    });
    /*$('.checkbtn').click(function(){

        var name = $('.service_eng').val();
        $.ajax({
                    type: 'get',
                    url: '<?php echo url('/');?>/checkservice',
                    data: 'servicename='+name,
                    datatype: "json",
                    async: false,
                    cache: false,
                    timeout: 30000,
                        success: function (response) {
                             
                        }
            });
    }); */
    $(document).ready(function() {
    $('.select2').select2();
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/laraveltest/resources/views//products/form.blade.php ENDPATH**/ ?>