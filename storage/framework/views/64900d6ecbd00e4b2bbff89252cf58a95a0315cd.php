<?php if(isset($updating) && $updating): ?>
<?php $__env->startSection('title', 'Update Sessions'); ?>
<?php else: ?>
<?php $__env->startSection('title','Add Sessions'); ?>
<?php endif; ?>

<?php $__env->startSection('styles'); ?>
<style>
    .hidden {
        display: none;
    }

    #playerModal {
        z-index: 9999 !important;
    }

    .modal-body {
        overflow-y: scroll;
    }

    #playerModalEdit {
        z-index: 9999 !important;
    }

    .auto-input {
        z-index: 1000 !important;
    }

    .ui-autocomplete-input {
        z-index: 5000;
    }

    .ui-widget-content {
        z-index: 9999;
    }

    .hide {
        display: none;
    }
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="edit-event-wrapper">
    <div class="container-fluid mt-3 mb-4">
        <div class="row">
            <div class=" mb-4">
                <h6 class="h6 mb-0 dashboard-title">Team Form :</h6>
            </div>
        </div>
    </div>

    <div class="container-fluid edit-events">
        <div class="row">
            <div class="container-fluid edit-events">
                <div class="">

                    <div class="w-100 teamsdiv">
                        <div class="headingV1">
                            <h4 class="h4 mb-4">Team Players 1 :</h4>
                        </div>
                        <div class="customClear"></div>
                        <br>
                        <form class="team_form">
                            <div class="form-row">
                                <input type="hidden" name="session_id" class="session_id" value="<?php echo e($sessions->id); ?>">
                                <input type="hidden" name="team_id" class="team_id" <?php if(isset($team1_details)): ?>
                                    value="<?php echo e($team1_details->id); ?>" <?php else: ?> value="" <?php endif; ?>>

                                <div class="col">
                                    <input type="text" name="team_name" class="form-control team_name"
                                        placeholder="Name of team one in english" <?php if(isset($team1_details)): ?>
                                        value="<?php echo e($team1_details->name); ?>" <?php endif; ?>>
                                    <p class="errorName text-center hidden" style="color:red"></p>
                                </div>
                                <div class="col">
                                    <input type="text" name="team_name_ar" class="form-control team_name_ar"
                                        placeholder="Name of team one in arabic" <?php if(isset($team1_details)): ?>
                                        value="<?php echo e($team1_details->name_ar); ?>" <?php endif; ?>>
                                    <p class="errorNameAr text-center hidden" style="color:red"></p>
                                </div>

                                <button type="button" class="buttonV1 send_Teamform" style="background: #44bd96;"
                                    <?php if(isset($team1_details)): ?> data-teamId="<?php echo e($team1_details->id); ?>" <?php endif; ?>>
                                    <?php if(isset($team1_details)): ?>
                                    Edit Team
                                    <?php else: ?>
                                    Add Team
                                    <?php endif; ?>
                                </button>
                            </div>
                        </form>

                        <div class="teamSection <?php if(!isset($team1_details)): ?> hide <?php endif; ?>" id="teamSectionOne">
                            <div class="teamName">Team 1 / Players List</div>
                            <div class=" w-100  justify-content-between mb-2 listGroup" id="teams">

                                <?php if(isset($team_one)): ?>
                                <?php $__currentLoopData = $team_one; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$team1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="form-group  col-sm-3 playerLister">
                                    <div class="listV1">
                                        <img class="imgHolderV1 rounded-circle" <?php if($team1->image): ?>
                                        src="<?php echo e(asset('backend/images/profile/'.$team1->image)); ?>"
                                        <?php else: ?> src="<?php echo e(asset('backend/assets/images/avatar.png')); ?>"
                                        <?php endif; ?> width="25%">
                                        <div class="listContV1">
                                            <a class="numberV1"><?php echo e($key+1); ?></a>
                                            <h2><?php echo e($team1->username); ?><br><span><?php echo e($team1->category); ?></span></h2>
                                            <div class="listDtlV1">
                                                <div class=" w-100 d-flex justify-content-between mb-2">
                                                    <div class="form-group  col-sm-6">Age :</div>
                                                    <div class="form-group  col-sm-6"><?php echo e($team1->age); ?></div>
                                                </div>
                                                <div class=" w-100 d-flex justify-content-between mb-2">
                                                    <div class="form-group  col-sm-6">Nationality :</div>
                                                    <div class="form-group  col-sm-6"><?php echo e($team1->country_name); ?></div>
                                                </div>
                                            </div>
                                            <a class="buttonV1 editPlayer" data-toggle="modal"
                                                data-target="#playerModalEdit" data-teams-id="<?php echo e($team1->team_id); ?>"
                                                data-id="<?php echo e($team1->id); ?>" data-user-id="<?php echo e($team1->user_id); ?>"
                                                data-name="<?php echo e($team1->username); ?>" data-mname="<?php echo e($team1->middle_name); ?>"
                                                data-lname="<?php echo e($team1->last_name); ?>" data-email="<?php echo e($team1->email); ?>"
                                                data-age="<?php echo e($team1->age); ?>" data-country="<?php echo e($team1->country); ?>"
                                                data-category="<?php echo e($team1->attendees_category_id); ?>">Edit</a>
                                            <a class="buttonV1 deletePlayer" style="background: #fb7f86"
                                                data-player-id="<?php echo e($team1->user_id); ?>">Delete</a>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                            </div>
                            <a class="buttonV1 addPlayerMain" data-toggle="modal" data-target="#playerModal"
                                <?php if(isset($team1_details)): ?> data-team-id="<?php echo e($team1_details->id); ?>" <?php endif; ?>>ADD PLAYER</a>

                            <a class="buttonV1 deleteTeam" style="background: #f05059;margin: 0 14px 14px 0;"
                                <?php if(isset($team1_details)): ?> data-teamId="<?php echo e($team1_details->id); ?>" <?php endif; ?>>Delete Team</a>
                            <div class="customClear"></div>
                        </div>

                    </div>

                    <div class=" w-100 teamsdiv">
                        <div class="headingV1">
                            <h4 class="h4 mb-4">Team Players 2 :</h4>
                        </div>
                        <div class="customClear"></div>
                        <br>
                        <form class="team_form">
                            <div class="form-row">
                                <input type="hidden" name="session_id" class="session_id" value="<?php echo e($sessions->id); ?>">
                                <input type="hidden" name="team_id" class="team_id" <?php if(isset($team2_details)): ?>
                                    value="<?php echo e($team2_details->id); ?>" <?php else: ?> value="" <?php endif; ?>>
                                <div class="col">
                                    <input type="text" name="team_name" class="form-control team_name"
                                        placeholder="Name of team two in english" <?php if(isset($team2_details)): ?>
                                        value="<?php echo e($team2_details->name); ?>" <?php endif; ?>>
                                    <p class="errorName text-center hidden" style="color:red"></p>
                                </div>
                                <div class="col">
                                    <input type="text" name="team_name_ar" class="form-control team_name_ar"
                                        placeholder="Name of team two in arabic" <?php if(isset($team2_details)): ?>
                                        value="<?php echo e($team2_details->name_ar); ?>" <?php endif; ?>>
                                    <p class="errorNameAr text-center hidden" style="color:red"></p>
                                </div>
                                <button type="button" class="buttonV1 send_Teamform" style="background: #44bd96;"
                                    <?php if(isset($team2_details)): ?> data-teamId="<?php echo e($team2_details->id); ?>" <?php endif; ?>>
                                    <?php if(isset($team2_details)): ?>
                                    Edit Team
                                    <?php else: ?>
                                    Add Team
                                    <?php endif; ?>
                                </button>
                            </div>
                        </form>


                        <div class="teamSection <?php if(!isset($team2_details)): ?> hide <?php endif; ?>" id="teamSectionTwo">
                            <div class="teamName">Team 2 / Players List</div>
                            <div class=" w-100  justify-content-between mb-2 listGroup">
                                <?php if(isset($team_two)): ?>
                                <?php $__currentLoopData = $team_two; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$team2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="form-group  col-sm-3 playerLister">
                                    <div class="listV1">
                                        <img class="imgHolderV1 rounded-circle" <?php if($team2->image): ?>
                                        src="<?php echo e(asset('backend/images/profile/'.$team2->image)); ?>"
                                        <?php else: ?> src="<?php echo e(asset('backend/assets/images/avatar.png')); ?>"
                                        <?php endif; ?> width="25%">
                                        <div class="listContV1">
                                            <a class="numberV1"><?php echo e($key+1); ?></a>
                                            <h2><?php echo e($team2->username); ?><br><span><?php echo e($team2->category); ?></span></h2>
                                            <div class="listDtlV1">
                                                <div class=" w-100 d-flex justify-content-between mb-2">
                                                    <div class="form-group  col-sm-6">Age :</div>
                                                    <div class="form-group  col-sm-6"><?php echo e($team2->age); ?></div>
                                                </div>
                                                <div class=" w-100 d-flex justify-content-between mb-2">
                                                    <div class="form-group  col-sm-6">Nationality :</div>
                                                    <div class="form-group  col-sm-6"><?php echo e($team2->country_name); ?></div>
                                                </div>
                                            </div>
                                            <a class="buttonV1 editPlayer" data-toggle="modal"
                                                data-target="#playerModalEdit" data-teams-id="<?php echo e($team2->team_id); ?>"
                                                data-id="<?php echo e($team2->id); ?>" data-user-id="<?php echo e($team2->user_id); ?>"
                                                data-name="<?php echo e($team2->username); ?>" data-mname="<?php echo e($team2->middle_name); ?>"
                                                data-lname="<?php echo e($team2->last_name); ?>" data-email="<?php echo e($team2->email); ?>"
                                                data-age="<?php echo e($team2->age); ?>" data-country="<?php echo e($team2->country); ?>"
                                                data-category="<?php echo e($team2->attendees_category_id); ?>">Edit</a>
                                            <a class="buttonV1 deletePlayer" style="background: #fb7f86"
                                                data-player-id="<?php echo e($team2->user_id); ?>">Delete</a>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>

                            <a class="buttonV1 addPlayerMain" data-toggle="modal" data-target="#playerModal"
                                <?php if(isset($team2_details)): ?> data-team-id="<?php echo e($team2_details->id); ?>" <?php endif; ?>>ADD PLAYER</a>
                            <a class="buttonV1 deleteTeam" style="background: #f05059;margin: 0 14px 14px 0;"
                                <?php if(isset($team2_details)): ?> data-teamId="<?php echo e($team1_details->id); ?>" <?php endif; ?>>Delete Team</a>
                            <div class="customClear"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
<div class="container-fluid mt-5">
    <div class=" d-flex">
        <div class=" col-sm-4 mt-3 mb-5">
            <?php if(isset($show)): ?>
            <?php if($show): ?>
            <button type="submit" class="btn btn-default">
                <a href="<?php echo e(route('admin.events.index')); ?>">Back</a>
            </button>
            <?php endif; ?>

            <?php else: ?>
            <button type="submit" class="btn btn-default btn-purple mr-2">Save</button>
            <button type="submit" class="btn btn-default">
                <a href="<?php echo e(route('admin.events.index')); ?>">Cancel</a>
            </button>
            <?php endif; ?>
        </div>
    </div>
</div>

</form>

<?php $__env->stopSection(); ?>


<div class="modalV1" id="playerModal">
    <h2>Add Player</h2>
    <div id="profile_preview">
        <img class="imgHolderV1 rounded-circle" src="" width="25%" id="preview_img">
    </div>
    <form class="players_form">
        <div class="row col-md-12 modal-body">
            <input type="hidden" name="teams_id" id="teams_id">
            <input type="hidden" name="user_id" id="user_id">
            <input type="hidden" name="session_id" value="<?php echo e($sessions->id); ?>">
            <div class="col-md-12">
                <label>First Name *</label>
                <input class="form-control auto-input autocomplete_txt" type="text" name="name" id="nameT1"
                    data-type="name" placeholder="First Name">
                <span class="errorPalyername text-center hidden" style="color:red"></span>
                <label>Middle Name</label>
                <input class="form-control" type="text" name="middle_name" id="middleNameT1" placeholder="Middle Name">

                <label>Last Name</label>
                <input class="form-control" type="text" name="last_name" id="lastNameT1" placeholder="Last Name">

                <label>Email *</label>
                <input class="form-control autocomplete_txt" type="email" name="email" id="emailT1" data-type="email"
                    placeholder="Email">
                <span class="erroremail text-center hidden" style="color:red"></span>

                <label>Age</label>
                <input class="form-control" type="number" name="age" id="ageT1" placeholder="Age">

                <label>Image</label>
                <input class="form-control" type="file" name="image" id="imageT1" placeholder="Image">

                <label>Country *</label>
                <select name="country" class="form-control" id="counrtyselect">
                    <option value=''>Select country</option>
                    <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($value->id); ?>" data-country="<?php echo e($value->id); ?>"><?php echo e($value->name); ?>

                    </option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <span class="errorcountry_id text-center hidden" style="color:red"></span>

                <label>Category *</label>
                <select name="category" id="categoryT1" class="form-control">
                    <option value=''>Select category</option>
                    <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($values->id); ?>"
                        <?php echo e((old("attendees_category_id") == $values->id ? "selected":"")); ?>>
                        <?php echo e($values->name); ?>

                    </option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <span class="errorcategory_id text-center hidden" style="color:red"></span>

            </div>
        </div>
        <button type="button" class="buttonV1 send_Playerform" id="">Save</button>
        <a class="buttonV1" style="background: #fb7f86" data-dismiss="modal">Cancel</a>
    </form>

</div>


<div class="modalV1" id="playerModalEdit">
    <h2>Add Player</h2>
    <img class="imgHolderV1 rounded-circle" src="<?php echo e(asset('backend/assets/images/avatar.png')); ?>">
    <form class="players_form">
        <div class="row col-md-12">
            <input type="hidden" name="teams_id" id="teams_idEdit">
            <input type="hidden" name="user_id" id="user_idEdit">
            <input type="hidden" name="player_id" id="player_idEdit">
            <input type="hidden" name="session_id" value="<?php echo e($sessions->id); ?>">
            <div class="col-md-12">
                <label>First Name *</label>
                <input class="form-control auto-input " type="text" name="name" id="nameEdit" data-type="name"
                    placeholder="First Name">
                <p class="errorPalyername text-center hidden" style="color:red"></p>
                <label>Middle Name</label>
                <input class="form-control" type="text" name="middle_name" id="middleNameEdit"
                    placeholder="Middle Name">

                <label>Last Name</label>
                <input class="form-control" type="text" name="last_name" id="lastNameEdit" placeholder="Last Name">

                <label>Email *</label>
                <input class="form-control" type="email" name="email" id="emailEdit" data-type="email"
                    placeholder="Email">
                <p class="erroremail text-center hidden" style="color:red"></p>

                <label>Age</label>
                <input class="form-control" type="number" name="age" id="ageEdit" placeholder="Age">

                <label>Image</label>
                <input class="form-control" type="file" name="image" id="imageEdit" placeholder="Image">

                <label>Country *</label>
                <select name="country" class="form-control" id="counrtyselectEdit">
                    <option value=''>Select country</option>
                    <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($value->id); ?>" data-countryedit="<?php echo e($value->id); ?>"><?php echo e($value->name); ?>

                    </option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <p class="errorcountry_id text-center hidden" style="color:red"></p>

                <label>Category *</label>
                <select name="category" id="categoryEdit" class="form-control">
                    <option value=''>Select category</option>
                    <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($values->id); ?>" data-categoryedit="<?php echo e($values->id); ?>">
                        <?php echo e($values->name); ?>

                    </option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <p class="errorcategory_id  text-center hidden" style="color:red"></p>
            </div>
        </div>
        <button type="button" class="buttonV1 send_Playerform">Save</button>
        <a class="buttonV1" style="background: #fb7f86" data-dismiss="modal">Cancel</a>
    </form>

</div>

<div class="overLayer"></div>
<?php $__env->startSection('scripts'); ?>

<script>
    var category = <?php echo json_encode($category-> toArray()); ?>;
    var APP_URL = <?php echo json_encode(url('/')); ?>

    var session_id = $(".session_id").val();

    $('#imageT1').change(function(){           
        let reader = new FileReader();       
        reader.onload = (e) => {        
            $('#preview_img').attr('src', e.target.result); 
        }       
        reader.readAsDataURL(this.files[0]);          
    });

    //add team
    $(".send_Teamform").click(function() {
        
        var url = '<?php echo e(route('admin.team.store')); ?>';
        var data = $(this).parents('form').serialize();

        var _input = $(this);
        var _parent = $(this).parents();

            $.ajax({
                type: 'POST',
                url: url,
                data: data,

                success: function(data) {
                    console.log(data);
                    if ((data.errors)) {
                        setTimeout(function () {                            
                            toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                        }, 500);

                        if (data.errors.team_name) {
                            _input.parents('form').find('.errorName').text(data.errors.team_name);
                        }
                        if (data.errors.team_name_ar) {                            
                            _input.parents('form').find('.errorNameAr').text(data.errors.team_name_ar);
                        }
                    } else {
                        $('.errorName').hide();
                        $('.errorNameAr').hide();

                        _input.html("Edit Team");                        
                        _parent.trigger('reset');
                        
                        _input.parents('form').find('.team_name').val(data['this_team']['name']);
                        _input.parents('form').find('.team_name_ar').val(data['this_team']['name_ar']);
                        _input.parents('form').find('.team_id').val(data['this_team']['id']);
                        
                        _parent.parent('.teamsdiv').find('.teamSection').show();
                        
                        toastr.success('Successfully added!', 'Success Alert', {timeOut: 5000}); 
                        
                    }
                },
            });
    });

    //delete team
    $(".deleteTeam").click(function(){
        
        _parent = $(this).parent().parent().find('form');
        _team_id = _parent.find('.team_id').val();  
        var url='<?php echo e(route('admin.team.destroy')); ?>';
        
        $.ajax(
        {
            url: url+'/'+_team_id,
            type: 'DELETE',
            data: {
                "id": _team_id,
            },
            success: function (data){
                if (data.errors) {
                    setTimeout(function () {                            
                        toastr.error('Somthing went wrong!', 'Error Alert', {timeOut: 5000});
                    }, 500);
                } else{
                    toastr.success('Successfully deleted!', 'Success Alert', {timeOut: 100000}); 
                    setTimeout(function () {
                                    window.location.reload();
                                }, 500);
                }
            }
        });
    
    });

    //autocomplete players
    $(document).on('focus', '.autocomplete_txt', function () {
            type = $(this).data('type');

            if (type == 'name') autoType = 'name';
            if (type == 'email') autoType = 'email';

            $(this).autocomplete({
                minLength: 0,
                source: function (request, response) {

                    $.ajax({
                        url: "<?php echo e(route('admin.team.autocompletes')); ?>",
                        dataType: "json",
                        data: {
                            term: request.term,
                            type: type,
                            session_id: session_id
                        },
                        success: function (data) {
                            var array = $.map(data, function (item) {
                                return {
                                    label: item[autoType],
                                    value: item[autoType],
                                    data: item
                                }
                            });
                            response(array)
                        }
                    });
                },
                select: function (event, ui) {
                    var data = ui.item.data;

                    $('#user_id').val(data.id);
                    $('#nameT1').val(data.name);
                    $('#middleNameT1').val(data.middle_name);
                    $('#lastNameT1').val(data.last_name);
                    $('#emailT1').val(data.email);
                    $('#ageT1').val(data.age);
                    if(data.image != null){
                        $("#profile_preview").html('<img src="' + APP_URL + '/backend/images/profile/' +
                        data.image + '" class="img-fluid" alt="preview" width="25%">');
                    } 
                    // else{
                    //     $("#profile_preview").html('<img src="' + APP_URL + '/backend/images/img_avatar.png" class="img-fluid" alt="preview" width="25%">');
                        
                    // }
                    
                    $("#counrtyselect option[data-country='" + data.country +"']").attr("selected","selected");
                    
                }
            });

    });

    // team id pass to modal
    $(document).on('click', '.addPlayerMain', function() {
        $('.errorPalyername').addClass('hidden');
        $('.erroremail').addClass('hidden');
        $('.errorcategory_id').addClass('hidden');
        $('.errorcountry_id').addClass('hidden');

        _parent = $(this).parent().parent().find('form');
        _team_id = _parent.find('.team_id').val();
        $('#playerModal form')[0].reset();
        $('#playerModal').find('#teams_id').val(_team_id)    
        $('#playerModal').modal('show');
    });

    
    //add players
    // $(".send_Playerform").click(function() {
    $('.send_Playerform').click(function(evt){
    // Stop the button from submitting the form:
    evt.preventDefault();
    
    // Serialize the entire form:
    var data = new FormData(this.form);
        
        var url = "<?php echo e(route('admin.player.store')); ?>";

            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                processData: false,
                contentType: false,
                success: function(data) {                       
                    
                    if ((data.errors)) {
                        setTimeout(function () {                            
                            toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                        }, 500);

                        if (data.errors.name) {
                            $('.errorPalyername').removeClass('hidden');
                            $('.errorPalyername').text(data.errors.name);                           

                        }
                        if (data.errors.email) {
                            $('.erroremail').removeClass('hidden');
                            $('.erroremail').text(data.errors.email);
                        }
                        if (data.errors.category) {
                            $('.errorcategory_id').removeClass('hidden');
                            $('.errorcategory_id').text(data.errors.category); 
                        }
                        if (data.errors.country) {
                            $('.errorcountry_id').removeClass('hidden');
                            $('.errorcountry_id').text(data.errors.country); 
                        }
                    } else {                       
                        $('.errorPalyername').addClass('hidden');
                        $('.erroremail').addClass('hidden');
                        $('.errorcategory_id').addClass('hidden');
                        $('.errorcountry_id').addClass('hidden');

                        toastr.success('Successfully added!', 'Success Alert', {timeOut: 5000}); 
                        $('#playerModal').modal('hide');
                        $('#playerModal form')[0].reset();

                        $('#playerModalEdit').modal('hide');//edit
                        $('#playerModalEdit form')[0].reset();//edit
                        setTimeout(function () {
                                    window.location.reload();
                                }, 500);
                        // var card = '';
                        //     $(data).each(function (index, value) {    
                        //         no = index+1;
                        //             card += '<div class="form-group  col-sm-3 playerLister">'+
                        //             ' <div class="listV1">';
                        //             if(value.image){
                        //                 card +=  '<img class="imgHolderV1 rounded-circle" src="' + APP_URL + '/backend/images/profile/'+value.image+'" alt="preview" width="25%">';
                        //             }else{
                        //                 card +=  '<img class="imgHolderV1 rounded-circle"  src="' + APP_URL + '/backend/images/img_avatar.png" width="25%">';
                        //             }
                                    
                        //             card +='<div class="listContV1"><a class="numberV1">'+no+'</a><h2>'+value.username+'&nbsp;'+value.middle_name+'&nbsp;'+value.last_name+'<br><span>'+value.category+'</span></h2>'+
                        //             '<div class="listDtlV1"><div class=" w-100 d-flex justify-content-between mb-2">'+
                        //             '<div class="form-group  col-sm-6">Age :</div>'+
                        //             '<div class="form-group  col-sm-6">'+value.age+'</div></div>'+
                        //             '<div class=" w-100 d-flex justify-content-between mb-2">'+
                        //             '<div class="form-group  col-sm-6">Nationality :</div>'+
                        //             '<div class="form-group  col-sm-6">'+value.country_name+'</div></div></div>'+
                        //             '<a class="buttonV1 editPlayer" data-toggle="modal" data-target="#playerModalEdit" '+
                        //                         'data-teams-id="team2->team_id"'+
                        //                         'data-user-id="'+value.user_id+'"'+
                        //                         'data-name="'+value.username+'" '+
                        //                         'data-mname="'+value.middle_name+'"'+
                        //                         'data-lname="'+value.last_name+'" '+
                        //                         'data-name="'+value.email+'"'+
                        //                         'data-age="'+value.age+'" '+
                        //                         'data-country="'+value.country+'"'+
                        //                         'data-category="'+value.attendees_category_id+'">Edit</a>'+
                        //             '<a class="buttonV1 deletePlayer" style="background: #fb7f86" data-player-id="'+value.user_id+'">Delete</a></div></div></div>';
                                
                        //     });
                            
                        // $('#teams').html(card);
                        
                    }
                },
            });
    });   
    

    // Edit a player
    $(document).on('click', '.editPlayer', function() {
        $('.errorPalyername').addClass('hidden');
        $('.erroremail').addClass('hidden');
        $('.errorcategory_id').addClass('hidden');
        $('.errorcountry_id').addClass('hidden');

        $('#teams_idEdit').val($(this).data('teams-id'));
        $('#player_idEdit').val($(this).data('id'));
        $('#user_idEdit').val($(this).data('user-id'));
        $('#nameEdit').val($(this).data('name'));
        $('#middleNameEdit').val($(this).data('mname'));
        $('#lastNameEdit').val($(this).data('lname'));
        $('#emailEdit').val($(this).data('email'));
        $('#ageEdit').val($(this).data('age'));
        countryId = $("#counrtyselectEdit").val($(this).data('country')); 
        $("#counrtyselect option[data-countryedit='" + countryId +"']").attr("selected","selected");

        categoryId = $("#categoryEdit").val($(this).data('category')); 
        $("#categoryEdit option[data-categoryedit='" + categoryId +"']").attr("selected","selected");

        id = $('#player_idEdit').val();
        $('#playerModalEdit').modal('show');

        
    });

    //delete player
    $('body').on('click', '.deletePlayer', function() {
        
        var playerId = $(this).data("player-id");
        var sessionId = $(".session_id").val();
        
        var url='<?php echo e(route('admin.player.remove')); ?>';
    
        $.ajax(
        {
            url: url+'/'+playerId,
            type: 'DELETE',
            data: {
                "id": playerId,"session_id":sessionId
            },
            success: function (data){
                if (data.errors) {
                    setTimeout(function () {                            
                        toastr.error('Somthing went wrong!', 'Error Alert', {timeOut: 5000});
                    }, 500);
                } else{
                    toastr.success('Successfully deleted!', 'Success Alert', {timeOut: 5000}); 

                    setTimeout(function () {
                                    window.location.reload();
                                }, 500);
                }
            }
        });
    
    });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/team/create.blade.php ENDPATH**/ ?>