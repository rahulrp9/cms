<?php if(isset($updating) && $updating): ?>
<?php $__env->startSection('title', 'Update settings'); ?>
<?php else: ?>
<?php $__env->startSection('title','Add Settings'); ?>
<?php endif; ?>
<?php $__env->startSection('content'); ?>
<div class="edit-event-wrapper">
    <div class="container-fluid mt-3 mb-4">
        <div class="row">
            <div class=" mb-4">
                <h6 class="h6 mb-0 dashboard-title">Settings Form:</h6>
            </div>
        </div>
    </div>

    <div class="container-fluid edit-events">
        <div class="row">
                <form class="form-horizontal form-datetimepicker col-sm-6" action="<?php echo e(route('admin.settings.store')); ?>"
                    method="post" enctype="multipart/form-data">
                    <?php echo e(csrf_field()); ?>

                    
                    <div class="form-group d-flex">
                        <label class="control-label col-sm-6" for="name">Is Otp Verification  <em>*</em>:</label>
                        <div class="row">
                            <div class="form-check col-sm-4">
                                <label class="custom-radio-button">
                                    <input type="radio" value="0" name="key" class="form-check-input"
                                        id="exampleCheck1" <?php echo e(isset($settings)? $settings->key_value == 0? 'checked' :'':''); ?>>
                                    <span class="helping-el"></span> <span class="form-check-label label-text"
                                        for="exampleCheck1"></span>Disabled </label>
                            </div>
                            <div class="form-check col-sm-4">
                                <label class="custom-radio-button">
                                    <input type="radio" value="1" name="key" class="form-check-input"
                                        id="exampleCheck2" <?php echo e(isset($settings)? $settings->key_value == 1? 'checked' :'':''); ?>>
                                    <span class="helping-el"></span> <span class="form-check-label label-text"
                                        for="exampleCheck2"></span>Enabled </label>
                            </div>

                            <?php if($errors->has('key')): ?>
                            <label for="key" class="error"> <?php echo e($errors->first('key')); ?> </label>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="container-fluid mt-5">
                        <div class=" d-flex">
                            <div class=" col-sm-4 mt-3 mb-5">
                                <button type="submit" class="btn btn-default btn-purple mr-2">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
        </div>


    </div>
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('scripts'); ?>


    <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/settings/create.blade.php ENDPATH**/ ?>