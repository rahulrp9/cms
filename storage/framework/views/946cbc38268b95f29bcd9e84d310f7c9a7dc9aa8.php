<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>FoodApp | Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="<?php echo e(asset('backend/assets/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('backend/assets/css/styles.css')); ?>" rel="stylesheet" type="text/css" />
</head>
<style type="text/css">
    .login{
background-size: cover;
    }
</style>
<body>
    <div class="login d-flex">
        <div class=" col-lg-4 col-md-4 mx-auto login-center">
            <div class="content">
                <form class="login-form" method="POST" action="<?php echo e(route('login')); ?>">
                    <?php echo csrf_field(); ?>
                    <div class="logo text-center">
                       
                    </div>
                    <h6 class="form-title text-center">
                        Let’s start with Log in!
                        </h3>
                        <div class="form-group">
                            <input id="email" type="email" class="form-control envelope <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>"
                            name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" autofocus placeholder="User Name">
                            <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>
                        
                        <div class="form-group">
                            <input id="password" type="password" class="form-control password <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="password" required
                            autocomplete="current-password" placeholder="Password">
                            <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>
                        <div style="display: table;width: 100%;" >
                            <div style="display: table-row; margin-left: auto; margin-right: auto;">
                                <div style="display: table-cell;vertical-align: top;">
                                <input type="checkbox" name="remember_me" id="remember_me" class="" >
                                </div>
                                <div style="display: table-cell; vertical-align: top;">
                                Remember me
                                </div>
                            </div>
                        </div>
                      
                        <div class="form-actions">
                            <button type="submit" class="btn btn-block">Login</button>
                        </div>
                        <div class="form-actions d-flex "> <a href="<?php echo e(route('password.request')); ?>" class="forget-password">Forgot Password?</a> 
                            <!-- <a href="javascript:void(0);" class="register">Register</a>  --></div>
                </form>
            </div>
        </div>
    </div>
    <script src="<?php echo e(asset('backend/assets/js/jquery-3.3.1.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('backend/assets/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
</body>

</html><?php /**PATH /var/www/html/laraveltest/resources/views/auth/login.blade.php ENDPATH**/ ?>