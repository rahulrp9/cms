<?php if(isset($updating) && $updating): ?>
<?php $__env->startSection('title', 'Update Category'); ?>
<?php else: ?>
<?php $__env->startSection('title','Add Category'); ?>
<?php endif; ?>
<?php $__env->startSection('content'); ?>
<div class="edit-event-wrapper">
    <div class="container-fluid mt-3 mb-4">
        <div class="row">
            <div class=" mb-4">
                <h6 class="h6 mb-0 dashboard-title">Category Form:</h6>
            </div>
        </div>
    </div>

    <div class="container-fluid edit-events">
        <div class="row">
            <?php if(isset($updating) && $updating): ?>
            <form class="form-horizontal form-datetimepicker col-sm-6"
                action="<?php echo e(route('admin.category.update', $category->id)); ?>" method="post"
                enctype="multipart/form-data">
                <?php echo e(method_field('PUT')); ?> <?php else: ?>
                <form class="form-horizontal form-datetimepicker col-sm-6" action="<?php echo e(route('admin.category.store')); ?>"
                    method="post" enctype="multipart/form-data">
                    <?php endif; ?> <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="event_id" value="<?php echo e(Request::segment(3)); ?>">
                    <div class="form-group d-flex">
                        <label class="control-label col-sm-6" for="name">Category name <em>*</em>:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name" placeholder="Enter category name"
                                maxlength="60" name="name"
                                value="<?php echo e(isset($category) ? $category->name : old('name')); ?>" <?php if(@isset($show)): ?>
                                readonly <?php endif; ?>>

                            <?php if($errors->has('name')): ?>
                            <label for="name" class="error">
                                <?php echo e($errors->first('name')); ?>

                            </label>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <label class="control-label col-sm-6" for="name">Category name (ar) <em>*</em>:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name"
                                placeholder="Enter category name in arabic" maxlength="60" name="name_ar"
                                value="<?php echo e(isset($category) ? $category->name_ar : old('name_ar')); ?>" <?php if(@isset($show)): ?>
                                readonly <?php endif; ?>>

                            <?php if($errors->has('name_ar')): ?>
                            <label for="name" class="error">
                                <?php echo e($errors->first('name_ar')); ?>

                            </label>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="container-fluid mt-5">
                        <div class=" d-flex">
                            <div class=" col-sm-8 mt-3 mb-5">
                                <?php if(isset($show)): ?>
                                <?php if($show): ?>
                                <button type="submit" class="btn btn-default">
                                    <a href="<?php echo e(route('admin.category.index')); ?>">Back</a>
                                </button>
                                <?php endif; ?>

                                <?php else: ?>
                                <button type="submit" class="btn btn-default btn-purple mr-2">Save</button>
                                <button type="submit" class="btn btn-default">
                                    <a href="<?php echo e(route('admin.category.index')); ?>">Cancel</a>
                                </button>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </form>
        </div>


    </div>
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('scripts'); ?>


    <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/category/create.blade.php ENDPATH**/ ?>