<?php if(isset($updating) && $updating): ?>
<?php $__env->startSection('title', 'Update Event'); ?>
<?php else: ?>
<?php $__env->startSection('title','Add Event'); ?>
<?php endif; ?> 
<style>
    #mapModal {
        width: 85%;
        height: 75%;
        border-radius: 10px;
        margin: auto;
        background: #ffffff;
    }

    #map {
        width: 100%;
        height: 85%;
    }

    #googleSearch {
        margin: auto;
        text-overflow: ellipsis;
        width: 600px;
    }

    .seatlabel {
        position: absolute;
        margin-top: -2px;
        margin-left: 4px;
    }
</style>

<script type="text/javascript">
    "use strict";
    var map, infoWindow, pos, mapMarker;
    var LATITUDE, LONGITUDE;
    let markers = [];
    let infoWindows = [];

    function initMap() {

        var doha_quatar = {
            lat: 25.285475,
            lng: 51.531115
        };

        map = new google.maps.Map(
            document.getElementById('map'), {
                zoom: 8,
                center: doha_quatar,
                zoomControl: true,
                mapTypeControl: true,
                scaleControl: true,
                streetViewControl: true,
            });
        var defaultMarker = new google.maps.Marker({
            position: doha_quatar,
            map: map
        });
        infoWindow = new google.maps.InfoWindow();

        // const searchInput = document.getElementById('googleSearch');
        const searchInput = document.createElement('input');
        searchInput.id = 'googleSearch';
        searchInput.name = 'googleSearch';
        searchInput.class = 'form-control';
        searchInput.placeholder = 'Search places here'
        searchInput.type = 'text';
        const searchBox = new google.maps.places.SearchBox(searchInput);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(searchInput);

        map.addListener("bounds_changed", () => {
            searchBox.setBounds(map.getBounds());
        });

        searchBox.addListener("places_changed", () => {
            const places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            markers.forEach(marker => {
                marker.setMap(null);
            });

            markers = [];

            const bound = new google.maps.LatLngBounds();

            places.forEach(place => {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }

                const icon = {
                    url: place.icon,
                    size: new google.maps.Size(50, 50),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                markers.push(
                    new google.maps.Marker({
                        map,
                        icon,
                        title: place.name,
                        position: place.geometry.location
                    })
                );

                if (place.geometry.viewport) {
                    bound.union(place.geometry.viewport);
                } else {
                    bound.extend(place.geometry.location);
                }
            });
            map.fitBounds(bound);
        });

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                position => {
                    pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    defaultMarker.setPosition(pos);
                    infoWindow.setPosition(pos);
                    infoWindow.setContent("Your are  here");
                    infoWindow.open(map, defaultMarker);
                    infoWindows.push(infoWindow);
                    markers.push(defaultMarker);
                    map.setCenter(pos);
                },
                () => {
                    locationError(true, infoWindow, map.getCenter());
                }
            );
        } else {
            locationError(false, infoWindow, map.getCenter());
        }

        function locationError(geolocationStatus, infoWin, pos) {
            infoWin.setPosition(pos);
            infoWin.setContent(
                geolocationStatus ?
                "your browser doesn't allow for access location. Default location Doha Qatar" :
                "Error: Your browser doesn't support geolocation."
            );
            infoWin.open(map, defaultMarker);
            infoWindows.push(infoWin);
            markers.push(defaultMarker);
        }

        map.addListener('click', function (mapEvent) {
            LATITUDE = mapEvent.latLng.lat();
            LONGITUDE = mapEvent.latLng.lng();
            addMarker(mapEvent.latLng);

        });
    }

    function addMarker(latLng) {
        for (let i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }

        for (let i = 0; i < infoWindows.length; i++) {
            infoWindows[i].setMap(null);
        }

        const infoWin = new google.maps.InfoWindow({
            position: latLng,
            content: "Latitude:" + LATITUDE + ", Longitude:" + LONGITUDE,
            map: map
        });
        const marker = new google.maps.Marker({
            map: map,
            position: latLng,
            draggable: false
        });
        document.getElementById('longitude').value = LONGITUDE;
        document.getElementById('latitude').value = LATITUDE;
        infoWin.open(map, marker);
        infoWindows.push(infoWin);
        markers.push(marker);
    }
</script>
<?php $__env->startSection('content'); ?>

<div class="edit-event-wrapper">
    <div class="container-fluid mt-3 mb-4">
        <div class="row">
            <div class=" mb-4">
                <h6 class="h6 mb-0 dashboard-title">Event Form :</h6>
            </div>
        </div>
    </div>
    <div class="container-fluid edit-events">
        <?php if(isset($updating) && $updating): ?>
        <form class="form-horizontal" action="<?php echo e(route('admin.events.update', $events->id)); ?>" method="post"
            enctype="multipart/form-data">
            <?php echo e(method_field('PUT')); ?> <?php else: ?>
            <form class="form-horizontal" action="<?php echo e(route('admin.events.store')); ?>" method="post"
                enctype="multipart/form-data">
                <?php endif; ?> <?php echo e(csrf_field()); ?>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="fname"><span>Event Name <em>*</em>:</span></label>
                            <div class="input-field">
                                <input type="text" class="form-control" id="name" placeholder="Event name in english"
                                    maxlength="250" name="name"
                                    value="<?php echo e(isset($events) ? $events->name : old('name')); ?>">
                                <?php if($errors->has('name')): ?>
                                <label for="name" class="error"> <?php echo e($errors->first('name')); ?> </label>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="fname"><span class="arabic-label">Event Name
                                    (ar) <em>*</em>:</span></label>
                            <div class="input-field ">
                                <input type="text" class="form-control" id="name" placeholder="Event Name in Arabic"
                                    maxlength="250" name="name_ar"
                                    value="<?php echo e(isset($events) ? $events->name_ar : old('name_ar')); ?>">
                                <?php if($errors->has('name_ar')): ?>
                                <label for="name_ar" class="error"> <?php echo e($errors->first('name_ar')); ?> </label>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label " for="email"><span>Description <em>*</em>:</span></label>
                            <div class="input-field">
                                <textarea class="form-control" id="description" rows="4"
                                    name="description"><?php echo e(isset($events) ? $events->description : old('description')); ?></textarea>
                                <?php if($errors->has('description')): ?>
                                <label for="description" class="error"> <?php echo e($errors->first('description')); ?> </label>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label " for="email"><span class="arabic-label">Description
                                    (ar) <em>*</em>:</span></label>
                            <div class="input-field ">
                                <textarea class="form-control" id="description" rows="4"
                                    name="description_ar"><?php echo e(isset($events) ? $events->description_ar : old('description_ar')); ?></textarea>
                                <?php if($errors->has('description_ar')): ?>
                                <label for="description_ar" class="error">
                                    <?php echo e($errors->first('description_ar')); ?></label>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group section-select">
                            <label class="form-check-label ">Event Type </label>
                            <div class="row">
                                <div class="form-check col-sm-4">
                                    <label class="custom-radio-button">
                                        <input type="radio" value="1" name="type" class="form-check-input"
                                            id="exampleCheck1"
                                            <?php echo e(isset($events)? $events->type == 1? 'checked' :'':''); ?>>
                                        <span class="helping-el"></span> <span class="form-check-label label-text"
                                            for="exampleCheck1"></span>Private </label>
                                </div>
                                <div class="form-check col-sm-4">
                                    <label class="custom-radio-button">
                                        <input type="radio" value="0" name="type" class="form-check-input"
                                            id="exampleCheck2"
                                            <?php echo e(isset($events)? $events->type == 0? 'checked' :'':''); ?>>
                                        <span class="helping-el"></span> <span class="form-check-label label-text"
                                            for="exampleCheck2"></span>Public </label>
                                </div>

                                <?php if($errors->has('type')): ?>
                                <label for="type" class="error"> <?php echo e($errors->first('type')); ?> </label>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="lname"> <span>Category <em>*</em>:</span></label>
                            <div class="input-field venue-text">
                                <div class=" input-group venue">
                                    <select name="event_category_id" id="event_category_id"
                                        class="form-control select2">
                                        <option value="">Select Category</option>

                                        <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <option value="<?php echo e($value->id); ?>" <?php if(isset($events) && $value->id ==
                                            $events->event_category_id ): ?> <?php echo e('selected'); ?> <?php endif; ?>
                                            <?php echo e((old("event_category_id") == $value->id ? "selected":"")); ?>><?php echo e($value->name); ?>

                                        </option>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <?php if($errors->has('country_id')): ?>
                        <label for="country_id" class="error"> <?php echo e($errors->first('country_id')); ?> </label>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group section-select justify-content-between">
                            <label class="control-label " for="comment">Date <em>*</em>:</label>
                            <div class="input-field  d-flex align-items-center justify-content-between">
                                <div class="input-group date-picker" data-date-format="mm-dd-yyyy">
                                    <input class="form-control datepicker1" type="text" readonly="" name="from_date"
                                        value="<?php echo e(isset($events) ? $events->from_date : old('from_date')); ?>">
                                </div>
                                <em class="text-uppercase">To</em>
                                <div class="input-group " data-date-format="mm-dd-yyyy">
                                    <input class="form-control datepicker2" type="text" readonly="" name="to_date"
                                        value="<?php echo e(isset($events) ? $events->to_date : old('to_date')); ?>"> </div>

                            </div>
                            <?php if($errors->has('from_date')): ?>
                            <br><label for="from_date" class="error"> <?php echo e($errors->first('from_date')); ?> </label><br>
                            <?php endif; ?>
                            <?php if($errors->has('to_date')): ?>

                            <label for="to_date" class="error"> <?php echo e($errors->first('to_date')); ?> </label>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group section-select">
                            <label class="control-label " for="comment">Time :</label>
                            <div class="input-field d-flex align-items-center justify-content-between">
                                <div id="datetimepicker" class="time-picker  input-group date">
                                    <input class="form-control" data-format="hh:mm" type="time" name="start_time"
                                        value="<?php echo e(isset($events) ? $events->start_time : old('start_time')); ?>"
                                        pattern="([1]?[0-9]|2[0-3]):[0-5][0-9]">
                                    <?php if($errors->has('start_time')): ?>
                                    <label for="start_time" class="error"> <?php echo e($errors->first('start_time')); ?></label>
                                    <?php endif; ?>
                                </div>
                                <em class="text-uppercase">To</em>
                                <div id="datetimepicker1" class="time-picker  input-group date">
                                    <input class="form-control" data-format="hh:mm" type="time" name="end_time"
                                        value="<?php echo e(isset($events) ? $events->end_time : old('end_time')); ?>"
                                        pattern="([1]?[0-9]|2[0-3]):[0-5][0-9]">
                                    <?php if($errors->has('end_time')): ?>
                                    <label for="end_time" class="error"> <?php echo e($errors->first('end_time')); ?> </label>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-md-12">
                        <div class="form-group section-select">
                            <label class="control-label " for="comment">Duration :</label>

                            <div class="input-field d-flex align-items-center justify-content-between">
                                <div class="input-group">
                                    <input class="form-control  col-xs-2 numeric" type="text" name="in_days"
                                        value="<?php echo e(isset($events) ? $events->in_days : old('in_days')); ?>"
                                        placeholder="Event Duration in Days" style="width: 40px;">
                                    <?php if($errors->has('in_days')): ?>
                                    <label for="in_days" class="error"> <?php echo e($errors->first('in_days')); ?> </label>
                                    <?php endif; ?>
                                </div>
                                <div class="input-group">
                                    <input class="form-control" type="text" name="in_hours"
                                        value="<?php echo e(isset($events) ? $events->in_hours : old('in_hours')); ?>"
                                        placeholder="Event Duration in Hours">
                                    <?php if($errors->has('in_hours')): ?>
                                    <label for="in_hours" class="error"> <?php echo e($errors->first('in_hours')); ?> </label>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="Roomname"> <span>Duration in Days :</span></label>
                            <div class="input-field venue-text">
                                <input class="form-control  col-xs-2 numeric" type="text" name="in_days"
                                        value="<?php echo e(isset($events) ? $events->in_days : old('in_days')); ?>"
                                        placeholder="Event Duration in Days" >
                                    <?php if($errors->has('in_days')): ?>
                                    <label for="in_days" class="error"> <?php echo e($errors->first('in_days')); ?> </label>
                                    <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="Roomname"> <span>Duration in Hours :</span></label>
                            <div class="input-field venue-text">
                                <input class="form-control" type="text" name="in_hours"
                                        value="<?php echo e(isset($events) ? $events->in_hours : old('in_hours')); ?>"
                                        placeholder="Event Duration in Hours">
                                    <?php if($errors->has('in_hours')): ?>
                                    <label for="in_hours" class="error"> <?php echo e($errors->first('in_hours')); ?> </label>
                                    <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="Roomname"> <span>Is seat Limit :</span></label>
                            <label class="custom-radio-button">
                                <input type="checkbox" value="1" name="is_seat_limit" id="is_seat_limit"
                                    class="seatlabel" id="exampleCheck1"
                                    <?php echo e(isset($events)? $events->seat_limit != 0? 'checked' :'':''); ?>>
                                <span class="helping-el"></span> <span class="form-check-label label-text"
                                    for="exampleCheck1"></span></label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" id="seatLimit">
                            <label class="control-label " for="Roomname"> <span>No of Seat:</span></label>
                            <label class="custom-radio-button col-md-2">
                                <input type="number" name="seat_limit"
                                    value="<?php echo e(isset($events) ? $events->seat_limit : old('seat_limit')); ?>"
                                    class="seatlabel numeric">
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="lname"> <span>Venue Name<em>*</em>:</span></label>
                            <div class="input-field venue-text">
                                <div class=" input-group venue">
                                    <input type="text" class="form-control" id="venue" name="venue"
                                        value="<?php echo e(isset($events) ? $events->venue : old('venue')); ?>"
                                        style="width:150px;" placeholder="Enter Venu in english">
                                    <span class="input-group-addon"><img
                                            src="<?php echo e(asset('backend/assets/images/globe.png')); ?>" alt="time"></span>

                                </div>

                            </div>

                        </div>
                        <?php if($errors->has('venue')): ?>
                        <label for="venue" class="error"> <?php echo e($errors->first('venue')); ?> </label>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="lname"><span class="arabic-label">Venue Name (ar)
                                    <em>*</em>:</span></label>
                            <div class="input-field venue-text">
                                <div class=" input-group venue">
                                    <input type="text" class="form-control" id="venue" name="venue_ar"
                                        value="<?php echo e(isset($events) ? $events->venue_ar : old('venue_ar')); ?>"
                                        placeholder="Enter Venu in arabic">
                                    <span class="input-group-addon"><img
                                            src="<?php echo e(asset('backend/assets/images/globe.png')); ?>" alt="time"></span>
                                </div>
                            </div>

                        </div>
                        <?php if($errors->has('venue_ar')): ?>
                        <label for="venue_ar" class="error"> <?php echo e($errors->first('venue_ar')); ?> </label>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 pull-right">
                        <button type="button" id="modalButton" style="margin-top: 10px;margin-left: 20px;padding: 3px;font-size: 12px;"
                            class="btn btn-purple pull-right" data-toggle="modal" data-target="#mapModal"> Fetch Address
                            from map
                        </button>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="Roomname"> <span>Address <em>*</em>:</span></label>
                            <div class="input-field venue-text">
                                <input type="text" class="form-control" id="name" name="address"
                                    value="<?php echo e(isset($events) ? $events->address : old('address')); ?>"
                                    placeholder="Enter Event Address In English">
                                <?php if($errors->has('address')): ?>
                                <label for="address" class="error"> <?php echo e($errors->first('address')); ?> </label>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="Roomname"> <span>Address (ar)<em>*</em>:</span></label>
                            <div class="input-field venue-text">
                                <input type="text" class="form-control" id="name" name="address_ar"
                                    value="<?php echo e(isset($events) ? $events->address_ar : old('address_ar')); ?>"
                                    placeholder="Enter Event Address In Arabic">
                                <?php if($errors->has('address_ar')): ?>
                                <label for="address_ar" class="error"> <?php echo e($errors->first('address_ar')); ?> </label>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="Roomname"> <span>Room :</span></label>
                            <div class="input-field venue-text">
                                <input type="text" class="form-control" id="room" name="room"
                                    value="<?php echo e(isset($events) ? $events->room : old('room')); ?>"
                                    placeholder="Enter Room Details In English">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="Roomname"> <span>Room (ar) :</span></label>
                            <div class="input-field venue-text">
                                <input type="text" class="form-control" id="name" name="room_ar"
                                    value="<?php echo e(isset($events) ? $events->room_ar : old('room_ar')); ?>"
                                    placeholder="Enter Room Details In Arabic">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="Roomname"> <span>Hall :</span></label>
                            <div class="input-field venue-text">
                                <input type="text" class="form-control" id="hall" name="hall"
                                    value="<?php echo e(isset($events) ? $events->hall : old('hall')); ?>"
                                    placeholder="Enter Hall Details In English">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="Roomname"> <span>Hall (ar) :</span></label>
                            <div class="input-field venue-text">
                                <input type="text" class="form-control" id="name" name="hall_ar"
                                    value="<?php echo e(isset($events) ? $events->hall_ar : old('hall_ar')); ?>"
                                    placeholder="Enter Hall Details In Arabic">
                            </div>
                        </div>
                    </div>
                </div>
               
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="Roomname"> <span>Floor :</span></label>
                            <div class="input-field venue-text">
                                <input type="text" class="form-control" id="floor" name="floor"
                                    value="<?php echo e(isset($events) ? $events->floor : old('floor')); ?>"
                                    placeholder="Enter Floor Details In English">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="Roomname"> <span>Floor (ar):</span></label>
                            <div class="input-field venue-text">
                                <input type="text" class="form-control" id="name" name="floor_ar"
                                    value="<?php echo e(isset($events) ? $events->floor_ar : old('floor_ar')); ?>"
                                    placeholder="Enter Floor Details In Arabic">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="lname"> <span>Latitude
                                    :</span></label>
                            <div class="input-field venue-text">
                                <div class=" input-group venue">
                                    <input type="text" class="form-control" id="latitude" name="latitude"
                                        value="<?php echo e(isset($events) ? $events->latitude : old('latitude')); ?>"
                                        style="width:150px;" placeholder="Enter Latitude">
                                </div>
                            </div>
                            <?php if($errors->has('latitude')): ?>
                            <label for="latitude" class="error"> <?php echo e($errors->first('latitude')); ?> </label>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="lname"><span>Longitude:</span></label>
                            <div class="input-field venue-text">
                                <div class=" input-group venue">
                                    <input type="text" class="form-control" id="longitude" name="longitude"
                                        value="<?php echo e(isset($events) ? $events->longitude : old('longitude')); ?>"
                                        placeholder="Enter Longitude" style="width:150px;">
                                </div>
                            </div>
                            <?php if($errors->has('longitude')): ?>
                            <label for="longitude" class="error"> <?php echo e($errors->first('longitude')); ?> </label>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="Roomname"> <span>Street <em>*</em>:</span></label>
                            <div class="input-field venue-text">
                                <input type="text" class="form-control" id="name" name="street"
                                    value="<?php echo e(isset($events) ? $events->street : old('street')); ?>"
                                    placeholder="Enter Street In English">
                                <?php if($errors->has('street')): ?>
                                <label for="street" class="error"> <?php echo e($errors->first('street')); ?> </label>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label " for="Roomname"> <span>Street (ar)<em>*</em>:</span></label>
                            <div class="input-field venue-text">
                                <input type="text" class="form-control" id="name" name="street_ar"
                                    value="<?php echo e(isset($events) ? $events->street_ar : old('street_ar')); ?>"
                                    placeholder="Enter Street In Arabic">
                                <?php if($errors->has('street_ar')): ?>
                                <label for="street_ar" class="error"> <?php echo e($errors->first('street_ar')); ?> </label>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="lname"> <span>City <em>*</em>:</span></label>
                            <div class="input-field venue-text">
                                <div class=" input-group venue">
                                    <select name="city_id" id="city_id" class="form-control select2">
                                        <option value="">Select City</option>

                                        <?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <option value="<?php echo e($values->id); ?>" <?php if(isset($events) && $values->id ==
                                            $events->city_id ): ?> <?php echo e('selected'); ?> <?php endif; ?>
                                            <?php echo e((old("city_id") == $values->id ? "selected":"")); ?>><?php echo e($values->name); ?>

                                        </option>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </select>

                                </div>
                            </div>

                        </div>
                        <?php if($errors->has('city_id')): ?>
                        <label for="city_id" class="error"> <?php echo e($errors->first('city_id')); ?> </label>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="lname"> <span>Country <em>*</em>:</span></label>
                            <div class="input-field venue-text">
                                <div class=" input-group venue">
                                    <select name="country_id" id="country_id" class="form-control select2">
                                        <option value="">Select Country</option>

                                        <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <option value="<?php echo e($value->id); ?>" <?php if(isset($events) && $value->id ==
                                            $events->country_id ): ?> <?php echo e('selected'); ?> <?php endif; ?>
                                            <?php echo e((old("country_id") == $value->id ? "selected":"")); ?>><?php echo e($value->name); ?>

                                        </option>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <?php if($errors->has('country_id')): ?>
                        <label for="country_id" class="error"> <?php echo e($errors->first('country_id')); ?> </label>
                        <?php endif; ?>
                    </div>
                </div>
                

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="control-label " for="pwd">Event Itinerary :</label>
                            <div class="input-field ">
                                <div class="container-image">
                                    <input type="file" id="input-file4" name="itinerary" accept="image/*" hidden />
                                    <label class="btn-upload btn btn-purple" for="input-file4" role="button"> Choose
                                        File </label>
                                </div>
                                <div class="file-upload-titles"> <span>No File Selected</span>
                                    <p>Maximum Size of 2MB, JPG, PNG</p>
                                </div>

                                <div class="form-group" style="max-width: inherit; margin-top: 10px">
                                    <?php if($events->itinerary): ?>
                                    <img src="<?php echo e(asset('images/events/'.$events->floor_plan)); ?>"
                                        style="height: 100px; width: 100px;" id="itinerary">

                                    <?php else: ?>
                                    <img src="<?php echo e(asset('backend/assets/images/imageplace.jpg')); ?>" style="height: 100px; width: 100px;" id="itinerary">

                                    <?php endif; ?>
                                </div>
                                <?php if($errors->has('itinerary')): ?>
                                <label for="itinerary" class="error"> <?php echo e($errors->first('itinerary')); ?> </label>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="control-label" for="pwd">Floor Plan:</label>
                            <div class="input-field ">
                                <div class="container-image">
                                    <input type="file" id="input-file3" name="floor_plan" accept="image/*"
                                        onchange={handleChange()} hidden>
                                    <label class="btn-upload btn btn-purple" for="input-file3" role="button"> Choose
                                        File </label>
                                </div>
                                <div class="file-upload-titles"> <span>No File Selected</span>
                                    <p>Maximum Size of 2MB, JPG, GIF, PNG</p>
                                </div>

                                <div class="form-group" style="max-width: inherit; margin-top: 10px">
                                    <?php if($events->floor_plan): ?>
                                    <img src="<?php echo e(asset('images/events/'.$events->floor_plan)); ?>"
                                        style="height: 100px; width: 100px;" id="floor_plan">
                                    <?php else: ?>
                                    <img src="<?php echo e(asset('backend/assets/images/imageplace.jpg')); ?>" style="height: 100px; width: 100px;" id="floor_plan">
                                    <?php endif; ?>
                                </div>
                                <?php if($errors->has('floor_plan')): ?>
                                <label for="floor_plan" class="error"> <?php echo e($errors->first('floor_plan')); ?> </label>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="control-label " for="pwd">Event Banner <em>*</em>:</label>
                            <div class="input-field ">
                                <div class="container-image">
                                    <input type="file" id="input-file" name="image" accept="image/*"
                                        onchange={handleChange()} hidden />
                                    <label class="btn-upload btn btn-purple" for="input-file" role="button"> Choose
                                        File </label>
                                </div>
                                <div class="file-upload-titles"> <span>No File Selected</span>
                                    <p>Maximum Size of 2MB, JPG, PNG</p>
                                </div>

                                <div class="form-group" style="max-width: inherit; margin-top: 10px">
                                    <?php if($events->image): ?>
                                    <img src="<?php echo e(asset('images/events/'.$events->image)); ?>" id="banner"
                                        style="height: 100px; width: 100px;">
                                    <?php else: ?>
                                    <img src="<?php echo e(asset('backend/assets/images/imageplace.jpg')); ?>" id="banner" style="height: 100px; width: 100px;">
                                    <?php endif; ?>
                                </div>

                                <?php if($errors->has('image')): ?>
                                <label for="image" class="error"> <?php echo e($errors->first('image')); ?> </label>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="control-label " for="pwd">Event logo:</label>
                            <div class="input-field ">
                                <div class="container-image">
                                    <input type="file" id="input-file2" name="icon" accept="image/*"
                                        onchange={handleChange()} hidden>
                                    <label class="btn-upload btn btn-purple" for="input-file2" role="button"> Choose
                                        File </label>
                                </div>
                                <div class="file-upload-titles"> <span>No File Selected</span>
                                    <p>Maximum Size of 2MB, JPG, GIF, PNG</p>
                                </div>

                                <div class="form-group" style="max-width: inherit; margin-top: 10px">
                                    <?php if($events->icon): ?>
                                    <img src="<?php echo e(asset('images/events/'.$events->icon)); ?>"
                                        style="height: 100px; width: 100px;" id="icon">
                                    <?php else: ?>
                                    <img src="<?php echo e(asset('backend/assets/images/imageplace.jpg')); ?>" style="height: 100px; width: 100px;" id="icon">
                                    <?php endif; ?>
                                </div>

                                <?php if($errors->has('icon')): ?>
                                <label for="icon" class="error"> <?php echo e($errors->first('icon')); ?> </label>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div>
                </div>
                <!--    <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="control-label " for="pwd">Event Video Url:</label>
                            <div class="input-field ">
                                <input type="text" class="form-control" id="name" name="video"
                                    placeholder="Enter Youtube url">
                            </div>
                            <div class="file-upload-titles">
                            </div>
                        </div>

                    </div>
                </div> -->
    </div>

    <div class="container-fluid mt-5">
        <div class=" d-flex">
            <div class=" col-sm-4 mt-3 mb-5">
                <?php if(isset($show)): ?>
                <?php if($show): ?>
                <button type="submit" class="btn btn-default">
                    <a href="<?php echo e(route('admin.events.index')); ?>">Back</a>
                </button>
                <?php endif; ?>

                <?php else: ?>
                <button type="submit" class="btn btn-default btn-purple mr-2">Save</button>
                <button type="submit" class="btn btn-default">
                    <a href="<?php echo e(route('admin.events.index')); ?>">Cancel</a>
                </button>
                <?php endif; ?>
            </div>
        </div>
    </div>

</div>
</form>


<div id="mapModal" class="modal">
    <div class="modal-header">
        <!--    <input placeholder="search places" type="text" class="form-control" id="googleSearch" name="googleSearch">-->
        <span class="close btn" data-dismiss="modal" id="modalClose">&times;</span>
    </div>
    <div class="modal-body">
        <div class="map modal-content" id="map">
            <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
            <script defer
                src="https://maps.googleapis.com/maps/api/js?key=<?php echo e(env('GOOGLE_MAPS_API_KEY')); ?>&callback=initMap&libraries=places">
            </script>
        </div>
    </div>

</div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    $(function () {
        $('#seatLimit').hide();
        //show it when the checkbox is clicked
        if ($('input[name="is_seat_limit"]').is(':checked')) {
            $('#seatLimit').show();
        } else {
            $('input[name="is_seat_limit"]').on('click', function () {
                if ($(this).prop('checked')) {
                    $('#seatLimit').fadeIn();
                } else {
                    $('#seatLimit').hide();
                }
            });
        }

    });

    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#banner').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#icon').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#floor_plan').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#itinerary').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }




    $("#input-file").change(function () {
        readURL1(this);
    });
    $("#input-file2").change(function () {
        readURL2(this);
    });

    $("#input-file3").change(function () {
        readURL3(this);
    });
    $("#input-file4").change(function () {
        readURL4(this);
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/events/create.blade.php ENDPATH**/ ?>