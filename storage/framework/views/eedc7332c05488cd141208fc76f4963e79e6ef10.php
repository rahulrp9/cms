<?php if(isset($updating) && $updating): ?>
<?php $__env->startSection('title', 'Update Event Attendees'); ?>
<?php else: ?>
<?php $__env->startSection('title','Add Event Attendees'); ?>
<?php endif; ?>
<?php $__env->startSection('content'); ?>
<div class="edit-event-wrapper">
    <div class="container-fluid mt-3 mb-4">
        <div class="row">
            <div class=" mb-4">
                <h6 class="h6 mb-0 dashboard-title"><?php echo e($events->name); ?> Attendees Form:</h6>

            </div>

        </div>
    </div>
    <?php if(isset($updating)): ?>
    <div class="add-search">
        <a href="<?php echo e(route('admin.events.attendees',$events->id)); ?>" class="btn btn-primary btn-purple"
            title="Add New">Add New</a>
    </div>
    <?php endif; ?>

    <div class="container-fluid edit-events">
        <div class="row">

            <?php if(isset($updating) && $updating): ?>
            <form class="form-horizontal form-datetimepicker col-sm-12"
                action="<?php echo e(route('admin.attendees.update', $attendees->id)); ?>" method="post"
                enctype="multipart/form-data">
                <?php echo e(method_field('PUT')); ?> <?php else: ?>
                <form class="form-horizontal form-datetimepicker col-sm-12"
                    action="<?php echo e(route('admin.attendees.store')); ?>" method="post" enctype="multipart/form-data">
                    <?php endif; ?> <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="event_id" id="event_id" value="<?php echo e($events->id); ?>"
                        data-type="<?php echo e($events->id); ?>">

                    <?php if(isset($updating)): ?>

                    <div class="row show_user_name">
                        <div class="form-group  col-sm-3">
                            <label class="control-label " for="name">First Name*:</label>
                            <div class="input-field">
                                <input type="text" class="form-control autocomplete_txt" maxlength="60" name="name"
                                    id="firstname" data-type="name"
                                    value="<?php echo e(isset($attendees) ? $attendees->username : old('username')); ?>">
                                <?php if($errors->has('name')): ?>
                                <label for="name" class="error">
                                    <?php echo e($errors->first('name')); ?>

                                </label>
                                <?php endif; ?>
                            </div>
                        </div>
                        <input type="hidden" name="user_id" id="user_id" value="<?php echo e($attendees->user_id); ?>">

                        <div class="form-group  col-sm-3">
                            <label class="control-label " for="middle_name">Middle Name:</label>
                            <div class="input-field">
                                <input type="text" class="form-control" maxlength="60" name="middle_name"
                                    id="middle_name"
                                    value="<?php echo e(isset($attendees) ? $attendees->middle_name : old('middle_name')); ?>">
                                <?php if($errors->has('middle_name')): ?>
                                <label for="middle_name" class="error">
                                    <?php echo e($errors->first('middle_name')); ?>

                                </label>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group  col-sm-3">
                            <label class="control-label" for="last_name">Last Name:</label>
                            <div class="input-field">
                                <input type="text" class="form-control" maxlength="60" name="last_name" id="last_name"
                                    value="<?php echo e(isset($attendees) ? $attendees->last_name : old('last_name')); ?>">
                                <?php if($errors->has('last_name')): ?>
                                <label for="last_name" class="error">
                                    <?php echo e($errors->first('last_name')); ?>

                                </label>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group  col-sm-3">
                            <label class="control-label " for="desigination">Designation:</label>
                            <div class="input-field">
                                <input type="text" class="form-control" maxlength="60" id="designation"
                                    name="designation"
                                    value="<?php echo e(isset($attendees) ? $attendees->designation : old('designation')); ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row ">


                        <div class="form-group  col-sm-3">
                            <label class="control-label " for="email">Email *:</label>
                            <div class="input-field">
                                <input type="email" class="form-control" maxlength="60" name="email" id="email"
                                    value="<?php echo e(isset($attendees) ? $attendees->email : old('email')); ?>">
                                <?php if($errors->has('email')): ?>
                                <label for="email" class="error">
                                    <?php echo e($errors->first('email')); ?>

                                </label>
                                <?php endif; ?>
                            </div>
                        </div>


                        <div class="form-group  col-sm-3">
                            <label class="control-label " for="country_id">Nationality *:</label>
                            <div class="input-field">
                                <select name="country_id" class="form-control select2" id="country_id">
                                    <option value="">Select Country</option>
                                    <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($value->id); ?>" data-value="<?php echo e($value->id); ?>"
                                        data-isd="<?php echo e($value->isd_code); ?>" <?php if(isset($attendees) && $value->id ==
                                        $attendees->country_id ): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php echo e($value->name); ?>

                                    </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <?php if($errors->has('country_id')): ?>
                                <label for="country_id" class="error">
                                    <?php echo e($errors->first('country_id')); ?>

                                </label>
                                <?php endif; ?>
                            </div>
                        </div>
                        

        <div class="form-group  col-sm-4">
            <label class="control-label " for="nationality_select">Phone *:</label>
            <div class="input-group-prepend">
                <select class="select2" id="user_isd_code" name="user_isd_code">
                    <option value="">Code</option>
                    <?php if(isset($country)): ?>
                    <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($value->isd_code); ?>" data-isdCode="<?php echo e($value->id); ?>" <?php if(isset($attendees) &&
                        $value->isd_code == $attendees->user_isd_code ): ?> <?php echo e('selected'); ?> <?php endif; ?>>
                        <?php echo e($value->isd_code); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </select>
                <input type="text" class="form-control" maxlength="60" id="phone" name="phone" data-type="phone"
                    value="<?php echo e(isset($attendees) ? $attendees->phone : old('phone')); ?>">
            </div>

            <?php if($errors->has('phone')): ?>
            <label for="phone" class="error">
                <?php echo e($errors->first('phone')); ?>

            </label>
            <?php endif; ?>
        </div>

        <div class="form-group  col-sm-2">
            <label class="control-label " for="rfid">RFID:</label>
            <div class="input-field">
                <input type="text" class="form-control" maxlength="60" name="rfid"
                    value="<?php echo e(isset($attendees) ? $attendees->rfid : old('rfid')); ?>">
                <?php if($errors->has('rfid')): ?>
                <label for="rfid" class="error">
                    <?php echo e($errors->first('rfid')); ?>

                </label>
                <?php endif; ?>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="form-group  col-sm-6 companyname" id="">
            <label class="control-label " for="name">Company Name:</label>
            <div class="input-field">
                <input type="text" class="form-control autocomplete_txt" id="company" maxlength="60" name="company"
                    data-type="comapny" placeholder="Enter Company"
                    value="<?php echo e(isset($attendees) ? $attendees->company : old('company')); ?>">
                <?php if($errors->has('company')): ?>
                <label for="company" class="error">
                    <?php echo e($errors->first('company')); ?>

                </label>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group  col-sm-6">
            <label class="control-label " for="category_select">Category *:</label>
            <div class="input-field">
                <select name="category" id="category" class="form-control">
                    <option value=''>Select category</option>
                    <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($values->id); ?>" <?php if(isset($attendees) && $values->id ==
                        $attendees->attendees_category_id ): ?> <?php echo e('selected'); ?> <?php endif; ?>>
                        <?php echo e($values->name); ?>

                    </option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <?php if($errors->has('category')): ?>
                <label for="category" class="error">
                    <?php echo e($errors->first('category')); ?>

                </label>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-5">
            <label class="control-label" for="input_file">Image (Profile/Logo) :</label>

            <div class="container-image">
                <input type="file" id="input-file" name="image" accept="image/*" onchange={handleChange()} hidden />
                <label class="btn-upload btn btn-purple" for="input-file" role="button"> Choose File
                </label>
            </div>
            <div class="file-upload-titles"> <span>No File Selected</span>
                <p>Maximum Size of 700k, JPG, GIF, PNG</p>

            </div>

        </div>

        <?php if($attendees->image): ?>
        <div class="form-group col-sm-5">
            <label class="control-label" for="pwd">Preview :</label>

            <div class="container-image">
                <div class="preview-img" id="profile_preview">
                    <img id="profile_preview" src="<?php echo e(asset($attendees->image)); ?>" class="img-fluid" alt="preview" />
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>

    <?php else: ?>
    <div class="row show_user_name">
        <div class="form-group  col-sm-3">
            <label class="control-label " for="name">First Name *:</label>
            <div class="input-field">
                <input type="text" class="form-control autocomplete_txt" id="firstname" maxlength="60" name="name"
                    data-type="name" placeholder="Search or Enter Name">
                <?php if($errors->has('name')): ?>
                <label for="name" class="error">
                    <?php echo e($errors->first('name')); ?>

                </label>
                <?php endif; ?>
            </div>
        </div>

        <input type="hidden" name="user_id" id="user_id">

        <div class="form-group  col-sm-3">
            <label class="control-label " for="middle_name">Middle Name:</label>
            <div class="input-field">
                <input type="text" class="form-control" id="middle_name" maxlength="60" name="middle_name"
                    placeholder="Enter Middle Name">
                <?php if($errors->has('middle_name')): ?>
                <label for="middle_name" class="error">
                    <?php echo e($errors->first('middle_name')); ?>

                </label>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group  col-sm-3">
            <label class="control-label" for="last_name">Last Name:</label>
            <div class="input-field">
                <input type="text" class="form-control" id="last_name" maxlength="60" name="last_name"
                    placeholder="Enter Last Name">
                <?php if($errors->has('last_name')): ?>
                <label for="last_name" class="error">
                    <?php echo e($errors->first('last_name')); ?>

                </label>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group  col-sm-3">
            <label class="control-label " for="desigination">Designation:</label>
            <div class="input-field">
                <input type="text" class="form-control" maxlength="60" id="designation" name="designation"
                    placeholder="Enter Designation">
            </div>
        </div>
    </div>

    <div class="row ">


        <div class="form-group  col-sm-3">
            <label class="control-label " for="email">Email *:</label>
            <div class="input-field">
                <input type="text" class="form-control" maxlength="60" id="email" name="email"
                    placeholder="Search By Email">
                <?php if($errors->has('email')): ?>
                <label for="email" class="error">
                    <?php echo e($errors->first('email')); ?>

                </label>
                <?php endif; ?>
            </div>
        </div>


        <div class="form-group  col-sm-3">
            <label class="control-label " for="country_id">Nationality *:</label>
            <div class="input-field">
                <select name="country_id" id="country_id" class="form-control select2 counrtyselect">
                    <option value="">Select Country</option>
                    <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($value->id); ?>" data-value="<?php echo e($value->id); ?>" data-isd="<?php echo e($value->isd_code); ?>">
                        <?php echo e($value->name); ?>

                    </option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <?php if($errors->has('country_id')): ?>
                <label for="country_id" class="error">
                    <?php echo e($errors->first('country_id')); ?>

                </label>
                <?php endif; ?>
            </div>
        </div>
        
<div class="form-group  col-sm-4">
    <label class="control-label " for="nationality_select">Phone *:</label>
    <div class="input-group-prepend">
        <select class="select2" id="user_isd_code" name="user_isd_code">
            <option value="">Code</option>
            <?php if(isset($country)): ?>
            <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($value->isd_code); ?>" data-isdCode="<?php echo e($value->id); ?>">
                <?php echo e($value->isd_code); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
        </select>
        <input type="text" class="form-control" maxlength="60" id="phone" name="phone" data-type="phone">
    </div>

    <?php if($errors->has('phone')): ?>
    <label for="phone" class="error">
        <?php echo e($errors->first('phone')); ?>

    </label>
    <?php endif; ?>
</div>


<div class="form-group  col-sm-2">
    <label class="control-label " for="rfid">RFID:</label>
    <div class="input-field">
        <input type="text" class="form-control" maxlength="60" id="rfid" name="rfid" placeholder="Enter RFID">
        <?php if($errors->has('rfid')): ?>
        <label for="rfid" class="error">
            <?php echo e($errors->first('rfid')); ?>

        </label>
        <?php endif; ?>
    </div>
</div>

</div>

<div class="row">
    <div class="form-group  col-sm-6 companyname" id="">
        <label class="control-label " for="name">Company Name:</label>
        <div class="input-field">
            <input type="text" class="form-control autocomplete_txt" id="company" maxlength="60" name="company"
                data-type="company" placeholder="Enter Company">
            <?php if($errors->has('company')): ?>
            <label for="company" class="error">
                <?php echo e($errors->first('company')); ?>

            </label>
            <?php endif; ?>
        </div>
    </div>
    <div class="form-group  col-sm-6">
        <label class="control-label " for="category_select">Category *:</label>
        <div class="input-field">
            <select name="category" id="category" class="form-control">
                <option value=''>Select category</option>
                <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($values->id); ?>" <?php echo e((old("attendees_category_id") == $values->id ? "selected":"")); ?>>
                    <?php echo e($values->name); ?>

                </option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
            <?php if($errors->has('category')): ?>
            <label for="category" class="error">
                <?php echo e($errors->first('category')); ?>

            </label>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-5">
        <label class="control-label" for="input_file">Image (Profile/Logo) :</label>

        <div class="container-image">
            <input type="file" id="input-file" name="image" accept="image/*" onchange={handleChange()} hidden />
            <label class="btn-upload btn btn-purple" for="input-file" role="button"> Choose File
            </label>
        </div>
        <div class="file-upload-titles"> <span>No File Selected</span>
            <p>Maximum Size of 700k, JPG, PNG</p>

        </div>

    </div>
    <div class="form-group col-sm-5">
        <label class="control-label" for="pwd">Preview :</label>

        <div class="container-image">
            <div class="preview-img" id="profile_preview">

            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<br>
<div class="row">
    <div class="mt-3 mb-5">
        <button id="save_button" name="save_button" type="submit" class="btn btn-purple mr-2"> Save
        </button>
        <a href="<?php echo e(route('admin.events.index')); ?>" class="btn btn-default mr-2"> Back
        </a>
    </div>
</div>
</form>
</div>
<?php if(!isset($updating)): ?>
<div class="container-fluid edit-events">
    <div class="row ">
        <table class="table table-registerd-users ">
            <thead>
                <tr>
                    <th scope="col">Sl.No</th>
                    <th scope="col">Name</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Category</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if($attendee): ?>
                <?php $__currentLoopData = $attendee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <tr>
                    <td><?php echo e($loop->iteration); ?></td>
                    <td>
                        <?php if($values->username != null): ?>
                        <?php echo e($values->username); ?>

                        <?php else: ?>
                        <?php echo e($values->company); ?>

                        <?php endif; ?>
                    </td>
                    <td><?php echo e($values->phone); ?></td>
                    <td><?php echo e($values->category); ?></td>
                    <td class="action-buttons">

                        <a href="<?php echo e(route('admin.attendees.edit',$values->id)); ?>" type="button" class="btn edit"
                            title="Edit">
                            <img src="<?php echo e(asset('backend/assets/images/edit.png')); ?>" alt="preview" />
                        </a>
                        <form action="<?php echo e(route('admin.attendees.destroy', $values->id)); ?>" method="post">
                            <?php echo e(method_field('DELETE')); ?>

                            <?php echo e(csrf_field()); ?>

                            <button type="submit" class="btn trash" title="Delete"
                                onclick="return confirm('Are you sure you want to delete?')">
                                <img src="<?php echo e(asset('backend/assets/images/trash.png')); ?>" alt="preview" />
                            </button>
                        </form>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </tbody>
        </table>
        <ul class="pagination d-flex mt-3 mb-3">
            <?php echo e($attendee->links()); ?>

        </ul>
    </div>
</div>
<?php endif; ?>
</div>

<script type="text/javascript">
    var category = <?php echo json_encode($category->toArray()); ?>;
        var APP_URL = <?php echo json_encode(url('/')); ?>


        $('#input-file').change(function(){           
            let reader = new FileReader();       
            reader.onload = (e) => {        
                $('#preview_img').attr('src', e.target.result); 
            }       
            reader.readAsDataURL(this.files[0]);          
        });



        //autocomplete script
        $(document).on('focus', '.autocomplete_txt', function () {
            type = $(this).data('type');
            event_id = $('#event_id').val();


            if (type == 'name') autoType = 'name';
            if (type == 'phone') autoType = 'phone';
            if (type == 'company') autoType = 'company';

            $(this).autocomplete({
                minLength: 0,
                source: function (request, response) {
                    $.ajax({
                        url: "<?php echo e(route('admin.attendees.autocomplete')); ?>",
                        dataType: "json",
                        data: {
                            term: request.term,
                            type: type,
                            event_id: event_id
                        },
                        success: function (data) {
                            var array = $.map(data, function (item) {
                                return {
                                    label: item[autoType],
                                    value: item[autoType],
                                    data: item
                                }
                            });
                            response(array)
                        }
                    });
                },
                select: function (event, ui) {
                    var data = ui.item.data;
                    $('#user_id').val(data.id);
                    $('#firstname').val(data.name);
                    $('#phone').val(data.phone);
                    $('#middle_name').val(data.middle_name);
                    $('#last_name').val(data.last_name);
                    $('#email').val(data.email);
                    $('#phone').val(data.phone); 
                    $('#designation').val(data.designation); 
                    $('#rfid').val(data.rfid); 
                    $('#company').val(data.company);
                    $('#user_isd_code').val(data.user_isd_code);
                    $('.counrtyselect').val(data.country_id);
                    $('.counrtyselect').select2().trigger('change');                  
                    //$("#country_id option[data-value='" + data.country_id +"']").attr("selected","selected");
                    $("#profile_preview").html('<img src="'+APP_URL+'/'+data.image+'" id="preview_img" class="img-fluid" alt="preview">');                  
                }
            });


        });

        $(function() {
            $('.delete-model').on('click', function(){
                var target_id = $(this).data('id');
                var delete_route = '<?php echo e(route('admin.attendees.destroy')); ?>'+'/'+target_id;
                var delete_form = $('#delete-form');

                delete_form.attr("action", delete_route);
            });

            $(document).on('click', '.modal-dismiss', function (e) {
                var delete_form = $('#delete-form');
                delete_form.removeAttr("action");
            });

        });
    $(document).ready(function() {

        $('#selectall').click(function(event) { //on click
            if(this.checked) { // check select status
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"
                });
            }else{
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"
                });
            }
        });

        /*Remove All*/
        $( "#removeall" ).click(function() {


            var selectedVal = new Array();
                $('input[name="check"]:checked').each(function() {
            selectedVal.push(this.value);
            });


            var values= selectedVal;

            var valC=selectedVal.length;
                var url='<?php echo e(route('admin.attendees.destroy')); ?>';
            if(valC != 0){
            if(confirm('Are you sure you want to delete?')){
                $.ajax({
                type: "DELETE",
                    url: url+'/'+values,

                    success: function(data){

                        location.reload();
                        }

                });
            }
            else{
                //alert("fg");
            }
            }else{
            $('#msg').html("Please select at least one record").show().delay(600000000).hide('slow');
            }
        });
        /*End*/

    });

    $("#country_id").change(function () {
        var value = $(this).find(':selected').data('isd')
        $('#user_isd_code').val(value);
        $('#user_isd_code').select2().trigger('change');
        
    });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/attendees/create.blade.php ENDPATH**/ ?>