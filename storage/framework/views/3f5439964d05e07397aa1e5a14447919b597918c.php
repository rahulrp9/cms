<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script src="https://www.google.com/jsapi"></script>
    <!-- <script>
        window.onload = function() {
            google.charts.load("current", {
                packages: ["corechart"]
            });
            google.charts.setOnLoadCallback(drawChart);
        };    

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Gender', 'Speakers (in millions)'],
                ['Male', 596],
                ['Female', 120]

            ]);

            var options = {
                legend: 'none',
                pieSliceText: 'label',

                pieStartAngle: 100,
                slices: {
                    0: {
                        color: '#821938'
                    },
                    1: {
                        color: '#c34368'
                    }
                }
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);


            google.charts.load('current', {
                packages: ['corechart', 'bar']
            });
            google.charts.setOnLoadCallback(drawColColors);

            function drawColColors() {
                var data = google.visualization.arrayToDataTable([
                    ['Element', 'Density', {
                        role: 'style'
                    }, {
                        role: 'annotation'
                    }],
                    ['24-', 850, '#821938', '850'],
                    ['24-45', 420, '#821938', '420'],
                    ['46-65', 650, '#821938', '650'],
                    ['65+', 80, 'color: #821938', '80']
                ]);

                var view = new google.visualization.DataView(data);
                view.setColumns([0, 1, {
                        calc: "stringify",
                        sourceColumn: 1,
                        type: "string",
                        role: "annotation"
                    },
                    2
                ]);

                var options = {

                    width: 480,
                    height: 400,
                    bar: {
                        groupWidth: "95%"
                    },
                    legend: {
                        position: "none"
                    },
                };

                var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                chart.draw(data, options);
                var chart = new google.visualization.ColumnChart(document.getElementById('chart_div2'));
                chart.draw(data, options);
                var chart = new google.visualization.ColumnChart(document.getElementById('chart_div3'));
                chart.draw(data, options);
            }

        }
    </script> -->

    <!-- <script>
        google.charts.load('current', {
            packages: ['corechart', 'bar']
        });
        google.charts.setOnLoadCallback(drawColColors);

        function drawColColors() {
            var data = google.visualization.arrayToDataTable([
                ['Element', 'Density', {
                    role: 'style'
                }, {
                    role: 'annotation'
                }],
                ['', 25, '#821938', '25'],
                ['', 30, '#821938', '30'],
                ['', 11, '#821938', '11'],
                ['', 25, '#821938', '25'],
                ['', 23, '#821938', '23'],
                ['', 11, '#821938', '11'],
                ['', 51, '#821938', '51'],
                ['', 12, '#821938', '12'],
                ['', 18, '#821938', '18'],
                ['', 52, '#821938', '52'],
                ['', 39, '#821938', '39'],
                ['', 20, '#821938', '20'],
                ['', 29, '#821938', '29'],
                ['', 55, '#821938', '55'],
                ['', 23, '#821938', '23'],
                ['', 48, '#821938', '48']

            ]);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1, {
                    calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"
                },
                2
            ]);

            var options = {

                width: 1100,
                height: 400,
                bar: {
                    groupWidth: "45%"
                },
                legend: {
                    position: "none"
                },
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('chart_divlines'));
            chart.draw(data, options);

        }
    </script> -->
    <style>
        .pdf {
            display: flex;
            justify-content: center;
        }

        * {
            box-sizing: border-box;
        }

        td {
            padding: 0;
        }

        table {
            border-spacing: 0px;
        }

        :focus {
            outline: none;
        }

        svg[Attributes] {
            width: 100% !important;
            height: 100% !important;
        }

        img {
            border: none;
            margin: 0px;
            max-width: 100%;
        }

        @media  print {
            * {
                -webkit-print-color-adjust: exact !important;
                /*Chrome, Safari */
                color-adjust: exact !important;
                /*Firefox*/
            }
        }
    </style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="background: #fff;
font-family: 'Poppins', sans-serif;">
    <section class="page-container">
        <div style="width: 1200px; height: 100%; padding-left: 50px; padding-right: 50px; ">
            <div style="margin-top: 80px; ">
                <form style="display: flex;
                justify-content: space-between;
                align-items: center;">

                </form>
            </div>
            <div style="margin-top: 20px; 
    border-radius: 16px; 
     box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important;
      text-align: center;
        background-color: #fff !important;">
                <div style=" height:350px;
       text-align: center;
        border-top-left-radius: 10px;
         border-top-right-radius: 10px;
          background-color: #8e1b3e !important;"> <img
                        src="http://eventeam.zoondia.org/backend/assets/images/pdf-logo.png" alt="Eventeam"
                        style="padding-top: 140px;  "> </div>
                <div style=" height:350px; text-align: center;  ">
                    <h1 style="color: #1e1e1e;
        font-size: 36px;
        line-height: 35px;
         text-align: center;
     font-weight: 600
     ;padding-top: 85px;
      margin: 0;
       padding-bottom: 15px;">Eventeam Digital Economy<br> Award & Conference</h1>
                    <h2 style="color: #1e1e1e;
        font-size: 22px;
        line-height: 35px;
          text-align: center;
        font-weight: 400;  margin:0px; ">25–28 November 2020<br> Doha Exhibition & Convention Center</h2>
                </div>
            </div>
            <div style="margin-top: 30px;
    border-radius: 16px;
     padding: 110px 0;
      box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important; 
        background-color: #fff !important;">
                <div style="  display: flex;
      justify-content:space-around; padding:0 65px;">
                    <div> <img src="http://eventeam.zoondia.org/backend/assets/images/conference.png" alt="Eventeam"
                            style="border-radius:15px;
                        margin-right: 35px;"> </div>
                    <div>
                        <h1 style="color: #1e1e1e;font-size: 43px;
          line-height: 35px; 
         font-weight: 600;
         padding-top: 40px;
          margin: 0; padding-bottom: 5px;">Digital Economy</h1>
                        <h1 style="color: #8e1b3e;
          font-size: 43px;line-height: 35px; 
                            font-weight: 600;
                             margin: 0;
                              padding-bottom: 15px;"> Conference</h1>
                        <h2 style="color: #242424;font-size: 22px; 
            font-weight: 400;  margin:0px; ">5 Session</h2>
                        <div>
                            <h3 style="color: #a2a2a2;
            font-size: 22px;
                font-weight: 400;
                padding-top: 30px;
                 padding-bottom: 12px;
                  margin: 0;">Sponsors</h3>
                            <div> <img src="http://eventeam.zoondia.org/backend/assets/images/sponsor1.png"
                                    alt="Eventeam" style="
                        margin-right: 15px;"> <img src="http://eventeam.zoondia.org/backend/assets/images/sponsor2.png"
                                    alt="Eventeam"> </div>
                        </div>
                        <div>
                            <h3 style="color: #a2a2a2;font-size: 22px; 
                font-weight: 400;padding-top: 5px;
    margin: 0;
    padding-bottom: 12px; ">Exhibitors</h3>
                            <div> <img src="http://eventeam.zoondia.org/backend/assets/images/sponsor2.png"
                                    alt="Eventeam"> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin-top: 30px;
    border-radius: 16px;
      box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important;
       text-align: center;
       padding-bottom:130px; 
        background-color: #fff !important;">
                <div>
                    <h1 style="color: #000;
        font-size: 35px;
         text-align: center;
     font-weight: 700;
     padding-bottom:50px;
     padding-top: 150px;
      margin: 0;">Total Visitor Attendance</h1>
                    <div style="border-radius: 16px;
         margin: auto;
          width: 958px;
           background-color: #f0f3f6;
            display: flex;
            justify-content: space-between;
             padding:40px 90px 70px 90px ;">
                        <div> <img src="http://eventeam.zoondia.org/backend/assets/images/registered.png"
                                alt="Eventeam">
                            <h4 style="color: #000;font-size: 28px; text-align: center;
                             font-weight: 600;padding-bottom:10px;padding-top: 30px; margin: 0;"> Registered</h4>
                            <h2 style="color: #821938;font-size: 40px; text-align: center;
                             font-weight: 600;padding-bottom:10px; margin: 0;"> 2,278</h2>
                        </div>
                        <div>
                            <h2 style="color: #c0c0c0;font-size: 28px; text-align: center;
                             font-weight: 400;font-style:italic;padding-top:60px;padding-bottom:10px; margin: 0;"> Made
                            </h2>
                        </div>
                        <div> <img src="http://eventeam.zoondia.org/backend/assets/images/visits.png" alt="Eventeam">
                            <h4 style="color: #000;font-size: 28px; text-align: center;
                             font-weight: 600;padding-bottom:10px;padding-top: 30px; margin: 0;"> Visits</h4>
                            <h2 style="color: #821938;font-size: 40px; text-align: center;
                             font-weight: 600;padding-bottom:10px; margin: 0;"> 1,895</h2>
                        </div>
                        <div>
                            <h2 style="color: #c0c0c0;font-size: 28px; text-align: center;
                             font-weight: 400; font-style:italic;padding-bottom:10px;padding-top:60px; margin: 0;"> In
                            </h2>
                        </div>
                        <div> <img src="http://eventeam.zoondia.org/backend/assets/images/days.png" alt="Eventeam">
                            <h4 style="color: #000;font-size: 28px; text-align: center;
                             font-weight: 600;padding-bottom:10px;padding-top: 30px; margin: 0;"> Days</h4>
                            <h2 style="color: #821938;font-size: 40px; text-align: center;
                             font-weight: 600;padding-bottom:10px; margin: 0;"> 3 Days</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div
                style="margin-top: 30px;border-radius: 16px;  box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important; padding-bottom:130px;  background-color: #fff !important;">
                <div>
                    <div style=" display: flex; justify-content: space-around; padding-top: 80px;">
                        <div style=" display: flex;justify-content:space-between;">
                            <div> <img src="http://eventeam.zoondia.org/backend/assets/images/registered.png"
                                    alt="Eventeam"></div>
                            <div style="padding-left: 15px;">
                                <h4 style="color: #000;font-size: 28px; 
                             font-weight: 600;padding-bottom:7px;margin: 0;"> Registered</h4>
                                <p
                                    style="color: #c0c0c0;margin: 0;font-size: 18px; padding-bottom:10px; font-weight: 400;">
                                    Data Source = Visitor + Anonymous Visitor</p>
                                <h2 style="color: #821938;font-size: 28px; 
                             font-weight: 600; margin: 0;"> 2,278 Total</h2>
                            </div>
                        </div>
                        <div style=" display: flex;justify-content:space-between;">
                            <div> <img src="http://eventeam.zoondia.org/backend/assets/images/visits.png"
                                    alt="Eventeam"> </div>
                            <div style="padding-left: 15px;">
                                <h4 style="color: #000;font-size: 28px; 
                             font-weight: 600;padding-bottom:7px; margin: 0;"> Visits</h4>
                                <p
                                    style="color: #c0c0c0;margin: 0;font-size: 18px; padding-bottom:10px; font-weight: 400;">
                                    Data Source = Visitor</p>
                                <h2 style="color: #821938;font-size: 28px; 
                             font-weight: 600; margin: 0;"> 1,895 Total</h2>
                            </div>
                        </div>
                    </div>
                    <div style="display: flex; justify-content: space-between; ">
                        <div>
                            <div id="chart_div"></div>
                        </div>
                        <div>
                            <div id="chart_div2"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div
                style="margin-top: 30px;border-radius: 16px; height: 700px; box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important; margin-bottom:30px;   background-color: #fff !important;">
                <h4 style="color: #000;font-size: 35px; 
                font-weight: 700;padding-bottom:7px; padding-top: 80px; margin: 0; text-align: center;"> Session
                    Attendance Numbers Day 1</h4>
                <div id="chart_divlines"></div>
            </div>
            <div
                style="margin-top: 30px;border-radius: 16px; height: 700px; box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important; margin-bottom:30px;  background-color: #fff !important;">
                <div style="display: flex; justify-content: space-between; padding:  80px 80px  0 80px;;">

                    <div>
                        <h4 style="color: #000;font-size: 35px; 
                        font-weight: 700;padding-bottom:7px; margin: 0;"> Gender</h4>
                        <p
                            style="color: #c0c0c0;margin: 0;font-style: italic;font-size: 18px; padding-bottom:10px; font-weight: 400;">
                            Data Source = Registered</p>

                    </div>
                    <div style="">
                        <h4 style="color: #000;font-size: 35px; 
                        font-weight: 700;padding-bottom:7px; margin: 0;"> Age</h4>
                        <div style="display: flex; justify-content: space-between;">
                            <p
                                style="color: #c0c0c0;margin: 0;font-style: italic; font-size: 18px;margin: 0; padding-bottom:10px; font-weight: 400;">
                                Data Source = Registered</p>
                            <div style="margin-left:30px;">
                                <p style="display: flex;font-style: italic;font-size: 18px; margin: 0;"><span
                                        style="width: 20px; height: 20px; background-color: #821938;display: block;margin-right: 8px;"></span>
                                    Male</p>
                                <p style="display: flex;font-style: italic;font-size: 18px;margin: 0; "><span
                                        style="width: 20px; height: 20px; background-color: #c34368;display: block;margin-right: 8px;"></span>
                                    Female</p>
                            </div>
                        </div>

                    </div>

                </div>
                <div style="display: flex; justify-content: space-between;">
                    <div id="chart_div3"></div>

                    <div id="piechart" style="width:450px; height: 450px;"></div>
                </div>
            </div>

            <div
                style="margin-top: 30px;border-radius: 16px; height: 700px; box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important; margin-bottom:80px; display: flex; align-items: center; justify-content: center;  background-color: #fff !important;">
                <div style="text-align: center;">
                    <h1 style="color: #000;font-size: 35px;text-transform:uppercase;line-height: 35px; 
     font-weight: 700;padding-top: 40px; margin: 0; padding-bottom: 30px; text-align: center;">Thank you for<br>
                        choosing </h1>
                    <img src="http://eventeam.zoondia.org/backend/assets/images/pdf-logo1.png" alt="Eventeam">
                    <div style=" padding-top: 30px;">
                        <div style=" display: flex;     justify-content: center;">
                            <h2 style="color: #242424;font-size: 20px; 
        font-weight: 500;  margin:0px; "> <img src="http://eventeam.zoondia.org/backend/assets/images/location.png"
                                    alt="Eventeam" style="margin-right: 5px;">PO Box 80307, Doha - Qatar</h2>
                            <h2 style=" color: #242424;font-size: 20px; font-weight: 500; margin:0px; "> <img
                                    src="http://eventeam.zoondia.org/backend/assets/images/p-mail.png " alt="Eventeam "
                                    style="margin-right: 5px;margin-left: 10px;">Info@eventeam.qa</h2>
                        </div>
                        <div style=" display: flex;     justify-content: center;">
                            <h2 style="color: #242424;font-size: 20px; 
                       font-weight: 500;  margin:0px; "> <img
                                    src="http://eventeam.zoondia.org/backend/assets/images/p-globe.png" alt="Eventeam"
                                    style="margin-right: 5px;"> www.eventeam.qa</h2>
                            <h2 style=" color: #242424;font-size: 20px; font-weight: 500; margin:0px; "> <img
                                    src="http://eventeam.zoondia.org/backend/assets/images/p-phone.png " alt="Eventeam "
                                    style="margin-right: 5px;margin-left: 10px; ">+974 4417 5055</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        window.onload = function () {

            google.load("visualization", "1.1", {

                packages: ["corechart"],

                callback: 'drawChart1'

            });

            google.load("visualization", "1.1", {

                packages: ["corechart"],

                callback: 'drawChart'

            });
            google.load("visualization", "1.1", {

                packages: ["corechart"],

                callback: 'drawChart2'

            });
            google.load("visualization", "1.1", {

                packages: ["corechart"],

                callback: 'drawChart3'

            });
            google.load("visualization", "46", {

                packages: ["corechart"],

                callback: 'drawChart4'

            });



        };



        function drawChart1() {

            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Pizza');

            data.addColumn('number', 'Populartiy');

            data.addRows([

                ['Laravel', 33],

                ['Codeigniter', 26],

                ['Symfony', 22],

                ['CakePHP', 10],

                ['Slim', 9]

            ]);



            var options = {

                title: 'Popularity of Types of Framework',

                sliceVisibilityThreshold: .2

            };



            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
        }

        function drawChart() {

            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Pizza');

            data.addColumn('number', 'Populartiy');

            data.addRows([

                ['Laravel', 33],

                ['Codeigniter', 26],

                ['Symfony', 22],

                ['CakePHP', 10],

                ['Slim', 9]

            ]);



            var options = {

                title: 'Popularity of Types of Framework',

                sliceVisibilityThreshold: .2

            };



            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }

        function drawChart2() {

            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Pizza');

            data.addColumn('number', 'Populartiy');

            data.addRows([

                ['Laravel', 33],

                ['Codeigniter', 26],

                ['Symfony', 22],

                ['CakePHP', 10],

                ['Slim', 9]

            ]);



            var options = {

                title: 'Popularity of Types of Framework',

                sliceVisibilityThreshold: .2

            };



            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div2'));

            chart.draw(data, options);
        }

        function drawChart3() {

            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Pizza');

            data.addColumn('number', 'Populartiy');

            data.addRows([

                ['Laravel', 33],

                ['Codeigniter', 26],

                ['Symfony', 22],

                ['CakePHP', 10],

                ['Slim', 9]

            ]);



            var options = {

                title: 'Popularity of Types of Framework',

                sliceVisibilityThreshold: .2

            };



            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div3'));

            chart.draw(data, options);
        }

        function drawChart4() {

            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Pizza');

            data.addColumn('number', 'Populartiy');

            data.addRows([

                ['Laravel', 33],

                ['Codeigniter', 26],

                ['Symfony', 22],

                ['CakePHP', 10],

                ['Slim', 9]

            ]);



            var options = {

                title: 'Popularity of Types of Framework',

                sliceVisibilityThreshold: .2

            };



            var chart = new google.visualization.ColumnChart(document.getElementById('chart_divlines'));

            chart.draw(data, options);
        }
    </script>

</body>

</html><?php /**PATH /var/www/html/eventeam/resources/views/event_pdf.blade.php ENDPATH**/ ?>