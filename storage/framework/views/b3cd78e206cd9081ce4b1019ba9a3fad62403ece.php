<?php $__env->startSection('title', 'Dashboard'); ?>
<?php $__env->startSection('content'); ?>
<style type="text/css">
    .searchinputbox {
    width: 44% !important;
}
</style>
<!-- Main content -->
<div class="container-fluid">
    <div class="row">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h6 class="h6 mb-0 dashboard-title"><!-- Registration --></h6>
        </div>
    </div>
</div>
<div class="container-fluid mt-5">
                    <div class="align-items-center justify-content-between mb-5">
                        <h4 class="h4 mb-4">Event Registration :</h4>
                        <form class="d-flex align-items-center form-inline navbar-search">
                            <?php echo e(csrf_field()); ?>

                            
                            <div class="input-group sm-8 searchinputbox" >
                                <input type="text" id="searchName_1" class="autocomplete_txt form-control bg-light border-0 small " 
                                       placeholder="Search By Name, Email Or Phone No" aria-label="Search" aria-describedby="basic-addon2">
                                <!-- <div class="input-group-append">
                                    <button class="btn btn-primary" type="button"> Search </button>
                                </div> -->
                            </div>
                            <!-- <button class="btn btn-primary" type="button"> Submit </button> -->

                        </form>
                    </div>
                </div>

                <div class="edit-event-wrapper">

                    <div class="container-fluid edit-events">
                        <div class="row pt-3">
                            <form class="form-horizontal form-datetimepicker w-100 regform" enctype="multipart/form-data" 
                                  action="<?php echo e(route('admin.registration.newregistration')); ?>" method="post" >
                                <?php echo e(csrf_field()); ?>

                                <input type="hidden" id="eventid" name="eventid" value="0" />
                                <input type="hidden" id="userid" name="userid" value="0" class="hiduserid">
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                <select class="form-control mb-0 w-100 envtid" id="exampleFormControlSelect1" name="exampleFormControlSelect1" >
                                    <option value=0>Select Event</option>
                                    
                                    <?php if(isset($events)): ?>
                                        <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eve): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option  <?php if($selected_id == $eve->id ): ?> selected="true" <?php endif; ?> value="<?php echo e($eve->id); ?>" ><?php echo e($eve->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                    <?php endif; ?>
                                </select>
                                    <?php if($errors->has('eventid')): ?>
                                        <label for="exampleFormControlSelect1" class="error">
                                            <?php echo e($errors->first('eventid')); ?>

                                        </label>
                                    <?php endif; ?>
                            </div>
                                </div>    
                                <div class="row ">


                                    <div class="form-group col-sm-6">
                                        <label class="control-label" for="input_file">Profile Image:</label>

                                        <div class="container-image">
                                            <input type="file" id="input-file" name="input-file" accept="image/*" onchange={handleChange()} hidden />
                                            <label class="btn-upload btn btn-purple" for="input-file" role="button"> Choose File </label>
                                        </div>
                                        <div class="file-upload-titles"> <span>No File Selected</span>
                                            <p>Maximum Size of 700k, JPG, GIF, PNG</p>
                                            <a href="#." class="btn btn-delete">Delete This Image</a> </div>

                                    </div>
                                    <div class="form-group col-sm-5">
                                        <label class="control-label" for="pwd">Preview :</label>

                                        <div class="container-image">
                                            <div class="preview-img">
                                                <img id="image_preview" src="<?php echo e(asset('backend/assets/images/img_avatar.png')); ?>" 
                                                     class="img-fluid" alt="preview" />
                                            </div>
                                        </div>


                                    </div>

                                </div>
                                <div class="row w-100">
                                    <div class="form-group  col-sm-4">
                                        <label class="control-label " for="name">First Name <em>*</em>:</label>
                                        <div class="input-field">
                                            <input type="text" class="form-control" id="name" maxlength="60" name="name">
                                            <?php if($errors->has('name')): ?>
                                                <label for="name" class="error">
                                                    <?php echo e($errors->first('name')); ?>

                                                </label>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <div class="form-group  col-sm-4">
                                        <label class="control-label " for="midd_name">Middle Name :</label>
                                        <div class="input-field">
                                            <input type="text" class="form-control" id="midd_name" maxlength="60" name="midd_name">
                                            <?php if($errors->has('midd_name')): ?>
                                                <label for="midd_name" class="error">
                                                    <?php echo e($errors->first('midd_name')); ?>

                                                </label>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <div class="form-group  col-sm-4">
                                        <label class="control-label" for="last_name">Last Name <em>*</em>:</label>
                                        <div class="input-field">
                                            <input type="text" class="form-control" id="last_name" maxlength="60" name="last_name">
                                            <?php if($errors->has('last_name')): ?>
                                                <label for="last_name" class="error">
                                                    <?php echo e($errors->first('last_name')); ?>

                                                </label>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row w-100">
                                    <div class="form-group  col-sm-4">
                                        <label class="control-label " for="phone">Phone <em>*</em>:</label>
                                        <div class="input-field">
                                            <input type="text" class="form-control" maxlength="60" id="phone" name="phone">
                                            <?php if($errors->has('phone')): ?>
                                                <label for="phone" class="error">
                                                    <?php echo e($errors->first('phone')); ?>

                                                </label>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <div class="form-group  col-sm-4">
                                        <label class="control-label " for="email">Email <em>*</em>:</label>
                                        <div class="input-field">
                                            <input type="text" class="form-control" maxlength="60" id="email" name="email">
                                            <?php if($errors->has('email')): ?>
                                                <label for="email" class="error">
                                                    <?php echo e($errors->first('email')); ?>

                                                </label>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <div class="form-group  col-sm-4">
                                        <label class="control-label " for="nationality_select">Nationality:</label>
                                        <div class="input-field">
                                            <select class="form-control mb-0 w-100 " id="nationality_select" name="nationality_select">
                                                
                                                <option>Select </option>
                                                <?php if(isset($country)): ?>
                                                    <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($value->id); ?>" ><?php echo e($value->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                              </select>
                                            <?php if($errors->has('nationality_select')): ?>
                                                <label for="nationality_select" class="error">
                                                    <?php echo e($errors->first('nationality_select')); ?>

                                                </label>
                                            <?php endif; ?>
                                        </div>
                                    </div>


                                </div>
                                <div class="row w-100">
                                    

                                    <div class="form-group  col-sm-4">
                                        <label class="control-label " for="desigination">Designation:</label>
                                        <div class="input-field">
                                            <input type="text" class="form-control" maxlength="60" id="desigination" name="desigination">
                                        </div>
                                    </div>
                                    <div class="form-group  col-sm-4">
                                        <label class="control-label " for="category_select">Attendees Category <em>*</em>:</label>
                                        <div class="input-field">
                                            <select class="form-control mb-0 w-100 " id="category_select" name="category_select">
                                                
                                                <option value=0 >Select </option>
                                                <?php if(isset($category)): ?>
                                                    <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($value->id); ?>" ><?php echo e($value->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>
                                            <?php if($errors->has('category_select')): ?>
                                                <label for="category_select" class="error">
                                                    <?php echo e($errors->first('category_select')); ?>

                                                </label>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group  col-sm-4">
                                        <label class="control-label " for="gender">Gender:</label>
                                        <div class="input-field">
                                            <select class="form-control mb-0 w-100 " id="gender" name="gender">
                                                <option value="" >Select Gender</option>
                                                <option value="1" >Male </option>
                                                <option value="2" >Female </option>
                                             </select>   
                                            <?php if($errors->has('gender')): ?>
                                                <label for="gender" class="error">
                                                    <?php echo e($errors->first('gender')); ?>

                                                </label>
                                            <?php endif; ?>
                                        </div>
                                    </div>


                                </div>

                                <div class="row w-100">

                                    <div class="form-group  col-sm-4">
                                        <label class="control-label " for="dob">Date Of Birth:</label>
                                        <div class="input-field">
                                            <input type="text" class="form-control" maxlength="60" id="dob" name="dob">  
                                            <?php if($errors->has('dob')): ?>
                                                <label for="dob" class="error">
                                                    <?php echo e($errors->first('dob')); ?>

                                                </label>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                </div>

                                


                                <div class="mt-3 mb-5">
                                    <button id="save_button" name="save_button" type="button" class="btn btn-purple mr-2 clicksubmit"> Register </button>
                                    <button id="cancel_button" name="cancel_button"  type="button" class="btn btn-default mr-2">Clear</button>
                                    <button type="button" class="btn btn-purple tagcheck">Print Tag</button>
                                </div>

                            </form>



                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <h3 class="h3 mb-3 mt-5">Event Registration List :</h3>


                    <table class="table  table-register " id="users_table" >
                        <thead>
                            <tr>
                                <th scope="col">Sl.No</th>
                                <th scope="col"> Name</th>
                                <th scope="col">Phone Number</th>
                                <th scope="col">Category</th>
                                <th scope="col">Print</th>


                            </tr>
                        </thead>
                        <tbody id="table_body">
                            <tr>
                                <td colspan="5" style="text-align: center;" >No data found !!!</td>
                               
                            </tr>
                            

                        </tbody>
                    </table>
                    <div class=" row justify-content-end ">
                        <ul id="pagelinks" class="pagination d-flex mt-3 mb-3">
<!--                            <li class="paginate_button page-item active"><a href="#" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                            <li class="paginate_button page-item "><a href="#" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
                            <li class="paginate_button page-item "><a href="#" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
                            <li class="paginate_button page-item "><a href="#" data-dt-idx="4" tabindex="0" class="page-link">4</a></li>
                            <li class="paginate_button page-item "><a href="#" data-dt-idx="5" tabindex="0" class="page-link">5</a></li>-->
                        </ul>
                    </div>

                </div>
        
<!-- /.content -->
<script type="text/javascript" >
    $(document).ready(function(event){
        $('.tagcheck').hide();
        $('#dob').datepicker();
        event_id = parseInt($('#exampleFormControlSelect1').val());
        $('#eventid').val(event_id);
        
        loadPagination(event_id,1);
        
        $('#pagelinks').on('click','.paginate_button',function(e){
            var page = parseInt($(this).attr('page'));
            loadPagination(event_id,page);
        });
        
        $('#cancel_button').on('click',function(e){
           window.location.reload();
        });
        
        function preView(img)
        {
            if(img.files && img.files[0])
            {
                var reader = new FileReader();
                
                reader.onload = function(e){
                    $('#image_preview').attr('src',e.target.result);
                }
                
                reader.readAsDataURL(img.files[0]);
            }
        }
    
       $('#input-file').change(function(){
           preView(this);
       });
        
       $('.clicksubmit').click(function(){
        var entId = $('.envtid').val();
        if(entId != 0){
              $( ".regform" ).submit();
        }
        else{

            alert("Please Select Event");
            return false;
        }

       });

       $('.envtid').change(function(){

            var uid    = $('.hiduserid').val();
            var envtid = $('.envtid').val();
            if(envtid != 0){
                $.ajax({
                    type: 'POST',
                    url: '<?php echo e(route('admin.checkeventuser')); ?>',
                    datatype: "json",
                    data: {'uid': uid,_token: "<?php echo e(csrf_token()); ?>",envtid:envtid},
                    cache: false,
                    success: function (response) {

                        if(response.type == 2){

                            //$('.clicksubmit').hide();
                            //$('#cancel_button').hide();
                            $('#category_select').val(response.category);
                            $('.tagcheck').show();
                            document.getElementById('save_button').textContent = "Save & countinue";
                           
                        }
                    }
                });
            }    

       });

        
    });
    
    
       
       
    
    event_combo_box = document.getElementById('exampleFormControlSelect1');
    
    event_combo_box.onchange = function(){
        $('#eventid').val($(this).val());
         event_id = parseInt($('#exampleFormControlSelect1').val());
         loadPagination(event_id,1);
    }
    
    $(document).on('focus','.autocomplete_txt', function(){
       
        event_id = parseInt($('#exampleFormControlSelect1').val());
        
        /*if(event_id == 0)
        {
            alert("Select an event");
            $('#exampleFormControlSelect1').focus();
            return; 
        }*/
        
        $(this).autocomplete({
           minLength: 0,
           source: function(request, response){
             $.ajax({
                 url:"<?php echo e(route('admin.registration.autocomplete')); ?>",
                 dataType: "json",
                 data: {
                     term: request.term,
                     event_id:event_id
                 },
                 success: function (data) {
                        var array = $.map(data, function (item) {
                                return {
                                    label: item['name'],
                                    value: item['name'],
                                    data: item
                                }
                            });
                            response(array)
                    }
             });  
           },
           select: function(event, ui){
                var data = ui.item.data;
                    id_arr = $(this).attr('id');
                    id = id_arr.split("_");
                    elementId = id[id.length - 1];
                    $('#searchName_' + elementId).val(data.name);
                    //console.log(data.id);
                    $('.envtid').val(0);
                    loadUserData(data.id,event_id);
                    //$('.tagcheck').show();
           }
        });
    });
    
function loadUserData(userId,event_id)
{
    $('#userid').val(userId);
    
    $.ajax({
        url: "<?php echo e(route('admin.registration.fillform')); ?>",
        type: 'GET',
        data:{
          user_id:userId,
          event_id:event_id  
        },
        success: function (data) {
            fillRegisterForm(data);
          }
    });
}


function fillRegisterForm(data)
{
    $('#name').val(data.user.name);
    $('#midd_name').val(data.user.middle_name);
    $('#last_name').val(data.user.last_name);
    $('#phone').val(data.user.phone);
    $('#email').val(data.user.email);
    $('#gender').val(data.user.gender);
    $('#dob').val(data.user.dob);
    $('#nationality_select').val(data.user.country_id);
    //document.getElementById('save_button').textContent = "Save & countinue";
    if(data.user.image){

        $('#image_preview').attr("src",'/'+data.user.image);
    }
    selectNation(data.selected_country,data.country);
    if(data.inevent == 1){

       $('.tagcheck').show(); 
       //document.getElementById('save_button').textContent = "Update";
    }
    else{
        $('.tagcheck').hide(); 
       //document.getElementById('save_button').textContent = "Save & countinue";
    }
}

function selectNation(countryCode,Country)
{
    cntCode = parseInt(countryCode);
    
    if(cntCode > 0)
    {
        var nation_select = document.getElementById('nationality_select');
        var selectLen = nation_select.length;
        
        for(i = 1 ; i <= selectLen ; i++)
        {
            nation_select.remove('option');
        }
        var optElement = document.createElement('option');
        optElement.value = 0;
        optElement.innerHTML = " Select ";
        nation_select.appendChild(optElement);
        
        Country.forEach(function(val){
            var optElement = document.createElement('option');
            optElement.value = val.id;
            if(cntCode == val.id)
            {
                optElement.selected = true;
            }
            optElement.innerHTML = val.name;
            nation_select.appendChild(optElement);
        });
    }
    
//    
//    document.getElementById('name').disabled = true;
//    document.getElementById('midd_name').disabled = true;
//    document.getElementById('last_name').disabled = true;
//    document.getElementById('phone').disabled =  true;
//    document.getElementById('email').disabled =  true;
//    document.getElementById('nationality_select').disabled = true;
//    
}

function populateValues(userdata)
{
    var current_page = userdata.current_page;
    var last_page_url = userdata.last_page_url;
    var next_page_url = userdata.next_page_url;
    var prev_page_url = userdata.prev_page_url;
    var content_length = userdata.length;
    var last_page = userdata.last_page;
    var total = userdata.total;
    var usercontent = userdata.data;
    
  
   var user_table = document.getElementById('users_table').getElementsByTagName('tbody')[0];
   
   for(i = 0 ; i <= usercontent.length - 1 ; i++ )
    {
            var newRow = user_table.insertRow();

            var newCell1 = newRow.insertCell(0);
            var newText1 = document.createTextNode(i+1);
            newCell1.appendChild(newText1);

            var newCell2 = newRow.insertCell(1);
            var newText2 = document.createTextNode(usercontent[i].username);
            newCell2.appendChild(newText2);

            var newCell3 = newRow.insertCell(2);
            var newText3 = document.createTextNode(usercontent[i].phone);
            newCell3.appendChild(newText3);

            var newCell4 = newRow.insertCell(3);
            var newText4 = document.createTextNode(usercontent[i].name);
            newCell4.appendChild(newText4); 
            
            var newCell5 = newRow.insertCell(4);
            var newLink =  document.createElement('a');
            var newText5 = document.createTextNode("print tag");
            newLink.className = 'btn btn-primary btn-purple';
            newLink.appendChild(newText5);
            if(usercontent[i].ticket_id == 0)
            {
                newLink.href = ""
            }
            else
            {
                newLink.href = ""
            }
            
            newCell5.appendChild(newLink); 
            
    }
    
    
    var page_ul = document.getElementById('pagelinks');
    if(last_page > 8)
    {
        if(last_page - current_page <= 2)
        {
            if(current_page == last_page)
            {
              
                var PAGELIMIT = 8;
            }
            else if (current_page == last_page - 1) {
                var PAGELIMIT = 8;
            }
            else
            {
                var balancepage = last_page - current_page;
                var startPage = current_page - 6;
                var nextPages = current_page + balancepage;
                var PAGELIMIT = nextPages - startPage;
                console.log(last_page);
            }
        }
        else
        {
            var PAGELIMIT = 8;
        }
    }
    
    if(last_page <= 8)
    {
             var PAGELIMIT = last_page;   
    }
   
    if(current_page > 8)
    {
        if(current_page == last_page)
        {
            var page = last_page - 7;
        }
        else if (current_page == last_page - 1) {
            var page = last_page - 7;
        }
        else
        {
            var page = current_page - 5;
        }
    }
    else
    {
        if(current_page == last_page || current_page == last_page -1)
        {
             var page = 1;
        }
        else
        {
             var page = 1;
        }       
    }

        if(current_page != 1)
                {
                    var lielement = document.createElement('li');
                    lielement.className = 'paginate_button page-item';
                    var linkelement = document.createElement('a');
                    linkelement.className = 'page-link';
                    linkelement.setAttribute('data-dt-idx',1);
                    lielement.setAttribute('page',1);
                    linkelement.tabindex = 0;
                    var textNode = document.createTextNode("<<");
                    linkelement.appendChild(textNode);
                    lielement.appendChild(linkelement);
                    page_ul.appendChild(lielement);    
                }
                
                if(current_page != 1)
                {        
                    var lielement = document.createElement('li');
                    lielement.className = 'paginate_button page-item';
                    var linkelement = document.createElement('a');
                    linkelement.className = 'page-link';
                    linkelement.setAttribute('data-dt-idx',current_page -1);
                    lielement.setAttribute('page',current_page-1);
                    linkelement.tabindex = 0;
                    var textNode = document.createTextNode("<");
                    linkelement.appendChild(textNode);
                    lielement.appendChild(linkelement);
                    page_ul.appendChild(lielement);
                }
    for( i = 1 ; i <= PAGELIMIT ; i++)
    {
                

                var lielement = document.createElement('li');
                    if(current_page == page)
                    {
                        lielement.className = 'paginate_button page-item active';
                    }
                    else
                    {
                        lielement.className = 'paginate_button page-item';
                    }
                var linkelement = document.createElement('a');
                linkelement.className = 'page-link';
                linkelement.setAttribute('data-dt-idx',page);
                lielement.setAttribute('page',page);
                linkelement.tabindex = 0;
                var textNode = document.createTextNode(page);
                linkelement.appendChild(textNode);
                lielement.appendChild(linkelement);
                page_ul.appendChild(lielement);
                       
        page = page + 1;                
        
    }
    
            if(last_page >= 8)
            {
                if(current_page + 3 <= last_page)
                {
                    var lielement = document.createElement('li');
                    lielement.className = 'paginate_button page-item';
                    var linkelement = document.createElement('a');
                    linkelement.className = 'page-link';
                    linkelement.setAttribute('data-dt-idx',1);
                    lielement.setAttribute('page',current_page + 1);
                    linkelement.tabindex = 0;
                    var textNode = document.createTextNode(">");
                    linkelement.appendChild(textNode);
                    lielement.appendChild(linkelement);
                    page_ul.appendChild(lielement);    
                  
                  
                    if(current_page + 4 <= last_page)
                    {
                        var lielement = document.createElement('li');
                        lielement.className = 'paginate_button page-item';
                        var linkelement = document.createElement('a');
                        linkelement.className = 'page-link';
                        linkelement.setAttribute('data-dt-idx',page-1);
                        lielement.setAttribute('page',last_page);
                        linkelement.tabindex = 0;
                        var textNode = document.createTextNode(">>");
                        linkelement.appendChild(textNode);
                        lielement.appendChild(linkelement);
                        page_ul.appendChild(lielement);
                    }
                }
            }

}


function loadPagination(event_id,page)
{
     $.ajax({
            url: "<?php echo e(route('admin.registration.pagination')); ?>",
            type: 'GET',
            data: {
                eventid : event_id,
                page:page
            },
            success: function (data) {
                        var dataArray = data.data;
                        $('#table_body').empty();
                        $('#pagelinks').empty();
                        console.log(dataArray.length);
                        if(dataArray.length === 0)
                        {
                            var user_table = document.getElementById('users_table').getElementsByTagName('tbody')[0];
                            var newRow = user_table.insertRow();
                            var newCell1 = newRow.insertCell(0);
                            newCell1.colSpan = 5;
                            var newText1 = document.createTextNode("No content found !!!!");
                            newCell1.appendChild(newText1);
                        }
                        if(dataArray.length > 0)
                        {
                            populateValues(data);
                        }
                    }
        });
}

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/registration.blade.php ENDPATH**/ ?>