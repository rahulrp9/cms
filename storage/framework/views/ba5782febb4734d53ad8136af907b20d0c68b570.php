<?php if(isset($updating) && $updating): ?>
<?php $__env->startSection('title', 'Update Sessions'); ?>
<?php else: ?>
<?php $__env->startSection('title','Add Sessions'); ?>
<?php endif; ?>


<?php $__env->startSection('content'); ?>

<div class="edit-event-wrapper">
    <div class="container-fluid mt-3 mb-4">
        <div class="row">
            <div class=" mb-4">
                <h6 class="h6 mb-0 dashboard-title"><?php echo e($events->name); ?> Sessions Form :</h6>
            </div>
        </div>
    </div>

    <div class="container-fluid edit-events">
        <?php if(isset($updating) && $updating): ?>
        <form class="form-horizontal" action="<?php echo e(route('admin.sessions.update', $sessions->id)); ?>" method="post"
            enctype="multipart/form-data">
            <?php echo e(method_field('PUT')); ?>

            <input type="hidden" id="session_id" name="session_id" value="<?php echo e($sessions->id); ?>">
            <?php else: ?>
            <form class="form-horizontal" action="<?php echo e(route('admin.sessions.store')); ?>" method="post"
                enctype="multipart/form-data">
                <?php endif; ?> <?php echo e(csrf_field()); ?>

                <div class="row">

                    <div class="col-md-6">
                        <div class="contact-form">
                            <input type="hidden" name="event_id" value="<?php echo e($events->id); ?>">
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="fname"><span>Sessions Name
                                        <em>*</em>:</span></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name"
                                        placeholder="Sessions name in english" maxlength="250" name="name"
                                        value="<?php echo e(isset($sessions) ? $sessions->name : old('name')); ?>"
                                        <?php if(@isset($show)): ?> readonly <?php endif; ?>>
                                    <?php if($errors->has('name')): ?>
                                    <label for="name" class="error">
                                        <?php echo e($errors->first('name')); ?>

                                    </label>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="lname"> <span>Venue
                                        <em>*</em>:</span></label>
                                <div class="col-sm-10 venue-text">
                                    <div class=" input-group venue">
                                        <input type="text" class="form-control" id="venue" name="venue"
                                            value="<?php echo e(isset($sessions) ? $sessions->venue : old('venue')); ?>"
                                            style="width:150px;" placeholder="Venu in english">
                                        <span class="input-group-addon"><img
                                                src="<?php echo e(asset('backend/assets/images/globe.png')); ?>" alt="time" /></span>
                                    </div>

                                </div>
                                <?php if($errors->has('venue')): ?>
                                <label for="venue" class="error">
                                    <?php echo e($errors->first('venue')); ?>

                                </label>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="email"><span>Description
                                        <em>*</em>:</span></label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="description" rows="4" name="description"
                                        <?php if(@isset($show)): ?> readonly
                                        <?php endif; ?>><?php echo e(isset($sessions) ? $sessions->description : old('description')); ?></textarea>
                                    <?php if($errors->has('description')): ?>
                                    <label for="description" class="error">
                                        <?php echo e($errors->first('description')); ?>

                                    </label>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group section-select justify-content-between">
                                <label class="control-label col-sm-5" for="comment">Date :</label>
                                <div class="col-sm-9 d-flex align-items-center justify-content-between">
                                    <div class="input-group " data-date-format="mm-dd-yyyy">
                                        <input class="form-control datepicker1" type="text" readonly name="from_date"
                                            value="<?php echo e(isset($sessions) ? $sessions->from_date : old('from_date')); ?>">
                                        

                                    </div>
                                    <?php if($errors->has('from_date')): ?>
                                    <label for="from_date" class="error">
                                        <?php echo e($errors->first('from_date')); ?>

                                    </label>
                                    <?php endif; ?>
                                    <em class="text-uppercase">To</em>
                                    <div class="input-group " data-date-format="mm-dd-yyyy">
                                        <input class="form-control datepicker2" type="text" readonly name="to_date"
                                            value="<?php echo e(isset($sessions) ? $sessions->to_date : old('to_date')); ?>">
                                        

                                    </div>
                                    <?php if($errors->has('to_date')): ?>
                                    <label for="to_date" class="error">
                                        <?php echo e($errors->first('to_date')); ?>

                                    </label>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group section-select">
                                <label class="form-check-label col-sm-4">Type </label>
                                <div class="form-check col-sm-4">
                                    <label class="custom-radio-button">
                                        <input type="radio" value="1" name="type" class="form-check-input"
                                            id="exampleCheck1">
                                        <span class="helping-el"></span> <span class="form-check-label label-text"
                                            for="exampleCheck1"></span>Private</span> </label>
                                </div>
                                <div class="form-check col-sm-4">
                                    <label class="custom-radio-button">
                                        <input type="radio" value="0" name="type" class="form-check-input"
                                            id="exampleCheck2" checked>
                                        <span class="helping-el"></span> <span class="form-check-label label-text"
                                            for="exampleCheck2"></span>Public</span> </label>
                                </div>
                            </div>
                            <?php if($errors->has('type')): ?>
                            <label for="type" class="error">
                                <?php echo e($errors->first('type')); ?>

                            </label>
                            <?php endif; ?>

                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="control-label" for="pwd">Floor Plan:</label>
                                    <div class="input-field ">
                                        <div class="container-image">
                                            <input type="file" id="input-file3" name="floor_plan" accept="image/*"
                                                onchange={handleChange()} hidden>
                                            <label class="btn-upload btn btn-purple" for="input-file3" role="button">
                                                Choose
                                                File </label>
                                        </div>
                                        <div class="file-upload-titles"> <span>No File Selected</span>
                                            <p>Maximum Size of 2MB, JPG, GIF, PNG</p>
                                        </div>

                                        <div class="form-group" style="max-width: inherit; margin-top: 10px">
                                            <?php if(isset($sessions->session_floor_plan)): ?>
                                            <img src="<?php echo e(asset('images/sessions/'.$sessions->session_floor_plan)); ?>"
                                                style="height: 100px; width: 100px;" id="floor_plan">

                                            <?php else: ?>
                                            <img src="<?php echo e(asset('backend/assets/images/imageplace.jpg')); ?>"
                                                style="height: 100px; width: 100px;" id="floor_plan">

                                            <?php endif; ?>
                                        </div>
                                        <?php if($errors->has('floor_plan')): ?>
                                        <label for="floor_plan" class="error"> <?php echo e($errors->first('floor_plan')); ?>

                                        </label>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-6">
                        <div class="contact-form">
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="fname"><span class="arabic-label">Sessions
                                        Name
                                        (ar) <em>*</em>:</span></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name"
                                        placeholder="Sessions name in arabic" maxlength="250" name="name_ar"
                                        value="<?php echo e(isset($sessions) ? $sessions->name_ar : old('name_ar')); ?>">
                                    <?php if($errors->has('name_ar')): ?>
                                    <label for="name_ar" class="error">
                                        <?php echo e($errors->first('name_ar')); ?>

                                    </label>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="lname"><span class="arabic-label">Venue (ar)
                                        <em>*</em>:</span></label>
                                <div class="col-sm-10 venue-text">
                                    <div class=" input-group venue">
                                        <input type="text" class="form-control" id="venue" name="venue_ar"
                                            value="<?php echo e(isset($sessions) ? $sessions->venue_ar : old('venue_ar')); ?>"
                                            placeholder="Venu in arabic">
                                        <span class="input-group-addon"><img
                                                src="<?php echo e(asset('backend/assets/images/globe.png')); ?>" alt="time" /></span>

                                    </div>
                                </div>
                                <?php if($errors->has('venue_ar')): ?>
                                <label for="venue_ar" class="error">
                                    <?php echo e($errors->first('venue_ar')); ?>

                                </label>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5" for="email"><span class="arabic-label">Description
                                    (ar) <em>*</em>:</span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="description" rows="4" name="description_ar"
                                    <?php if(@isset($show)): ?> readonly
                                    <?php endif; ?>><?php echo e(isset($sessions) ? $sessions->description_ar : old('description_ar')); ?></textarea>
                                <?php if($errors->has('description_ar')): ?>
                                <label for="description_ar" class="error">
                                    <?php echo e($errors->first('description_ar')); ?>

                                </label>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group section-select">
                            <label class="control-label col-sm-5" for="comment">Time :</label>
                            <div class="col-sm-9 d-flex align-items-center justify-content-between">
                                <div id="datetimepicker" class="time-picker  input-group date">
                                    <input class="form-control" data-format=" hh:mm:ss" type="time" name="start_time"
                                        value="<?php echo e(isset($sessions) ? $sessions->start_time : old('start_time')); ?>">
                                    <?php if($errors->has('start_time')): ?>
                                    <label for="start_time" class="error">
                                        <?php echo e($errors->first('start_time')); ?>

                                    </label>
                                    <?php endif; ?>
                                </div>
                                <em class="text-uppercase">To</em>
                                <div id="datetimepicker1" class="time-picker  input-group date">
                                    <input class="form-control" data-format=" hh:mm:ss" type="time" name="end_time"
                                        value="<?php echo e(isset($sessions) ? $sessions->end_time : old('end_time')); ?>">
                                    <?php if($errors->has('end_time')): ?>
                                    <label for="end_time" class="error">
                                        <?php echo e($errors->first('end_time')); ?>

                                    </label>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="control-label " for="pwd">Banner Image <em>*</em>:</label>
                                <div class="input-field ">
                                    <div class="container-image">
                                        <input type="file" id="input-file" name="session_image" accept="image/*"
                                            onchange={handleChange()} hidden />
                                        <label class="btn-upload btn btn-purple" for="input-file" role="button"> Choose
                                            File </label>
                                    </div>
                                    <div class="file-upload-titles"> <span>No File Selected</span>
                                        <p>Maximum Size of 2MB, JPG, PNG</p>
                                    </div>

                                    <div class="form-group" style="max-width: inherit; margin-top: 10px">

                                        <?php if(isset($sessions->session_image)): ?>
                                        <img src="<?php echo e(asset('images/sessions/'.$sessions->session_image)); ?>"
                                            style="height: 100px; width: 100px;" id="banner">

                                        <?php else: ?>
                                        <img src="<?php echo e(asset('backend/assets/images/imageplace.jpg')); ?>"
                                            style="height: 100px; width: 100px;" id="banner">

                                        <?php endif; ?>
                                    </div>

                                    <?php if($errors->has('session_image')): ?>
                                    <label for="session_image" class="error"> <?php echo e($errors->first('session_image')); ?>

                                    </label>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
    <div class="container-fluid mt-5">
        <div class=" d-flex">
            <div class=" col-sm-4 mt-3 mb-5">
                <button type="submit" class="btn btn-default btn-purple mr-2">Save</button>
                <button type="submit" class="btn btn-default">
                    <a href="<?php echo e(route('admin.sessions.show',$events->id)); ?>">Cancel</a>
                </button>
            </div>
        </div>
    </div>

    </form>


    <?php $__env->stopSection(); ?>



    <?php $__env->startSection('scripts'); ?>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#banner').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#floor_plan').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
        $("#input-file").change(function () {
            readURL(this);
        });

        $("#input-file3").change(function () {
            readURL3(this);
        });
    </script>

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/sessions/create.blade.php ENDPATH**/ ?>