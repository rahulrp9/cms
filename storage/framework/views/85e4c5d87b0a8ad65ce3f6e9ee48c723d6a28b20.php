<?php $__env->startSection('content'); ?>
<style type="text/css">
    .box .row{
         margin-bottom: 15px;
    }
</style>
<?php if(session()->has('message')): ?>
    <div class="alert alert-success">
        <?php echo e(session()->get('message')); ?>

    </div>
<?php endif; ?>
    <div class="row">
    <div class="col-md-12">
            <div class="col-md-6">
                <h1 class="box-title">Order Details  </h1>
            </div>    
    <div class="pull-right box-tools">
                    <a href="<?php echo e(URL::to('dashboard/orders')); ?>">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-default" type="button" data-original-title="Service List">
                            <i class="fa fa-list"></i>
                        </button>
                    </a>               
                </div>
    </div>            
    </div>
        <div class="box">
            <div class="box-body pad" id="tab_1">
                <div class="row">
                    <label class="col-sm-3 control-label">Order Id</label>
                    <div class="col-sm-6">
                        <?php echo e($order->id); ?>

                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">User Name</label>
                    <div class="col-sm-3">
                        <?php echo e($order->name); ?>

                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Delivery Type</label>
                    <div class="col-sm-3">
                        <?php echo e($order->delivery_type); ?>

                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Order Amount</label>
                    <div class="col-sm-3">
                        <?php echo e($order->amount); ?>

                    </div>
                </div>
                <form id="formSpace" action="<?php echo e(URL::to('dashboard/orders')); ?>/<?php echo e($order->id); ?>" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <?php echo csrf_field(); ?>

                    <?php if(isset($order)): ?>
                        <?php echo e(method_field('PUT')); ?>

                        <input type="hidden" name="service_id" value="<?php echo e($order->id); ?>"/>
                    <?php endif; ?>
                <div class="row">
                    <label class="col-sm-3 control-label">Order Status</label>
                    <div class="col-sm-3">
                        <select name="statusselect" class="statusselect">
                            
                            <option value="1" <?php if($order->ostatus == 1): ?> selected <?php endif; ?>>Not Processed</option>
                            <option value="2" <?php if($order->ostatus == 2): ?> selected <?php endif; ?>>Processed</option>
                            <option value="3" <?php if($order->ostatus == 3): ?> selected <?php endif; ?>>Complete</option>
                        </select>
                    </div>
                </div>
            </form>
              
        </div>
    </div>

<script type="text/javascript">
    
    $('.statusselect').change(function(){

       var statval = $(this).val();
       $('#formSpace').submit();
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/laraveltest/resources/views//orders/more.blade.php ENDPATH**/ ?>