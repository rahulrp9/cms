   
<?php $__env->startSection('title', 'Registered Users List'); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid mt-5">
    <div class="row  align-items-center justify-content-between mb-5">
        <h6 class="h6 mb-0 dashboard-title">Registered Users List:</h6>
        <div class="add-search">
            <form class="d-sm-inline-block form-inline navbar-search" action="<?php echo e(route('admin.registerdusers.search')); ?>" method="post"
                enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search By Name"
                        aria-label="Search" aria-describedby="basic-addon2" name="search">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit"> Search </button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>
<div class="container-fluid table-register">
    <div class="row ">
        <table class="table table-registerd-users ">
            <thead>
                <tr>
                    <th scope="col">Sl.No</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Phone Number</th>
                    <th scope="col">Email Address</th>
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($users[0])): ?> <?php $i=1; ?>
                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($i); ?></td>
                    <td><?php echo e($values->name); ?> <?php echo e($values->middle_name); ?> <?php echo e($values->last_name); ?></td>
                    <td><?php echo e($values->phone); ?></td>
                    <td><?php echo e($values->email); ?></td>
                </tr>
                <?php $i++; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </tbody>
        </table>
        <ul class="pagination d-flex mt-3 mb-3">
            <?php echo e($users->links()); ?>

        </ul>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/registeruser/list.blade.php ENDPATH**/ ?>