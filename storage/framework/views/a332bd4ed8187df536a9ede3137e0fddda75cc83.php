<?php if(isset($updating) && $updating): ?>
<?php $__env->startSection('title', 'Update App Content'); ?>
<?php else: ?>
<?php $__env->startSection('title','Add App Content'); ?>
<?php endif; ?>
<?php $__env->startSection('content'); ?>
<div class="edit-event-wrapper">
    <div class="container-fluid mt-5">
        <div class="row">
            <div class=" mb-4">
                <h6 class="h6 mb-0 dashboard-title">App Content Form:</h6> 
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form action="<?php echo e(route('admin.pageContents.store')); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="row">
                <div class="col-sm-6">
                    <input type="hidden" name="type" value="<?php echo e($type); ?>">
                    <div class="form-group">
                        <label>Title <em>*</em></label>
                        <input type="text" class="form-control" placeholder="Title" maxlength="60" name="title"
                            value="<?php echo e(isset($pageContents) ? $pageContents->title : old('title')); ?>">
                        <?php if($errors->has('title')): ?>
                        <label for="title" class="error">
                            <?php echo e($errors->first('title')); ?>

                        </label>
                        <?php endif; ?>
                    </div>


                    <div class="form-group">

                        <label>Contents <em>*</em></label>
                        
                        <textarea class="form-control editor1" rows="9"
                            name="content" id="editor1"><?php echo e($pageContents->content ?? old('content')); ?></textarea>
                        <?php if($errors->has('content')): ?>
                        <label for="content" class="error">
                            <?php echo e($errors->first('content')); ?>

                        </label>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Title (ar) <em>*</em></label>
                        <input type="text" class="form-control" placeholder="Title in arabic" maxlength="60"
                            name="title_ar"
                            value="<?php echo e(isset($pageContents) ? $pageContents->title_ar : old('title_ar')); ?>">
                        <?php if($errors->has('title_ar')): ?>
                        <label for="title_ar" class="error">
                            <?php echo e($errors->first('title_ar')); ?>

                        </label>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">

                        <label>Contents (ar) <em>*</em></label>
                        
                        <textarea class="form-control editor2" rows="9"
                            name="content_ar" id="editor2"><?php echo e($pageContents->content_ar ?? old('content_ar')); ?></textarea>
                        <?php if($errors->has('content_ar')): ?>
                        <label for="content_ar" class="error">
                            <?php echo e($errors->first('content_ar')); ?>

                        </label>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-end mb-3">
                <button type="submit" class="btn btn-purple mr-2">Save</button>
                
            </div>
        </form>



    </div>

</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/pageContents/create.blade.php ENDPATH**/ ?>