<?php $__env->startSection('content'); ?>
<link rel="stylesheet" href="<?php echo e(URL::asset('css/custom_style2.css')); ?>">
<div class="contentHolderV1">
    <h2>Service Types</h2>
    <div class="col-md-12">
        <div class="col-md-6"> 
             <a href="<?php echo e(URL::to('services/create')); ?>" class="searchbtn">Add Service</a>
        </div>
        <div class="col-md-6"> 
            <a href="<?php echo e(URL::to('deleted/services')); ?>" class="searchbtn pull-right">Deleted Services</a>
        </div>
    </div>    
   
    <div class="tableHolder">
        <table style="width: 100%;" class="tableV1" cellpadding="0" cellspacing="0">
            <thead class="tableHeader">
                <tr>
                    <td>#</td>
                    <td>Service Type</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
               
               <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <?php  //$n++; ?>

                    <tr>
                        <td><?php echo e($loop->iteration); ?></td>
                        <td><a href="<?php echo e(URL::to('services', ['id' => ($service->id)])); ?>" class="view"><?php echo e($service->service_name_en); ?></a></td>
                        <td><div  class="actions">
                                    <a href="<?php echo e(URL::to('services/'.$service->id.'/edit')); ?>" class="edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="<?php echo e(URL::to('services', ['id' => ($service->id)])); ?>" class="view">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                     <?php echo e(Form::open(['method' => 'DELETE','style'=>"display: inline-block", 'route' => ['services.destroy', $service->id]])); ?>

                                        <?php echo csrf_field(); ?>

                                        <a class="delete" data-id=""><i class="fa fa-trash-alt"></i></a>
                                           
                                        <?php echo e(Form::close()); ?>

                                    
                                </div></td>
                       
                    </tr>
                    
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
<ul class="pagination">
        <?php echo $services->render(); ?>

    </ul>
</div>
<script type="text/javascript">
    
    $(function () {
        $('.delete').click(function(e){
            e.preventDefault();
            if(confirm('Are you sure you want to delete?'))
            {
                $(this).parent('form').submit();
            }else{
                return false;
            }
        });
});

    
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/laraveltest/resources/views/shops/service.blade.php ENDPATH**/ ?>