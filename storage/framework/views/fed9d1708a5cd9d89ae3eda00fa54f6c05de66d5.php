Hi <?php echo e($name); ?>,
<br>
<br>
  <p> We have sent you this email in response to your request to reset your password.</p>

 <p> Please login with the following password. You can change the password from My Profile.</p>
  <p>Password : <?php echo e($password); ?> </p>
<br><br>
Thanks,<br>
<?php /**PATH /var/www/html/andrixFoodApp/resources/views/emailtemplates/forgot_password_mail.blade.php ENDPATH**/ ?>