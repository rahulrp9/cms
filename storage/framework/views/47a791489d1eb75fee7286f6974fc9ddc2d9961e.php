<?php $__env->startSection('content'); ?>
<style type="text/css">
    .box .row{
         margin-bottom: 15px;
    }
</style>
    <div class="row">
    <div class="col-md-12">
            <div class="col-md-6">
                <h1 class="box-title">Product Details  </h1>
            </div>    
    <div class="pull-right box-tools">
                    <a href="<?php echo e(URL::to('dashboard/products')); ?>">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-default" type="button" data-original-title="Service List">
                            <i class="fa fa-list"></i>
                        </button>
                    </a>
     <?php if(isset($product)): ?>
                    <a href="<?php echo e(URL::to('dashboard/products/create')); ?>">
                        <button title="" data-toggle="tooltip" class="btn btn-default" type="button" data-original-title="Add Offer">
                            <i class="fa fa-plus"></i>
                        </button>
                    </a>
   
                    <a href="<?php echo e(URL::to('dashboard/products/'.$product->id.'/edit')); ?>">
                        <button title="" data-toggle="tooltip" class="btn btn-default" type="button" data-original-title="Edit Offer">
                            <i class="fa fa-edit"></i>
                        </button>
                    </a>
    <?php endif; ?>               
                </div>
    </div>            
    </div>
        <div class="box">
            <div class="box-body pad" id="tab_1">
                <div class="row">
                    <label class="col-sm-3 control-label">Product Name</label>
                    <div class="col-sm-6">
                        <?php echo e($product->name); ?>

                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Product Price</label>
                    <div class="col-sm-3">
                        <?php echo e($product->price); ?>%
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Product Category</label>
                    <div class="col-sm-3">
                        <?php echo e($product->category); ?>

                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Veg or Non Veg</label>
                    <div class="col-sm-3">
                        <?php echo e($product->isveg); ?>

                    </div>
                </div>
                <?php if(count($pcutomises)): ?>
                        <?php $__currentLoopData = $pcutomises; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pcutomise): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="row">
                        <label for="value" class="col-sm-3 control-label required">Customizable Product options</label>
                        <div class="col-sm-9">
                            <div class="">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="size[]" id="value" value='<?php if(isset($pcutomise)): ?><?php echo e($pcutomise->name); ?><?php endif; ?>' class="form-control service_eng"  placeholder="Enter Product Size" style="float: left;width: 30%" readonly="">
                                <input type="text" name="sizeval[]" id="value" value='<?php if(isset($pcutomise)): ?><?php echo e($pcutomise->price); ?><?php endif; ?>' class="form-control service_eng"  placeholder="Enter Product Value" style="float: left;width: 30%;margin-left: 10px;" readonly="">
                                <?php if($errors->has('value')): ?>value
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('value')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div> 
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>  
                <div class="row">
                    <label class="col-sm-3 control-label">Product Image</label>
                    <div class="col-sm-3">
                        <?php if(!empty($product->image)): ?>
                                    <a href="javascript:void(0)" class="pop" imgsrc="<?php echo e(asset($product->space_image)); ?>"><img src=" <?php echo e(asset($product->image)); ?>" class="img-responsive img-rounded" width="150" height="1150" /></a>
                        <?php else: ?>
                            <img src="<?php echo e(asset('assets/images/no-image.png')); ?>" />
                        <?php endif; ?>
                    </div>
                </div>
              
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/laraveltest/resources/views//products/more.blade.php ENDPATH**/ ?>