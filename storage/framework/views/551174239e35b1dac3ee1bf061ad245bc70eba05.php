<?php $__env->startSection('title', 'Dashboard'); ?>
<?php $__env->startSection('content'); ?>
<style type="text/css">
    table td:nth-of-type(1) {
        text-align: left;
    }
</style>
<!-- Main content -->
<div class="container-fluid">
    <div class="row">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h6 class="h6 mb-0 dashboard-title">Dashboard</h6>
        </div>
    </div>
</div>
<div class="container-fluid top-events">
    <div class="row">
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col"> <img src="<?php echo e(asset('backend/assets/images/users.png')); ?>" class="img-fill"
                                alt="users" />
                            <div class="col mr-2 d-flex align-items-center">
                                <div class="h5 mb-0  ">Registered <br>Users</div>
                                <div class="text-xs  text-primary text-uppercase mb-1"><?php echo e(isset($users) ? $users : 0); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col"> <img src="<?php echo e(asset('backend/assets/images/events.png')); ?>" class="img-fill"
                                alt="users" />
                            <div class="col mr-2 d-flex align-items-center">
                                <div class="h5 mb-0">Total Events</div>
                                <div class="text-xs  text-purple text-uppercase mb-1">
                                    <?php echo e(isset($total_events) ? $total_events : 0); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col"> <img src="<?php echo e(asset('backend/assets/images/sections.png')); ?>" class="img-fill"
                                alt="users" />
                            <div class="col mr-2 d-flex align-items-center">
                                <div class="h5 mb-0">Upcoming Events</div>
                                <div class="text-xs  text-info text-uppercase mb-1">
                                    <?php echo e(isset($upcoming_events) ? $upcoming_events : 0); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col"> <img src="<?php echo e(asset('backend/assets/images/sessions.png')); ?>" class="img-fill"
                                alt="users" />
                            <div class="col mr-2 d-flex align-items-center">
                                <div class="h5 mb-0">Upcoming Sessions</div>
                                <div class="text-xs  text-info text-uppercase mb-1">
                                    <?php echo e(isset($upcoming_sessions) ? $upcoming_sessions : 0); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row ">

    <div class=" col-lg-6 mb-4 mt-2 ">
        <h4 class="h4 mb-0 ">Upcoming Events</h4>
    </div>
</div>
<div class="container-fluid table-register">
    <table class="table table-registerd-users  create-list">
        <thead>
            <tr>
                <th scope="col">Sl.No</th>
                <th scope="col">Event Name</th>
                <th scope="col">Date</th>
                <th scope="col">Status</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>

            <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td scope="row"><?php echo e($key+1); ?></td>
                <td><?php echo e($values->name); ?></td>
                <td><?php echo e(date('d-m-Y', strtotime($values->from_date))); ?></td>
                <td>
                    <a href="<?php echo e(route('admin.events.status',$values->id)); ?>">
                        <?php if($values->status): ?>
                        <button type="button" class="btn btn-default btn-purple" title="Active">Active</button>
                        <?php else: ?>
                        <button type="button" class="btn btn-default " title="Inactive">Inactive</button>
                        <?php endif; ?>
                    </a>
                </td>
                <td class="action-buttons">
                    <a href="<?php echo e(route('admin.events.show',$values->id)); ?>" class="btn preview">
                        <img src="<?php echo e(asset('backend/assets/images/preview.png')); ?>" alt="preview" />
                    </a>
                </td>
            </tr>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>

</div>
<!-- /.content -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/home.blade.php ENDPATH**/ ?>