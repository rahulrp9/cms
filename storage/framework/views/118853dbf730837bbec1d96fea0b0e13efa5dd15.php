<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Eventeam | <?php echo $__env->yieldContent('title'); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
    <link rel="icon" href="<?php echo e(asset('backend/assets/images/favicon.png')); ?>" type="image/png" />
    <link href="<?php echo e(asset('backend/assets/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('backend/assets/css/styles.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.9/datepicker.min.css" rel="stylesheet"
        type="text/css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo e(asset('backend/assets/css/select2.min.css')); ?>">

    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
    <script src="<?php echo e(asset('backend/assets/js/jquery-3.3.1.min.js')); ?>" type="text/javascript"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <!-- <script src="https://cdn.ckeditor.com/ckeditor5/18.0.0/classic/ckeditor.js"></script> -->
    <script src="https://cdn.ckeditor.com/4.14.1/full-all/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/css/bootstrap-colorpicker.min.css"
        rel="stylesheet">


    <?php echo $__env->yieldContent('styles'); ?>
    <style type="text/css">
        label.error {
            color: #C10000;
            font-size: 0.9em;
            margin-top: -5px;
            padding: 0;
        }

        .has-error.select2-drop-active {
            border-color: #953b39;
        }

        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }

        .hidden {
            display: none;
        }

        #playerModal {
            z-index: 9999 !important;
        }

        .modal-body {
            overflow-y: scroll;
        }

        #playerModalEdit {
            z-index: 9999 !important;
        }

        .auto-input {
            z-index: 1000 !important;
        }

        .ui-autocomplete-input {
            z-index: 5000;
        }

        .ui-widget-content {
            z-index: 9999;
        }

        .hide {
            display: none;
        }

        .refresh {
            font-size:24px;
            text-decoration: none;
            background-color: #EEEEEE;
            color: #333333;
            padding: 2px 6px 2px 6px;
            border-top: 1px solid #CCCCCC;
            border-right: 1px solid #333333;
            border-bottom: 1px solid #333333;
            border-left: 1px solid #CCCCCC;
        }
    </style>
</head>

<body>
    <div id="wrapper" class="wrapper">

        <nav class="navbar  sidebar-left">
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion">
                <a class="sidebar-brand d-flex align-items-center justify-content-center"
                    href="<?php echo e(route("admin.index")); ?>">
                    <img src="<?php echo e(asset('backend/assets/images/logo-d.png')); ?>" alt="logo" /> </a>
                <div class="sidebar-heading">
                    <a href="<?php echo e(route("admin.registration")); ?>"> <span style="color: #fff;"> Registration</span></a>
                </div>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('dashboard')): ?>
                <li class="nav-item ">
                    <a class="nav-link user-event" href="<?php echo e(route("admin.index")); ?>"> <span> Dashboard</span>
                    </a>
                </li>
                <?php endif; ?>

                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('users')): ?>
                <li
                    class="nav-item dropdown <?php echo e(request()->is('dashboard/eventcategory') || request()->is('dashboard/eventcategory/*') ? 'active' : ''); ?>">
                    <a class="nav-link create-event dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <span>Users</span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('registerd_users')): ?>
                        <li class="nav-item">
                            <a class="nav-link " href="<?php echo e(route("admin.registerdusers.index")); ?>">
                                <span>Registerd users</span></a>
                        </li>
                        <?php endif; ?>

                        <!-- <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('non_registerd_users')): ?>
                        <li class="nav-item">
                            <a class="nav-link " href="#">
                                <span>Non Registerd users</span></a></li>
                                <?php endif; ?> -->
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('category')): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo e(route("admin.category.index")); ?>"> <span>
                                    User Categories</span> </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </li>
                <?php endif; ?>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('users')): ?>
                <li class="nav-item dropdown ">
                    <a class="nav-link create-event dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <span>Events</span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('events')): ?>
                        <li
                            class="nav-item  <?php echo e(request()->is('dashboard/events') || request()->is('dashboard/events/*') ? 'active' : ''); ?>">
                            <a class="nav-link" href="<?php echo e(route("admin.events.index")); ?>"> <span> Events </span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('eventcategory')): ?>
                        <li
                            class="nav-item  <?php echo e(request()->is('dashboard/eventcategory') || request()->is('dashboard/eventcategory/*') ? 'active' : ''); ?>">
                            <a class="nav-link" href="<?php echo e(route("admin.eventcategory.index")); ?>"> <span>
                                    Event Categories</span> </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </li>
                <?php endif; ?>

                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('archived_events')): ?>
                <li class="nav-item ">
                    <a class="nav-link user-event" href="#"> <span> Archived Events</span>
                    </a>
                </li>
                <?php endif; ?>

                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('scanning_app_users')): ?>
                <li
                    class="nav-item <?php echo e(request()->is('dashboard/scanningUsers') || request()->is('dashboard/scanningUsers/*') ? 'active' : ''); ?>">
                    <a class="nav-link user-event" href="#"> <span>Scanning App Users</span></a>
                    <!-- <?php echo e(route("admin.scanningUsers.index")); ?> -->
                </li>
                <?php endif; ?>

                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('app_contents')): ?>
                <li
                    class="nav-item dropdown <?php echo e(request()->is('dashboard/contactus') || request()->is('dashboard/contactus/*') ||
                        request()->is('dashboard/pageContents/aboutus') || request()->is('dashboard/pageContents/aboutus/*') ||
                        request()->is('dashboard/pageContents/termsconditions') || request()->is('dashboard/pageContents/termsconditions/*') ||
                        request()->is('dashboard/pageContents/privacypolicy') || request()->is('dashboard/pageContents/privacypolicy/*') ? 'active' : ''); ?>">

                    <a class="nav-link create-event dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <span>Mobile App Contents </span>
                    </a>

                    <ul class="dropdown-menu">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('contact_us')): ?>
                        <li
                            class="nav-item <?php echo e(request()->is('dashboard/contactus') || request()->is('dashboard/contactus/*') ? 'active' : ''); ?>">
                            <a class="nav-link " href="<?php echo e(route('admin.pageContents.contactUs')); ?>"><span>Contact
                                    Us</span></a>
                        </li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('about_us')): ?>
                        <li
                            class="nav-item <?php echo e(request()->is('dashboard/pageContents/aboutus') || request()->is('dashboard/pageContents/aboutus/*') ? 'active' : ''); ?>">
                            <a class="nav-link " href="<?php echo e(route('admin.pageContents.aboutus')); ?>"><span>About
                                    Us</span></a>
                        </li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('privacy_policy')): ?>
                        <li
                            class="nav-item <?php echo e(request()->is('dashboard/pageContents/privacypolicy') || request()->is('dashboard/pageContents/privacypolicy/*') ? 'active' : ''); ?>">
                            <a class="nav-link " href="<?php echo e(route('admin.pageContents.privacypolicy')); ?>"><span>Privacy
                                    Policy</span></a></li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('terms_and_conditions')): ?>
                        <li
                            class="nav-item <?php echo e(request()->is('dashboard/pageContents/termsconditions') || request()->is('dashboard/pageContents/termsconditions/*') ? 'active' : ''); ?>">
                            <a class="nav-link " href="<?php echo e(route('admin.pageContents.termsconditions')); ?>"><span>Terms &
                                    Conditions</span></a></li>
                        <?php endif; ?>
                    </ul>
                </li>
                <?php endif; ?>

                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('settings')): ?>
                <li
                    class="nav-item dropdown <?php echo e(request()->is('dashboard/country') || request()->is('dashboard/country/*') ||
                        request()->is('dashboard/city')|| request()->is('dashboard/settings') || request()->is('dashboard/settings/*') || request()->is('dashboard/city/*') ? 'active' : ''); ?>">
                    <a class="nav-link create-event dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <span>Settings </span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('settings')): ?>
                        <li
                            class="nav-item <?php echo e(request()->is('dashboard/settings') || request()->is('dashboard/settings/*') ? 'active' : ''); ?>">
                            <a class="nav-link " href="<?php echo e(route("admin.settings.create")); ?>"><span>Application
                                    Settings</span></a></li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('countries')): ?>
                        <li
                            class="nav-item <?php echo e(request()->is('dashboard/country') || request()->is('dashboard/country/*') ? 'active' : ''); ?>">
                            <a class="nav-link " href="<?php echo e(route("admin.country.index")); ?>"><span>Countries</span></a>
                        </li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('cities')): ?>
                        <li
                            class="nav-item <?php echo e(request()->is('dashboard/city') || request()->is('dashboard/city/*') ? 'active' : ''); ?>">
                            <a class="nav-link " href="<?php echo e(route("admin.city.index")); ?>"><span>Cities</span></a></li>
                        <?php endif; ?>



                    </ul>
                </li>
                <?php endif; ?>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('sent_notifications')): ?>
                <li class="nav-item">
                    <a class="nav-link user-event" href="#"><span>Sent Notifications</span></a></li>
                <?php endif; ?>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('users_manage')): ?>
                <li class="nav-item dropdown  <?php echo e(request()->is('dashboard/permissions') || request()->is('dashboard/permissions/*') ||
                    request()->is('dashboard/roles') || request()->is('dashboard/roles/*') ||
                    request()->is('dashboard/users') || request()->is('dashboard/users/*') ? 'active' : ''); ?>">
                    <a class="nav-link create-event dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <span>Admin & Permission </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li
                            class="nav-item <?php echo e(request()->is('dashboard/permissions') || request()->is('dashboard/permissions/*') ? 'active' : ''); ?>">
                            <a class="nav-link "
                                href="<?php echo e(route("admin.permissions.index")); ?>"><span>Permissions</span></a>
                        </li>
                        <li
                            class="nav-item <?php echo e(request()->is('dashboard/roles') || request()->is('dashboard/roles/*') ? 'active' : ''); ?>">
                            <a class="nav-link " href="<?php echo e(route("admin.roles.index")); ?>"><span>Role</span></a></li>
                        <li
                            class="nav-item <?php echo e(request()->is('dashboard/users') || request()->is('dashboard/users/*') ? 'active' : ''); ?>">
                            <a class="nav-link " href="<?php echo e(route("admin.users.index")); ?>"><span>Users</span></a></li>
                    </ul>
                </li>
                <?php endif; ?>

            </ul>
        </nav>
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                    <h3 class="top-title"><span></span> </h3>
                    <ul class="navbar-nav ml-auto">
                        
                        <div class="topbar-divider d-none d-sm-block"></div>
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php if(isset(Auth::user()->id)): ?>
                                <!-- <img class="img-profile rounded-circle" src="<?php echo e(asset('backend/images/'.Auth::user()->image )); ?>"> -->
                                <?php if(Auth::user()->image!=NULL): ?>
                                <img style="width: 45px;height: 45px;" class="img-profile rounded-circle"
                                    src="<?php echo e(asset('backend/images/'.Auth::user()->image)); ?>">
                                <?php else: ?>
                                <img class="img-profile rounded-circle"
                                    src="<?php echo e(asset('backend/assets/images/avatar.png')); ?>">
                                <?php endif; ?>

                                <?php else: ?>
                                <img class="img-profile rounded-circle"
                                    src="<?php echo e(asset('backend/assets/images/avatar.png')); ?>">
                                <?php endif; ?>
                                <span class="mr-1 d-lg-inline ">Welcome </span><?php if(isset(Auth::user()->id)): ?>
                                <?php echo e(Auth::user()->name); ?> <?php endif; ?></a>
                            <div class="dropdown-menu  dropdown-profile dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item pencil" href="<?php echo e(route('admin.user.profile', Auth::id())); ?>">
                                    Profile </a>
                                <hr class="sidebar-divider my-0">
                                <a class="dropdown-item logout" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"> Logout </a> </div>
                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                <?php echo csrf_field(); ?>
                            </form>
                        </li>
                    </ul>
                </nav>
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </div>
    </div>

    <script src="<?php echo e(asset('backend/assets/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('backend/assets/js/custom_scripts.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('backend/assets/js/select2.full.min.js')); ?>" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.9/datepicker.min.js" type="text/javascript">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/js/bootstrap-colorpicker.min.js">
    </script>

    <script type="text/javascript">
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
            /*CKEDITOR.replace( 'editor1',{
                 extraPlugins: 'sourcedialog'
             } );
            CKEDITOR.replace( 'editor2',{
                 extraPlugins: 'sourcedialog'
             } );*/
            ClassicEditor
                .create(document.querySelector('.editor1'))

                .catch(error => {
                    console.error(error);
                });

            ClassicEditor
                .create(document.querySelector('.editor2'))

                .catch(error => {
                    console.error(error);
                });

        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <?php echo $__env->yieldContent('scripts'); ?>
    <!-- <script src="https://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <?php echo Toastr::message(); ?>

</body>

</html><?php /**PATH /var/www/html/laraveltest/resources/views/admin/layouts/main.blade.php ENDPATH**/ ?>