<!-- resources/views/tasks/index.blade.php -->



<?php $__env->startSection('content'); ?>

<style type="text/css">
  .container {
    width: 100% !important;
  }.h4, h4 {
    font-size: 18px;
    font-weight: bold;
  }
  .boxcustom{
    background: #ccc none repeat scroll 0 0 !important;
  }
  @media  only screen and (max-width: 800px) {
    
    /* Force table to not be like tables anymore */
    #private-ajax table, 
    #private-ajax thead, 
    #private-ajax tbody, 
    #private-ajax th, 
    #private-ajax td, 
    #private-ajax tr { 
      display: block; 
      width: 100%
    }
    
    /* Hide table headers (but not display: none;, for accessibility) */
    #private-ajax thead tr { 
      position: absolute;
      top: -9999px;
      left: -9999px;
    }
    
    #private-ajax tr { border: 1px solid #ccc; }
    
    #private-ajax td { 
      /* Behave  like a "row" */
      border: none;
      border-bottom: 1px solid #eee; 
      position: relative;
      padding-left: 50%; 
      white-space: normal;
      text-align:left;
    }
    
    #private-ajax td:before { 
      /* Now like a table header */
      position: absolute;
      /* Top/left values mimic padding */
      top: 6px;
      left: 6px;
      width: 45%; 
      padding-right: 10px; 
      white-space: nowrap;
      text-align:left;
      font-weight: bold;
    }
    
  /*
  Label the data
  */
  #private-ajax td:before { content: attr(data-title); }
}
@media  only screen and (max-width: 800px) {
  
  /* Force table to not be like tables anymore */
  #private-ajax-user table, 
  #private-ajax-user thead, 
  #private-ajax-user tbody, 
  #private-ajax-user th, 
  #private-ajax-user td, 
  #private-ajax-user tr { 
    display: block; 
    width: 100%
  }
  
  /* Hide table headers (but not display: none;, for accessibility) */
  #private-ajax-user thead tr { 
    position: absolute;
    top: -9999px;
    left: -9999px;
  }
  
  #private-ajax-user tr { border: 1px solid #ccc; }
  
  #private-ajax-user td { 
    /* Behave  like a "row" */
    border: none;
    border-bottom: 1px solid #eee; 
    position: relative;
    padding-left: 50%; 
    white-space: normal;
    text-align:left;
  }
  
  #private-ajax-user td:before { 
    /* Now like a table header */
    position: absolute;
    /* Top/left values mimic padding */
    top: 6px;
    left: 6px;
    width: 45%; 
    padding-right: 10px; 
    white-space: nowrap;
    text-align:left;
    font-weight: bold;
  }
  
  /*
  Label the data
  */
  #private-ajax-user td:before { content: attr(data-title); }
}
</style>

<div class="box boxcustom">
  <div class="box-header">
    <h3 class="box-title"></h3>
  </div>
  <div class="box-body">
    <?php echo csrf_field(); ?>

    <div class="container">
      <div class="header clearfix">
                    <!-- <nav>
                       <ul class="nav nav-pills pull-right">
                         <li role="presentation" class="active"><a href="#">Home</a></li>
                         <li role="presentation"><a href="#">About</a></li>
                         <li role="presentation"><a href="#">Contact</a></li>
                       </ul>
                     </nav> -->
                   </div>
          <!--   <div class="">

                <div class="jumbotron">
                    <h1><?php echo e(trans('message.welcome')); ?></h1> 
                
                   <p><a class="btn btn-lg btn-success" href="<?php echo e(url('/adminProfileEdit')); ?>" role="button">Edit Profile</a></p>
                </div>
              </div> -->
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-aqua"><i class="fa fa-utensils"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Users</span>
                        <span class="info-box-number"><?php echo e($users); ?></span>
                      </div>

                    </div>
                    
                  </div>
                  <div class="col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-red"><i class="fa fa-dumbbell"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total shops</span>
                        <span class="info-box-number"><?php echo e($shops); ?></span>
                      </div>
                    </div>
                    
                  </div>
                  <div class="col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-green"><i class="fa fa-hotel"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Products</span>
                        <span class="info-box-number"><?php echo e($products); ?></span>
                      </div>
                    </div>
                    
                  </div>
                  <div class="col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-teal"><i class="fa fa-newspaper"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Orders</span>
                        <span class="info-box-number"><?php echo e($orders); ?></span>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </div>
          
        </div>
      </div>
    </div>
    
    

    
    
    <input type="hidden" id="back" autocomplete="off" value="" /> 
  </div>
</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/laraveltest/resources/views/admin/dashboard/dashboard.blade.php ENDPATH**/ ?>