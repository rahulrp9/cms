<?php $__env->startSection('content'); ?>
<link rel="stylesheet" href="<?php echo e(URL::asset('css/custom_style2.css')); ?>">
<style type="text/css">
    a {
    color: #3c8dbc;
    padding: 5px;
}
</style>
<div class="contentHolderV1">
    <h2>Delivery Areas</h2>
    <div class="row">
          <div class="col-md-6"> 
           <!--  <a href="<?php echo e(URL::to('deleted/services')); ?>" class="searchbtn pull-right">Deleted Services</a> -->
        </div>
        <div class=" pull-right" style="float: right;"> 
             <a href="<?php echo e(URL::to('dashboard/areas/create')); ?>" class="searchbtn">Add Delivery Area</a>
        </div>
      
    </div>    
   
    <div class="tableHolder">
        <table style="width: 100%;" class="tableV1" cellpadding="0" cellspacing="0">
            <thead class="tableHeader">
                <tr>
                    <td>#</td>
                    <td>Delivery Area</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
               
               <?php $__currentLoopData = $areas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <?php  //$n++; ?>

                    <tr>
                        <td><?php echo e($loop->iteration); ?></td>
                        <td><?php echo e($service->delivery_area); ?></td>
                        <td><div  class="actions">
                                    <a href="<?php echo e(URL::to('dashboard/areas/'.$service->id.'/edit')); ?>" class="edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <!-- <a href="<?php echo e(URL::to('dashboard/areas', ['id' => ($service->id)])); ?>" class="view">
                                        <i class="fa fa-eye"></i>
                                    </a> -->
                                    <form action="<?php echo e(route('admin.areas.destroy', $service->id)); ?>" method="post" style="float: left;">
                                         <?php echo e(method_field('DELETE')); ?>

                            <?php echo e(csrf_field()); ?>

                                        <a class="delete" data-id=""><i class="fa fa-trash-alt"></i></a>
                                           
                                         </form>
                                    
                                </div></td>
                       
                    </tr>
                    
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
<ul class="pagination">
        <?php echo e($areas->render()); ?>

    </ul>
</div>
<script type="text/javascript">
    
    $(function () {
        $('.delete').click(function(e){
            e.preventDefault();
            if(confirm('Are you sure you want to delete?'))
            {
                $(this).parent('form').submit();
            }else{
                return false;
            }
        });
});

    
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/laraveltest/resources/views/area/list.blade.php ENDPATH**/ ?>