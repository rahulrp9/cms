<?php $__env->startSection('title', 'Events List'); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid mt-5">
    <div class="row  align-items-center justify-content-between mb-5">

        <h6 class="h6 mb-0 dashboard-title"><?php echo e($events->name); ?> Sessions List :</h6>
        <div class="add-search">
            <a href="<?php echo e(route('admin.events.sessions',$events->id)); ?>" class="btn btn-primary btn-purple"
                title="Add New">Add New</a>
            <form class="d-sm-inline-block form-inline navbar-search" action="<?php echo e(route('admin.sessions.search')); ?>"
                method="post" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search By Name"
                        aria-label="Search" aria-describedby="basic-addon2" name="search">
                    <input type="hidden" name="event_id" value="<?php echo e($events->id); ?>">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit"> Search </button>
                        <a class="refresh" style="color:black" href="<?php echo e(route('admin.sessions.show',$events->id)); ?>"> <i
                                class="fa fa-refresh"></i>
                        </a>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

<div class="container-fluid table-register">
    <div class="row ">
        <table class="table table-registerd-users  create-list">
            <thead>
                <tr>
                    <th scope="col">Sl.No</th>
                    <th scope="col">Event Name</th>
                    <th scope="col">Event Venue</th>
                    <th scope="col">Date</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                    <th class="add-select" scope="col">Add</th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($sessions) !=0): ?>
                <?php $__currentLoopData = $sessions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                
                <tr>
                    <td scope="row"><?php echo e(($sessions->currentpage()-1) * $sessions->perpage() + $key + 1); ?></td>
                    <td><a href="<?php echo e(route('admin.sessionattendees.show',$values->id)); ?>"
                            style="color:black"><?php echo e($values->name); ?></a></td>
                    <td><?php echo e($values->venue); ?></td>
                    <td><?php echo e(date('d -m -y', strtotime($values->from_date))); ?></td>
                    <td>
                        <a href="<?php echo e(route('admin.sessions.status',$values->id)); ?>">
                            <?php if($values->status): ?>
                            <button type="button" class="btn btn-default btn-purple" title="Active">Active</button>
                            <?php else: ?>
                            <button type="button" class="btn btn-default " title="Inactive">Inactive</button>
                            <?php endif; ?>
                        </a>
                    </td>
                    <td class="action-buttons">
                        <a href="<?php echo e(route('admin.sessionattendees.show',$values->id)); ?>" class="btn preview"
                            title="View">
                            <img src="<?php echo e(asset('backend/assets/images/preview.png')); ?>" alt="preview" />
                        </a>

                        <a href="<?php echo e(route('admin.sessions.edit',$values->id)); ?>" type="button" class="btn edit"
                            title="Edit">
                            <img src="<?php echo e(asset('backend/assets/images/edit.png')); ?>" alt="preview" />
                        </a>

                        <form action="<?php echo e(route('admin.sessions.destroy', $values->id)); ?>" method="post">
                            <?php echo e(method_field('DELETE')); ?>

                            <?php echo e(csrf_field()); ?>

                            <button type="submit" class="btn trash"
                                onclick="return confirm('Are you sure you want to delete?')" title="Delete">
                                <img src="<?php echo e(asset('backend/assets/images/trash.png')); ?>" alt="preview" />
                            </button>
                        </form>
                    </td>
                    <td>
                        <span>
                            <?php if($events->event_category_id != 1): ?>
                            <a href="<?php echo e(route('admin.sessions.attendees',$values->id)); ?>"
                                class="btn btn-primary btn-purple" title="Add Attendees">Attendees</a>
                            <?php else: ?>
                            
                            <a href="<?php echo e(route('admin.team.create',$values->id)); ?>" class="btn btn-primary btn-purple"
                                title="Add Teams">Teams</a>
                            <?php endif; ?>
                        </span>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                <td colspan="5" style="text-align: center;">No Records Found !!!</td>
                <?php endif; ?>
            </tbody>
        </table>
        <ul class="pagination d-flex mt-3 mb-3">
            <?php echo e($sessions->links()); ?>

        </ul>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/sessions/list.blade.php ENDPATH**/ ?>