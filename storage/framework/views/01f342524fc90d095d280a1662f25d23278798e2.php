<?php $__env->startSection('content'); ?>
<!-- muliselect plugin's CSS  -->
<link href="<?php echo e(asset('assets/plugins/jasny-bootstrap/css/jasny-bootstrap.css')); ?>" rel="stylesheet" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <div class="col-md-12 ">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?php if(isset($shop)): ?>Shop Updation <?php else: ?> Shop Creation <?php endif; ?></small> </h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
                  <a href="<?php echo e(URL::to('dashboard/shops')); ?>"><button title="List" data-toggle="tooltip" class="btn btn-default" type="button">
                          <i class="fa fa-list"></i></button>
                  </a>
                
                  <?php if(isset($shop)): ?>
                        <a href="<?php echo e(URL::to('dashboard/shops/create')); ?>"><button title="New" data-toggle="tooltip" class="btn btn-default" type="button">
                                <i class="fa fa-plus"></i></button>
                        </a>
                        <a href="<?php echo e(URL::to('dashboard/shops/'.$shop->id)); ?>"><button title="More" data-toggle="tooltip" class="btn btn-default" type="button">
                                <i class="fa fa-eye"></i></button>
                        </a>

                  <?php endif; ?>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
                <div class="flash_messages">
                </div>
                <form id="formSpace" action="<?php echo e(URL::to('dashboard/shops')); ?><?php if(isset($shop)): ?>/<?php echo e($shop->id); ?> <?php endif; ?>" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <?php echo csrf_field(); ?>

                    <?php if(isset($shop)): ?>
                        <?php echo e(method_field('PUT')); ?>

                        <input type="hidden" name="service_id" value="<?php echo e($shop->id); ?>"/>
                    <?php endif; ?>
                    <!-- First Name-->
                    <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                        <label for="name" class="col-sm-3 control-label required">Shop Name</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="name" id="name" value='<?php if(isset($shop)): ?><?php echo e($shop->name); ?><?php endif; ?>' class="form-control service_eng" required="" placeholder="Enter Shop Name">
                                <?php if($errors->has('name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                        <label for="email" class="col-sm-3 control-label required">Shop Email</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="email" name="email" id="email" value='<?php if(isset($shop)): ?><?php echo e($shop->email); ?><?php endif; ?>' class="form-control service_eng" required="" placeholder="Enter Shop Email">
                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                     <div class="form-group <?php echo e($errors->has('phone') ? ' has-error' : ''); ?>">
                        <label for="phone" class="col-sm-3 control-label required">Shop Phone</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="number" name="phone" id="phone" value='<?php if(isset($shop)): ?><?php echo e($shop->phone); ?><?php endif; ?>' class="form-control service_eng" required="" placeholder="Enter Shop Phone">
                                <?php if($errors->has('phone')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('phone')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?php echo e($errors->has('address') ? ' has-error' : ''); ?>">
                        <label for="address" class="col-sm-3 control-label required">Shop Address</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="address" id="address" value='<?php if(isset($shop)): ?><?php echo e($shop->address); ?><?php endif; ?>' class="form-control" required="" placeholder="Enter Shop Address">
                                <?php if($errors->has('address')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('address')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group <?php echo e($errors->has('street') ? ' has-error' : ''); ?>">
                        <label for="street" class="col-sm-3 control-label required">Shop Street</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="street" id="street" value='<?php if(isset($shop)): ?><?php echo e($shop->street); ?><?php endif; ?>' class="form-control"  required="" placeholder="Enter Shop Street">
                                <?php if($errors->has('street')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('street')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?php echo e($errors->has('latitude') ? ' has-error' : ''); ?>">
                        <label for="latitude" class="col-sm-3 control-label required">Latitude</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="latitude" id="latitude" value='<?php if(isset($shop)): ?><?php echo e($shop->latitude); ?><?php endif; ?>' class="form-control" required="" placeholder="Enter Shop Latitude">
                                <?php if($errors->has('latitude')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('latitude')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?php echo e($errors->has('longitude') ? ' has-error' : ''); ?>">
                        <label for="longitude" class="col-sm-3 control-label required">Longitude</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="longitude" id="longitude" value='<?php if(isset($shop)): ?><?php echo e($shop->longitude); ?><?php endif; ?>' class="form-control" required="" placeholder="Enter Shop Longitude">
                                <?php if($errors->has('longitude')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('longitude')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <!-- SpaceImage-->
                
                        <div class="form-group">
                                <label for="title_ar" class="col-sm-3 control-label required">Shop Logo</label>
                                <div class="imgUploadHolder col-sm-6">
                                    <?php if(isset($shop->image) && $shop->image != ''): ?>
                                    <input type="hidden" name="lasthidimg" value="<?php echo e($shop->image); ?>" class="lasthidimg">
                                    <?php endif; ?>
                                    <div class="col-lg-3">
                                   <input type="file" class="space_img " name="servicelogo" id="space_img"   <?php if(isset($shop->image) && $shop->image != ''): ?>
                                   value="<?php echo e($shop->image); ?>" <?php else: ?> required <?php endif; ?>>
                                  <label class="errorimg" for="imageInput" style="color: red"></label>
                                    <div style="margin-top: 10px;">
                                    <img src="" class="imagePreview" width="150" height="150" style="display: none;">
                                    <?php 
                                    $url = url('/');
                                    $nurl = str_replace("index.php","",$url);
                                    ?>
                                    <?php if(isset($shop->image) && $shop->image != ''): ?>
                                   <img src="<?php echo e($nurl); ?><?php echo e($shop->image); ?>" width="150" height="150" class="oldpreview">
                                   
                                   <?php endif; ?>
                                   </div>
                                   </div>

                                </div>
                            </div>
                         
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button id="btn-submit" type="submit" class="btn btn-default checkbtn">
                               <i class="fa fa-floppy-o" aria-hidden="true"></i> Save
                            </button>
                            <button id="btn-submit" type="button" class="btn btn-default" onclick="window.history.back()">
                                <i class="fa fa-refresh"></i> Cancel
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script type="text/javascript">
     $('.space_img').change(function(event) {
        var fileSize = this.files[0].size;
        var maxAllowedSize = 2000000;
        if(fileSize > maxAllowedSize){
            $('.errorimg').html('Please upload a smaller file');
           $('.space_img').val('');
        }
        else{
            $('.errorimg').html('');
            if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.oldpreview').fadeOut();
                        $('.imagePreview').attr('src', e.target.result);
                        $('.imagePreview').fadeIn();
                    }
                    reader.readAsDataURL(this.files[0]);
                }
        }

    });

    /*$('.checkbtn').click(function(){

        var name = $('.service_eng').val();
        $.ajax({
                    type: 'get',
                    url: '<?php echo url('/');?>/checkservice',
                    data: 'servicename='+name,
                    datatype: "json",
                    async: false,
                    cache: false,
                    timeout: 30000,
                        success: function (response) {
                             
                        }
            });
    }); */
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/laraveltest/resources/views//shops/form.blade.php ENDPATH**/ ?>