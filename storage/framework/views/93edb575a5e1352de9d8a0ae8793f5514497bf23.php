<?php if(isset($updating) && $updating): ?>
<?php $__env->startSection('title', 'Update Profile'); ?>
<?php else: ?>
<?php $__env->startSection('title','Add Profile'); ?>
<?php endif; ?>
<?php $__env->startSection('content'); ?>
<div class="edit-event-wrapper">
    <div class="container-fluid mt-3 mb-4">
        <div class="row">
            <div class=" mb-4">
                <h6 class="h6 mb-0 dashboard-title">Profile :</h6>
            </div>
        </div>
    </div>
    <div class="container-fluid edit-events">
        <div class="row">
            <form class="form-horizontal form-datetimepicker col-sm-6"
                action="<?php echo e(route('admin.user.update', $admin->id)); ?>" method="post" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="form-group d-flex">
                    <label class="control-label col-sm-3" for="name">Name:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" placeholder="Name *" maxlength="60"
                            name="name" value="<?php echo e(isset($admin) ? $admin->name : old('name')); ?>"
                            onkeypress='validate(event)'>

                        <?php if($errors->has('name')): ?>
                        <label for="name" class="error"><?php echo e($errors->first('name')); ?> </label>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group d-flex">
                    <label class="control-label col-sm-3" for="name">Email Address:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="venue" placeholder="Email *" maxlength="60"
                            name="email" value="<?php echo e(isset($admin) ? $admin->email : old('email')); ?>">

                        <?php if($errors->has('email')): ?>
                        <label for="email" class="error">
                            <?php echo e($errors->first('email')); ?>

                        </label>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group d-flex">
                    <label class="control-label col-sm-3" for="name">Password:</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" id="pwd" placeholder="Password *" maxlength="60"
                            name="password">

                        <?php if($errors->has('password')): ?>
                        <label for="password" class="error"><?php echo e($errors->first('password')); ?></label> <?php endif; ?>
                    </div>
                </div>
                <div class="form-group d-flex">
                    <label class="control-label col-sm-3" for="name">Confirm Password:</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" id="pwd" placeholder="Confirm Password *"
                            maxlength="60" name="cpassword">

                        <?php if($errors->has('cpassword')): ?>
                        <label for="password" class="error"><?php echo e($errors->first('cpassword')); ?></label> <?php endif; ?>
                    </div>
                </div>
                <div class="form-group d-flex">
                    <label class="control-label col-sm-3" for="pwd">Image:</label>
                    <div class="col-sm-9">
                        <div class="container-image">
                            <input type="file" id="input-file" name="image" accept="image/*" onchange={handleChange()}
                                hidden />
                            <label class="btn-upload btn btn-purple" for="input-file" role="button"> Choose File
                            </label>
                        </div>
                        <div class="file-upload-titles"> <span>No File Selected</span>
                            <p>Maximum Size of 700k, JPG, PNG</p>

                        </div>
                    </div>
                </div>
                <?php if($admin->image): ?>
                <div class="form-group" style="max-width: inherit; margin-top: 10px">
                    <img src="<?php echo e(asset('backend/images/'.$admin->image)); ?>" style="height: 100px; width: 100px;">
                    <?php if($errors->has('image')): ?>
                    <label for="image" class="error"><?php echo e($errors->first('image')); ?></label>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
                <div class="container-fluid mt-5">
                    <div class=" d-flex">
                        <div class=" col-sm-4 mt-3 mb-5">
                            <button type="submit" class="btn btn-default btn-purple mr-2">Submit</button>
                            <button type="submit" class="btn btn-default">
                                <a href="<?php echo e(route('admin.users.index')); ?>">Cancel</a>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/user/show.blade.php ENDPATH**/ ?>