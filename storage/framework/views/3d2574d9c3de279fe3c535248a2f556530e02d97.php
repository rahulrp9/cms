<?php $__env->startSection('title', 'Event Attendees'); ?>
<?php $__env->startSection('content'); ?>
<div class="edit-event-wrapper">
    <div class="container-fluid mt-3 mb-4">
        <div class="row justify-content-between">
            <div class=" col-lg-6 mb-4 ">
                <h2 class="h2 mb-0 ">Event Attendees</h2>
            </div>
            <div class=" mb-4 d-flex ">
                <a href="<?php echo e(route('admin.events.attendees',$events->id)); ?>" class="btn btn-primary btn-purple pull-right"
                    title="Add New">Add Attendees</a>
            </div>
        </div>
    </div>
    <div class="container-fluid edit-events ">
        <table class="table table-striped" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Category</th>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $attendees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($loop->iteration); ?></td>
                    <td><?php echo e($values->category); ?></td>
                    <td><?php echo e($values->username); ?></td>

                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </tbody>
        </table>
        <div class="row justify-content-end">
            <ul class=" pagination d-flex mt-3 mb-3 ">
                <?php echo e($attendees->links()); ?>

            </ul>
        </div>
    </div>
    <div class="container-fluid mt-5">
            <div class=" d-flex">
                <div class=" col-sm-4 mt-3 mb-5">
                    <button type="submit" class="btn btn-default">
                        <a href="<?php echo e(url()->previous()); ?>">Back</a>
                    </button>
                </div>
            </div>
        </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/attendees/show.blade.php ENDPATH**/ ?>