<?php if(isset($updating) && $updating): ?>
<?php $__env->startSection('title', 'Update Sessions'); ?>
<?php else: ?>
<?php $__env->startSection('title','Add Sessions'); ?>
<?php endif; ?>

<?php $__env->startSection('styles'); ?>
<style>
    .hidden {
        display: none;
    }

    #playerModal {
        z-index: 9999 !important;
    }

    .modal-body {
        overflow-y: scroll;
    }

    #playerModalEdit {
        z-index: 9999 !important;
    }

    .auto-input {
        z-index: 1000 !important;
    }

    .ui-autocomplete-input {
        z-index: 5000;
    }

    .ui-widget-content {
        z-index: 9999;
    }

    .hide {
        display: none;
    }
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<script src="<?php echo e(asset('backend/assets/trumbowyg/trumbowyg.js')); ?>"></script>
<link href="<?php echo e(asset('backend/assets/trumbowyg/trumbowyg.css')); ?>" rel="stylesheet" type="text/css" />
<script src="<?php echo e(asset('backend/assets/trumbowyg/plugins/fontsize/trumbowyg.fontsize.min.js')); ?>"></script>
<link rel="stylesheet" href="<?php echo e(asset('backend/assets/trumbowyg/plugins/colors/ui/trumbowyg.colors.min.css')); ?>">
<script src="<?php echo e(asset('backend/assets/trumbowyg/plugins/colors/trumbowyg.colors.min.js')); ?>"></script>
<div class="edit-event-wrapper">
    <div class="container-fluid mt-3 mb-4">
        <div class="row">
            <div class=" mb-4">
                <h6 class="h6 mb-0 dashboard-title">Update <?php echo e($event->name); ?> Template</h6>
            </div>
        </div>
    </div>
    <div class="container-fluid edit-events id-card">
        <form class="form-horizontal">
            <input type="hidden" name="" class="event_id" value="<?php echo e($eventId); ?>">
            <div class="row">

                <div class=" form-idcard">


                    <div class="form-group ">
                        <label class="control-label " for="pwd">Event Template:</label>
                        <div class="id-card-img">
                            <!-- <div class="container-image">
                                <input type="file" id="input-file" name="input-file" accept="image/*" onchange={handleChange()} hidden />
                                <label class="btn-upload btn btn-purple" for="input-file" role="button"> Choose File </label>
                            </div>
                            <div class="file-upload-titles"> <span>No File Selected</span>
                                <p>Maximum Size of 700k, JPG, GIF, PNG</p>
                                <a href="#." class="btn btn-delete">Delete This Image</a> </div> -->
                                <textarea id='long_desc' name='long_desc' class="templatevalue">

                                    <?php if($eventTemplate): ?>
                                    <?php echo e($eventTemplate); ?>

                                    <?php endif; ?>
                                </textarea>
                            </div>
                        </div>




                    </div>

                    <div class=" form-add-file col-sm-12">

                        <div class="form-group d-flex">
                            <label class="control-label col-sm-3 " for="name ">
                               Select Your Template:
                           </label>
                           <div class="col-sm-9 ">
                            <select class="form-control changetemp template_id" id="exampleFormControlSelect1 ">
                              <option value="" selected="">--Choose Template--</option>  
                              <?php if($templates): ?>
                              <?php $__currentLoopData = $templates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $template): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($template->id); ?>"><?php echo e($template->template_name); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              <?php endif; ?>
                          </select>
                      </div>
                  </div>
                  <div class="row">
                      <div class="form-group d-flex col-sm-6">
                        <label class="control-label col-sm-2" for="pwd">Event Logo:</label>
                        <div class="col-sm-4">
                            <div class="hiddendiv" style="display: none;"></div>
                            <div class="container-image">
                                <input type="file" id="input-file" name="input-file" accept="image/*" class="evntlogo"  onchange={handleChange()} hidden/>
                                <label class="btn-upload btn btn-purple" for="input-file" role="button"> Choose File </label>
                            </div>
                            <div class="file-upload-titles"> <span>No File Selected</span>
                                <p>Maximum Size of 700k, JPG, GIF, PNG</p>
                                <!-- <a href="#." class="btn btn-delete">Delete This Logo</a> --> </div>
                            </div>
                        </div>
                        <div class="form-group d-flex col-sm-6">
                            <label class="control-label col-sm-3" for="pwd">Tag Background:</label>
                            <div class="col-sm-3">
                                <div class="hiddendiv2" style="display: none;"></div>
                                <div class="container-image">
                                    <input type="file" id="input-file2" name="input-file" accept="image/*" class="evntlogo2" onchange={handleChange()} hidden />
                                    <label class="btn-upload btn btn-purple" for="input-file2" role="button"> Choose File </label>
                                </div>
                                <div class="file-upload-titles"> <span>No File Selected</span>
                                    <p>Maximum Size of 700k, JPG, GIF, PNG</p>
                                    <!-- <a href="#." class="btn btn-delete">Delete This Logo</a> --> </div>
                                </div>
                            </div>
                        </div>


                        <div class=" form-group text-center mt-5 ">
                            <button type="button" class="btn btn-default btn-purple subbtn">Update </button>
                        </div>
                    </div>






                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        $(window).on('load', function (){
        /*CKEDITOR.replace( 'long_desc',{
             extraPlugins: 'sourcedialog'
         });*/
         $('#long_desc').trumbowyg({
            btns: [
            ['viewHTML'],
                    ['undo', 'redo'], // Only supported in Blink browsers
                    ['formatting'],
                    ['strong', 'em', 'del'],
                    ['superscript', 'subscript'],
                    ['link'],
                    ['insertImage'],
                    ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                    ['unorderedList', 'orderedList'],
                    ['horizontalRule'],
                    ['removeformat'],
                    ['fullscreen'],
                    ['foreColor', 'backColor'],
                    ['fontsize']
                    ],
                    plugins: {
                        fontsize: {
                            sizeList: [
                            '14px',
                            '18px',
                            '22px'
                            ],
                            allowCustomSize: false
                        },
                        colors: {
                            foreColorList: [
                            'ff0000', '00ff00', '0000ff'
                            ],
                            backColorList: [
                            'ff0000', '00ff00', '0000ff'
                            ]
                        },
                    }
                });

         $('.changetemp').change(function(){

            var id = $('.changetemp').val();
            $.ajax({

                url: '<?php echo e(route('admin.gettemplate')); ?>',
                datatype: "json",
                data: { _token: "<?php echo e(csrf_token()); ?>",id:id} ,
                type: 'post',
                success: function (response) {
                    var newhtml = response;
                    //CKEDITOR.instances.long_desc.setData(newhtml);
                    $('#long_desc').trumbowyg('html',newhtml);
                },
                error: function (response) {
                    alert("qq");
                }
            });

        });

         $('.subbtn').click(function(){

            var event_id = $('.event_id').val();
            var template_id = $('.template_id').val();
            var templatevalue = $('.templatevalue').val();
            $.ajax({

                url: '<?php echo e(route('admin.savetemplate')); ?>',
                datatype: "json",
                data: { _token: "<?php echo e(csrf_token()); ?>",event_id:event_id,template_id:template_id,templatevalue:templatevalue} ,
                type: 'post',
                success: function (response) {
                    var loc = window.location;
                    //window.location = loc.protocol+"//"+loc.hostname+"/dashboard/events";
                    window.location.href = "/dashboard/events";
                },
                error: function (response) {
                    alert("Something Went Wrong!");
                }
            });
        });

         $(".evntlogo").on("change", function() {
            var file_data = $('.evntlogo').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            $.ajax({
                url: '<?php echo e(route('admin.image.testpageimage')); ?>',
                    dataType: 'text', // what to expect back from the server
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (response) {
                        var templatevalue = $('.templatevalue').val();
                        $('.hiddendiv').html('');
                        $('.hiddendiv').html(templatevalue);
                        //var url = "http://127.0.0.1:8000/backend/tagimg/"+response;
                        $('.hiddendiv').find('.evntsrclogo').attr("src",response);

                        var newhtml = $('.hiddendiv').html();
                        //$('.templatevalue').val(newhtml);
                        $('#long_desc').trumbowyg('html',newhtml);
                    },
                    error: function (response) {
                    alert("qq");
                }
            });
        });
        $(".evntlogo2").on("change", function() {
            var file_data = $('.evntlogo2').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            $.ajax({
                url: '<?php echo e(route('admin.image.testpageimage')); ?>',
                    dataType: 'text', // what to expect back from the server
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (response) {
                        var templatevalue = $('.templatevalue').val();
                        $('.hiddendiv2').html('');
                        $('.hiddendiv2').html(templatevalue);
                        //var url = "http://127.0.0.1:8000/backend/tagimg/"+response;
                        //$('.hiddendiv2').find('.evntsrclogo').attr("src",url);
                        $('.hiddendiv2').find('.maindiv').css("background-image", "url('"+response+"')");

                        var newhtml = $('.hiddendiv2').html();
                        //$('.templatevalue').val(newhtml);
                        $('#long_desc').trumbowyg('html',newhtml);
                    },
                    error: function (response) {
                    alert("qq");
                }
            });
        }); 
     });    
 </script>
 <?php $__env->stopSection(); ?> 
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/templates/template.blade.php ENDPATH**/ ?>