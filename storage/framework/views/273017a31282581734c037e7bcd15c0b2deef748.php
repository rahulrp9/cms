<?php $__env->startSection('title', 'Events List'); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid mt-5">
    <div class="row  align-items-center justify-content-between mb-5">

        <h6 class="h6 mb-0 dashboard-title">Events List :</h6>
        <div class="add-search">
            <a href="<?php echo e(route('admin.events.create')); ?>" class="btn btn-primary btn-purple" title="Add New">Add
                New</a><br>

            <form class="d-sm-inline-block form-inline navbar-search" action="<?php echo e(route('admin.events.search')); ?>"
                method="post" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search By Name"
                        aria-label="Search" aria-describedby="basic-addon2" name="search">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit"> Search </button>
                        <a class="refresh" style="color:black" href="<?php echo e(route("admin.events.index")); ?>"> <i
                                class="fa fa-refresh"></i>
                        </a>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

<div class="container-fluid table-register">
    <div class="row ">
        <table class="table table-registerd-users  create-list">
            <thead>
                <tr>
                    <th scope="col">Sl.No</th>
                    <th scope="col">Event Name</th>
                    <th scope="col">City</th>
                    <th scope="col">Date</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                    <!-- <th class="add-select" scope="col"></th> -->
                </tr>
            </thead>
            <tbody>
                <?php if(count($events)): ?>
                <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td scope="row"><?php echo e(($events->currentpage()-1) * $events->perpage() + $key + 1); ?></td>
                    <td><a href="<?php echo e(route('admin.events.show',$values->id)); ?>"
                            style="color:black"><?php echo e($values->name); ?></a></td>
                    <td><?php echo e($values->city->name); ?></td>
                    <td><?php echo e(date('d -m -y', strtotime($values->from_date))); ?></td>
                    <td>
                        <a href="<?php echo e(route('admin.events.status',$values->id)); ?>">
                            <?php if($values->status): ?>
                            <button type="button" class="btn btn-default btn-purple" title="Active">Active</button>
                            <?php else: ?>
                            <button type="button" class="btn btn-default " title="Inactive">Inactive</button>
                            <?php endif; ?>
                        </a>
                    </td>
                    <td class="action-buttons">

                        <a href="<?php echo e(route('admin.events.show',$values->id)); ?>" class="btn preview" title="View">
                            <img src="<?php echo e(asset('backend/assets/images/preview.png')); ?>" alt="preview" />
                        </a>
                        <a href="<?php echo e(route('admin.events.edit',$values->id)); ?>" type="button" class="btn edit"
                            title="Edit">
                            <img src="<?php echo e(asset('backend/assets/images/edit.png')); ?>" alt="preview" />
                        </a>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('event_delete')): ?>
                        <form action="<?php echo e(route('admin.events.destroy', $values->id)); ?>" method="post">
                            <?php echo e(method_field('DELETE')); ?>

                            <?php echo e(csrf_field()); ?>

                            <button type="submit" class="btn trash" title="Delete"
                                onclick="return confirm('Are you sure you want to delete?')">
                                <img src="<?php echo e(asset('backend/assets/images/trash.png')); ?>" alt="preview" />
                            </button>
                        </form>
                        <?php endif; ?>
                        <a href="<?php echo e(route('admin.events.pdfpage', $values->id)); ?>" title="View Report">
                            <i class="fa fa-download" aria-hidden="true"></i>

                        </a>
                        <a href="<?php echo e(route('admin.templates', $values->id)); ?>" title="Tag Design">
                        <i class="fa fa-id-card"></i>
                    </a>
                    </td>
                    <!-- <td>
                        <div class="form-group">
                            <select class="form-control" id="change" onchange="location = this.value;">
                                <option value="that">Add New</option>
                                <option value="<?php echo e(route('admin.sessions.show',$values->id)); ?>"><a
                                        href="<?php echo e(route('admin.sessions.show',$values->id)); ?>">Sessions</a>
                                </option>
                                <option value="<?php echo e(route('admin.events.sponsors',$values->id)); ?>"><a
                                        href="<?php echo e(route('admin.events.sponsors',$values->id)); ?>">Add Sponsors</a>
                                </option>
                                <option value="<?php echo e(route('admin.events.exhibitors',$values->id)); ?>"><a
                                        href="<?php echo e(route('admin.events.exhibitors',$values->id)); ?>">Add Exhibitors</a>
                                </option>
                                <option value="<?php echo e(route('admin.events.attendees',$values->id)); ?>"><a
                                        href="<?php echo e(route('admin.events.attendees',$values->id)); ?>">Add Attendees</a>
                                </option>
                                <option value="<?php echo e(route('admin.templates',$values->id)); ?>"><a
                                        href="<?php echo e(route('admin.templates',$values->id)); ?>">Add Template</a>
                                </option>
                            </select>
                        </div>
                    </td> -->
                </tr>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                <td colspan="5" style="text-align: center;">No Records Found !!!</td>
                <?php endif; ?>
            </tbody>
        </table>
        <ul class="pagination d-flex mt-3 mb-3">
            <?php echo e($events->links()); ?>

        </ul>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    $(function() {
        $('.delete-model').on('click', function(){
            var target_id = $(this).data('id');
            var delete_route = '<?php echo e(route('admin.events.destroy')); ?>'+'/'+target_id;
            var delete_form = $('#delete-form');

            delete_form.attr("action", delete_route);
        });

        $(document).on('click', '.modal-dismiss', function (e) {
            var delete_form = $('#delete-form');
            delete_form.removeAttr("action");
        });

    });
    $(document).ready(function() {

        $('#selectall').click(function(event) { //on click
            if(this.checked) { // check select status
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"
                });
            }else{
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"
                });
            }
        });

        /*Remove All*/
        $( "#removeall" ).click(function() {


            var selectedVal = new Array();
                $('input[name="check"]:checked').each(function() {
            selectedVal.push(this.value);
            });


            var values= selectedVal;

            var valC=selectedVal.length;
                var url='<?php echo e(route('admin.events.destroy')); ?>';
            if(valC != 0){
            if(confirm('Are you sure you want to delete?')){
                $.ajax({
                type: "DELETE",
                    url: url+'/'+values,

                    success: function(data){

                        location.reload();
                        }

                });
            }
            else{
                //alert("fg");
            }
            }else{
            $('#msg').html("Please select at least one record").show().delay(600000000).hide('slow');
            }
        });
        /*End*/

    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/eventeam/resources/views/admin/events/list.blade.php ENDPATH**/ ?>