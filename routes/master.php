<?php

/*
|--------------------------------------------------------------------------
| Web Routes(Public Routes)
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>['isCustomer']],function(){
	//auth
	Route::post('/login', 'Auth\AuthController@customer_login')->name('customer_login');
	Route::post('/customer/logout', 'Auth\AuthController@logout')->name('customer_logout');
	//home
	

	Route::get('/about', "IndexController@about")->name('about');
	
});

Route::get('/','IndexController@index')->name('index');	