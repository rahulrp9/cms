<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('logout', 'Auth\AuthController@logout');
Route::name('admin.')->group(function () {
    Route::group(['prefix' => 'dashboard', 'namespace' => 'Admin', 'middleware' => ['auth', 'isAdmin']], function () {

        Route::get('/', 'HomeController@index')->name('index');
        Route::get('user/profile/{id}', 'HomeController@profile')->name('user.profile');
        Route::post('user/profile/{id}', 'HomeController@update')->name('user.update');
        #Route::get('customers', 'HomeController@customers')->name('customers.index');
        //Category
        Route::get('categories', 'CategoriesController@categories');
        Route::get('categories/add/{id?}', 'CategoriesController@addcategory');
        Route::post('savecategory', 'CategoriesController@savecategory');
        Route::get('checkcatname', 'CategoriesController@checkcatname');
        Route::get('editcat/{id}', 'CategoriesController@editcat');
        Route::post('updatecat', 'CategoriesController@updatecat');
        Route::get('delcat/{id}', 'CategoriesController@delcat');
        Route::get('getcategoryById/{id}', 'CategoriesController@getcategoryById');
        Route::get('viewcat/{id}', 'CategoriesController@viewcat');
        Route::get('category/publish', 'CategoriesController@publish');
        Route::get('category/unpublish', 'CategoriesController@unpublish');
        Route::get('category/hidden', 'CategoriesController@hidden');

        Route::resource('shops', 'ShopController');
        Route::resource('users', 'UserController');
        Route::resource('offers', 'OfferController');
        Route::resource('coupons', 'CouponController');
        Route::resource('orders', 'OrderController');
        Route::resource('products', 'ProductController');
        Route::resource('addons', 'AddonController');
        Route::resource('areas', 'AreaController');
        Route::resource('shopareas', 'ShopAreaController');
        Route::resource('shopcategories', 'ShopCategoryController');

        Route::get('shopsbtn/publish', 'ShopController@publish');
        Route::get('shopsbtn/unpublish', 'ShopController@unpublish');
    });
});

Route::group(['prefix' => 'dashboard', 'namespace' => 'Admin'], function () {
    Auth::routes();
    Route::post('/login', 'Auth\LoginController@login')->name('login');
});
