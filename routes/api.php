<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('menu/initial-data', 'Api\AuthController@appData');
Route::get('test', 'Api\CommonController@test');
Route::post('getsign', 'Api\CommonController@getSign');
#Route::post('generic/app-data', 'Api\CommonController@appData');
Route::post('generic/app-data', 'Api\CommonController@appData');

/*Route::group(['middleware' => ['check_api_signature']], function() {*/

	Route::post('testjwt/sss', 'Api\CommonController@test');
	Route::post('user/registration', 'Api\CommonController@userRegistration');
	Route::post('user/verify-otp', 'Api\CommonController@userVerify');
	Route::post('user/resend-code', 'Api\CommonController@userResendCode');
	Route::post('user/login', 'Api\CommonController@userLogin');
	Route::get('getcountries', 'Api\CommonController@getCountries');
	
	Route::post('user/forgotpassword', 'Api\CommonController@forgotPassword');



	Route::post('scan/login', 'Api\ScanningController@scanLogin');
	

	
/*});*/

/*Route::group(['middleware' => ['jwt.verify']], function() {


});	*/
	Route::post('testjwt/ssss', 'Api\CommonController@test');
	Route::post('user/shop-list', 'Api\ShopController@shopList');
	Route::post('user/category-list', 'Api\ShopController@categoryList');
	Route::post('user/getproducts', 'Api\ShopController@getProducts');
	Route::post('user/addcart', 'Api\ShopController@addCart');
	Route::post('user/updatecart', 'Api\ShopController@updateCart');
	Route::post('user/cartlisting', 'Api\ShopController@cartListing');
	Route::post('user/checkout', 'Api\ShopController@checkout');
	Route::post('user/paymentstatus', 'Api\ShopController@paymentStatus');
	Route::post('user/orderlist', 'Api\ShopController@orderList');
	Route::post('user/orderdetail', 'Api\ShopController@orderDetail');
	Route::post('product/search', 'Api\ShopController@productSearch');

	Route::post('general/contents', 'Api\CommonController@webContent');
	Route::post('user/addrating', 'Api\ShopController@addRating');
	Route::post('user/addaddress', 'Api\ShopController@addAddress');
	Route::post('user/notifications', 'Api\CommonController@notifications');
	Route::post('user/removeaddress', 'Api\ShopController@removeAddress');
	Route::post('user/addresslistings', 'Api\CommonController@addressListing');