
var customJS;

jQuery(document).ready(function ($) {

	customJS = {

		common: {
			commonJS: function () {
				var $windowWidth = $(window).width();

				$('.pageNav li').removeClass('selected');
				$('.megaMenuWrapper').hide();
				$('.megaMenu').hide();
				$('.pageNav li, .megaMenuWrapper').addClass('chcMenu');

				$('.pageNav li a').on('mouseover', function (event) {
					$('.pageNav li').removeClass('selected');
					$(this).parent('li').addClass('selected');
					var getRel = $(this).attr('rel');
					$('.megaMenuWrapper').stop().slideDown(200);
					$('.megaMenu').stop().slideUp(200);
					$('#' + getRel).stop().slideDown(200);
				});

				$('body').on('mousemove', function (event) {
					if (!$(event.target).closest('.chcMenu').length) {
						$('.megaMenuWrapper').stop().slideUp(200);
						$('.megaMenu').stop().slideUp(200);
						$('.pageNav li').removeClass('selected');
					}
				});

				//Header Fixed
				var headerHeight
				function fixedHeader() {
					headerHeight = $('.pageHeader').outerHeight();
					$('#pageContainer').css('padding-top', headerHeight);
				}


				$(window).scroll(function () {
					if ($(this).scrollTop() > 0) {
						$('.pageHeader').addClass("sticky");
						$('body').addClass("body-scroll");
					} else {
						$('.pageHeader').removeClass("sticky");
						$('body').removeClass("body-scroll");
					}

					$.scrollify.update();

					if ($('.pageHeader').hasClass("sticky")) {
						$('.logoText img').removeClass('animated fadeInLeft').addClass('animated fadeOutLeft');
					} else {
						$('.logoText img').removeClass('animated fadeOutLeft').addClass('animated fadeInLeft');
					}

				});



				$(".bgImage img").each(function () {
					var imagePath = $(this).attr("src");
					$(this).closest('.bgImage').css('background-image', 'url(' + imagePath + ')');

				});
				$('.favourite span').removeClass('selected');
				$('.favourite').click(function () {
					$(this).find('span').toggleClass('selected');
				});
				$('.showMore .btnShow').click(function () {
					$(this).closest('.showMore').toggleClass('more');

					if ($(this).closest('.showMore').hasClass('more')) {
						$(this).text('Show Less');
					} else {
						$(this).text('Show More');
					}

				});
				var showChar = 770;
				$('.showMore article ').each(function () {
					var content = $(this).html();
					if (content.length < showChar) {
						$(this).closest('.showMore').find('.btnShow').hide();

					}

				});
				
				// ------------------- Slider -----------------

				if ($('.pdtSlider').size() > 0) {
					$('.pdtSlider').slick({
						infinite: true,
						slidesToShow: 1,
						dots: false,
						variableWidth: true
					});
				}
				if ($('.pdtCategoryList').size() > 0) {
					$('.pdtCategoryList ul').slick({
						infinite: true,
						slidesToShow: 1,
						dots: false,
						centerMode: false,
						variableWidth: true,

					});
				}
				if ($('.innerBannerSlider').size() > 0) {
					$('.innerBannerSlider').slick({
						dots: false,
						arrows: true
					});
				}
				if ($('.pdtThumbSlider').size() > 0) {
					$('.pdtThumbSlider ul').slick({
						infinite: true,
						slidesToShow: 1,
						dots: false,
						variableWidth: true
					});
				}

				if ($('.large-slider').size() > 0) {
					$('.large-slider').slick({
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: false,
						asNavFor: '.thumb-slider'
					});
					$('.thumb-slider').slick({
						slidesToShow: 4,
						slidesToScroll: 1,
						asNavFor: '.large-slider',
						dots: false,
						focusOnSelect: true,
						responsive: [
							{
								breakpoint: 991,
								settings: {
									centerMode: true,
									slidesToShow: 2
								}
							},
							{
								breakpoint: 767,
								settings: {
									centerMode: false,
									slidesToShow: 4
								}
							},
							{
								breakpoint: 380,
								settings: {
									centerMode: true,
									slidesToShow: 2
								}
							}
						]
					});
				}

				if ($('.relatedPdtSlider').size() > 0) {
					$('.relatedPdtSlider').slick({
						infinite: true,
						slidesToShow: 4,
						slidesToScroll: 1,
						dots: false,
						variableWidth: true,
						arrows: true,
						responsive: [
							{
								breakpoint: 767,
								settings: {
									centerMode: true,
									slidesToShow: 2
								}
							},
							{
								breakpoint: 600,
								settings: {
									centerMode: true,
									slidesToShow: 1
								}
							}
						]
					});
				}
				if ($('.fluidPdtSlider').size() > 0) {
					$('.fluidPdtSlider').slick({
						dots: false,
						infinite: true,
						speed: 300,
						slidesToShow: 4,
						slidesToScroll: 1,
						variableWidth: true
					});
				}
				if ($('.testimonialSlder').size() > 0) {
					$('.testimonialSlder').slick({
						dots: true,
						arrows: false
					});
				}
				if ($('.vendorSlider').size() > 0) {
					$('.vendorSlider').slick({
						dots: false,
						infinite: true,
						arrows: false,
						slidesToShow: 6,
						slidesToScroll: 2,
						variableWidth: true
					});
				}

				//----------- Tab Accordion --------------
				// $('.wizardProgress li:eq(0)').addClass('selected');
				// $('.wizardContent:eq(0)').show(); 
				$('.wizardProgress li').removeClass('selected');
				$('body').on('click', '.wizardProgress li', function () {

					$(this).addClass('selected').siblings().removeClass('selected');
					var getRel = $(this).attr('rel');
					var ConHeight = $('#' + getRel).outerHeight() + 20;
					$(this).closest('.wizardProgress').find('.wizardDtls').animate({ height: ConHeight });
					$(this).closest('.wizardProgress').find('.wizardDtls').find('.wizardContent').hide();
					$('#' + getRel).fadeIn(600);
				});
				$('.accdnTab:eq(0)').addClass('active');
				$('body').on('click', '.accdnTab', function () {
					$('.tab-pane').hide();
					$(this).addClass('active').siblings().removeClass('active');
					$(this).next('.tab-pane').slideDown().siblings('.tab-pane').slideUp();
				});

				$('.catTabHolder li:eq(0)').addClass('selected');
				$('.catPdtTabContent:eq(0)').show();
				$('body').on('click', '.catTabHolder li', function () {
					$('.catTabHolder li').removeClass('selected');
					$(this).addClass('selected');
					var getRel = $(this).attr('rel');
					// var ConHeight = $('#' + getRel).height() + 80; $('.catTabDtls').animate({ height: ConHeight });
					$('.catPdtTabContent').hide(); $('#' + getRel).fadeIn(600);
				});

				$('.pdtTabs li:eq(0)').addClass('selected');
				$('.tabContent:eq(0)').show();
				$('body').on('click', '.pdtTabs li a', function () {
					$('.pdtTabs li').removeClass('selected');
					$(this).parent('li').addClass('selected');
					var getRel = $(this).attr('rel');
					// var ConHeight = $('#'+getRel).height()+80;  $('.pdtTabDtls').animate({height:ConHeight});
					$('.tabContent').hide();
					$(getRel).fadeIn(600);
					$(getRel).find('.catTabHolder li:eq(0)').addClass('selected');
					$(getRel).find('.catPdtTabContent:eq(0)').show();
				});

				$('.accdnTab').on('click', function () {
					$('.tabContent').hide();
					$(this).next('.tabContent').slideDown().siblings('.tabContent').slideUp();
					$('.accdnTab').next('.tabContent').find('.catTabHolder li:eq(0)').addClass('selected');
					$('.accdnTab').next('.tabContent').find('.catPdtTabContent:eq(0)').show();

				});


				//-------------------- Mobile Nav -------------------------

				$('body').on('click', '.iconMobNav', function () {
					$('#pageContainer').toggleClass('leftAnimo');
				});

				if ($windowWidth > 1024) {
					$('#pageContainer').removeClass('leftAnimo');
				}


				$('body').on('click', '.iconMobNav', function () {
					$(this).toggleClass('open');
				});

				//---------------------- Mobile Scrolltop --------------------

				var currentScrollPos;
				$(".iconMobNav").on('click', function () {
					if ($(this).hasClass('activeMenu')) {
						$(this).removeClass('activeMenu');
						$(".mobNav").removeClass('menuShow');
						$('html,body').stop().animate({ scrollTop: currentScrollPos }, { queue: false, duration: 1000 });
					} else {
						$(this).addClass('activeMenu');
						currentScrollPos = $(window).scrollTop();
						$(".mobNav").addClass('menuShow');;
						$('html,body').stop().animate({ scrollTop: 0 }, { queue: false, duration: 1000 });
					}
				});
				$(".furnitureBlock a").on('click', function () {
					$('#furnitureModal').modal('toggle');

					setTimeout(function () {
						iScroll = new IScroll('#scrollWrapper', {
							mouseWheel: true,
							scrollbars: true
						});
					}, 500);
				});

				// --------------------- Equal Height -------------------------------

				var highestWrapper = 0;
				function equalHeight() {

					$('.equalHeight, .relatedPdtList .slick-slide').each(function () {
						if ($(this).outerHeight() > highestWrapper)
							highestWrapper = $(this).outerHeight();
					});

					$('.equalHeight,.relatedPdtList .slick-slide').each(function () {
						$(this).css("min-height", highestWrapper);
					});

				}


				var sideContentH = 0;
				function listingHeight() {
					sideContentH = $('.sidebarContent').outerHeight();
					$('.listingContent').css("height", sideContentH);
				}
				// var pdtPopularH  = 0;
				// function pdtPopularHeight() {
				// 	pdtPopularH = $('.pdtPopular').outerHeight();
				// 	$('.checkPopular').css("min-height", pdtPopularH);
				// }

				function catTabHeight() {
					var pdtContainerH = $('.ourProductsWrapper').outerHeight(),
						pdtHeadH = $('.ourProductsWrapper h2').outerHeight(),
						pdtTabsH = $('.ourProductsWrapper .pdtTabs').outerHeight(),
						tabTopH = pdtHeadH + pdtTabsH;
					catTabHolderH = pdtContainerH - tabTopH;
					$('.catTabHolder').css("min-height", catTabHolderH - 70);

				}

				// ----------------------- Home Animations -------------------------------


				var popularSliderRight = 0;
				function rightOffset() {


					var right = ($windowWidth - ($('.container').offset().left + $('.container').outerWidth()));
					var offsetVal = right - popularSliderRight;
					$('.pdtPopular').css('right', -(offsetVal));

				}


				$('body').on('click','.pdtCategoryList li', function () {
					$('.catTabDtls').removeClass('animated bounceInLeft');
					$('.ourProductsWrapper .pdtSlider ').removeClass('animated bounceInRight');
					$('.ourProductsWrapper .pdtSlider .pdtBlock').each(function (i) {
						$(this).addClass('animated zoomIn');
					});
					$(".ourProductsWrapper").addClass("sliderZoom");
				});



				$(window).load(function () {

					if ($('.homepage').size() > 0) {


						$('.furnitureBlock').each(function (i) {
							var getThis = $(this);
							setInterval(function () {
								getThis.removeClass('animated fadeOut').addClass('animated fadeIn');
								getThis.find('span ').removeClass('animated bounceOutLeft').addClass('animated bounceInLeft');
								getThis.find('a').removeClass('animated zoomOut').addClass('animated zoomIn');
							}, 300 * i);

						});

					}
					if ($('#scrollWrapper').size() > 0) {
						setTimeout(function () {
							iScroll = new IScroll('#scrollWrapper', {
								mouseWheel: true,
								scrollbars: true
							});
						}, 500);
					}

					if ($('.homepage').size() > 0) {
						$('.pageLogo').addClass('animated fadeOut');
						$('.logoText img').removeClass('animated fadeOutLeft').addClass('animated fadeInLeft');
						$('.navBar ').addClass('animated bounceOutUp');
						$('.furnitureBlock').removeClass('animated fadeOut');
						$('.furnitureBlock').find('span ').addClass('animated bounceOutLeft');
						$('.furnitureBlock').find('a').addClass('animated zoomOut');

						$('.pageLogo').removeClass('animated fadeOut ').addClass('animated fadeIn');
						$('.navBar ').removeClass('animated bounceOutUp').addClass('animated bounceInDown');

						$('.pdtPopularWrapper h2').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
						$(".pdtPopularWrapper .pdtPopular ").removeClass('animated bounceInRight').addClass('animated bounceOutRight');
						$('.ourProductsWrapper h2').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
						$('.ourProductsWrapper .pdtTabs').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
						$('.ourProductsWrapper .accdnTab').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
						$('.ourProductsWrapper .pdtCategoryList').removeClass('animated bounceInRight').addClass('animated bounceOutRight');
						$('.ourProductsWrapper .catTabDtls').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
						$('.ourProductsWrapper .btnViewAll').removeClass('animated zoomIn').addClass('animated zoomOut');

						$(".ourProductsWrapper .pdtSlider").removeClass('animated bounceInRight').addClass('animated bounceOutRight');
						$('.categoryWrapper h2').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
						$('.categoryWrapper .btnViewAll').removeClass('animated zoomIn').addClass('animated zoomOut');
						if ($windowWidth > 767) {
							$('.categoryWrapper .categoryBlock').removeClass('animated bounceInDown bounceInLeft').addClass('animated bounceOutUp');
							$('.categoryWrapper .categoryBlock').find('span').removeClass('animated bounceOutDown bounceInLeft').addClass('animated bounceInUp');

						} else {
							$('.categoryWrapper .categoryBlock').removeClass('animated bounceInLeft bounceInDown').addClass('animated bounceOutLeft');
							$('.categoryWrapper .categoryBlock').find('span').removeClass('animated bounceInLeft bounceInUp').addClass('animated bounceOutLeft');
						}


						// $('.paymentFeaturesWrapper').removeClass('animated fadeIn ').addClass('animated fadeOut');
						$('.paymentFeaturesWrapper .featuresHolder').removeClass('animated fadeIn ').addClass('animated fadeOut');
						$('.offerWrapper').removeClass('animated fadeIn ').addClass('animated fadeOut');
						$('.offerWrapper  h2').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
						$('.offerInputs p,.offerInputs .input-group').removeClass('animated fadeIn ').addClass('animated fadeOut');
						$('.offerInputs').removeClass('animated fadeIn ').addClass('animated fadeOut');
						$('.offerWrapper  figure').removeClass('animated bounceInRight').addClass('animated bounceOutRight');

						$('.footerLogo').removeClass('animated fadeIn ').addClass('animated fadeOut');
						$('.footerNav nav').removeClass('animated fadeIn ').addClass('animated fadeOut');
						$('.copyright').removeClass('animated fadeIn ').addClass('animated fadeOut');
						$('.socialMedia li').removeClass('animated fadeIn ').addClass('animated fadeOut');

					}



				});


				$(window).on('load resize', function () {

					setTimeout(function () {
						equalHeight();
						listingHeight();
						fixedHeader();
						rightOffset();
						if ($windowWidth >= 1440) {
							catTabHeight();
						}
					}, 300);

					
					if ($('.homepage').size() > 0) {
						var ourProductOffset = $('.ourProductsWrapper').offset().top-82;
						$('.pdtTabs li a').click(function () {
							
							$('html,body').stop().animate({ scrollTop: ourProductOffset  }, { queue: false, duration: 1000 });
							$.scrollify.update();
						});
						
					}

					if ($('.homepage').size() > 0) {

						$(window).scroll(function () {

							var wScroll = $(window).scrollTop(),
								wHeight = $(window).height() * 0.5,
								pdtPopular = $('.pdtPopularWrapper').offset().top - wHeight,
								ourProducts = $('.ourProductsWrapper').offset().top - wHeight,
								category = $('.categoryWrapper').offset().top - wHeight,
								offer = $('.offerWrapper ').offset().top - wHeight;
							if (wScroll >= pdtPopular) {


								if ($windowWidth > 767) {
									$('.ourProductsWrapper h2').removeClass('animated bounceOutLeft').addClass('animated bounceInLeft');
									$('.ourProductsWrapper .pdtTabs').removeClass('animated bounceOutLeft').addClass('animated bounceInLeft');
								}
								$('.pdtPopularWrapper h2').removeClass('animated bounceOutLeft').addClass('animated bounceInLeft')
								$(".pdtPopularWrapper .pdtPopular ").removeClass('animated bounceOutRight').addClass('animated bounceInRight');
							}
							else {

								if ($windowWidth > 767) {

									$('.ourProductsWrapper h2').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
									$('.ourProductsWrapper .pdtTabs').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
								}
								$('.pdtPopularWrapper h2').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
								$(".pdtPopularWrapper .pdtPopular ").removeClass('animated bounceInRight').addClass('animated bounceOutRight');



							}
							if (wScroll >= ourProducts) {

								if ($(".ourProductsWrapper ").hasClass('sliderZoom')) {
									$('.ourProductsWrapper h2').removeClass('animated bounceInLeft bounceOutLeft');
									$('.ourProductsWrapper .pdtTabs').removeClass('animated bounceInLeft bounceOutLeft');
									$('.ourProductsWrapper .accdnTab').removeClass('animated bounceInLeft bounceOutLeft');
									$('.ourProductsWrapper .pdtCategoryList').removeClass('animated bounceInRight bounceOutRight');
									$('.ourProductsWrapper .catTabDtls').removeClass('animated bounceInLeft bounceOutLeft');
									$('.ourProductsWrapper .btnViewAll').removeClass('animated zoomIn zoomOut');
									$(".ourProductsWrapper .pdtSlider ").removeClass('animated bounceInRight bounceOutRight');
								} else {
									if ($windowWidth <= 767) {
										$('.ourProductsWrapper h2').removeClass('animated bounceOutLeft').addClass('animated bounceInLeft');
										$('.ourProductsWrapper .pdtTabs').removeClass('animated bounceOutLeft').addClass('animated bounceInLeft');
										$('.ourProductsWrapper .accdnTab').removeClass('animated bounceOutLeft').addClass('animated bounceInLeft');
									}

									$('.ourProductsWrapper .pdtCategoryList').removeClass('animated bounceOutRight').addClass('animated bounceInRight');
									$('.ourProductsWrapper .catTabDtls').removeClass('animated bounceOutLeft').addClass('animated bounceInLeft');
									$('.ourProductsWrapper .btnViewAll').removeClass('animated zoomOut').addClass('animated zoomIn');
									$(".ourProductsWrapper .pdtSlider ").removeClass('animated bounceOutRight').addClass('animated bounceInRight');
								}


							}
							else {
								if ($windowWidth <= 767) {
									$('.ourProductsWrapper h2').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
									$('.ourProductsWrapper .pdtTabs').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
									$('.ourProductsWrapper .accdnTab').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
								}
								$(".ourProductsWrapper ").removeClass('sliderZoom');
								$('.ourProductsWrapper .pdtCategoryList').removeClass('animated bounceInRight').addClass('animated bounceOutRight');
								$('.ourProductsWrapper .catTabDtls').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
								$('.ourProductsWrapper .btnViewAll').removeClass('animated zoomIn').addClass('animated zoomOut');
							}
							if (wScroll >= category) {
								$('.categoryWrapper h2').removeClass('animated bounceOutLeft').addClass('animated bounceInLeft');
								$('.categoryWrapper .btnViewAll').removeClass('animated zoomOut').addClass('animated zoomIn');
								if ($windowWidth > 767) {
									$('.categoryWrapper .categoryBlock').each(function (i) {
										var getThis = $(this);
										setTimeout(function () {
											getThis.removeClass('animated bounceOutUp bounceOutLeft').addClass('animated bounceInDown');
											getThis.find('span').removeClass('animated bounceOutDown bounceOutLeft').addClass('animated bounceInUp');
										}, 300 * i);
									});
								}
								else {
									$('.categoryWrapper .categoryBlock').each(function (i) {
										var getThis = $(this);
										setTimeout(function () {
											getThis.removeClass('animated bounceOutLeft bounceOutUp').addClass('animated bounceInLeft');
											getThis.find('span').removeClass('animated bounceOutLeft bounceOutDown').addClass('animated bounceInLeft');
										}, 300 * i);
									});
								}

							}
							else {
								$('.categoryWrapper h2').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
								$('.categoryWrapper .btnViewAll').removeClass('animated zoomIn').addClass('animated zoomOut');
								if ($windowWidth > 767) {
									$('.categoryWrapper .categoryBlock').each(function (i) {
										var getThis = $(this);
										setTimeout(function () {
											getThis.removeClass('animated bounceInDown bounceInLeft').addClass('animated bounceOutUp');
											getThis.find('span').removeClass('animated bounceInUp bounceInLeft').addClass('animated bounceOutDown');
										}, 300 * i);
									});
								}
								else {
									$('.categoryWrapper .categoryBlock').each(function (i) {
										var getThis = $(this);
										setTimeout(function () {
											getThis.removeClass('animated bounceInLeft bounceInDown').addClass('animated bounceOutLeft');
											getThis.find('span').removeClass('animated bounceInLeft bounceInUp').addClass('animated bounceOutLeft');
										}, 300 * i);
									});
								}

							}

							if (wScroll >= offer) {
								//$('.paymentFeaturesWrapper').removeClass('animated fadeOut ').addClass('animated fadeIn');
								$('.paymentFeaturesWrapper .featuresHolder').each(function (i) {
									var getThis = $(this);
									setTimeout(function () {
										getThis.removeClass('animated fadeOut ').addClass('animated fadeIn');
									}, 200 * i);
								});
								$('.offerWrapper').removeClass('animated fadeOut ').addClass('animated fadeIn');
								$('.offerInputs').removeClass('animated fadeOut ').addClass('animated fadeIn');
								$('.offerWrapper  h2').removeClass('animated bounceOutLeft').addClass('animated bounceInLeft');
								$('.offerInputs p,.offerInputs .input-group').removeClass('animated fadeOut ').addClass('animated fadeIn');
								$('.offerWrapper  figure').removeClass('animated bounceOutRight').addClass('animated bounceInRight');
								$('.footerLogo').removeClass('animated fadeOut ').addClass('animated fadeIn');
								$('.footerNav nav').removeClass('animated fadeOut ').addClass('animated fadeIn');
								$('.copyright').removeClass('animated fadeOut ').addClass('animated fadeIn');
								$('.socialMedia li').each(function (i) {
									var getThis = $(this);
									setTimeout(function () {
										getThis.removeClass('animated fadeOut ').addClass('animated fadeIn');
									}, 200 * i);
								});

							}
							else {
								//$('.paymentFeaturesWrapper').removeClass('animated fadeIn ').addClass('animated fadeOut');
								$('.paymentFeaturesWrapper .featuresHolder').removeClass('animated fadeIn ').addClass('animated fadeOut');
								$('.paymentFeaturesWrapper .featuresHolder').removeClass('animated fadeIn ').addClass('animated fadeOut');
								$('.offerWrapper').removeClass('animated fadeIn ').addClass('animated fadeOut');
								$('.offerInputs').removeClass('animated fadeIn ').addClass('animated fadeOut');
								$('.offerWrapper  h2').removeClass('animated bounceInLeft').addClass('animated bounceOutLeft');
								$('.offerInputs p,.offerInputs .input-group').removeClass('animated fadeIn ').addClass('animated fadeOut');
								$('.offerWrapper  figure').removeClass('animated bounceInRight').addClass('animated bounceOutRight');
								$('.footerLogo').removeClass('animated fadeIn ').addClass('animated fadeOut');
								$('.footerNav nav').removeClass('animated fadeIn ').addClass('animated fadeOut');
								$('.copyright').removeClass('animated fadeIn ').addClass('animated fadeOut');
								$('.socialMedia li').each(function (i) {
									var getThis = $(this);
									setTimeout(function () {
										getThis.removeClass('animated fadeIn ').addClass('animated fadeOut');
									}, 200 * i);
								});

							}


						});
					}
				});



				// ----------------------- Home Animations End -------------------------------





				// ------------------------ Layout Grid --------------------------
				$('.layout li:eq(0)').addClass('selected');

				$('body').on('click', '.layout li', function () {
					$(this).addClass('selected').siblings().removeClass('selected');
					if ($(this).hasClass('list')) {
						$('.listingContent').addClass('listLayout');
					} else {
						$('.listingContent').removeClass('listLayout');
					}
				});
				// ------------------------- Accordion List Group-----------------
				$('body').on('click', '.accdnList .list-group > .list-group-item', function () {
					$(this).toggleClass('selected');
				});
				// ------------------------ Range Slider ----------------------------
				if ($('.rangeSlider').size() > 0) {

					$(".rangeSlider").slider({
						range: true,
						min: 10,
						max: 25,
						values: [15, 20],
						slide: function (event, ui) {
							$("#tooltipMin").text('$' + ui.values[0]);
							$("#tooltipMax").text('$' + ui.values[1]);

						},

						start: function (event, ui) {
							$(".tooltip").show();
						},
						stop: function (event, ui) {
							$(".tooltip").hide();
						}
					});

					$(window).load(function () {

						$(".rangeSlider").find('.ui-slider-handle').first().append('<em id="tooltipMin" class="tooltip" ></em>');
						$(".rangeSlider").find('.ui-slider-handle').last().append('<em id="tooltipMax" class="tooltip" ></em>');
						var min = $(".rangeSlider").slider("option", "min");
						var max = $(".rangeSlider").slider("option", "max");
						$('#min').text("$" + min);
						$('#max').text("$" + max);
					});
				}

				// ----------------- Product List Animation --------------------------------

				$('.productList').removeClass('selected');
				$('.pdtAddCart').removeClass('animated fadeIn ').addClass('animated fadeOut ');
				$('.topWrap').removeClass('animated fadeInDown ').addClass('animated fadeOutUp ');
				$('.bottomWrap').removeClass('animated fadeInUp ').addClass('animated fadeOutDown');
				$('.productList').on('mouseover', function () {
					$(this).addClass('selected').siblings().removeClass('selected');
					$(this).find('.pdtAddCart').removeClass('animated fadeOut ').addClass('animated fadeIn');
					$(this).find('.topWrap').removeClass('animated fadeOutUp ').addClass('animated fadeInDown ');
					$(this).find('.bottomWrap').removeClass('animated fadeOutDown ').addClass('animated fadeInUp ');
				}).on('mouseleave', function () {
					$(this).removeClass('selected');
					$(this).find('.pdtAddCart').removeClass('animated fadeIn ').addClass('animated fadeOut ');
					$(this).find('.topWrap').removeClass('animated fadeInDown ').addClass('animated fadeOutUp ');
					$(this).find('.bottomWrap').removeClass('animated fadeInUp ').addClass('animated fadeOutDown');
				});

				// ------------------ Form Inputs ----------------------

				var quantitiy = 0;
				$('.quantity-right-plus').click(function (e) {
					e.preventDefault();
					quantity = parseInt($('#quantity').val());

					$('#quantity').val(quantity + 1);

				});

				$('.quantity-left-minus').click(function (e) {
					e.preventDefault();
					quantity = parseInt($('#quantity').val());

					if (quantity > 0) {
						$('#quantity').val(quantity - 1);
					}
				});

				if ($('.paymentWizard').size() > 0) {


					$(' a[data-toggle="tab"]').on('show.bs.tab', function (e) {

						var $target = $(e.target);

						if ($target.hasClass('disabled')) {
							return false;
						}
					});

					$(".next-step").click(function (e) {
						var $active = $('.nav-tabs li a.active');
						var $activeLi = $active.parent();
						var id = $activeLi.next().find('a').attr('id');

						$activeLi.next().find('a').removeClass('disabled');
						nextTab($activeLi);

					});
					$(".prev-step").click(function (e) {
						var $active = $('.nav-tabs li a.active');
						var $activeLi = $active.parent();

						prevTab($activeLi);

					});


					function nextTab(elem) {
						$(elem).next().find('a[data-toggle="tab"]').click();
					}
					function prevTab(elem) {
						$(elem).prev().find('a[data-toggle="tab"]').click();
					}

				}

				$('.imgMagnify').on('mousemove', function () {
					$('.zoomContainer').css('z-index', 5);
				});
				$('.pdtDtlsWrapper').on('mousemove', function () {
					$('.zoomContainer').css('z-index', 0);
				})


				$('.form-upload .uploadEdit').click(function(){
					$(this).closest('.form-upload').find('input[type="file"]').trigger('click');
				});
				$('.form-edit .editInput').click(function(){
					$(this).closest('.form-edit').find('input').removeAttr('readonly');
					// $(this).remove();
				});



			},

			html5Tags: function () {
				document.createElement('header');
				document.createElement('section');
				document.createElement('nav');
				document.createElement('footer');
				document.createElement('menu');
				document.createElement('hgroup');
				document.createElement('article');
				document.createElement('aside');
				document.createElement('details');
				document.createElement('figure');
				document.createElement('time');
				document.createElement('mark');
			},

			commonInput: function () {

				//ajax form submit with file upload
				/*function jqContact() {
					$('.getOverlay').fadeIn();
					var $form = $('#UpdateForm'); // set your form ID
					jQuery(document).ready(function($) {
						var formData = new FormData($('form#UpdateForm')[0]);
						$.ajax({
							type: 'POST',
							url: $form.attr('action'),
							cache: false,
						    contentType: false,
						    processData: false,
							data: formData,
							success: function (msg) {
								submitcount43122 = 0;
								window.location.reload();
							},
							error: function (msg) {
								alert('Update error.');
								submitcount43122 = 0;
								$('.getOverlay').fadeOut();
							}
						 });
					});
					
				}*/

				var $inputText = $('.queryInput input, .queryInput textarea');
				$inputText.each(function () {
					var $thisHH = $(this);
					if (!$(this).val()) {
						$(this).parent().find('label').show();
					} else {
						setTimeout(function () {
							$thisHH.parent().find('label').hide();
						}, 100);
					}

				});
				$inputText.focus(function () {
					if (!$(this).val()) {
						$(this).parent().find('label').addClass('showLab');
					}
				});
				$inputText.keydown(function () {
					if (!$(this).val()) {
						$(this).parent().find('label').hide();
					}
				});
				$inputText.on("blur", function () {
					var $thisH = $(this);
					if (!$(this).val()) {
						$(this).parent().find('label').show().removeClass('showLab');
					} else {
						$thisH.parent().find('label').hide();
					}

				});

			}

		}//end commonJS

	};


	customJS.common.commonJS();
	customJS.common.html5Tags();
	customJS.common.commonInput();

});
