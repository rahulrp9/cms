$(document).ready(function(){


$('.treeList li > span').click(function(){

$(this).parent('li').toggleClass('open');
if($(this).parent('li').hasClass('open')){
$(this).parent('li').find('>ul').slideDown();
        }else{
$(this).parent('li').find('>ul').slideUp();
        }
    });



$('body').on('click','.variantAddWrapper .btn-add-new',function(){
$(this).closest('.variantAddWrapper').find('.addNewVariant').slideDown();
    });
$('body').on('click','.addNewVariant .close',function(){
$(this).closest('.addNewVariant').slideUp();
    });


// $('.colourPicker').ColorPicker({
    //     color: '#0000ff',
    //     onShow: function (colpkr) {
    //         $(colpkr).fadeIn(500);
    //         return false;
    //     },
    //     onHide: function (colpkr) {
    //         $(colpkr).fadeOut(500);
    //         return false;
    //     },
    //     onChange: function (hsb, hex, rgb) {
    //         $('.colourPicker span').css('backgroundColor', '#' + hex);
    //         $('.colorInput input').val(hex);
    //     }
    // });'
    
    function colorPicker(){       
$(".picker").spectrum({
showPaletteOnly: true,
// togglePaletteOnly: true,
                // togglePaletteMoreText: 'more',
                // togglePaletteLessText: 'less',
                color: 'blanchedalmond',
palette: [
                    ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
                    ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
                    ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
                    ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
                    ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
                    ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
                    ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
                    ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
                ]
        });

$(".picker").change(function(){
var colorVal = $(this).spectrum("get");
$(this).closest('.colorHolder').find('.colorInput').val(colorVal.toHexString());
        });
    }
colorPicker();       

$('body').on('click','.variantPanel .btn-add-more',function(){
var $nxtVariant = $(this).closest('.variantPanel').find('.addVariantWrapper:eq(0)').clone();
$nxtVariant.find('input').val('');
$(this).closest('.variantPanel').find('.addVariantOptions').append($nxtVariant);


    });
$('body').on('click','.colorPanel .btn-add-more',function(){
var $nxtVariant = '<div class="addVariantWrapper clearfix">'+
'<div class="row m-0">'+
'<div class="colorHolder col-sm-12 ">'+
'<div class="colourPicker"><input type="text" class="picker" /></div>'+
'<div class="colorCode">Colour Code: <input type="text" class="form-control colorInput"></div>'+
'</div>'+
'<div class="form-group col-sm-6">'+
'<label>English</label>'+
'<input type="email" class="form-control colorNameEng" placeholder="Enter Size in English">'+
'</div>'+
'<div class="form-group col-sm-6">'+
'<label>Arabic</label>'+
'<input type="email" class="form-control" placeholder="Enter Size in Arabic">'+
'</div>'+
'</div>'+
'</div>'
            $(this).closest('.colorPanel').find('.addVariantOptions').append($nxtVariant);

colorPicker();   

    });        


});