var customJS;

jQuery(document).ready(function ($) {
    ! function (s) {
        "use strict";
        s("#sidebarToggle, #sidebarToggleTop").on("click", function (e) { s("body").toggleClass("sidebar-toggled"), s(".sidebar-left").toggleClass("toggled"), s(".sidebar-left").hasClass("toggled") && s(".sidebar-left .collapse").collapse("hide") }), s(window).resize(function () { s(window).width() < 768 && s(".sidebar-left .collapse").collapse("hide"), s(window).width() < 480 && !s(".sidebar-left").hasClass("toggled") && (s("body").addClass("sidebar-toggled"), s(".sidebar-left").addClass("toggled"), s(".sidebar-left .collapse").collapse("hide")) }), s("body.fixed-nav .sidebar-left").on("mousewheel DOMMouseScroll wheel", function (e) {
            if (768 < s(window).width()) {
                var o = e.originalEvent,
                    l = o.wheelDelta || -o.detail;
                this.scrollTop += 30 * (l < 0 ? 1 : -1), e.preventDefault()
            }
        }), s(document).on("scroll", function () { 100 < s(this).scrollTop() ? s(".scroll-to-top").fadeIn() : s(".scroll-to-top").fadeOut() }), s(document).on("click", "a.scroll-to-top", function (e) {
            var o = s(this);
            s("html, body").stop().animate({ scrollTop: s(o.attr("href")).offset().top }, 1e3, "easeInOutExpo"), e.preventDefault()
        })
    }(jQuery);

    customJS = {

        common: {
            commonJS: function () {

                $(function () {
                    $(".datepicker1").datepicker({
                        autoclose: true,
                        todayHighlight: true,
                        format: "yyyy-mm-dd"
                    }).datepicker('update', new Date());
                });

                $(function () {
                    $(".datepicker2").datepicker({
                        autoclose: true,
                        todayHighlight: true,
                        format: "yyyy-mm-dd"
                    }).datepicker('update', new Date());
                });

                $(function () {
                    $('#datetimepicker').datetimepicker({
                        language: 'pt-BR'
                    });
                });

                $(function () {
                    $('#datetimepicker1').datetimepicker({
                        language: 'pt-BR'
                    });
                });

                /*$('.checkradios').checkradios();*/
                $(function () {

                    $("table").ready(function () {
                        //$("table").addClass("table-responsive");
                    });
                });

            },

            html5Tags: function () {
                document.createElement('header');
                document.createElement('section');
                document.createElement('nav');
                document.createElement('footer');
                document.createElement('menu');
                document.createElement('hgroup');
                document.createElement('article');
                document.createElement('aside');
                document.createElement('details');
                document.createElement('figure');
                document.createElement('time');
                document.createElement('mark');
            }

        } //end commonJS

    };


    customJS.common.commonJS();
    customJS.common.html5Tags();

});