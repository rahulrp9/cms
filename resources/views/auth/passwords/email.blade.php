
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Eventeam | Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="{{ asset('backend/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/styles.css')}}" rel="stylesheet" type="text/css" />
</head>

<body>
    <div class="login d-flex">
        <div class=" col-lg-4 col-md-4 mx-auto login-center">
            <div class="content">
                <form class="login-form" method="POST" action="{{ route('password.email')  }}">
                    @csrf
                    <div class="logo text-center">
                        <a href="{{ route('index') }}"> <img src="{{ asset('backend/assets/images/logo.png')}}" alt="logo" /> </a>
                    </div>
                    <h6 class="form-title text-center">
                        Reset Password
                        </h3>
                        <div class="form-group">
                            <input id="email" type="email" class="form-control envelope @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="User Name">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-block">{{ __('Send Password Reset Link') }}</button>
                        </div>
                         <div class="form-actions">
                        <button type="submit" class="btn btn-defualt btn-block"><a
                                          href="{{ route('login') }}">Back</a></button>
                       </div>
                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset('backend/assets/js/jquery-3.3.1.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('backend/assets/js/bootstrap.min.js')}}" type="text/javascript"></script>
</body>

</html>