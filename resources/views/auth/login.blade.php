<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>FoodApp | Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="{{ asset('backend/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/styles.css')}}" rel="stylesheet" type="text/css" />
</head>
<style type="text/css">
    .login{
background-size: cover;
    }
</style>
<body>
    <div class="login d-flex">
        <div class=" col-lg-4 col-md-4 mx-auto login-center">
            <div class="content">
                <form class="login-form" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="logo text-center">
                       
                    </div>
                    <h6 class="form-title text-center">
                        Let’s start with Log in!
                        </h3>
                        <div class="form-group">
                            <input id="email" type="email" class="form-control envelope @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="User Name">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <input id="password" type="password" class="form-control password @error('password') is-invalid @enderror" name="password" required
                            autocomplete="current-password" placeholder="Password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div style="display: table;width: 100%;" >
                            <div style="display: table-row; margin-left: auto; margin-right: auto;">
                                <div style="display: table-cell;vertical-align: top;">
                                <input type="checkbox" name="remember_me" id="remember_me" class="" >
                                </div>
                                <div style="display: table-cell; vertical-align: top;">
                                Remember me
                                </div>
                            </div>
                        </div>
                      
                        <div class="form-actions">
                            <button type="submit" class="btn btn-block">Login</button>
                        </div>
                        <div class="form-actions d-flex "> <a href="{{ route('password.request') }}" class="forget-password">Forgot Password?</a> 
                            <!-- <a href="javascript:void(0);" class="register">Register</a>  --></div>
                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset('backend/assets/js/jquery-3.3.1.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('backend/assets/js/bootstrap.min.js')}}" type="text/javascript"></script>
</body>

</html>