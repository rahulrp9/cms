@extends('layouts.app')
@section('content')
<style type="text/css">
    .box .row{
         margin-bottom: 15px;
    }
</style>
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
    <div class="row">
    <div class="col-md-12">
            <div class="col-md-6">
                <h1 class="box-title">Order Details  </h1>
            </div>    
    <div class="pull-right box-tools">
                    <a href="{{URL::to('dashboard/orders')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-default" type="button" data-original-title="Service List">
                            <i class="fa fa-list"></i>
                        </button>
                    </a>               
                </div>
    </div>            
    </div>
        <div class="box">
            <div class="box-body pad" id="tab_1">
                <div class="row">
                    <label class="col-sm-3 control-label">Order Id</label>
                    <div class="col-sm-6">
                        {{ $order->id }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">User Name</label>
                    <div class="col-sm-3">
                        {{ $order->name }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Delivery Type</label>
                    <div class="col-sm-3">
                        {{ $order->delivery_type }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Order Amount</label>
                    <div class="col-sm-3">
                        {{ $order->amount }}
                    </div>
                </div>
                <form id="formSpace" action="{{ URL::to('dashboard/orders') }}/{{ $order->id }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    {!! csrf_field() !!}
                    @if(isset($order))
                        {{ method_field('PUT') }}
                        <input type="hidden" name="service_id" value="{{$order->id}}"/>
                    @endif
                <div class="row">
                    <label class="col-sm-3 control-label">Order Status</label>
                    <div class="col-sm-3">
                        <select name="statusselect" class="statusselect">
                            
                            <option value="1" @if($order->ostatus == 1) selected @endif>Not Processed</option>
                            <option value="2" @if($order->ostatus == 2) selected @endif>Processed</option>
                            <option value="3" @if($order->ostatus == 3) selected @endif>Complete</option>
                        </select>
                    </div>
                </div>
            </form>
              
        </div>
    </div>

<script type="text/javascript">
    
    $('.statusselect').change(function(){

       var statval = $(this).val();
       $('#formSpace').submit();
    });
</script>
@endsection