@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{ URL::asset('css/custom_style2.css')}}">
<style type="text/css">
    a {
    color: #3c8dbc;
    padding: 5px;
}
</style>
<div class="contentHolderV1">
    <h2>Orders</h2>
    <div class="row">
          <div class="col-md-6"> 
           <!--  <a href="{{ URL::to('deleted/services') }}" class="searchbtn pull-right">Deleted Services</a> -->
        </div>
       <!--  <div class=" pull-right" style="float: right;"> 
             <a href="{{ URL::to('dashboard/coupons/create') }}" class="searchbtn">Add Coupons</a>
        </div> -->
      
    </div>    
   
    <div class="tableHolder">
        <table style="width: 100%;" class="tableV1" cellpadding="0" cellspacing="0">
            <thead class="tableHeader">
                <tr>
                    <td>#</td>
                    <td>Order Id</td>
                    <td>Shop Name</td>
                    <td>User Name</td>
                    <td>Order Value</td>
                    <td>Order Status</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
               
               @foreach($orders as $service)
               <?php  //$n++; ?>

                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $service->id }}</td>
                        <td>{{$service->shopname}}</td>
                        <td>{{$service->name}}</td>
                        <td>{{$service->amount}}</td>
                        <td>
                            
                            @if($service->ostatus == 1)Not Processed @endif
                            @if($service->ostatus == 2)Processed @endif
                            @if($service->ostatus == 3)Complete @endif
                        </td>
                        <td><div  class="actions">
                                    <!-- <a href="{{ URL::to('dashboard/coupons/'.$service->id.'/edit') }}" class="edit">
                                        <i class="fa fa-edit"></i>
                                    </a> -->
                                    <a href="{{ URL::to('dashboard/orders', ['id' => ($service->id)]) }}" class="view">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <!-- <form action="{{ route('admin.coupons.destroy', $service->id) }}" method="post" style="float: left;">
                                         {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                                        <a class="delete" data-id=""><i class="fa fa-trash-alt"></i></a>
                                           
                                         </form> -->
                                    
                                </div></td>
                       
                    </tr>
                    
                     @endforeach
            </tbody>
        </table>
    </div>
<ul class="pagination">
        {{ $orders->render() }}
    </ul>
</div>
<script type="text/javascript">
    
    $(function () {
        $('.delete').click(function(e){
            e.preventDefault();
            if(confirm('Are you sure you want to delete?'))
            {
                $(this).parent('form').submit();
            }else{
                return false;
            }
        });
});

    
</script>
@endsection