<ul>
@foreach($childs as $child)
 <?php  
                if ($child->status == 1) {
                                $cstatus =  "Active";
                                $class   = "active";
                            } else {
                                  $cstatus = "Inactive";
                                  $class   = "inactive";
                            }
               ?>
                <li><a href="javascript:void(0);">{{ $child->name }}<i><img src="<?php echo url('/');?>{{$child->image}}"></i></a><div  class="actions"><span class="status {{$class}}">{{$cstatus}}</span><a href="{{ URL::to('dashboard/viewcat', ['id' => ($child->id)]) }}" class="add"><i class="fa fa-eye"></i></a><a href="{{ URL::to('dashboard/delcat', ['id' => ($child->id)]) }}" class="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a><a href="{{ URL::to('dashboard/editcat', ['id' => ($child->id)]) }}" class="edit"><i class="fa fa-edit"></i></a></div>
                  @if(count($child->childs))
                                    @include('admin/manageChild',['childs' => $child->childs])

                                @endif

                </li>
             @endforeach
              </ul>
              

