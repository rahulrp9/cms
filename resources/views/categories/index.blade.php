@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{ URL::asset('css/custom_style2.css')}}">
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"> -->
<script src="{{ URL::asset('js/custom_script.js')}}"></script>
<style type="text/css">
    .sepbtn{
        float: right;
        padding: 10px;
    }
</style>
<div class="contentHolderV1d">
 <div class="row">
    <div class="col-md-6 pull-left">
    <h2>
        Category Management
      </h2>
      </div>
      <div class="col-md-6">
      <div class="pull-right box-tools">
        <a href="{{ URL::to('dashboard/categories/add') }}">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-success" type="button" data-original-title="Add Category">
                            <i class="fa fa-plus"></i>
                        </button>
                    </a>
        </div>            
      </div>
       
       </div>
    
   
    <div class="tableHolder" style="margin-top: 50px;">
        <table style="width: 100%;" class="tableV1" cellpadding="0" cellspacing="0">
            <thead class="tableHeader">
                <tr>
                   <!--  <td>#</td> -->
                    <td>Category Name</td>
                    <!-- <td>Status</td>
                    <td class="actionHolder">Actions</td> -->
                </tr>
            </thead>
            <tbody>
                <?php $n = $categories->perPage() * ($categories->currentPage() - 1);  ?>
               @foreach($categories as $category)
               <?php  $n++; ?>

                    <tr>
                        <!-- <td>{{$n}}</td> -->
                        <td><span class="treeList">
                        <ul>
                      
                            <li>
                                <span></span>
                                @php 
                                    $url = url('/');
                                    $nurl = str_replace("index.php","",$url);
                                @endphp    
                                <a href="javascript:void(0);">{{ $category->name }}<i><img src="{{$nurl}}{{$category->image}}"></i> </a>
                                <div  class="actions">
                                    <span class="status"> 
                                      <!-- <select id="pending" data-pdtid="{{$category->id}}" class="form-control">
                                        <option value="2" @if($category->status == 2) selected='selected' @endif>Hidden</option>
                                        <option value="1" @if($category->status == 1) selected='selected' @endif>Active</option>
                                        <option value="0" @if($category->status == 0) selected='selected' @endif>Inactive</option>
                                    </select> -->
                                    <!-- @if($category->status == 1)
                                     <button data-pdtid="{{$category->id}}" class="publish btn-primary">Active</button> @else <button data-pdtid="{{$category->id}}" class="unpublish btn-primary">InActive</button> 
                                     @endif -->
                                      </span>
                                    <a href="{{ URL::to('dashboard/viewcat', ['id' => ($category->id)]) }}" class="add btn btn-success btn-sm"><i class="fa fa-eye"></i></a>
                                    <a href="{{ URL::to('dashboard/delcat', ['id' => ($category->id)]) }}" class="delete btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                                    <a href="{{ URL::to('dashboard/editcat', ['id' => ($category->id)]) }}" class="edit btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                </div>
                                @if(count($category->childs))
                                    @include('categories/manageChild',['childs' => $category->childs])

                                @endif

              
            </li>

            </ul></span>
            </td>
                    
                       
                    </tr>
                    
                     @endforeach
            </tbody>
        </table>
    </div>
    <ul class="pagination">
        {!! $categories->render() !!}
    </ul>
</div>

<script type="text/javascript">
    
    /*$(document).on('click', '.publish', function (){

        var pdtid = $(this).attr('data-pdtid');
        $.ajax({
                type: 'get',
                url: '<?php echo url('/');?>/admin/category/unpublish',
                data: 'pdtid='+pdtid,
                datatype: "json",
                async: false,
                cache: false,
                timeout: 30000,
                    success: function (response) {
                        location.reload();
                    }
        });


    });


    $(document).on('click', '.unpublish', function (){

        var pdtid = $(this).attr('data-pdtid');
        $.ajax({
                type: 'get',
                url: '<?php echo url('/');?>/admin/category/publish',
                data: 'pdtid='+pdtid,
                datatype: "json",
                async: false,
                cache: false,
                timeout: 30000,
                    success: function (response) {
                        location.reload();
                    }
        });


    });*/
        $(document).on('change', '#pending', function (){
            if(confirm("Are you sure?")){
                var pdtid = $(this).attr('data-pdtid');
                $pending=$(this).val();
                if($pending==1)
                    $url= '<?php echo url('/');?>/admin/category/publish';
                else if($pending==2)
                    $url= '<?php echo url('/');?>/admin/category/hidden';
                 else
                    $url= '<?php echo url('/');?>/admin/category/unpublish';
                $.ajax({
                    type: 'get',
                    url: $url,
                    data: 'pdtid='+pdtid,
                    datatype: "json",
                    async: false,
                    cache: false,
                    timeout: 30000,
                    success: function (response) {
                        location.reload();
                    }
                });
            }else{
                return false;
            }
        });


</script>
@endsection