@extends('layouts.app')
@section('content')
<style type="text/css">
    .col-md-6 {
    float: left;
    width: 50%;
}
</style>
<script src="{{ URL::asset('js/spectrum.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('css/spectrum.css')}}">
<link rel="stylesheet" href="{{ asset('css/custom_style_product.css')}}">
<div class="contentHolderV1">
<div class="row">
<div class="col-md-6">
    <h1>Edit Category</h1>
    </div>
    <div class="col-md-6">
    <div class="pull-right box-tools">
                    <a href="{{url('dashboard/categories')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-default" type="button" data-original-title="List Categories">
                            <i class="fa fa-list"></i>
                        </button>
                    </a>
    </div> 
    </div>
    </div>               
        <form method="post" action="{{ url('dashboard/updatecat') }}" id="addcategoryform" enctype="multipart/form-data" class="formTypeV1">
            {{ csrf_field() }}
            <input type="hidden" id="base_path" value="<?php echo url('/');?>">
            <input type="hidden" name="id" id="category_id" value="{{$cats->id}}">
            <input type="hidden" name="hidimage" id="hidimage" value="{{$cats->image}}">
            <div class="row">
                <div class="col-lg-6">
                    <label>Category Name English</label>
                    <div class="form-group">
                        <input class="form-control" type="text" name="cat_name_en" id="cat_name_en" value="{{$cats->name}}">
                        <div class="customClear"></div>
                        <div class="tag_en_error error" style="color: red;"></div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <label>Category Image</label>
                    <div class="form-group">
                        @php 
                                    $url = url('/');
                                    $nurl = str_replace("index.php","",$url);
                                @endphp 
                        <input class="form-control mb-2" type="file" name="image" id="image">
                        <img src="{{$nurl}}{{$cats->image}}" style="width:150px;height:150px;">
                        <div class="customClear"></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label>Status</label>
                    <div class="form-group">
                        <select class="form-control" name="cat_status" id="cat_id">
                            <option value="">-- Select Status--</option>
                            <option value="0" <?php if($cats->status == 0){echo "selected";}?>> Inactive</option>
                            <option value="1" <?php if($cats->status == 1){echo "selected";}?>> Active</option>
                            <option value="2" <?php if($cats->status == 2){echo "selected";}?>> Hidden</option>
                        </select>
                        <div class="customClear"></div>
                    </div>
                </div>
                
            </div>
            <div class="col-md-12 text-center" style="margin-bottom: 20px;">
                    <input class="commonButton btn btn-default btn-info" id="saveCategory" type="submit" value="Update">

                </div>
        </form>



            <!-- <input class="commonButton" id="saveCategory" type="button" value="SAVE">  -->

</div>






<script>
    $(document).ready(function () {





        /*$("#saveCategory").click(function (event) {
            event.preventDefault();
            var base_path = $('#base_path').val();
            var tag_en = $('#cat_name_en').val();
            var tag_ar = $('#cat_name_ar').val();
            var category_id = $('#category_id').val();
            var error = 0;
            var errors2 = 0;
            if (tag_en == '') {
                $('.tag_en_error').html('Please Enter English Category Name');
                errors = 1;
                return false;
            }    
            if (tag_ar == '') {
                $('.tag_ar_error').html('Please Enter Arabic Category Name');
                errors = 1;
                return false;
            }
            $.ajax({
                    type: 'get',
                    url: base_path+'/dashboard/checkcatname',
                    data: 'tag_en='+tag_en+'&tag_ar='+tag_ar+'&category_id='+category_id,
                    datatype: "json",
                    async: false,
                    cache: false,
                    timeout: 30000,
                    success: function (response) {
                            //console.log(response.Result.response.catar);
                            if (response == 1) {
                                $('.tag_en_error').html('This Category name is already exist!');
                                errors = 1;
                            }
                            else if (response == 2) {
                                $('.tag_ar_error').html('This Category name is already exist!');
                                errors = 1;
                            } else {
                               errors = 0;
                               $('.categoryNameError').html('');
                           }
                       }
                   });




                if (errors == 1)
                {
                    return false;
                }

                else
                {
                //return false;
                $( "#addcategoryform" ).submit();
            }
        });*/


    });


</script>
@endsection
