@extends('layouts.app')
@section('content')
<style type="text/css">
    .col-md-6 {
        float: left;
        width: 50%;
    }
</style>
<script src="{{ URL::asset('js/spectrum.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('css/spectrum.css')}}">
<link rel="stylesheet" href="{{ asset('css/custom_style_product.css')}}">
<div class="contentHolderV1">
    <h1>Add Category</h1>
 <div class="pull-right box-tools">
                    <a href="{{url('dashboard/categories')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-default" type="button" data-original-title="List Categories">
                            <i class="fa fa-list"></i>
                        </button>
                    </a>
                    </div>
    <form method="post" action="{{ url('dashboard/savecategory') }}" id="addcategoryform" enctype="multipart/form-data" class="formTypeV1">
        {{ csrf_field() }}
        <input type="hidden" id="base_path" value="<?php echo url('/');?>">
        <div class="row">
            <div class="col-lg-10">
                <label for="inputEmail3" class="col-sm-6 form-control-label">Select Parent Category</label>

                    <div class="form-group">
                        @if($catid)
                        <select name="pcategory" class="form-control" disabled="">
                           @foreach($categories as $catgeory)
                        
                           <option value="{{$catgeory->id}}" <?php if($catid == $catgeory->id) echo "selected";?>>{{$catgeory->name}}</option>
                           @endforeach    
                                           </select>
                                           <input type="hidden" name="pcategory" value="{{$catid}}">
                                           @else
                                           <select name="pcategory" class="form-control">
                          <option value="0">Root Category</option>
                              <!-- @foreach($categories as $catgeory)
                        
                                <option value="{{$catgeory->id}}">{{$catgeory->cat_name_en}}</option>
                        
                                @endforeach -->
                            </select>
                            @endif
                        </div>
                        <div class="customClear"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10">
                <label or="inputEmail3" class="col-sm-6 form-control-label">Category Name English</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="cat_name_en" id="cat_name_en">
                </div>
                <div class="customClear"></div>
                <div class="tag_en_error error" style="color: red;"></div>
            </div>
             <!-- <div class="col-lg-10">
                <label for="inputEmail3" class="col-sm-6 form-control-label">Category Description English</label>
                <div class="form-group">
                    <textarea class="form-control" name="des_en"></textarea>
                </div>
                <div class="customClear"></div>
                <div class="tagd_ar_error error"></div>
            </div> -->
<!--             <div class="col-lg-6">
                <label for="inputEmail3" class="col-sm-4 form-control-label">Category Name Arabic</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="cat_name_ar" id="cat_name_ar">
                </div>
                <div class="customClear"></div>
                <div class="tag_ar_error error" style="color: red;"></div>
            </div> -->
           
           <!--  <div class="col-lg-6">
                <label for="inputEmail3" class="col-sm-4 form-control-label">Category Description Arabic</label>
                <div class="form-group">
                    <textarea name="des_ar" class="form-control"></textarea>
                </div>
                <div class="customClear"></div>
                <div class="tagd_ar_error error"></div>
            </div> -->
            <!-- <div class="col-lg-6">
                <label>Status</label>
                <div class="form-group">
                    <select class="form-control" name="cat_status" id="cat_id">
                        <option value="">-- Select Status--</option>
                        <option value="0"> Inactive</option>
                        <option value="1"> Active</option>
                        <option value="2"> Hidden</option>
                    </select>
                    <div class="customClear"></div>
                    <div class="tag_en_error error"></div>
                </div>
            </div> -->
            <div class="col-lg-6">
                <label for="inputEmail3" class="col-sm-4 form-control-label">Category Image</label>
                <div class="form-group">
                    <input type="file" class="form-control" name="image" id="image">
                </div>
                <p class="help-block" style="color: red">Recommended size 500*330</p>
                <div class="customClear"></div>
                <div class="categoryImageError error"></div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <input class="commonButton btn btn-default btn-info" id="saveCategory" type="submit" value="SAVE">
                </div>
            </div>
        </div>
            </div>
</form>
     </div>
              <?php /*  <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-4 form-control-label">Category Color</label>
                   <input type="text" name="togglePaletteOnly" id="togglePaletteOnly">
                   <input type="hidden" name="hidcolor" class="hidcolor" value="">
                    <div class="customClear"></div>
                    <div class="categoryImageError error"></div>
                                            </div>    */ ?>
                                                                                                                                                                                                                                   

 



</div>  

<div class="customClear"></div>
</div>

<script>
    $(document).ready(function () {

        $("#togglePaletteOnly").spectrum({
            showPaletteOnly: true,
            showPalette: true,
            color: 'blanchedalmond',
            palette: [
            ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
            ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
            ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
            ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
            ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
            ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
            ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
            ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
            ]
        });



        /*$("#saveCategory").click(function (event) {
            event.preventDefault();
            var base_path = $('#base_path').val();
            var tag_en = $('#cat_name_en').val();
            var tag_ar = $('#cat_name_ar').val();
            // var color = $("#togglePaletteOnly").spectrum('get').toHexString();
            //
            // $('.hidcolor').val(color);
            var error = 0;
            var errors2 = 0;
            if (tag_en == '') {
                $('.tag_en_error').html('Please Enter English Category Name');
                errors = 1;
                return false;
            }    
            if (tag_ar == '') {
                $('.tag_ar_error').html('Please Enter Arabic Category Name');
                errors = 1;
                return false;
            } 

                //$('.categoryNameError').html('');
                $.ajax({
                    type: 'get',
                    url: base_path+'/admin/checkcatname',
                    data: 'tag_en='+tag_en+'&tag_ar='+tag_ar,
                    datatype: "json",
                    async: false,
                    cache: false,
                    timeout: 30000,
                    success: function (response) {
                            //console.log(response.Result.response.catar);
                            if (response == 1) {
                                $('.tag_en_error').html('This Category name is already exist!');
                                errors = 1;
                            }
                            else if (response == 2) {
                                $('.tag_ar_error').html('This Category name is already exist!');
                                errors = 1;
                            } else {
                               errors = 0;
                               $('.categoryNameError').html('');
                           }
                       }
                   });




                if (errors == 1)
                {
                    return false;
                }

                else
                {
                //return false;
                $( "#addcategoryform" ).submit();
            }
        });*/


    });


</script>
@endsection
