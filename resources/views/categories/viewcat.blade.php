@extends('layouts.app')
@section('content')
<style type="text/css">
    .col-md-6 {
    float: left;
    width: 50%;
}
.alert-warning{
    background-color: #fcf8e3 !important;
    color: #8a6d3b !important;
}
</style>
<script src="{{ URL::asset('js/spectrum.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('css/spectrum.css')}}">

<div class="contentHolderV1">
<div class="row"> 
<div class="col-md-6">
    <h1>View Category Details</h1>
    </div>

    <div class="col-md-6 ">
     <div class="pull-right box-tools">
                    <a href="{{url('dashboard/categories')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-default" type="button" data-original-title="List Categories">
                            <i class="fa fa-list"></i>
                        </button>
                    </a>
                    @if($ccount < 15)
                    <a href="{{url('dashboard/categories/add/'.$catid)}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-success" type="button" data-original-title="Add Category">
                            <i class="fa fa-plus"></i>
                        </button>
                    </a>
                    @else
    
    
        <div class="alert alert-warning">
          <strong>Warning!</strong> Already exceed the limit for adding Sub-Category!
        </div>
    @endif
                    <a href="{{ url('dashboard/editcat', ['id' => ($catid)]) }}">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-info" type="button" data-original-title="Edit Category">
                            <i class="fa fa-edit"></i>
                        </button>
                    </a>
                </div>

    </div>
    </div>
       
           <div class="row">
               <div class="col-lg-6">
                    <label for="inputEmail3" class="col-sm-4 form-control-label">Category Name English</label>
                   <div class="form-group">
                        <input class="form-control" type="text" name="cat_name_en" id="cat_name_en" value="{{$cats->name}}" disabled="">
                   </div>
                    <div class="customClear"></div>
                    <div class="tag_en_error error"></div>
               </div>
               <div class="col-lg-6">
                @php 
                                    $url = url('/');
                                    $nurl = str_replace("index.php","",$url);
                                @endphp
                    <label for="inputEmail3" class="col-sm-4 form-control-label">Category Image</label>
                    <img src="{{$nurl}}{{$cats->image}}" style="width:150px;height:150px;">
                    <div class="customClear"></div>
                    <div class="categoryImageError error"></div>
                   
               </div>
               <div class="col-lg-6">
                    <label for="inputEmail3" class="col-sm-4 form-control-label">Status</label>
                    <div class="form-group">
                        <select class="form-control" name="cat_status" id="cat_id" disabled="">
                        
                            <option value="" disabled="">-- Select Status--</option>
                            <option value="0" <?php if($cats->status == 0){echo "selected";}?>> InActive</option>
                            <option value="1" <?php if($cats->status == 1){echo "selected";}?>> Active</option>
                        
                        </select>
                    </div>
                    <div class="customClear"></div>
                    <div class="tag_ar_error error"></div>
                   
               </div>
           </div>
            
       
    



<div class="customClear"></div>
</div>

<script>
    $(document).ready(function () {





        $("#saveCategory").click(function (event) {
            event.preventDefault();
            var base_path = $('#base_path').val();
            var tag_en = $('#cat_name_en').val();
            var tag_ar = $('#cat_name_ar').val();
            var color = $("#togglePaletteOnly").spectrum('get').toHexString();
            console.log(color);
            $('.hidcolor').val(color);
            var error = 0;
            var errors2 = 0;
                //return false;
            $( "#addcategoryform" ).submit();
        });

       
    });


</script>
@endsection
