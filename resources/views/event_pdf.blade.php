<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Eventeam</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    {{-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> --}}
    <style>
        .pdf {
            display: flex;
            justify-content: center;
        }

        * {
            box-sizing: border-box;
        }

        td {
            padding: 0;
        }

        table {
            border-spacing: 0px;
        }

        :focus {
            outline: none;
        }

        svg[Attributes] {
            width: 100% !important;
            height: 100% !important;
            ;
        }

        img {
            border: none;
            margin: 0px;
            max-width: 100%;
        }



        @media print {
            * {
                -webkit-print-color-adjust: exact !important;
                /*Chrome, Safari */
                color-adjust: exact !important;
                /*Firefox*/
            }
        }
    </style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="background: #fff;
font-family: 'Poppins', sans-serif;">
    <section class="page-container">
        <div style="width: 1200px; height: 100%; padding-left: 50px; padding-right: 50px; ">
            

            <div style="margin-top: 20px; 
                border-radius: 16px; 
                box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important;
                text-align: center;
                background-color: #fff !important;">
                <div style=" height:350px;
                    text-align: center;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;
                    background-color: #8e1b3e !important;">
                    <img src="{{ asset('images/events/'.$event->icon) }}" alt="Eventeam" style="padding-top: 140px;  ">
                </div>
                <div style=" height:350px; text-align: center;  ">
                    <h1 style="color: #1e1e1e;
                        font-size: 36px;
                        line-height: 35px;
                        text-align: center;
                        font-weight: 600
                        ;padding-top: 85px;
                        margin: 0;
                        padding-bottom: 15px;">
                        {{ $event->name }}
                    </h1>
                    <h2 style="color: #1e1e1e;
                        font-size: 22px;
                        line-height: 35px;
                        text-align: center;
                        font-weight: 400;  margin:0px; ">
                        {{date('d  MM  yy', strtotime($event->from_date))}}<br>{{ $event->venue }}
                    </h2>
                </div>
            </div>
            <div style="margin-top: 30px;
                border-radius: 16px;
                padding: 110px 0;
                box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important; 
                background-color: #fff !important;">
                <div style="  display: flex;
                    justify-content:space-around; padding:0 65px;">
                    <div>
                        <img src="{{ asset('images/events/'.$event->image) }}" alt="Eventeam" style="border-radius:15px;
                        margin-right: 35px;"> </div>
                    <div>
                        <h1 style="color: #1e1e1e;font-size: 43px;
                            line-height: 35px; 
                            font-weight: 600;
                            padding-top: 40px;
                            margin: 0; padding-bottom: 5px;">
                            {{ $event->name }}
                        </h1>
                        <h1 style="color: #8e1b3e;
                            font-size: 43px;line-height: 35px; 
                            font-weight: 600;
                            margin: 0;
                            padding-bottom: 15px;">
                            {{ $event->eventCategory->name }}
                        </h1>
                        <h2 style="color: #242424;font-size: 22px; 
                            font-weight: 400;  margin:0px; ">{{ count($sessions) }} Session
                        </h2>
                        @if(count($attendees_sponsors)>0)
                        <div>
                            <h3 style="color: #a2a2a2;
                                font-size: 22px;
                                font-weight: 400;
                                padding-top: 30px;
                                padding-bottom: 12px;
                                margin: 0;">Sponsors</h3>
                            <div>
                                @foreach ($attendees_sponsors as $value)
                                <img src="{{ asset('backend/images/profile/'.$value->image) }}" alt="Eventeam">
                                @endforeach
                            </div>
                        </div>
                        @endif
                        @if(count($attendees_exhibitor)>0)
                        <div>
                            <h3 style="color: #a2a2a2;font-size: 22px; 
                                font-weight: 400;padding-top: 5px;
                                margin: 0;
                                padding-bottom: 12px; ">
                                Exhibitors
                            </h3>
                            <div>
                                @foreach ($attendees_sponsors as $value)
                                <img src="{{ asset('backend/images/profile/'.$value->image) }}" alt="Eventeam">
                                @endforeach
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

            <div style="margin-top: 30px;
                border-radius: 16px;
                box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important;
                text-align: center;
                padding-bottom:130px; 
                background-color: #fff !important;">
                <div>
                    <h1 style="color: #000;
                        font-size: 35px;
                        text-align: center;
                        font-weight: 700;
                        padding-bottom:50px;
                        padding-top: 150px;
                        margin: 0;">Total Visitor Attendance
                    </h1>
                    <div style="border-radius: 16px;
                        margin: auto;
                        width: 958px;
                        background-color: #f0f3f6;
                        display: flex;
                        justify-content: space-between;
                        padding:40px 90px 70px 90px ;">
                        <div>
                            <img src="{{ asset('backend/assets/images/registered.png')}}" alt="Eventeam">
                            <h4 style="color: #000;font-size: 28px; text-align: center;
                             font-weight: 600;padding-bottom:10px;padding-top: 30px; margin: 0;"> Registered</h4>
                            <h2 style="color: #821938;font-size: 40px; text-align: center;
                             font-weight: 600;padding-bottom:10px; margin: 0;">
                                {{ isset($registered) ? count($registered) : 0 }}</h2>
                        </div>
                        <div>
                            <h2 style="color: #c0c0c0;font-size: 28px; text-align: center;
                             font-weight: 400;font-style:italic;padding-top:60px;padding-bottom:10px; margin: 0;"> Made
                            </h2>
                        </div>
                        <div> <img src="{{ asset('backend/assets/images/visits.png')}}" alt="Eventeam">
                            <h4 style="color: #000;font-size: 28px; text-align: center;
                             font-weight: 600;padding-bottom:10px;padding-top: 30px; margin: 0;"> Visits</h4>
                            <h2 style="color: #821938;font-size: 40px; text-align: center;
                             font-weight: 600;padding-bottom:10px; margin: 0;"> 1,895</h2>
                        </div>
                        <div>
                            <h2 style="color: #c0c0c0;font-size: 28px; text-align: center;
                             font-weight: 400; font-style:italic;padding-bottom:10px;padding-top:60px; margin: 0;"> In
                            </h2>
                        </div>
                        @php
                        $to = \Carbon\Carbon::createFromFormat('Y-m-d', $event->from_date);
                        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $event->to_date);
                        $diff_in_days = $to->diffInDays($from);
                        @endphp

                        <div> <img src="{{ asset('backend/assets/images/days.png')}}" alt="Eventeam">
                            <h4 style="color: #000;font-size: 28px; text-align: center;
                             font-weight: 600;padding-bottom:10px;padding-top: 30px; margin: 0;"> Days</h4>
                            <h2 style="color: #821938;font-size: 40px; text-align: center;
                             font-weight: 600;padding-bottom:10px; margin: 0;">
                                {{ isset($diff_in_days) ? $diff_in_days : 0 }}</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div
                style="margin-top: 30px;border-radius: 16px;  box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important; padding-bottom:130px;  background-color: #fff !important;">
                <div>
                    <div style=" display: flex; justify-content: space-around; padding-top: 80px;">
                        <div style=" display: flex;justify-content:space-between;">
                            <div> <img src="{{ asset('backend/assets/images/registered.png')}}" alt="Eventeam"></div>
                            <div style="padding-left: 15px;">
                                <h4 style="color: #000;font-size: 28px; 
                             font-weight: 600;padding-bottom:7px;margin: 0;"> Registered</h4>
                                <p
                                    style="color: #c0c0c0;margin: 0;font-size: 18px; padding-bottom:10px; font-weight: 400;">
                                    Data Source = Visitor + Anonymous Visitor</p>
                                <h2 style="color: #821938;font-size: 28px; 
                             font-weight: 600; margin: 0;">
                                    {{ isset($registerd_attendees_count) ? $registerd_attendees_count : 0 }}
                                    Total</h2>
                            </div>
                        </div>
                        <div style=" display: flex;justify-content:space-between;">
                            <div> <img src="{{ asset('backend/assets/images/visits.png')}}" alt="Eventeam"> </div>
                            <div style="padding-left: 15px;">
                                <h4 style="color: #000;font-size: 28px; 
                             font-weight: 600;padding-bottom:7px; margin: 0;"> Visits</h4>
                                <p
                                    style="color: #c0c0c0;margin: 0;font-size: 18px; padding-bottom:10px; font-weight: 400;">
                                    Data Source = Visitor</p>
                                <h2 style="color: #821938;font-size: 28px; 
                             font-weight: 600; margin: 0;"> 1,895 Total</h2>
                            </div>
                        </div>
                    </div>
                    <div style="display: flex; justify-content: space-between; ">
                        @if(count($registerd_attendees))
                        <div>
                            <div id="chart_registered"></div>
                        </div>
                        @endif
                        @if(count($visitors)>0)
                        <div>
                            <div id="chart_visitors"></div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

            <div
                style="margin-top: 30px;border-radius: 16px; height: 700px; box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important; margin-bottom:30px;   background-color: #fff !important;">
                <h4 style="color: #000;font-size: 35px; 
                font-weight: 700;padding-bottom:7px; padding-top: 80px; margin: 0; text-align: center;"> Session
                    Attendance Numbers Day 1</h4>
                <div id="chart_divlines"></div>
            </div>
            <div
                style="margin-top: 30px;border-radius: 16px; height: 700px; box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important; margin-bottom:30px;  background-color: #fff !important;">
                <div style="display: flex; justify-content: space-between; padding:  80px 80px  0 80px;;">

                    <div>
                        <h4 style="color: #000;font-size: 35px; 
                        font-weight: 700;padding-bottom:7px; margin: 0;"> Age</h4>
                        <p
                            style="color: #c0c0c0;margin: 0;font-style: italic;font-size: 18px; padding-bottom:10px; font-weight: 400;">
                            Data Source = Registered</p>
                        {{-- <div id="chart_age"></div> --}}
                    </div>
                    <div style="">
                        <h4 style="color: #000;font-size: 35px; 
                        font-weight: 700;padding-bottom:7px; margin: 0;">Gender </h4>
                        <div style="display: flex; justify-content: space-between;">
                            <p
                                style="color: #c0c0c0;margin: 0;font-style: italic; font-size: 18px;margin: 0; padding-bottom:10px; font-weight: 400;">
                                Data Source = Registered</p>
                            {{-- <div style="margin-left:30px;">
                                <p style="display: flex;font-style: italic;font-size: 18px; margin: 0;"><span
                                        style="width: 20px; height: 20px; background-color: #821938;display: block;margin-right: 8px;"></span>
                                    Male</p>
                                <p style="display: flex;font-style: italic;font-size: 18px;margin: 0; "><span
                                        style="width: 20px; height: 20px; background-color: #c34368;display: block;margin-right: 8px;"></span>
                                    Female</p>
                            </div> --}}
                            {{-- <div id="pie_chart_gender"></div> --}}
                        </div>

                    </div>

                </div>
                <div style="display: flex; justify-content: space-between;">
                    @if(count($age)>0)<div id="chart_age"></div>@endif

                    @if(count($gender)>0)<div id="pie_chart_gender" style="width:450px; height: 450px;"></div>@endif
                </div>
            </div>

            <div
                style="margin-top: 30px;border-radius: 16px; height: 700px; box-shadow: 0 0.15rem 1.75rem 0 rgba(58, 59, 69, 0.15) !important; margin-bottom:80px; display: flex; align-items: center; justify-content: center;  background-color: #fff !important;">
                <div style="text-align: center;">
                    <h1 style="color: #000;font-size: 35px;text-transform:uppercase;line-height: 35px; 
     font-weight: 700;padding-top: 40px; margin: 0; padding-bottom: 30px; text-align: center;">Thank you for<br>
                        choosing </h1>
                    <img src="{{ asset('backend/assets/images/pdf-logo1.png')}}" alt="Eventeam">
                    <div style=" padding-top: 30px;">
                        <div style=" display: flex;     justify-content: center;">
                            <h2 style="color: #242424;font-size: 20px; 
        font-weight: 500;  margin:0px; "> <img src="{{ asset('backend/assets/images/location.png')}}" alt="Eventeam"
                                    style="margin-right: 5px;">PO Box 80307, Doha - Qatar</h2>
                            <h2 style=" color: #242424;font-size: 20px; font-weight: 500; margin:0px; "> <img
                                    src="{{ asset('backend/assets/images/p-mail.png')}} " alt="Eventeam "
                                    style="margin-right: 5px;margin-left: 10px;">Info@eventeam.qa</h2>
                        </div>
                        <div style=" display: flex;     justify-content: center;">
                            <h2 style="color: #242424;font-size: 20px; 
                       font-weight: 500;  margin:0px; "> <img src="{{ asset('backend/assets/images/p-globe.png')}}"
                                    alt="Eventeam" style="margin-right: 5px;"> www.eventeam.qa</h2>
                            <h2 style=" color: #242424;font-size: 20px; font-weight: 500; margin:0px; "> <img
                                    src="{{ asset('backend/assets/images/p-phone.png')}} " alt="Eventeam "
                                    style="margin-right: 5px;margin-left: 10px; ">+974 4417 5055</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
  
        function drawChart() {
          var data = google.visualization.arrayToDataTable([
              ['Date', 'Attendees'],
  
              @php
                foreach($gender as $value) {
                    echo "['".$value->date."', ".$value->gender."],";
                }
              @endphp
          ]);
  
          var options = {
            title : 'Percentage of Male and Female Attendees'
        };
            var chart = new google.visualization.PieChart(document.getElementById('pie_chart_gender'));
            chart.draw(data, options);
        }



        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawChart1);
  
        function drawChart1() {
          var data = google.visualization.arrayToDataTable([
              ['Date', 'Attendees'],
  
              @php
                foreach($attendees as $employee) {
                    echo "['".$employee->date."', ".$employee->data."],";
                }
              @endphp
          ]);
  
          var options = {
            chart: {
              title: 'Bar Graph | Session Attendees'
            },
            bars: 'vertical'
          };
          var chart = new google.charts.Bar(document.getElementById('chart_divlines'));
          chart.draw(data, google.charts.Bar.convertOptions(options));
        }
    
        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawChart2);
        
        function drawChart2() {
            
          var data = google.visualization.arrayToDataTable([
              ['Date', 'Attendees Age'],
            
  
              @php
                foreach($age as $key=> $rows) {
                    echo "['".$key."', ".$rows."],";
                }
              @endphp
          ]);
  
          var options = {
            chart: {
              title: 'Bar Graph | Session Attendees Age'
            },
            bars: 'vertical'
          };
          var chart = new google.charts.Bar(document.getElementById('chart_age'));
          chart.draw(data, google.charts.Bar.convertOptions(options));
        }

     
        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawChart3);
  
        function drawChart3() {
          var data = google.visualization.arrayToDataTable([
              ['Date', 'Registered Users'],
  
              @php
                foreach($registerd_attendees as $values) {
                    echo "['".$values->date."', ".$values->data."],";
                }
              @endphp
          ]);
  
          var options = {
            chart: {
              title: 'Bar Graph | Registered Users'
            },
            bars: 'vertical'
          };
          var chart = new google.charts.Bar(document.getElementById('chart_registered'));
          chart.draw(data, google.charts.Bar.convertOptions(options));
        }

        
        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawChart4);
  
        function drawChart4() {
          var data = google.visualization.arrayToDataTable([
              ['Date', 'Visitors'],
  
              @php
                foreach($visitors as $values) {
                    echo "['".$values->date."', ".$values->data."],";
                }
              @endphp
          ]);
  
          var options = {
            chart: {
              title: 'Bar Graph | Visitors'
            },
            bars: 'vertical'
          };
          var chart = new google.charts.Bar(document.getElementById('chart_visitors'));
          chart.draw(data, google.charts.Bar.convertOptions(options));
        }
        
        
    </script>
</body>

</html>