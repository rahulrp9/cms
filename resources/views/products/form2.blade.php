@extends('layouts.app')
@section('content')
<!-- muliselect plugin's CSS  -->
<link href="{{asset('assets/plugins/jasny-bootstrap/css/jasny-bootstrap.css')}}" rel="stylesheet" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<style type="text/css">
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
        color: black;
    }
</style>
    <div class="col-md-12 ">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">@if(isset($product))Product Updation @else Add Product @endif</small> </h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
                  <a href="{{ URL::to('dashboard/products') }}"><button title="List" data-toggle="tooltip" class="btn btn-default" type="button">
                          <i class="fa fa-list"></i></button>
                  </a>
                
                  @if(isset($product))
                        <a href="{{ URL::to('dashboard/products/create') }}"><button title="New" data-toggle="tooltip" class="btn btn-default" type="button">
                                <i class="fa fa-plus"></i></button>
                        </a>
                        <a href="{{ URL::to('dashboard/products/'.$product->id) }}"><button title="More" data-toggle="tooltip" class="btn btn-default" type="button">
                                <i class="fa fa-eye"></i></button>
                        </a>

                  @endif
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
                <div class="flash_messages">
                </div>
                <form id="formSpace" action="{{ URL::to('dashboard/products') }}@if(isset($product))/{{ $product->id }} @endif" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    {!! csrf_field() !!}
                    @if(isset($product))
                        {{ method_field('PUT') }}
                        <input type="hidden" name="service_id" value="{{$product->id}}"/>
                    @endif
                    <!-- First Name-->
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-sm-3 control-label required">Product Name</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="name" id="name" value='@if(isset($product)){{ $product->name }}@endif' class="form-control service_eng" required="" placeholder="Enter Product Name">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
                        <label for="value" class="col-sm-3 control-label required">Product Price</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="value" id="value" value='@if(isset($product)){{ $product->price }}@endif' class="form-control service_eng" required="" placeholder="Enter Product Value">
                                @if ($errors->has('value'))value
                                    <span class="help-block">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                        <label for="code" class="col-sm-3 control-label required">Product Category</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <select name="catgeory" id="SPECIAL">
                                  <option>Please Select</div>
                                    @if(count($mainCategory))
                                        @foreach($mainCategory as $main)
                                            <optgroup label="{{$main['name']}}">
                                                <option data-img="" value="{{$main['id']}}" @if($product->catgeory == $main['id']) selected @endif>{{$main['name']}}</option>
                                                @if(count($main['sub']))
                                                    @foreach($main['sub'] as $sub)
                                                        <option data-img="" value="{{$sub->id}}" @if($product->catgeory == $sub->id) selected @endif>{{$sub->name}}</option>
                                                    @endforeach    
                                                @endif    
     
                                            </optgroup>
                                        @endforeach    
                                    @endif    

                                   </select>
                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('isveg') ? ' has-error' : '' }}">
                        <label for="isveg" class="col-sm-3 control-label required">Veg or Non Veg</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                 <input type="radio" id="isveg" name="isveg" value="0" @if($product->isveg == 0) checked @endif >
                                <label for="male">Non Veg</label><br>
                                <input type="radio" id="isveg" name="isveg" value="1" @if($product->isveg == 1) checked @endif>
                                <label for="female">Veg</label><br>

                                @if ($errors->has('isveg'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('isveg') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                        <label for="type" class="col-sm-3 control-label required">Product Customizable</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="checkbox" id="vehicle1" name="type" value="2" @if($product->type == 2) checked @endif>
                                <label for="vehicle1"> Customizable</label><br>
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                        <label for="type" class="col-sm-3 control-label required">Is Addon</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="checkbox" id="vehicle1" name="isaddon" value="1" @if($product->isaddon == 1) checked @endif>
                                <label for="vehicle1"> Addon</label><br>
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div> -->


                   
                    <!-- SpaceImage-->
                
                        <div class="form-group">
                                <label for="title_ar" class="col-sm-3 control-label required">Product Image</label>
                                <div class="imgUploadHolder col-sm-6">
                                    @if(isset($product->image) && $product->image != '')
                                    <input type="hidden" name="lasthidimg" value="{{$product->image}}" class="lasthidimg">
                                    @endif
                                    <div class="col-lg-3">
                                   <input type="file" class="space_img " name="servicelogo" id="space_img"   @if(isset($product->image) && $product->image != '')
                                   value="{{$product->image}}" @else required @endif>
                                  <label class="errorimg" for="imageInput" style="color: red"></label>
                                    <div style="margin-top: 10px;">
                                    <img src="" class="imagePreview" width="150" height="150" style="display: none;">
                                    @if(isset($product->image) && $product->image != '')
                                   <img src="<?php echo url('/');?>{{$product->image}}" width="150" height="150" class="oldpreview">
                                   
                                   @endif
                                   </div>
                                   </div>

                                </div>
                            </div>
                        <div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
                        <label for="value" class="col-sm-3 control-label required">Select Addons</label>
                        <div class="col-sm-9">
                            <div class="">
                                <div class="input-group-prepend"></div>
                               <select name="addons[]" multiple="" class="select2">
                                <option value="">Select Addon</option>
                                @if($addons)
                                    @foreach($addons as $addon)
                                        <option value="{{$addon->id}}" @if(in_array($addon->id, $paddons)) selected @endif>{{$addon->name}}</option>
                                    @endforeach
                                @endif
                               </select>
                            </div>
                        </div>
                    </div>  
                    @if(count($pcutomises))
                        @foreach($pcutomises as $pcutomise)
                            <div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
                        <label for="value" class="col-sm-3 control-label required">Customizable Product options</label>
                        <div class="col-sm-9">
                            <div class="">
                                <div class="input-group-prepend"></div>
                                <!-- <input type="text" name="size[]" id="value" value='@if(isset($pcutomise)){{ $pcutomise->name }}@endif' class="form-control service_eng"  placeholder="Enter Product Size" style="float: left;width: 30%"> -->
                                <select class="form-control service_eng" name="size[]" style="float: left;width: 30%">
                                    @if($sizegroups)
                                         <option value="" selected="">Select option</option>
                                        @foreach($sizegroups as $sizegroup)
                                            <option value="{{$sizegroup->id}}" @if(isset($pcutomise) && $pcutomise->name == $sizegroup->id) selected @endif>{{$sizegroup->name}}</option>
                                        @endforeach
                                    @endif

                                </select>
                                <input type="text" name="sizeval[]" id="value" value='@if(isset($pcutomise)){{ $pcutomise->price }}@endif' class="form-control service_eng"  placeholder="Enter Product Value" style="float: left;width: 30%;margin-left: 10px;">
                                @if ($errors->has('value'))value
                                    <span class="help-block">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div> 
                        @endforeach
                    @endif  
                        <!-- <div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
                        <label for="value" class="col-sm-3 control-label required">Customizable Product options</label>
                        <div class="col-sm-9">
                            <div class="">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="size[]" id="value" value='@if(isset($product)){{ $product->value }}@endif' class="form-control service_eng"  placeholder="Enter Product Size" style="float: left;width: 30%">
                                <input type="text" name="sizeval[]" id="value" value='@if(isset($product)){{ $product->value }}@endif' class="form-control service_eng"  placeholder="Enter Product Value" style="float: left;width: 30%;margin-left: 10px;">
                                @if ($errors->has('value'))value
                                    <span class="help-block">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div> 
                    <div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
                        <label for="value" class="col-sm-3 control-label required">Customizable Product options</label>
                        <div class="col-sm-9">
                            <div class="">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="size[]" id="value" value='@if(isset($product)){{ $product->value }}@endif' class="form-control service_eng"  placeholder="Enter Product Size" style="float: left;width: 30%">
                                <input type="text" name="sizeval[]" id="value" value='@if(isset($product)){{ $product->value }}@endif' class="form-control service_eng"  placeholder="Enter Product Value" style="float: left;width: 30%;margin-left: 10px;">
                                @if ($errors->has('value'))value
                                    <span class="help-block">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
                        <label for="value" class="col-sm-3 control-label required">Customizable Product options</label>
                        <div class="col-sm-9">
                            <div class="">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="size[]" id="value" value='@if(isset($product)){{ $product->value }}@endif' class="form-control service_eng"  placeholder="Enter Product Size" style="float: left;width: 30%">
                                <input type="text" name="sizeval[]" id="value" value='@if(isset($product)){{ $product->value }}@endif' class="form-control service_eng"  placeholder="Enter Product Value" style="float: left;width: 30%;margin-left: 10px;">
                                @if ($errors->has('value'))value
                                    <span class="help-block">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button id="btn-submit" type="submit" class="btn btn-default checkbtn">
                                <i class="fa fa-plus"></i> Save
                            </button>
                            <button id="btn-submit" type="button" class="btn btn-default" onclick="window.history.back()">
                                <i class="fa fa-history"></i> Cancel
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script type="text/javascript">
     $('.space_img').change(function(event) {
        var fileSize = this.files[0].size;
        var maxAllowedSize = 2000000;
        if(fileSize > maxAllowedSize){
            $('.errorimg').html('Please upload a smaller file');
           $('.space_img').val('');
        }
        else{
            $('.errorimg').html('');
            if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.oldpreview').fadeOut();
                        $('.imagePreview').attr('src', e.target.result);
                        $('.imagePreview').fadeIn();
                    }
                    reader.readAsDataURL(this.files[0]);
                }
        }

    });

    /*$('.checkbtn').click(function(){

        var name = $('.service_eng').val();
        $.ajax({
                    type: 'get',
                    url: '<?php echo url('/');?>/checkservice',
                    data: 'servicename='+name,
                    datatype: "json",
                    async: false,
                    cache: false,
                    timeout: 30000,
                        success: function (response) {
                             
                        }
            });
    }); */
    $(document).ready(function() {
    $('.select2').select2();
});
</script>
@endsection
