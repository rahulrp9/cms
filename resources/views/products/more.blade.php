@extends('layouts.app')
@section('content')
<style type="text/css">
    .box .row{
         margin-bottom: 15px;
    }
</style>
    <div class="row">
    <div class="col-md-12">
            <div class="col-md-6">
                <h1 class="box-title">Product Details  </h1>
            </div>    
    <div class="pull-right box-tools">
                    <a href="{{URL::to('dashboard/products')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-default" type="button" data-original-title="Service List">
                            <i class="fa fa-list"></i>
                        </button>
                    </a>
     @if(isset($product))
                    <a href="{{URL::to('dashboard/products/create')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default" type="button" data-original-title="Add Offer">
                            <i class="fa fa-plus"></i>
                        </button>
                    </a>
   
                    <a href="{{URL::to('dashboard/products/'.$product->id.'/edit')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default" type="button" data-original-title="Edit Offer">
                            <i class="fa fa-edit"></i>
                        </button>
                    </a>
    @endif               
                </div>
    </div>            
    </div>
        <div class="box">
            <div class="box-body pad" id="tab_1">
                <div class="row">
                    <label class="col-sm-3 control-label">Product Name</label>
                    <div class="col-sm-6">
                        {{ $product->name }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Product Price</label>
                    <div class="col-sm-3">
                        {{ $product->price }}%
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Product Category</label>
                    <div class="col-sm-3">
                        {{ $product->category }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Veg or Non Veg</label>
                    <div class="col-sm-3">
                        {{ $product->isveg }}
                    </div>
                </div>
                @if(count($pcutomises))
                        @foreach($pcutomises as $pcutomise)
                        <div class="row">
                        <label for="value" class="col-sm-3 control-label required">Customizable Product options</label>
                        <div class="col-sm-9">
                            <div class="">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="size[]" id="value" value='@if(isset($pcutomise)){{ $pcutomise->name }}@endif' class="form-control service_eng"  placeholder="Enter Product Size" style="float: left;width: 30%" readonly="">
                                <input type="text" name="sizeval[]" id="value" value='@if(isset($pcutomise)){{ $pcutomise->price }}@endif' class="form-control service_eng"  placeholder="Enter Product Value" style="float: left;width: 30%;margin-left: 10px;" readonly="">
                                @if ($errors->has('value'))value
                                    <span class="help-block">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div> 
                        @endforeach
                    @endif  
                <div class="row">
                    <label class="col-sm-3 control-label">Product Image</label>
                    <div class="col-sm-3">
                        @if(!empty($product->image))
                                    <a href="javascript:void(0)" class="pop" imgsrc="{{asset($product->space_image)}}"><img src=" {{ asset($product->image) }}" class="img-responsive img-rounded" width="150" height="1150" /></a>
                        @else
                            <img src="{{asset('assets/images/no-image.png')}}" />
                        @endif
                    </div>
                </div>
              
        </div>
    </div>
@endsection
