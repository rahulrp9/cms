@extends('layouts.app')
@section('content')
<!-- muliselect plugin's CSS  -->
<link href="{{asset('assets/plugins/jasny-bootstrap/css/jasny-bootstrap.css')}}" rel="stylesheet" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <div class="col-md-12 ">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">@if(isset($offer))Delivery Area Updation @else Delivery Area Creation @endif</small> </h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
                  <a href="{{ URL::to('dashboard/areas') }}"><button title="List" data-toggle="tooltip" class="btn btn-default" type="button">
                          <i class="fa fa-list"></i></button>
                  </a>
                
                  @if(isset($offer))
                        <a href="{{ URL::to('dashboard/areas/create') }}"><button title="New" data-toggle="tooltip" class="btn btn-default" type="button">
                                <i class="fa fa-plus"></i></button>
                        </a>
                        <a href="{{ URL::to('dashboard/areas/'.$offer->id) }}"><button title="More" data-toggle="tooltip" class="btn btn-default" type="button">
                                <i class="fa fa-eye"></i></button>
                        </a>

                  @endif
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
                <div class="flash_messages">
                </div>
                <form id="formSpace" action="{{ URL::to('dashboard/areas') }}@if(isset($area))/{{ $area->id }} @endif" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    {!! csrf_field() !!}
                    @if(isset($area))
                        {{ method_field('PUT') }}
                        <input type="hidden" name="service_id" value="{{$area->id}}"/>
                    @endif
                    <!-- First Name-->
                    <div class="form-group {{ $errors->has('delivery_area') ? ' has-error' : '' }}">
                        <label for="delivery_area" class="col-sm-3 control-label required">Delivery Area Name</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <input type="text" name="delivery_area" id="delivery_area" value='@if(isset($area)){{ $area->delivery_area }}@endif' class="form-control service_eng" required="" placeholder="Enter Offer Name">
                                @if ($errors->has('delivery_area'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('delivery_area') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    

                        
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button id="btn-submit" type="submit" class="btn btn-default checkbtn">
                                <i class="fa fa-plus"></i> Save
                            </button>
                            <button id="btn-submit" type="button" class="btn btn-default" onclick="window.history.back()">
                                <i class="fa fa-history"></i> Cancel
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script type="text/javascript">
     $('.space_img').change(function(event) {
        var fileSize = this.files[0].size;
        var maxAllowedSize = 2000000;
        if(fileSize > maxAllowedSize){
            $('.errorimg').html('Please upload a smaller file');
           $('.space_img').val('');
        }
        else{
            $('.errorimg').html('');
            if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.oldpreview').fadeOut();
                        $('.imagePreview').attr('src', e.target.result);
                        $('.imagePreview').fadeIn();
                    }
                    reader.readAsDataURL(this.files[0]);
                }
        }

    });

    /*$('.checkbtn').click(function(){

        var name = $('.service_eng').val();
        $.ajax({
                    type: 'get',
                    url: '<?php echo url('/');?>/checkservice',
                    data: 'servicename='+name,
                    datatype: "json",
                    async: false,
                    cache: false,
                    timeout: 30000,
                        success: function (response) {
                             
                        }
            });
    }); */
</script>
@endsection
