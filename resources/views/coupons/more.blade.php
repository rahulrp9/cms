@extends('layouts.app')
@section('content')
<style type="text/css">
    .box .row{
         margin-bottom: 15px;
    }
</style>
    <div class="row">
    <div class="col-md-12">
            <div class="col-md-6">
                <h1 class="box-title">Coupon Details  </h1>
            </div>    
    <div class="pull-right box-tools">
                    <a href="{{URL::to('dashboard/coupons')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-default" type="button" data-original-title="Service List">
                            <i class="fa fa-list"></i>
                        </button>
                    </a>
     @if(isset($coupon))
                    <a href="{{URL::to('dashboard/coupons/create')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default" type="button" data-original-title="Add Offer">
                            <i class="fa fa-plus"></i>
                        </button>
                    </a>
   
                    <a href="{{URL::to('dashboard/coupons/'.$coupon->id.'/edit')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default" type="button" data-original-title="Edit Offer">
                            <i class="fa fa-edit"></i>
                        </button>
                    </a>
    @endif               
                </div>
    </div>            
    </div>
        <div class="box">
            <div class="box-body pad" id="tab_1">
                <div class="row">
                    <label class="col-sm-3 control-label">Coupon Name</label>
                    <div class="col-sm-6">
                        {{ $coupon->name }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Coupon Value</label>
                    <div class="col-sm-3">
                        {{ $coupon->value }}%
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Coupon Code</label>
                    <div class="col-sm-3">
                        {{ $coupon->code }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Coupon Image</label>
                    <div class="col-sm-3">
                        @if(!empty($coupon->image))
                                    <a href="javascript:void(0)" class="pop" imgsrc="{{asset($coupon->space_image)}}"><img src=" {{ asset($coupon->image) }}" class="img-responsive img-rounded" width="150" height="1150" /></a>
                        @else
                            <img src="{{asset('assets/images/no-image.png')}}" />
                        @endif
                    </div>
                </div>
              
        </div>
    </div>
@endsection
