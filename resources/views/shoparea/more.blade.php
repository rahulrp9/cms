@extends('layouts.app')
@section('content')
<style type="text/css">
    .box .row{
         margin-bottom: 15px;
    }
</style>
    <div class="row">
    <div class="col-md-12">
            <div class="col-md-6">
                <h1 class="box-title">Offer Details  </h1>
            </div>    
    <div class="pull-right box-tools">
                    <a href="{{URL::to('dashboard/offers')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-default" type="button" data-original-title="Service List">
                            <i class="fa fa-list"></i>
                        </button>
                    </a>
     @if(isset($offer))
                    <a href="{{URL::to('dashboard/offers/create')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default" type="button" data-original-title="Add Offer">
                            <i class="fa fa-plus"></i>
                        </button>
                    </a>
   
                    <a href="{{URL::to('dashboard/offers/'.$offer->id.'/edit')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default" type="button" data-original-title="Edit Offer">
                            <i class="fa fa-edit"></i>
                        </button>
                    </a>
    @endif               
                </div>
    </div>            
    </div>
        <div class="box">
            <div class="box-body pad" id="tab_1">
                <div class="row">
                    <label class="col-sm-3 control-label">Offer Name</label>
                    <div class="col-sm-6">
                        {{ $offer->name }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Offer Value</label>
                    <div class="col-sm-3">
                        {{ $offer->value }}%
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Offer Image</label>
                    <div class="col-sm-3">
                        @if(!empty($offer->image))
                                    <a href="javascript:void(0)" class="pop" imgsrc="{{asset($offer->space_image)}}"><img src=" {{ asset($offer->image) }}" class="img-responsive img-rounded" width="150" height="1150" /></a>
                        @else
                            <img src="{{asset('assets/images/no-image.png')}}" />
                        @endif
                    </div>
                </div>
              
        </div>
    </div>
@endsection
