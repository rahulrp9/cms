@extends('layouts.app')
@section('content')
<!-- muliselect plugin's CSS  -->
<link href="{{asset('assets/plugins/jasny-bootstrap/css/jasny-bootstrap.css')}}" rel="stylesheet" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<style type="text/css">
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
        color: black;
    }
</style>
    <div class="col-md-12 ">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">@if(isset($offer))Delivery Area Updation @else Delivery Area Creation @endif</small> </h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
                  <a href="{{ URL::to('dashboard/shopareas') }}"><button title="List" data-toggle="tooltip" class="btn btn-default" type="button">
                          <i class="fa fa-list"></i></button>
                  </a>
                
                  @if(isset($offer))
                        <a href="{{ URL::to('dashboard/shopareas/create') }}"><button title="New" data-toggle="tooltip" class="btn btn-default" type="button">
                                <i class="fa fa-plus"></i></button>
                        </a>
                        <a href="{{ URL::to('dashboard/shopareas/'.$area->id) }}"><button title="More" data-toggle="tooltip" class="btn btn-default" type="button">
                                <i class="fa fa-eye"></i></button>
                        </a>

                  @endif
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
                <div class="flash_messages">
                </div>
                <form id="formSpace" action="{{ URL::to('dashboard/shopareas') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    {!! csrf_field() !!}
                    <!-- First Name-->
                    <div class="form-group {{ $errors->has('delivery_area') ? ' has-error' : '' }}">
                        <label for="delivery_area" class="col-sm-3 control-label required">Delivery Area</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend"></div>
                                <select name="delivery_areas[]" multiple="" class="select2" style="width: 150px;">
                                <option value="">Select Delivery Area</option>
                                @if($areas)
                                    @foreach($areas as $area1)
                                        <option value="{{$area1->id}}" @if(in_array($area1->id, $area)) selected @endif>{{$area1->delivery_area}}</option>
                                    @endforeach
                                @endif
                               </select>

                            </div>
                        </div>
                    </div>
                    

                        
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button id="btn-submit" type="submit" class="btn btn-default checkbtn">
                                <i class="fa fa-plus"></i> Save
                            </button>
                            <button id="btn-submit" type="button" class="btn btn-default" onclick="window.history.back()">
                                <i class="fa fa-history"></i> Cancel
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script type="text/javascript">
     $('.space_img').change(function(event) {
        var fileSize = this.files[0].size;
        var maxAllowedSize = 2000000;
        if(fileSize > maxAllowedSize){
            $('.errorimg').html('Please upload a smaller file');
           $('.space_img').val('');
        }
        else{
            $('.errorimg').html('');
            if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.oldpreview').fadeOut();
                        $('.imagePreview').attr('src', e.target.result);
                        $('.imagePreview').fadeIn();
                    }
                    reader.readAsDataURL(this.files[0]);
                }
        }

    });

    /*$('.checkbtn').click(function(){

        var name = $('.service_eng').val();
        $.ajax({
                    type: 'get',
                    url: '<?php echo url('/');?>/checkservice',
                    data: 'servicename='+name,
                    datatype: "json",
                    async: false,
                    cache: false,
                    timeout: 30000,
                        success: function (response) {
                             
                        }
            });
    }); */
     $(document).ready(function() {
    $('.select2').select2();
});
</script>
@endsection
