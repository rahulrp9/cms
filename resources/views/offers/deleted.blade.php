@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{ URL::asset('css/custom_style2.css')}}">
<div class="contentHolderV1">
    <h2>Service Types</h2>
    <div class="col-md-12">
        <div class="col-md-6"> 
             <a href="{{ URL::to('services/create') }}" class="searchbtn">Add Service</a>
        </div>
        <div class="col-md-6"> 
            <a href="{{ URL::to('services') }}" class="searchbtn pull-right">All Services</a>
        </div>
    </div>    
   
    <div class="tableHolder">
        <table style="width: 100%;" class="tableV1" cellpadding="0" cellspacing="0">
            <thead class="tableHeader">
                <tr>
                    <td>#</td>
                    <td>Service Type</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
               
               @foreach($services as $service)
               <?php  //$n++; ?>

                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td><a href="{{ URL::to('services', ['id' => ($service->id)]) }}" class="view">{{ $service->service_name_en }}</a></td>
                        <td><div  class="actions">
                                    <!-- <a href="{{ URL::to('services/'.$service->id.'/edit') }}" class="edit">
                                        <i class="fa fa-edit"></i>
                                    </a> -->
                                    <!-- <a href="{{ URL::to('services', ['id' => ($service->id)]) }}" class="view">
                                        <i class="fa fa-eye"></i>
                                    </a> -->
                                     <a href="{{ URL::to('restore/service', ['id' => ($service->id)]) }}" class="view">
                                       Restore
                                    </a>
                                   <!--   {{ Form::open(['method' => 'DELETE','style'=>"display: inline-block", 'route' => ['services.destroy', $service->id]]) }}
                                        {!! csrf_field() !!}
                                        <a class="delete" data-id=""><i class="fa fa-trash-alt"></i></a>
                                           
                                        {{ Form::close() }} -->
                                    
                                </div></td>
                       
                    </tr>
                    
                     @endforeach
            </tbody>
        </table>
    </div>
<ul class="pagination">
        {!! $services->render() !!}
    </ul>
</div>
<script type="text/javascript">
    
    $(function () {
        $('.delete').click(function(e){
            e.preventDefault();
            if(confirm('Are you sure you want to delete?'))
            {
                $(this).parent('form').submit();
            }else{
                return false;
            }
        });
});
</script>
@endsection