Hi {{ $name }},
<br>
<br>
  <p> We have sent you this email in response to your request to reset your password.</p>

 <p> Please login with the following password. You can change the password from My Profile.</p>
  <p>Password : {{$password}} </p>
<br><br>
Thanks,<br>
