<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>FoodApp | Dashboard</title>
        <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
        <link rel="manifest" href="/favicons/manifest.json">
        <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <script src="{{ URL::asset('js/jquery-2.1.1.min.js') }}"></script>
        <link rel="stylesheet" href="{{ URL::asset('theme/bootstrap/dist/css/bootstrap.min.css')}}">
        <!-- Font Awesome -->

        <link rel="stylesheet" href="{{ URL::asset('theme/font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
              crossorigin="anonymous">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::asset('theme/css/AdminLTE.min.css')}}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{ URL::asset('theme/css/skins/_all-skins.min.css')}}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        
        <!-- <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/chosen.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/diethub.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}">
        
        <link href="{{ asset('jsxc/css/jquery-ui.min.css') }}" media="all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('jsxc/css/jsxc.css') }}" media="all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/jsxc.example.css') }}" media="all" rel="stylesheet" type="text /css" />-->
        <link rel="stylesheet" href="{{ URL::asset('css/adminstyles.css')}}">
        <link rel="stylesheet" href="{{ URL::asset('css/adminstyle.css')}}">
       <!-- <link rel="stylesheet" href="{{ URL::asset('css/foundation.css')}}"> -->
        @stack('styles')
    </head>
    <body class="hold-transition skin-purple sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                @include('layouts.portions.mainHeader')
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                @include('layouts.portions.mainSidebar')
                <!-- /.sidebar -->
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">

                </section>
                <!-- Main content -->
                <section class="content innerContent">
                    <!-- Info boxes -->
                    <div class="row">
                        <div class="col-md-12">
                        @yield('content')
                        </div>
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                @include('layouts.portions.mainFooter')
            </footer>
        </div>
        <!-- ./wrapper -->
        <!-- jQuery 3 -->

       <!--  <script src="{{ URL::asset('theme/jquery/dist/jquery.min.js')}}"></script> -->
        <!-- Bootstrap 3.3.7 -->
        <script src="{{ URL::asset('theme/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <!-- AdminLTE App -->
        <script src="{{ URL::asset('theme/js/adminlte.min.js')}}"></script>
        
       <!--  <script src="{{ asset('assets/js/jquery.ui.widget.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.iframe-transport.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.fileupload.js') }}"></script>
        <script src="{{ asset('assets/js/chosen.jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap-typeahead.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.tablednd.min.js') }}"></script>        
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script> -->
        <script src="{!! asset('js/template/common.js') !!}" type="text/javascript"></script>
        @stack('scripts')
    </body>
</html>
