<a href="" class="logo">

    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>A</b>LT</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Diet</b>Hub</span>
</a>

<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <!-- <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a> -->
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                    <img src="{{url("")}}/{{Auth::user()->path}}" class="user-image" alt="User Image">
                    <span class="hidden-xs">{{Auth::user()->display_name}}</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="{{url("")}}/{{Auth::user()->path}}" class="img-circle" alt="User Image">

                        <p>
                            {{Auth::user()->display_name}}- {{Auth::user()->user_type}}
                            <small>{{Auth::user()->created_at->format('D-m-Y')}}</small>
                        </p>
                    </li>
                    <!-- Menu Body -->
                    @if(Auth::user()->user_type == 'Coach')
                    <!-- <li class="user-body">
                        <div class="row">
                            <div class="col-xs-4 text-center">

                                <a href="{{ url('/pi') }}">PI</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="{{ url('/custom_message') }}">Message</a>
                            </div>
                            <div class="col-xs-4 text-center">

                                <a href="{{ url('/subscription') }}">My Users</a>
                            </div>
                        </div>
                    </li> -->
                    @endif
                    <!-- Menu Footer-->
                      <?php 
      
      $crumbs = explode("/",$_SERVER["REQUEST_URI"]);
      foreach($crumbs as $crumb){
      $link[] = trim(ucfirst(str_replace(array(".php","_"),array(""," "),$crumb) . ' '));
} 

?>

                    <li class="user-footer">
                        @if(Auth::user()->user_type == 'Coach' && $link[1] !='Coach profileNewCoach')
                        <div class="pull-left">
                            <a href="{{ url('/coach_profileNew') }}" class="btn btn-default btn-flat">Edit Profile</a>
                        </div>
                        @endif
                        @if(Auth::user()->user_type == 'Admin')
                        <div class="pull-left">
                            <a href="{{ url('/adminProfileEdit') }}" class="btn btn-default btn-flat">Edit Profile</a>
                        </div>
                        @endif
                        @if(Auth::user()->user_type == 'Operator')
                        <div class="pull-left">
                            <a href="{{ url('/adminProfileEdit') }}" class="btn btn-default btn-flat">Edit Profile</a>
                        </div>
                        @endif
                        
                        <div class="pull-right">
                            <a href="{{ url('/logout') }}" onclick="fnLogout()" class="btn btn-default" ><i class="fa fa-log-out text-red"></i></span>Sign out</a>

                        </div>
                    </li>
                </ul>
            </li>

        </ul>
    </div>

</nav>

