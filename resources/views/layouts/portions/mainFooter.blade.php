<div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <?php
    $year = date("Y");
     $next = $year + 1;
    ?>
    <strong>Copyright &copy;{{$year}}-{{ $next }} <a href="">FoodApp</a>.</strong> All rights
    reserved.