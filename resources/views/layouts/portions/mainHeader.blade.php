<!-- Logo -->
<a href="/dashboard" class="logo">


    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>FoodApp</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>FoodApp</b></span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        @if(Auth::check())
            <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if(Auth::user()->path)
                             <img src="{{asset(Auth::user()->path)}}" class="user-image" alt="User Image">
                         @else
                             <img src="{{ asset('images/default-user.jpg') }}" alt="" class="user-image">
                         @endif
                       
                        <span class="hidden-xs">{{Auth::user()->display_name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            @if(Auth::user()->path)
                             <img src="{{asset(Auth::user()->path)}}" class="img-circle" alt="User Image">
                         @else
                             <img src="{{ asset('images/default-user.jpg') }}" alt="" class="img-circle">
                         @endif
                            <p>
                                {{Auth::user()->display_name}} - {{Auth::user()->user_type}}
                                <small>@php echo Date('d-M-Y'); @endphp</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <?php
                        $crumbs = explode("/", $_SERVER["REQUEST_URI"]);
                        foreach ($crumbs as $crumb) {
                            $link[] = trim(ucfirst(str_replace(array(".php", "_"), array("", " "), $crumb) . ' '));
                        }
                        ?>
                        <li class="user-footer">
                            <div class="pull-right">
                                <a href="{{ url('/logout') }}" onclick="fnLogout()" class="btn btn-default"><i
                                            class="fa fa-log-out text-red"></i></span>Sign out</a>

                            </div>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</nav>

