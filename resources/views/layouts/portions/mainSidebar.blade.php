<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
        @if(isset(Auth::user()->image))
                             <img src="{{asset(Auth::user()->image)}}" class="user-circle" alt="User Image">
                         @else
                             <img src="{{ asset('images/default-user.jpg') }}" alt="" class="user-circle">
                         @endif
        </div>
        <div class="pull-left info">
            <p>@if(isset(Auth::user()->display_name)) {{Auth::user()->display_name}}@endif</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
         <li class="{{ Request::is('dashboard') ? 'active' : '' }}" ><a href="{{ url('/dashboard') }}"  ><i class="fa fa-bars" style="color:#2E8B57"></i> <span>Dashboard</span></a></li>

        <!-- <li class="{{ Request::is('articles') ? 'active' : '' }}" ><a href="{{ url('/articles') }}"  ><i class="fa fa-newspaper" style="color:#004225"></i> <span>Articles</span></a></li> -->
        @if(Auth::user()->role_id == 1)
           
            <li class="{{ Request::is('dashboard/users') ? 'active' : '' }}" ><a   href="{{ url('/dashboard/users') }}" ><i class="fa fa-users" style="color:#2E8B57"></i> <span>Users</span></a></li>
             <li class="{{ Request::is('dashboard/shops') ? 'active' : '' }}" ><a   href="{{ url('/dashboard/shops') }}" ><i class="fa fa-industry" style="color:#2E8B57"></i> <span>Shops</span></a></li>
             
        @endif  
        @if(Auth::user()->role_id == 1)
          <li class="{{ Request::is('dashboard/categories') ? 'active' : '' }}" ><a   href="{{ url('/dashboard/categories') }}" ><i class="fa fa-utensils" style="color:#2E8B57"></i> <span>Categories</span></a></li>
          @else
          <li class="{{ Request::is('dashboard/shopcategories') ? 'active' : '' }}" ><a   href="{{ url('/dashboard/shopcategories') }}" >
            <i class="fa fa-utensils" style="color:#2E8B57"></i> <span>Categories</span></a></li>
          @endif 
        <li class="{{ Request::is('dashboard/orders') ? 'active' : '' }}" ><a   href="{{ url('/dashboard/orders') }}" ><i class="fa fa-shopping-cart" style="color:#2E8B57"></i> <span>Orders</span></a></li> 
        <li class="{{ Request::is('dashboard/products') ? 'active' : '' }}" ><a   href="{{ url('/dashboard/products') }}" ><i class="fa fa-tags" style="color:#2E8B57"></i> <span>Products</span></a></li>
        <li class="{{ Request::is('dashboard/addons') ? 'active' : '' }}" ><a   href="{{ url('/dashboard/addons') }}" ><i class="fa fa-puzzle-piece" style="color:#2E8B57"></i> <span>Addons</span></a></li>
         <li class="{{ Request::is('dashboard/offers') ? 'active' : '' }}" ><a   href="{{ url('/dashboard/offers') }}" ><i class="fa fa-gift" style="color:#2E8B57"></i> <span>Offers</span></a></li>
         <li class="{{ Request::is('dashboard/coupons') ? 'active' : '' }}" ><a   href="{{ url('/dashboard/coupons') }}" ><i class="fa fa-envelope-open" style="color:#2E8B57"></i> <span>Coupons</span></a></li>
         @if(Auth::user()->role_id == 1)
          <li class="{{ Request::is('dashboard/areas') ? 'active' : '' }}" ><a   href="{{ url('/dashboard/areas') }}" ><i class="fa fa-area-chart" style="color:#2E8B57"></i> <span>Delivery Areas</span></a></li>
          @else
          <li class="{{ Request::is('dashboard/shopareas') ? 'active' : '' }}" ><a   href="{{ url('/dashboard/shopareas') }}" ><i class="fa fa-area-chart" style="color:#2E8B57"></i> <span>Delivery Areas</span></a></li>
          @endif 
        <!-- <li class="{{ Request::is('dining') ? 'active' : '' }} treeview" ><a href="{{ url('/dining') }}"  ><i class="fa fa-hotel" style="color:#dd4b39"></i> <span>Dining</span><i class="fa fa-angle-left pull-right"></i></a>
             <ul class="treeview-menu menu-open">
                <li class="{{ Request::is('dining') ? 'active' : '' }}" ><a href="{{ url('/dining') }}"  ><i class="fa fa-utensils"></i> <span>Dining</span></a></li>
                <li class="{{ Request::is('dining/category') ? 'active' : '' }}" ><a href="{{ url('/dining/category') }}"  ><i class="fa fa-th"></i> <span>Dining Category</span></a></li>
                <li class="{{ Request::is('dining/healthymenu') ? 'active' : '' }}" ><a href="{{ url('/dining/healthymenu') }}"  ><i class="fa fa-th"></i> <span>Healthy Menu</span></a></li>
            </ul>
        </li> -->
        <!-- <li class="{{ Request::is('notifications') ? 'active' : '' }}" ><a href="{{ url('/notifications') }}"  ><i class="fa fa-bell" style="color:#dd4b39"></i> <span>Notifications</span></a></li> -->
        @if (Auth::check())
        <li><a href="{{ url('/logout') }}" onclick="fnLogout()"  ><i class="fa fa-sign-out-alt text-red" style="color:#2E8B57 !important"></i></span>Logout</a></li>
        @endif
    </ul>
</section>