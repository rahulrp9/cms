<section id="offerSection" data-section-name="offer" class="paymentOfferWrapper panel  vh-section col-dir-container" >
                <div class="paymentFeaturesWrapper animatedParent animateOnce" data-sequence='500'>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-lg-3 featuresHolder animated fadeInLeftShort" data-id='1'>
                                <figure>
                                    <img src="{{ URL::asset('images/iconFreeShipping.png') }}" alt="Free Shipping">
                                    <figcaption>Free Shipping
                                        <span>On Order Over $99</span>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="col-md-6 col-lg-3 featuresHolder animated fadeInLeftShort" data-id='2'>
                                <figure>
                                    <img src="{{ URL::asset('images/iconGuarantee.png') }}" alt="Guarantee">
                                    <figcaption>Guarantee
                                        <span>30 Days Money Back</span>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="col-md-6 col-lg-3 featuresHolder animated fadeInLeftShort" data-id='3'>
                                <figure>
                                    <img src="{{ URL::asset('images/iconPayment.png') }}" alt="Payment On Delivery">
                                    <figcaption>Payment On Delivery
                                        <span>Cash On Delivery Option</span>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="col-md-6 col-lg-3 featuresHolder animated fadeInLeftShort" data-id='4'>
                                <figure>
                                    <img src="{{ URL::asset('images/iconOnlineSupport.png') }}" alt="Online Support">
                                    <figcaption>Online Support
                                        <span>We Have Support 24/7</span>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="offerWrapper  fill-height-col animatedParent animateOnce" data-sequence='500'>

                    <div class="container">
                        <div class="row">

                            <div class="col-sm-7 offerInputs">
                                <h2 class='animated fadeInLeftShort' data-id='1'>Get Latest Offers </h2>
                                <p class='animated fadeInLeftShort' data-id='2'>Subscribe to our newsletter and get notified about updates, offers and new collections</p>
                                <div class="input-group">
                                    <input type="email" class="form-control" placeholder="Enter your Email Address" aria-label="Enter your Email Address" aria-describedby="button-offer">
                                    <div class="input-group-append">
                                        <button class="btn " type="button" id="button-offer">Subscribe</button>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                    <figure class='animated fadeInLeftShort' data-id='3'>
                        <img src="{{ URL::asset('images/imgGetOffer.png') }}" alt="offer">
                    </figure>
                </div>

                <footer id="footerSection" class="pageFooter animatedParent animateOnce" data-sequence='500'>

                    <div class="container">
                        <div class="row">
                            <div class="col-md-7 footerContact">
                                <div class="row">
                                    <div class="col-md-4 footerLogo animated fadeInLeftShort" data-id='2'>
                                        <figure>
                                            <a href="javascript:void(0);">
                                            <img src="{{ URL::asset('images/imgLogo.png') }}" alt="Athath Gate">
                                        </a>
                                        </figure>
                                        <a href="javascript:void(0);" class="linkEmail">contact@athathgate.com</a>
                                    </div>
                                    <div class="col-md-8 footerNav animated fadeInLeftShort" data-id='3'>
                                        <nav>
                                            Information
                                            <ul>
                                                <li>
                                                    <a href="javascript:void(0);">Home</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">About us</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">Contact us</a>
                                                </li>
                                            </ul>
                                        </nav>
                                        <nav>
                                            My Account
                                            <ul>
                                                <li>
                                                    <a href="javascript:void(0);">FAQ</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">Shipping and Returns</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">Store Policy</a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-5 copyright animated fadeInLeftShort" data-id='3'>
                                <span>Copyright © 2018 Athath Gate, Inc. All rights reserved.</span>
                                <div class="socialMedia">
                                    <ul>
                                        <li class="facebook">
                                            <a href="javascript:void(0);" target="_blank">
                                            <img src="{{ URL::asset('images/iconFacebook.png') }}" class="default" alt="Facebook" />
                                            <img src="{{ URL::asset('images/iconFacebookActive.png') }}" class="active" alt="Facebook" />
                                        </a>
                                        </li>
                                        <li class="twitter">
                                            <a href="javascript:void(0);" target="_blank">
                                            <img src="{{ URL::asset('images/iconTwitter.png') }}" class="default" alt="Twitter" />
                                            <img src="{{ URL::asset('images/iconTwitterActive.png') }}" class="active" alt="Twitter" />
                                        </a>
                                        </li>
                                        <li class="gmail">
                                            <a href="javascript:void(0);" target="_blank">
                                            <img src="{{ URL::asset('images/iconGooglePlus.png') }}" class="default" alt="Google Plus" />
                                            <img src="{{ URL::asset('images/iconGooglePlusActive.png') }}" class="active" alt="Google Plus" />
                                        </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </section>
        </section>
    </div>

</body>

</html>