<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Athath Gate</title>
    <link rel="icon" href="{{ URL::asset('favicon.png') }}" type="image/png" />
    <!--[if lt IE 9]><script src="js/IEupdate.js" type="text/javascript"></script><![endif]-->
    <script src="{{ URL::asset('js/jquery-2.1.1.min.js') }}" type="text/javascript"></script>
    <link type="text/css" href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet" media="all" />
    <link type="text/css" href="{{ URL::asset('css/slick.css') }}" rel="stylesheet" media="all" />
    <link type="text/css" href="{{ URL::asset('css/animate.css') }}" rel="stylesheet" media="all" />
    <link type="text/css" href="{{ URL::asset('css/styles.css') }}" rel="stylesheet" media="all" />
    
</head>
<?php 
 $crumbs = explode("/",$_SERVER["REQUEST_URI"]);
        if($crumbs[1]){$ctype = 'innerpage';} else{$ctype = 'homepage';}
        ?>
<body id="body" class="<?php echo $ctype;?>">
    <div class="layoutWrapper">
        <div class="mobileNav">
            <ul>
                <li>
                    <a href="javascript:void(0);">Home</a>
                </li>
                <li>
                    <a href="javascript:void(0);">About us</a>
                </li>
                <li>
                    <a href="javascript:void(0);">Contact us</a>
                </li>
                <li>
                    <a href="javascript:void(0);" rel="shop">Shop</a>
                    <ul>
                        <li>
                            <a href="javascript:void(0);">Furniture</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">BED &amp; bath</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Rugs</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Decor &amp; pillow</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">lighting</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">storage</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">kitchen</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Baby &amp; Kids</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Outdoor</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Renovation</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">ARM CHAIR</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">SHELF</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);">Checkout </a>
                </li>
                <li>
                    <a href="javascript:void(0);">My Account</a>
                    <ul>
                        <li>
                            <a href="javascript:void(0);">FAQ</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Shipping and Returns</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Store Policy</a>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
        <section id="pageContainer">
            <?php if($ctype == 'homepage'){ ?>
            <section id="topSection" data-section-name="home"  class=" panel   vh-section col-dir-container" >
            <?php } ?>
                <header id="headerSection" class="pageHeader fill-height-col ">

                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6 col-md-2  col-lg-3  pageLogo ">
                                <div class="mobNav">
                                    <div class="iconMobNav">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </div>
                                <figure>
                                    <a href="/">
                                        <div class="logo">
                                            <span class="logoImage">
                                                <img class="imglogo" src="{{ URL::asset('images/imgHeaderLogo.png') }}" alt="Athath Gate">
                                            </span>
                                            <span class="logoText">
                                                <img class="imglogoText1" src="{{ URL::asset('images/imgLogoText1.png') }}" alt="Athath Gate">
                                                <img class="imglogoText2" src="{{ URL::asset('images/imgLogoText2.png') }}" alt="Athath Gate">  
                                            </span>
                                        </div>
                                        
                                    </a>
                                </figure>
                            </div>
                            <div class="col-xs-6 col-md-10 col-lg-9 navSection">
                                <div class="navBar t-row">
                                    <div class="searchForm t-cell ">
                                        <form>
                                            <input type="email" class="form-control searchInput" placeholder="Search">
                                        </form>
                                    </div>
                                    <div class="pageNav t-cell ">
                                        <nav>
                                            <ul>
                                                <li>
                                                    <a href="/">Home</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);" rel="shop">Shop</a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    
                                    <div class="accountNav t-cell">
                                        <nav>
                                            <ul>
                                                <li>
                                                    <a href="">My Account </a>
                                                </li>
                                                @if(Auth::check())
                                                <li>
                                                    <a href="{{ URL::to('logout')}}">Logout</a>
                                                </li>
                                                @endif
                                            </ul>
                                        </nav>
                                    </div>
                                      
                                    <div class="cartNav t-cell">
                                        <nav>
                                            <ul>
                                                <li>
                                                @if(Auth::check())
                                                    <a href="javascript:void(0);" class="">
                                                @else
                                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#login-modal" class="">
                                                @endif        
                                                        <img src="{{ URL::asset('images/iconProfile.png') }}" alt="Profile" /> </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <img src="{{ URL::asset('images/iconFavourite.png') }}" alt="Favourite" />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::to('viewcart')}}">
                                                        <img src="{{ URL::asset('images/iconCart.png') }}" alt="Cart" />
                                                        <span>2</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <nav class="megaMenuWrapper">
                            <div id="shop" class="container megaMenu">
                                <ul>
                                    <li>
                                        <a href="javascript:void(0);">Furniture</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">BED &amp; bath</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Rugs</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Decor &amp; pillow</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">lighting</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">storage</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">kitchen</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Baby &amp; Kids</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Outdoor</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Renovation</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">ARM CHAIR</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">SHELF</a>
                                    </li>
                                </ul>
                            </div>

                        </nav>
                    </div>
                </header>
                

 @yield('content')

<section id="offerSection" data-section-name="offer" class="paymentOfferWrapper panel  vh-section col-dir-container" >
                <div class="paymentFeaturesWrapper ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-lg-3 featuresHolder">
                                <figure>
                                    <img src="{{ URL::asset('images/iconFreeShipping.png') }}" alt="Free Shipping">
                                    <figcaption>Free Shipping
                                        <span>On Order Over $99</span>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="col-md-6 col-lg-3 featuresHolder">
                                <figure>
                                    <img src="{{ URL::asset('images/iconGuarantee.png') }}" alt="Guarantee">
                                    <figcaption>Guarantee
                                        <span>30 Days Money Back</span>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="col-md-6 col-lg-3 featuresHolder">
                                <figure>
                                    <img src="{{ URL::asset('images/iconPayment.png') }}" alt="Payment On Delivery">
                                    <figcaption>Payment On Delivery
                                        <span>Cash On Delivery Option</span>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="col-md-6 col-lg-3 featuresHolder">
                                <figure>
                                    <img src="{{ URL::asset('images/iconOnlineSupport.png') }}" alt="Online Support">
                                    <figcaption>Online Support
                                        <span>We Have Support 24/7</span>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="offerWrapper  fill-height-col">

                    <div class="container">
                        <div class="row">

                            <div class="col-sm-7 offerInputs">
                                <h2>Get Latest Offers </h2>
                                <p>Subscribe to our newsletter and get notified about updates, offers and new collections</p>
                                <div class="input-group">
                                    <input type="email" class="form-control subemail" placeholder="Enter your Email Address" aria-label="Enter your Email Address" aria-describedby="button-offer">
                                    <div class="input-group-append">
                                    <input type="hidden" id="base_pathf" value="<?php echo url('/');?>">
                                     <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                     <input type="hidden" name="user_id" class="user_id" value="0">
                                        <button class="btn subsbtn" type="button" id="button-offer">Subscribe</button>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                    <figure>
                        <img src="{{ URL::asset('images/imgGetOffer.png') }}" alt="offer">
                    </figure>
                </div>

                <footer id="footerSection" class="pageFooter ">

                    <div class="container">
                        <div class="row">
                            <div class="col-md-7 footerContact">
                                <div class="row">
                                    <div class="col-md-4 footerLogo">
                                        <figure>
                                            <a href="javascript:void(0);">
                                            <img src="{{ URL::asset('images/imgLogo.png') }}" alt="Athath Gate">
                                        </a>
                                        </figure>
                                        <a href="javascript:void(0);" class="linkEmail">contact@athathgate.com</a>
                                    </div>
                                    <div class="col-md-8 footerNav">
                                        <nav>
                                            Information
                                            <ul>
                                                <li>
                                                    <a href="javascript:void(0);">Home</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">About us</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">Contact us</a>
                                                </li>
                                            </ul>
                                        </nav>
                                        <nav>
                                            My Account
                                            <ul>
                                                <li>
                                                    <a href="javascript:void(0);">FAQ</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">Shipping and Returns</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">Store Policy</a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-5 copyright">
                                <span>Copyright © 2018 Athath Gate, Inc. All rights reserved.</span>
                                <div class="socialMedia">
                                    <ul>
                                        <li class="facebook">
                                            <a href="javascript:void(0);" target="_blank">
                                            <img src="{{ URL::asset('images/iconFacebook.png') }}" class="default" alt="Facebook" />
                                            <img src="{{ URL::asset('images/iconFacebookActive.png') }}" class="active" alt="Facebook" />
                                        </a>
                                        </li>
                                        <li class="twitter">
                                            <a href="javascript:void(0);" target="_blank">
                                            <img src="{{ URL::asset('images/iconTwitter.png') }}" class="default" alt="Twitter" />
                                            <img src="{{ URL::asset('images/iconTwitterActive.png') }}" class="active" alt="Twitter" />
                                        </a>
                                        </li>
                                        <li class="gmail">
                                            <a href="javascript:void(0);" target="_blank">
                                            <img src="{{ URL::asset('images/iconGooglePlus.png') }}" class="default" alt="Google Plus" />
                                            <img src="{{ URL::asset('images/iconGooglePlusActive.png') }}" class="active" alt="Google Plus" />
                                        </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </section>
        </section>
    </div>
          <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-body mx-3">
      <div class="alert alert-success subspecsuccess" style="display: none;">
                     <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>   
                    Subscribed Successfully!</div>
                    <div class="alert alert-danger subspecfail" role="alert" style="display: none;">
                     <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>   
                    This EmailId already exist!</div>
       

      </div>
      
    </div>
  </div>
</div>
   <div class="modal fade login-modal" id="login-modal" tabindex="-1" role="dialog" aria-labelledby=""
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <input type="hidden" id="base_pathl" value="<?php echo url('/');?>">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ URL::asset('images/iconClose.png') }}" alt="Close">
                    </span>
                </button>
                <div class="modal-title text-center">Login</div>
                <p class='text-center'>Enter Your Username and <br>Password to login</p>
                <div class="form">
                    <form action="#">
                        <div class="form-group">
                            <input type='text' class="form-control lemail" placeholder="Username" name="email">
                        </div>
                        <div class="form-group">
                            <input type='password' class="form-control lpassword" placeholder="Password" name="password">
                            <div class="loginerror"></div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input remember" id="rememberme">
                            <label class="form-check-label" for="rememberme">Remember Me</label>
                        </div>
                    </div>
                    <div class="col-sm-6 text-left text-sm-right">
                        <a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#forgot-modal" class="lostpass">Lost your password?</a>
                    </div>
                </div>
                <div class="btn-wrapper text-center">
                    <button class="btn oval blue-1 loginbtn">LOGIN</button>
                </div>
                <div class="or"><span>or</span></div>
                <div class="text-center">
                    <a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#register-modal" class="create-ac">Create an Account >></a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade login-modal" id="register-modal" tabindex="-1" role="dialog" aria-labelledby=""
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ URL::asset('images/iconClose.png') }}" alt="Close">
                    </span>
                </button>
                <input type="hidden" id="base_pathr" value="<?php echo url('/');?>">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="modal-title text-center">CREATE AN ACCOUNT</div>
                <p class='text-center'>Creating an account has many beneficts: <br>checkout faster,keep more than one <br> address,track orders and more</p>
                <div class="form">
                    <form class="form-horizontal" method="POST" action="{{ url('register') }}">
                        <div class="form-group">
                            <input type='text' class="form-control fname" placeholder="First Name" required autofocus name="fname">
                        </div>
                        <div class="form-group">
                            <input type='text' class="form-control lname" placeholder="Last Name" name="lname">
                        </div>
                        <div class="form-group">
                            <input type='email' class="form-control remail" placeholder="Email" name="email">
                            <div class="emailerror"></div>
                        </div>
                        <div class="form-group">
                            <input type='password' class="form-control password" placeholder="Password" name="password">
                        </div>
                        <div class="form-group">
                            <input type='text' class="form-control phone_no" placeholder="Mobile No" name="phone_no">
                        </div>
                        <div class="form-group">
                           <div class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}" data-callback="recaptchaCallback"> </div>
                        </div>   
                    </form>
                </div>
               
                <div class="btn-wrapper text-center">
                    <button class="btn oval blue-1 register" type="submit" disabled>CREATE ACCOUNT</button>
                </div>
                <div class="or"><!-- <span></span> --></div>
                <div class="text-center">
                    <a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#login-modal" class="create-ac">
                    Back to Login >></a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade login-modal" id="forgot-modal" tabindex="-1" role="dialog" aria-labelledby=""
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <input type="hidden" id="base_pathf" value="<?php echo url('/');?>">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ URL::asset('images/iconClose.png') }}" alt="Close">
                    </span>
                </button>
                <div class="modal-title text-center">FORGET PASSWORD</div>
                <p class='text-center'>Lorem Ipsum is simply dummy text of the <br>printing and typesetting industry. Lorem <br>Ipsum is simply dummy text</p>
                <div class="form">
                    <form action="#">
                        <div class="form-group">
                            <input type='text' class="form-control femail" placeholder="Email" name="femail">
                        </div>
                    </form>
                </div>
                <div class="forgoterror"></div>
                <div class="btn-wrapper text-center">
                    <button class="btn oval blue-1 forgotbtn">CONTINUE</button>
                </div>
                </div>
            </div>
        </div>

        <div class="modal fade login-modal" id="conforgot-modal" tabindex="-1" role="dialog" aria-labelledby=""
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <input type="hidden" id="base_pathf" value="<?php echo url('/');?>">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ URL::asset('images/iconClose.png') }}" alt="Close">
                    </span>
                </button>
                <div class="modal-title text-center"></div>
                <p class='text-center'>Reset link Sent to Mail!!</p>
                <div class="forgoterror"></div>
                </div>
            </div>
        </div>
        @if(app('request')->input('token'))
        <div class="modal fade login-modal" id="reset-modal" tabindex="-1" role="dialog" aria-labelledby=""
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <input type="hidden" id="base_pathfo" value="<?php echo url('/');?>">
                <input type="hidden" name="_token" id="tokenreset" value="{{ app('request')->input('token') }}">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ URL::asset('images/iconClose.png') }}" alt="Close">
                    </span>
                </button>
                <div class="modal-title text-center">RESET PASSWORD</div>
                <p class='text-center'>Lorem Ipsum is simply dummy text of the <br>printing and typesetting industry. Lorem <br>Ipsum is simply dummy text</p>
                <div class="form">
                    <form action="#">
                        <div class="form-group">
                            <input type='password' class="form-control fpassword" placeholder="New Password" name="fpassword">
                        </div>
                        <div class="form-group">
                            <input type='password' class="form-control cpassword" placeholder="Confirm New Password" name="cpassword">
                            <input type="hidden" name="remail" class="remail" value="">
                        </div>
                    </form>
                </div>
                <div class="reseterror"></div>
                <div class="btn-wrapper text-center">
                    <button class="btn oval blue-1 resetbtn">SUBMIT</button>
                </div>
                </div>
            </div>
        </div>

       @endif 
    </div>
    <script src="{{ URL::asset('js/popper.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/iscroll.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/slick.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/easing.js') }}" type="text/javascript"></script>

    <script src="{{ URL::asset('js/jquery.scrollify.js') }}"></script>
    <script src="{{ URL::asset('js/custom_scripts.js') }}" type="text/javascript"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- <script type="text/javascript">
       $(function() {


        $.scrollify({
            section:".panel",
            scrollSpeed:1100
           
        });

    });

    </script> -->
    <script type="text/javascript">
    
    $(document).ready(function () {
         var token = $('#token').val(); 

            var hasParam = window.location.href.indexOf('token');
                if(hasParam) {
                  $('#reset-modal').modal();
                } else {
                  $('#reset-modal').hide();
                }
        $(".subsbtn").click(function() {
            var base_path = $('#base_pathf').val();
            var subemail  = $('.subemail').val();
            var user_id   = $('.user_id').val();
            if (subemail == '') {
                $('.subemail').css('border','1px solid red');
                errors = 1;
                return false;
            }
            else{
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                if (reg.test(subemail) == false) 
                {
                    alert('Invalid Email Address');
                    return false;
                }
                $('.subemail').css('border','none');  
            }
            $.ajax({
                        type: 'post',
                        url: base_path+'/subscription',
                        data: 'email='+subemail+'&user_id='+user_id+'&_token='+token,
                        datatype: "json",
                        async: false,
                        cache: false,
                        timeout: 30000,
                        success: function (response) {
                            $('#modalLoginForm').modal();
                             if (response == 1) {
                                $('.subspecfail').fadeIn();
                                setTimeout(function () {
                                     $('#modalLoginForm').modal('hide');
                                     $('.subspecfail').fadeOut();
                                 }, 1000);
                            }
                            else if (response == 2) {
                              $('.subspecsuccess').fadeIn();
                              setTimeout(function () {
                                     $('#modalLoginForm').modal('hide');
                                     $('.subspecsuccess').fadeOut();
                                 },1000); 
                            } else {
                                 errors = 0;
                                 $('#modalLoginForm').modal('hide');
                                 $('.subspecfail').fadeOut();
                                 $('.subspecsuccess').fadeOut();
                            }
                        }
                    });
        });


        $('.register').click(function(){

            var base_pathr = $('#base_pathr').val();
            var fname    = $('.fname').val();
            var lname    = $('.lname').val();
            var remail   = $('.remail').val();
            var password = $('.password').val();
            var phone_no = $('.phone_no').val();
            if(fname.trim() == '' ){
                $('.fname').css('border','1px solid red');
                $('.fname').focus();
                return false;
            }
            else{

                $('.fname').css('border','1px solid #d9d9d9');
            }
            if(lname.trim() == '' ){
                $('.lname').css('border','1px solid red');
                $('.lname').focus();
                return false;
            }
            else{

                $('.lname').css('border','1px solid #d9d9d9');
            }
            if (remail == '') {
                $('.remail').css('border','1px solid red');
                errors = 1;
                return false;
            }
            else{
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                if (reg.test(remail) == false) 
                {
                    $('.remail').css('border','1px solid red');
                    $('.remail').focus();
                    return false;
                }
                $('.remail').css('border','1px solid #d9d9d9');
            }
            if(password.trim() == '' ){
                $('.password').css('border','1px solid red');
                $('.password').focus();
                return false;
            }
            else{

                $('.password').css('border','1px solid #d9d9d9');
            }
            if(phone_no.trim() == '' ){
                $('.phone_no').css('border','1px solid red');
                $('.phone_no').focus();
                return false;
            }
            else{
                 var filter = /^[0-9-+]+$/;
                    if (filter.test(phone_no)) {
                        $('.phone_no').css('border','1px solid #d9d9d9');
                    }
                    else {
                            $('.phone_no').css('border','1px solid red');
                            $('.phone_no').focus();
                            return false;
                    }
            }
            $.ajax({
                        type: 'post',
                        url: base_pathr+'/register',
                        data: 'fname='+fname+'&lname='+lname+'&email='+remail+'&password='+password+'&phone='+phone_no+'&_token='+token,
                        datatype: "json",
                        async: false,
                        cache: false,
                        timeout: 30000,
                        success: function (response) {
                            console.log(response.Result.error);
                                console.log(response);
                            if(response.Result.status ==  false){
                                console.log(response.Result.message);
                                console.log(response);
                                $('.emailerror').html(response.Result.error);
                                $('.emailerror').fadeIn();
                            }else{

                                $('#register-modal').modal('hide');
                                $('.emailerror').fadeOut();
                                location.reload();
                            }

                            
                        }
            });   
            
        });


        $('.loginbtn').click(function(){
            $('.loginerror').fadeOut();
            var base_pathl = $('#base_pathl').val();
            var lemail     = $('.lemail').val();
            var lpassword  = $('.lpassword').val();
            
            if (lemail == '') {
                $('.lemail').css('border','1px solid red');
                errors = 1;
                return false;
            }
            else{
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                if (reg.test(lemail) == false) 
                {
                    $('.lemail').css('border','1px solid red');
                    $('.lemail').focus();
                    return false;
                }
                $('.lemail').css('border','1px solid #d9d9d9');
            }
            if(lpassword.trim() == '' ){
                $('.lpassword').css('border','1px solid red');
                $('.lpassword').focus();
                return false;
            }
            else{

                $('.lpassword').css('border','1px solid #d9d9d9');
            }
            
            $.ajax({
                        type: 'post',
                        url: base_pathl+'/login',
                        data: 'email='+lemail+'&password='+lpassword+'&_token='+token,
                        datatype: "json",
                        async: false,
                        cache: false,
                        timeout: 30000,
                        success: function (response) {
                            if(response.Result.status ==  false){
                                $('.loginerror').html(response.Result.error);
                                $('.loginerror').fadeIn();
                            }else{

                                $('#login-modal').modal('hide');
                                $('.loginerror').fadeOut();
                                location.reload();
                            }

                            
                        }
            });   
            
        });


        $('.forgotbtn').click(function(){
            $('.forgoterror').fadeOut();
            var base_pathf = $('#base_pathf').val();
            var femail     = $('.femail').val();
            
            if (femail == '') {
                $('.femail').css('border','1px solid red');
                errors = 1;
                return false;
            }
            else{
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                if (reg.test(femail) == false) 
                {
                    $('.femail').css('border','1px solid red');
                    $('.femail').focus();
                    return false;
                }
                $('.femail').css('border','1px solid #d9d9d9');
            }
            
            $.ajax({
                        type: 'post',
                        url: base_pathf+'/resetpass',
                        data: 'email='+femail+'&_token='+token,
                        datatype: "json",
                        async: false,
                        cache: false,
                        timeout: 30000,
                        success: function (response) {
                            //console.log(response);
                            if(response.Result.status ==  false){
                                $('.forgoterror').html(response.Result.error);
                                $('.forgoterror').fadeIn();
                            }else{


                                $('#forgot-modal').modal('hide');
                                $('#conforgot-modal').modal();
                                //$('.remail').val(femail);
                                $('.forgoterror').fadeOut();
                            }

                            
                        }
            });   
            
        });


        $('.resetbtn').click(function(){
            $('.reseterror').fadeOut();
            var base_pathfo = $('#base_pathfo').val();
            var fpassword   = $('.fpassword').val();
            var cpassword   = $('.cpassword').val();
            var remail      = $('.remail').val();
            var tokenreset  = $('#tokenreset').val();
            
            if (fpassword == '') {
                $('.fpassword').css('border','1px solid red');
                errors = 1;
                return false;
            }
            else{

                $('.fpassword').css('border','1px solid #d9d9d9');
            }
            if (cpassword == '') {
                $('.cpassword').css('border','1px solid red');
                errors = 1;
                return false;
            }
            else{
                
                $('.cpassword').css('border','1px solid #d9d9d9');
            }
            if(fpassword != cpassword){

                $('.reseterror').fadeIn();
                $('.reseterror').html('Password Doesnot Match!!');
                errors = 1;
                return false;
            }
            else{

                $('.reseterror').fadeOut();
                $('.reseterror').html('');
            }
            $.ajax({
                        type: 'post',
                        url: base_pathfo+'/updatepass',
                        data: 'fpassword='+fpassword+'&email='+remail+'&_token='+token+'&tokenreset='+tokenreset,
                        datatype: "json",
                        async: false,
                        cache: false,
                        timeout: 30000,
                        success: function (response) {
                            //console.log(response);
                            if(response.Result.status ==  false){
                                $('.reseterror').html(response.Result.error);
                                $('.reseterror').fadeIn();
                            }else{
                                $('.remail').val('');
                                $('.reseterror').fadeOut();
                                $('#reset-modal').modal('hide');
                                $('#login-modal').modal();
                                
                            }

                            
                        }
            });   
            
        }); 

         

    });    

        function recaptchaCallback() {
            $('.register').removeAttr('disabled');
        }
</script>
</body>

</html>            