@extends('layouts.app')
@section('content')
<style type="text/css">
    .box .row{
         margin-bottom: 15px;
    }
</style>
    <div class="row">
    <div class="col-md-12">
            <div class="col-md-6">
                <h1 class="box-title">Shop Details  </h1>
            </div>    
    <div class="pull-right box-tools">
                    <a href="{{URL::to('dashboard/shops')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default btn-default" type="button" data-original-title="Service List">
                            <i class="fa fa-list"></i>
                        </button>
                    </a>
     @if(isset($service))
                    <a href="{{URL::to('dashboard/shops/create')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default" type="button" data-original-title="Add Service">
                            <i class="fa fa-plus"></i>
                        </button>
                    </a>
   
                    <a href="{{URL::to('dashboard/shops/'.$service->id.'/edit')}}">
                        <button title="" data-toggle="tooltip" class="btn btn-default" type="button" data-original-title="Edit Service">
                            <i class="fa fa-edit"></i>
                        </button>
                    </a>
    @endif               
                </div>
    </div>            
    </div>
        <div class="box">
            <div class="box-body pad" id="tab_1">
                <div class="row">
                    <label class="col-sm-3 control-label">Shop English</label>
                    <div class="col-sm-6">
                        {{ $shop->name }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Email</label>
                    <div class="col-sm-3">
                        {{ $shop->email }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Phone</label>
                    <div class="col-sm-3">
                        {{ $shop->phone }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Address</label>
                    <div class="col-sm-3">
                        {{ $shop->address }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Street</label>
                    <div class="col-sm-3">
                        {{ $shop->street }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Latitude</label>
                    <div class="col-sm-3">
                        {{ $shop->latitude }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Longitude</label>
                    <div class="col-sm-3">
                        {{ $shop->longitude }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label">Shop Image</label>
                    <div class="col-sm-3">
                        @if(!empty($shop->image))
                                    <a href="javascript:void(0)" class="pop" imgsrc="{{asset($shop->space_image)}}"><img src=" {{ asset($shop->image) }}" class="img-responsive img-rounded" width="150" height="1150" /></a>
                        @else
                            <img src="{{asset('assets/images/no-image.png')}}" />
                        @endif
                    </div>
                </div>
              
        </div>
    </div>
@endsection
