@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{ URL::asset('css/custom_style2.css')}}">
<style type="text/css">
    a {
    color: #3c8dbc;
    padding: 5px;
}
</style>
<div class="contentHolderV1">
    <h2>Users</h2>
    <div class="row">
          <div class="col-md-6"> 
           <!--  <a href="{{ URL::to('deleted/services') }}" class="searchbtn pull-right">Deleted Services</a> -->
        </div>
       <!--  <div class=" pull-right" style="float: right;"> 
             <a href="{{ URL::to('dashboard/shops/create') }}" class="searchbtn">Add Shop</a>
        </div> -->
      
    </div>    
   
    <div class="tableHolder">
        <table style="width: 100%;" class="tableV1" cellpadding="0" cellspacing="0">
            <thead class="tableHeader">
                <tr>
                    <td>#</td>
                    <td>User Name</td>
                    <td>Phone</td>
                    <td>Status</td>
                    <!-- <td>Action</td> -->
                </tr>
            </thead>
            <tbody>
               
               @foreach($users as $service)
               <?php  //$n++; ?>

                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $service->name }}</td>
                        <td>{{$service->phone}}</td>
                        <td>@if($service->status == 1) Active @else InActive @endif</td>
                        <!-- <td><div  class="actions">
                                    <a href="{{ URL::to('dashboard/shops/'.$service->id.'/edit') }}" class="edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ URL::to('dashboard/shops', ['id' => ($service->id)]) }}" class="view">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <form action="{{ route('admin.shops.destroy', $service->id) }}" method="post" style="float: left;">
                                         {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                                        <a class="delete" data-id=""><i class="fa fa-trash-alt"></i></a>
                                           
                                         </form>
                                    
                                </div></td> -->
                       
                    </tr>
                    
                     @endforeach
            </tbody>
        </table>
    </div>
<ul class="pagination">
        {{ $users->render() }}
    </ul>
</div>
<script type="text/javascript">
    
    $(function () {
        $('.delete').click(function(e){
            e.preventDefault();
            if(confirm('Are you sure you want to delete?'))
            {
                $(this).parent('form').submit();
            }else{
                return false;
            }
        });
});

    
</script>
@endsection