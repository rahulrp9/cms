<!-- resources/views/tasks/index.blade.php -->

@extends('layouts.app')

@section('content')

<style type="text/css">
  .container {
    width: 100% !important;
  }.h4, h4 {
    font-size: 18px;
    font-weight: bold;
  }
  .boxcustom{
    background: #ccc none repeat scroll 0 0 !important;
  }
  @media only screen and (max-width: 800px) {
    
    /* Force table to not be like tables anymore */
    #private-ajax table, 
    #private-ajax thead, 
    #private-ajax tbody, 
    #private-ajax th, 
    #private-ajax td, 
    #private-ajax tr { 
      display: block; 
      width: 100%
    }
    
    /* Hide table headers (but not display: none;, for accessibility) */
    #private-ajax thead tr { 
      position: absolute;
      top: -9999px;
      left: -9999px;
    }
    
    #private-ajax tr { border: 1px solid #ccc; }
    
    #private-ajax td { 
      /* Behave  like a "row" */
      border: none;
      border-bottom: 1px solid #eee; 
      position: relative;
      padding-left: 50%; 
      white-space: normal;
      text-align:left;
    }
    
    #private-ajax td:before { 
      /* Now like a table header */
      position: absolute;
      /* Top/left values mimic padding */
      top: 6px;
      left: 6px;
      width: 45%; 
      padding-right: 10px; 
      white-space: nowrap;
      text-align:left;
      font-weight: bold;
    }
    
  /*
  Label the data
  */
  #private-ajax td:before { content: attr(data-title); }
}
@media only screen and (max-width: 800px) {
  
  /* Force table to not be like tables anymore */
  #private-ajax-user table, 
  #private-ajax-user thead, 
  #private-ajax-user tbody, 
  #private-ajax-user th, 
  #private-ajax-user td, 
  #private-ajax-user tr { 
    display: block; 
    width: 100%
  }
  
  /* Hide table headers (but not display: none;, for accessibility) */
  #private-ajax-user thead tr { 
    position: absolute;
    top: -9999px;
    left: -9999px;
  }
  
  #private-ajax-user tr { border: 1px solid #ccc; }
  
  #private-ajax-user td { 
    /* Behave  like a "row" */
    border: none;
    border-bottom: 1px solid #eee; 
    position: relative;
    padding-left: 50%; 
    white-space: normal;
    text-align:left;
  }
  
  #private-ajax-user td:before { 
    /* Now like a table header */
    position: absolute;
    /* Top/left values mimic padding */
    top: 6px;
    left: 6px;
    width: 45%; 
    padding-right: 10px; 
    white-space: nowrap;
    text-align:left;
    font-weight: bold;
  }
  
  /*
  Label the data
  */
  #private-ajax-user td:before { content: attr(data-title); }
}
</style>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.css">

<div class="box boxcustom">
  <div class="box-header">
    <h3 class="box-title"></h3>
  </div>
  <div class="box-body">
    {!! csrf_field() !!}
    <div class="container">
      <div class="header clearfix">
                    <!-- <nav>
                       <ul class="nav nav-pills pull-right">
                         <li role="presentation" class="active"><a href="#">Home</a></li>
                         <li role="presentation"><a href="#">About</a></li>
                         <li role="presentation"><a href="#">Contact</a></li>
                       </ul>
                     </nav> -->
                   </div>
          <!--   <div class="">

                <div class="jumbotron">
                    <h1>{{trans('message.welcome') }}</h1> 
                
                   <p><a class="btn btn-lg btn-success" href="{{ url('/adminProfileEdit') }}" role="button">Edit Profile</a></p>
                </div>
              </div> -->
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-aqua"><i class="fa fa-utensils"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Users</span>
                        <span class="info-box-number">{{$users}}</span>
                      </div>

                    </div>
                    
                  </div>
                  <div class="col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-red"><i class="fa fa-dumbbell"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total shops</span>
                        <span class="info-box-number">{{$shops}}</span>
                      </div>
                    </div>
                    
                  </div>
                  <div class="col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-green"><i class="fa fa-hotel"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Products</span>
                        <span class="info-box-number">{{$products}}</span>
                      </div>
                    </div>
                    
                  </div>
                  <div class="col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-teal"><i class="fa fa-newspaper"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Orders</span>
                        <span class="info-box-number">{{$orders}}</span>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </div>
          
        </div>
      </div>
    </div>
    <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-8">
                    <p class="text-center">
                      <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
                    </p>

                    <div class="chart">
                      <!-- Sales Chart Canvas -->
                      <canvas id="salesChart" height="180" style="height: 180px;"></canvas>
                    </div>
                    <!-- /.chart-responsive -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-4">
                    <p class="text-center">
                      <strong>Goal Completion</strong>
                    </p>

                    <div class="progress-group">
                      Add Products to Cart
                      <span class="float-right"><b>160</b>/200</span>
                      <div class="progress progress-sm">
                        <div class="progress-bar bg-primary" style="width: 80%"></div>
                      </div>
                    </div>
                    <!-- /.progress-group -->

                    <div class="progress-group">
                      Complete Purchase
                      <span class="float-right"><b>310</b>/400</span>
                      <div class="progress progress-sm">
                        <div class="progress-bar bg-danger" style="width: 75%"></div>
                      </div>
                    </div>

                    <!-- /.progress-group -->
                    <div class="progress-group">
                      <span class="progress-text">Visit Premium Page</span>
                      <span class="float-right"><b>480</b>/800</span>
                      <div class="progress progress-sm">
                        <div class="progress-bar bg-success" style="width: 60%"></div>
                      </div>
                    </div>

                    <!-- /.progress-group -->
                    <div class="progress-group">
                      Send Inquiries
                      <span class="float-right"><b>250</b>/500</span>
                      <div class="progress progress-sm">
                        <div class="progress-bar bg-warning" style="width: 50%"></div>
                      </div>
                    </div>
                    <!-- /.progress-group -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- ./card-body -->
              <!-- <div class="card-footer">
                <div class="row">
                  <div class="col-sm-3 col-6">
                    <div class="description-block border-right">
                      <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> 17%</span>
                      <h5 class="description-header">$35,210.43</h5>
                      <span class="description-text">TOTAL REVENUE</span>
                    </div>
                  </div>
                  <div class="col-sm-3 col-6">
                    <div class="description-block border-right">
                      <span class="description-percentage text-warning"><i class="fas fa-caret-left"></i> 0%</span>
                      <h5 class="description-header">$10,390.90</h5>
                      <span class="description-text">TOTAL COST</span>
                    </div>
                  </div>
                  <div class="col-sm-3 col-6">
                    <div class="description-block border-right">
                      <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> 20%</span>
                      <h5 class="description-header">$24,813.53</h5>
                      <span class="description-text">TOTAL PROFIT</span>
                    </div>
                  </div>
                  <div class="col-sm-3 col-6">
                    <div class="description-block">
                      <span class="description-percentage text-danger"><i class="fas fa-caret-down"></i> 18%</span>
                      <h5 class="description-header">1200</h5>
                      <span class="description-text">GOAL COMPLETIONS</span>
                    </div>
                  </div>
                </div>
              </div> -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

<div class="contentHolderV1">
    <h2>Recent Orders</h2>
    <div class="tableHolder">
    <table style="width: 100%;" class="tableV1" cellpadding="0" cellspacing="0">
        <thead class="tableHeader">
            <tr>
                <td>Order Number</td>
                <td>Product Name</td>
                <td>Seller Name</td>
                <td>Buyer Name</td>
                <td>Date</td>
                <td>Order Status</td>
                <td class="actionHolder">Actions</td>
            </tr>
        </thead>
 
    </table>
    </div>

</div>
    

    
    
    <input type="hidden" id="back" autocomplete="off" value="" /> 
  </div>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
  var salesChartCanvas = $('#salesChart').get(0).getContext('2d')

  var salesChartData = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'Digital Goods',
        backgroundColor: 'rgba(60,141,188,0.9)',
        borderColor: 'rgba(60,141,188,0.8)',
        pointRadius: false,
        pointColor: '#3b8bba',
        pointStrokeColor: 'rgba(60,141,188,1)',
        pointHighlightFill: '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data: [28, 48, 40, 19, 86, 27, 90]
      },
      {
        label: 'Electronics',
        backgroundColor: 'rgba(210, 214, 222, 1)',
        borderColor: 'rgba(210, 214, 222, 1)',
        pointRadius: false,
        pointColor: 'rgba(210, 214, 222, 1)',
        pointStrokeColor: '#c1c7d1',
        pointHighlightFill: '#fff',
        pointHighlightStroke: 'rgba(220,220,220,1)',
        data: [65, 59, 80, 81, 56, 55, 40]
      }
    ]
  }

  var salesChartOptions = {
    maintainAspectRatio: false,
    responsive: true,
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: false
        }
      }],
      yAxes: [{
        gridLines: {
          display: false
        }
      }]
    }
  }

  // This will get the first returned node in the jQuery collection.
  // eslint-disable-next-line no-unused-vars
  var salesChart = new Chart(salesChartCanvas, {
    type: 'line',
    data: salesChartData,
    options: salesChartOptions
  }
  )
  });
</script>
@endsection