@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{ URL::asset('css/custom_style2.css')}}">
<style type="text/css">
    a {
    color: #3c8dbc;
    padding: 5px;
}
</style>
<div class="contentHolderV1">
    <h2>Shops</h2>
    <div class="row">
          <div class="col-md-6"> 
           <!--  <a href="{{ URL::to('deleted/services') }}" class="searchbtn pull-right">Deleted Services</a> -->
        </div>
        <div class=" pull-right" style="float: right;"> 
             <a href="{{ URL::to('dashboard/shops/create') }}" class="searchbtn">Add Shop</a>
        </div>
      
    </div>    
   
    <div class="tableHolder">
        <table style="width: 100%;" class="tableV1" cellpadding="0" cellspacing="0">
            <thead class="tableHeader">
                <tr>
                    <td>#</td>
                    <td>Shop Name</td>
                    <td>Status</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
               
               @foreach($services as $service)
               <?php  //$n++; ?>

                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td><a href="{{ URL::to('dashboard/shops', ['id' => ($service->id)]) }}" class="view">{{ $service->name }}</a></td>
                        <td>
                         @if($service->status == 1)
                                     <button data-pdtid="{{$service->id}}" class="publish btn btn-primary">Active</button> @else <button data-pdtid="{{$service->id}}" class="unpublish btn btn-danger">InActive</button> 
                                     @endif
                        </td>             
                        <td><div  class="actions">
                                    <a href="{{ URL::to('dashboard/shops/'.$service->id.'/edit') }}" class="edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ URL::to('dashboard/shops', ['id' => ($service->id)]) }}" class="view">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <!-- <form action="{{ route('admin.shops.destroy', $service->id) }}" method="post" style="float: left;">
                                         {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                                        <a class="delete" data-id=""><i class="fa fa-trash-alt"></i></a>
                                           
                                         </form> -->
                                    
                                </div></td>
                       
                    </tr>
                    
                     @endforeach
            </tbody>
        </table>
    </div>
<ul class="pagination">
        {!! $services->render() !!}
    </ul>
</div>
<script type="text/javascript">
    
    $(function () {
        $('.delete').click(function(e){
            e.preventDefault();
            if(confirm('Are you sure you want to delete?'))
            {
                $(this).parent('form').submit();
            }else{
                return false;
            }
        });
    });
    $(document).on('click', '.publish', function (){

        var pdtid = $(this).attr('data-pdtid');
        $.ajax({
                type: 'get',
                url: '<?php echo url('/');?>/dashboard/shopsbtn/unpublish',
                data: 'pdtid='+pdtid,
                datatype: "json",
                async: false,
                cache: false,
                timeout: 30000,
                    success: function (response) {
                        location.reload();
                    }
        });


    });


    $(document).on('click', '.unpublish', function (){

        var pdtid = $(this).attr('data-pdtid');
        $.ajax({
                type: 'get',
                url: '<?php echo url('/');?>/dashboard/shopsbtn/publish',
                data: 'pdtid='+pdtid,
                datatype: "json",
                async: false,
                cache: false,
                timeout: 30000,
                    success: function (response) {
                        location.reload();
                    }
        });


    });

    
</script>
@endsection