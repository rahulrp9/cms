<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'environment' => env('PAYTM_ENVIRONMENT', 'local'),
    'merchant_id' => env('PAYTM_MERCHANT_ID', 'DdOFsV88869699136615'),
    'key' => env('PAYTM_MERCHANT_KEY', '!tEaU@&SuYWzCTC4'),
    'website' => env('PAYTM_MERCHANT_WEBSITE', 'WEBSTAGING'),
    'channel' => env('PAYTM_CHANNEL', 'WEB'),
    'industry' => env('PAYTM_INDUSTRY_TYPE', 'Retail'),
    'payment_mode' => env('PAYMENT_MODE_ONLY', 'YES'),
    'payment_type' => env('PAYMENT_TYPE_ID', 'CC,NB'),

];