<?php

use Illuminate\Database\Seeder;
use App\Model\Admin\AttendeesCategory;

class AttendeesCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = AttendeesCategory::create([
            'name' => 'Speakers',
            'name_ar' => 'مكبرات الصوت
            ',
            'status' => 1
        ]);
        $user = AttendeesCategory::create([
            'name' => 'Sponsors',
            'name_ar' => 'الرعاة',
            'status' => 1
        ]);
        $user = AttendeesCategory::create([
            'name' => 'Exhibitors',
            'name_ar' => 'العارضون',
            'status' => 1
        ]);
        $user = AttendeesCategory::create([
            'name' => 'Vip',
            'name_ar' => 'كبار الشخصيات
            ',
            'status' => 1
        ]);
        $user = AttendeesCategory::create([
            'name' => 'VVip',
            'name_ar' => 'رياضات',
            'status' => 1
        ]);
        $user = AttendeesCategory::create([
            'name' => 'Guest',
            'name_ar' => 'زائر
            ',
            'status' => 1
        ]);

        $user = AttendeesCategory::create([
            'name' => 'Coach',
            'name_ar' => 'مدرب
            ',
            'status' => 1
        ]);
        $user = AttendeesCategory::create([
            'name' => 'Visitors',
            'name_ar' => 'الزائرين
            ',
            'status' => 1
        ]);
        $user = AttendeesCategory::create([
            'name' => 'Players',
            'name_ar' => 'لاعبين',
            'status' => 1
        ]);
    }
}
