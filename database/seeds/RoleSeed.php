<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'administrator']);

        $role->givePermissionTo('users_manage');
        $role->givePermissionTo('dashboard');
        $role->givePermissionTo('users');
        $role->givePermissionTo('registerd_users');
        $role->givePermissionTo('non_registerd_users');
        $role->givePermissionTo('events');
        $role->givePermissionTo('archived_events');
        $role->givePermissionTo('category');
        $role->givePermissionTo('scanning_app_users');
        $role->givePermissionTo('app_contents');
        $role->givePermissionTo('contact_us');
        $role->givePermissionTo('privacy_policy');
        $role->givePermissionTo('terms_and_conditions');
        $role->givePermissionTo('settings');
        $role->givePermissionTo('countries');
        $role->givePermissionTo('cities');
        $role->givePermissionTo('sent_notifications');
        $role->givePermissionTo('event_delete');
        $role->givePermissionTo('about_us');
        $role->givePermissionTo('eventcategory');
    }
}