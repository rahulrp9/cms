<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin',
            'email' => 'andrixsoftwares@gmail.com',
            'password' => bcrypt('12345678')
        ]);
        $user->assignRole('administrator');
    }
}
