<?php

use Illuminate\Database\Seeder;
use App\Model\Admin\EventCategory;

class EventCategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = EventCategory::create([
            'name' => 'Sports',
            'name_ar' => 'رياضات',
            'status' => 1
        ]);
    }
}