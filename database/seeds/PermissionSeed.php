<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('cache:clear');
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'users_manage']);
        Permission::create(['name' => 'dashboard']);
        Permission::create(['name' => 'users']);
        Permission::create(['name' => 'registerd_users']);
        Permission::create(['name' => 'non_registerd_users']);
        Permission::create(['name' => 'events']);
        Permission::create(['name' => 'archived_events']);
        Permission::create(['name' => 'category']);
        Permission::create(['name' => 'scanning_app_users']);
        Permission::create(['name' => 'app_contents']);
        Permission::create(['name' => 'contact_us']);
        Permission::create(['name' => 'privacy_policy']);
        Permission::create(['name' => 'terms_and_conditions']);
        Permission::create(['name' => 'settings']);
        Permission::create(['name' => 'countries']);
        Permission::create(['name' => 'cities']);
        Permission::create(['name' => 'sent_notifications']);
        Permission::create(['name' => 'event_delete']);
        Permission::create(['name' => 'about_us']);
        Permission::create(['name' => 'eventcategory']);

    }
}