<?php

use Illuminate\Database\Seeder;

class CompanySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company')->insert([
            'name' => 'TEST PROJECT',
            'description' => '',
            'logo' => '',
            'year' => '2020',
            'email' => 'test@gmail.com',
            'phone' => '0099999999',
            'address' => 'Akshya Nagar 1st Block 1st Cross, Rammurthy nagar, Bangalore-560016'
        ]);
    }
}
