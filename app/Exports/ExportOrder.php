<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExportOrder implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public $view;
    public $data;

    public function __construct($view, $data = "") {
        $this->view = $view;
        $this->data = $data;
    }
    
    public function view(): View
    {
         libxml_use_internal_errors(true);
        $data = $this->data;
        $export = 1;
        return view($this->view,compact('data','export'));
    }
}
