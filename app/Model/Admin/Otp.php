<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{

    protected $table = 'otp';

    protected $fillable = [
    	'user_id','otp','phone','country_code'
    ];
}
