<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class cartCustom extends Model
{

    protected $table = 'cart_custom';

    protected $fillable = [
    	'cart_id','customise_id','quantity'
    ];
}
