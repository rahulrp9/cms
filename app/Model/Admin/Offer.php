<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{

    protected $table = 'offers';

    protected $fillable = [
    	'name','image','value','status','shop_id'
    ];
}
