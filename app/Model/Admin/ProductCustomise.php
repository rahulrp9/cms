<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class ProductCustomise extends Model
{

    protected $table = 'products_customise';

    protected $fillable = [
    	'product_id','name','status','price'
    ];
}
