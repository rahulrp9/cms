<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{

    protected $table = 'shops';

    protected $fillable = [
    	'user_id','name','image','is_opened','shop_type','min_time','min_order','address','street','latitude','longitude','common_type','status'
    ];
}
