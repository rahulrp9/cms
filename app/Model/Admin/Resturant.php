<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Resturant extends Model
{

    protected $table = 'resturants';

    protected $fillable = [
    	'user_id','name','image','status','address','lattitude','longitude'
    ];
}
