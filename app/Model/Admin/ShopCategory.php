<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class ShopCategory extends Model
{

    protected $table = 'shop_categories';

    protected $fillable = [
    	'category_id','shop_id'
    ];
}
