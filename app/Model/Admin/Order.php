<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'order';

    protected $fillable = [
    	'user_id','amount','paymethod','card_type','payment_status','address_id','delivery_type','status','order','shop_id'
    ];
}
