<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    //
    protected $table = 'usertoken';
    protected $fillable = [
        'user_id',
		'token',
    ];
}
