<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function scopeActive($query)
    {
        return $query->where('status', 1)->orderBy('id', 'DESC');
    }
    public function getAll()
    {
        return $this::orderBy('id', 'DESC')->get();
    }

    public function ActiveCity()
    {
        return $this->hasMany('App\Model\Admin\City', 'country_id')->where('status', 1);
    }

    public function ActiveAttendees()
    {
        return $this->hasMany('App\Model\Admin\EventAttendees', 'attendees_category_id')->where('status', 1);
    }

    public function events()
    {
        return $this->belongsTo(Events::class);
    }

    public function User()
    {
        return $this->belongsTo('App\User');
    }

    protected $table = 'countries';

    protected $fillable = ['name', 'name_ar', 'country_code', 'isd_code', 'status', 'flag_url', 'nationality'];
}
