<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function scopeActive($query)
    {
        return $query->where('status', 1)->orderBy('id', 'DESC')->paginate(10);
    }

    public function getAll()
    {
        return \DB::table('cities')
            ->select('cities.*', 'countries.name as country')
            ->leftJoin('countries', 'countries.id', '=', 'cities.countries_id')
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function getCountry()
    {
        return \DB::table('countries')->select('id', 'name')->where('status', 1)->get();
    }

    public function Country()
    {
        return $this->belongsTo('App\Model\Admin\Country', 'id');
    }

    public function events()
    {
        return $this->belongsTo(Events::class);
    }

    protected $table = 'cities';

    protected $fillable = [
        'country_id', 'name', 'name_ar', 'status'
    ];
}
