<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	protected $table = 'categories';

	protected $fillable = [
		'name','image','parent_id','status','shop_id'
	];
	public function childs() {

		return $this->hasMany('App\Model\Admin\Category','parent_id','id','name') ;

	}

	public function parent()
	{
		return $this->belongsTo('App\Model\Admin\Category', 'parent_id','name');
	}

	public function getParentsNames() {
		if($this->parent) {
			return $this->parent->getParentsNames(). " > " . $this->name;
		} else {
			return $this->name;
		}
	}
}
