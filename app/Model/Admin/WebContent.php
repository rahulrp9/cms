<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class WebContent extends Model
{

    protected $table = 'webcontents';

    protected $fillable = [
    	'type','name','content'
    ];
}
