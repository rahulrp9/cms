<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CartAddon extends Model
{

    protected $table = 'cart_addons';

    protected $fillable = [
    	'cart_id','addon_id'
    ];
}
