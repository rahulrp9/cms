<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = 'products';

    protected $fillable = [
    	'shop_id','name','price','image','tag','type','isveg','status','catgeory','isaddon'
    ];
}
