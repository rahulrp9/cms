<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{

    protected $table = 'settings';

    protected $fillable = [
    	'key','status'
    ];
}
