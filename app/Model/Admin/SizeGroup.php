<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class SizeGroup extends Model
{

    protected $table = 'sizegroups';

    protected $fillable = [
    	'group_name','name'
    ];
}
