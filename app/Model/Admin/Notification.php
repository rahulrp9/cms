<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

    protected $table = 'notifications';

    protected $fillable = [
    	'title','content','status','type_id','type'
    ];
}
