<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class ShopType extends Model
{

    protected $table = 'shop_type';

    protected $fillable = [
    	'name'
    ];
}
