<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class orderItem extends Model
{

    protected $table = 'order_items';

    protected $fillable = [
    	'order_id','cart_id'
    ];
}
