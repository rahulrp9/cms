<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{

    protected $table = 'cart';

    protected $fillable = [
    	'user_id','shop_id','product_id','quantity','custom','status'
    ];
}
