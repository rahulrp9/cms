<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{

    protected $table = 'user_details';

    protected $fillable = [
        'user_id', 'profile_desc', 'profile_desc_ar', 'company', 'company_ar', 'designation', 'designation_ar', 'address', 'address_ar', 'street', 'country_id', 'city_id'
    ];

    public function country()
    {
        return $this->hasOne('App\Model\Admin\Country', 'id', 'country_id');
    }
}
