<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{

    protected $table = 'user_address';

    protected $fillable = [
    	'user_id','default_address','name','mobile','pincode','strretaddress','city','landmark','state'
    ];
}
