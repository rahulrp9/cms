<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class DeliveryAreaList extends Model
{

    protected $table = 'delivery_area_lists';

    protected $fillable = [
    	'delivery_area','shop_id'
    ];
}
