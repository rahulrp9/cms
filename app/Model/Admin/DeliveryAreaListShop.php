<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class DeliveryAreaListShop extends Model
{

    protected $table = 'delivery_area_list_shops';

    protected $fillable = [
    	'delivery_area','shop_id'
    ];
}
