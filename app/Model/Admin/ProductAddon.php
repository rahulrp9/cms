<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class ProductAddon extends Model
{

    protected $table = 'product_addons';

    protected $fillable = [
    	'product_id','name'
    ];
}
