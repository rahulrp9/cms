<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{

    protected $table = 'ratings';

    protected $fillable = [
    	'user_id','restaurant_id','rating'
    ];
}
