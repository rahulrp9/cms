<?php

    namespace App\Http\Middleware;

    use Closure;
    use JWTAuth;
    use Exception;
    use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

    class JwtMiddleware extends BaseMiddleware
    {

        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Closure  $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            try {

                $user = JWTAuth::parseToken()->authenticate();

            } catch (Exception $e) {

                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){

                    $response = array(
                        'hasError' => TRUE,
                        'errorCode' => 2020,
                        'message' => "Oops! the auth token is invalid",
                        'response' => Null
                    );

                }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){

                    $response = array(
                        'hasError' => TRUE,
                        'errorCode' => 2020,
                        'message' => "Your session has expired. Please login again to continue",
                        'response' => Null
                    );

                }else{

                    $response = array(
                        'hasError' => TRUE,
                        'errorCode' => 1003,
                        'message' => "Oops! authorization token not found",
                        'response' => Null
                    );      
                }

                return response()->json($response);
            }
            return $next($request);
        }
    }