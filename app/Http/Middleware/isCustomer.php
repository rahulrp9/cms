<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Session;


use Closure;

class isCustomer 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(Auth::check());
        // Auth::logout();
        // dd('out');

        if (Auth::check()) {
            if (Auth::user()->role_id != 0) {
                Auth::logout();
                return $next($request);
            } else {
                return $next($request);

            }
        }else{
            return $next($request);
        }

    }
}
