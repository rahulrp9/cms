<?php

namespace App\Http\Middleware;

use Closure;
use Helper;

class CheckApiSignature
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
        
        $pathToRequest   = $request->getRequestUri();
        $methodUsing     = $request->method();
        $signatureHeader = $request->header( 'signature' );
        $timestamp       = $request->header( 'timestamp' );
        //$host            = $request->getHttpHost();
        $host            = $request->getSchemeAndHttpHost();
        
        if ( empty($signatureHeader) )  {
            $response = array(
                'hasError'  => TRUE,
                'errorCode' => 1001,
                'message'   => "Signature should not be empty",
                'response'  => array()
            );

            return response()->json($response);
        }
        if ( empty($timestamp) )  {
            $response = array(
                'hasError'  => TRUE,
                'errorCode' => 1001,
                'message'   => "Timestamp should not be empty",
                'response'  => array()
            );

            return response()->json($response); 
        }
        /*
         * add '/' with the end of the string 
         */
        /*if (substr($pathToRequest, -1) != '/') {
            $pathToRequest = $pathToRequest.'/';
        }*/
        $pathToRequest = str_replace("/api/","",$pathToRequest);
        $signature = Helper::getSignature($host, $pathToRequest, $methodUsing, $timestamp);
        
        if ( $signature!= $signatureHeader) {
            $response = array(
                'hasError'  => TRUE,
                'errorCode' => 1001,
                'message'   => "Invalid Signature",
                'response'  => array()
            );

            return response()->json($response); 
        }
        
        return $next($request);
    }
}
