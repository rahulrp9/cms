<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.home');
    }

    public function profile($id)
    {
        $data = [];
        $admins = new User;
        $data['admin'] = $admins::find($id);
        $data['updating'] = true;

        return view('admin.user.show', $data);
    }

    public function update(Request $request, $id)
    {
        $data = [];
        $admins = new User();
        $admin = $admins::find($id);

        $this->validate(
            $request,
            [
                'name' => 'required',
                'email' => 'required',
                'password' => 'same:confirm-password',
            ]
        );

        $admin->name = $request->input('name');
        $admin->email = $request->input('email');

        if($request->password !=''){
            if($request->password  === $request->cpassword){
                $admin->password = Hash::make($request->password);
            }else{
                return back()->with('error','Password OR Confirm is Not Match');
            }
        }

        if($request->file('image')){
          
          $image = $request->file('image');
          $new_name = rand() . '.' . $image->getClientOriginalExtension();
          $image->move(public_path('backend/images'), $new_name);
          
          $admin->image = $new_name;
        }
        //exit();
        if($admin->save())
        {            
            return redirect()->route('admin.index')->with('success','Profile updated successfully');
        }
        else
        {
            return back()->with('error','Profile editing failed');
        }
    }
    
}
