<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User as AuthUser;
use Hash;
use Exception;
use JWTAuth;
use Helper;
use DB;
use Carbon\Carbon;
use App\Model\Admin\Country;
use App\Model\Admin\Otp;
use App\Model\Admin\Settings;
use Mail;
use App\Model\Admin\WebContent;
use App\Model\Admin\Cart;
use App\Model\Admin\DeliveryAreaList;
use App\Model\Admin\Notification;
use App\Model\Admin\UserAddress;


class CommonController extends Controller
{
    
    public function test(){

        //Helper::getSignature('http://127.0.0.1:8001','testjwt/sss','POST','3433');
        exit('ddd');
    }

    public function getSign(Request $request){
        echo $request->getSchemeAndHttpHost().'::API::'.$request->path.'::'.$request->method().'::'.$request->time;
        echo '-------';
        $sign = Helper::getSignature($request->getSchemeAndHttpHost(),$request->path,$request->method(),$request->time);
        return $sign;
    }

    public function getCountries(){

        try{

            $countries = Country::select('id','name','country_code as countryCode','isd_code as code','flag_url as flagUrl','updated_at','nationality')
                        ->where('status',1)->get();
            $response = array(
                    'hasError' => FALSE,
                    'errorCode' => -1,
                    'message' => 'Success',
                    'response' => array(
                        'countries' => $countries
                    ),
                );
            return response()->json($response);

        }catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
        }
    }
    
    public function userRegistration(Request $request) {
        
        try {

            $messages = [

                'firstName.required'   => 'Please provide your first name',
                'lastName.required'    => 'Please provide your last name',
                //'middleName.required'  => 'Please provide your Middle Name',
                'mobile.required'       => 'Please provide your mobile number',
                //'countryId.required'   => 'Please provide your Country Code',
                'email.required'        => 'Please provide your password',
                /*'mobile.required' => 'Please choose your country', */
                //'password.required'     => 'Please provide your qar Id',    
            ];

            $validator = Validator::make($request->all(), [

                'firstName'     => 'required',
                'lastName'      => 'required',
                'mobile'        => 'required',
                //'middleName'    => 'required',
                //'countryId'     => 'required',
                'email'         => 'required',
                #'companyName'  => 'required',
                //'password'      => 'required'

            ], $messages);  
            
            if ($validator->fails()) {
                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'message' => 'Oops! something wrong with data',
                    'response' => NULL
                );
                return response()->json($response);
            }
            $phone_exists = AuthUser::where('phone', $request->mobile)->first();
            //$email_exists = AuthUser::where('email', $request->email)->first();
            //$countyDetails = Country::where('id',$request->countryId)->first();
      
            if ($phone_exists) {

                    $response = array(
                        'hasError' => TRUE,
                        'errorCode' => 1001,
                        'message' => 'Oops! this Phone No already exist',
                        'response' => NULL
                    );
                    return response()->json($response);
            
               
            }
            else{

                $userSave = new AuthUser();
                $userSave->name         = $request->firstName;
                $userSave->last_name    = $request->lastName;
                $userSave->phone        = $request->mobile;
                $userSave->password     = Hash::make($request->password);
                $userSave->status       = 1;
                $userSave->email        = $request->email;
                $userSave->role_id      = 0;
                $data = $userSave->save();
            }
            if ($data == TRUE) { 

                $otpCheck = Settings::where('key','otp')->first();
                    $otp = Helper::sendOtp($request->mobile);
                    $otp_model = new Otp;
                    $otp_model->user_id      = $userSave->id;
                    $otp_model->otp          = $otp;
                    //$otp_model->email        = $request->email;
                    $otp_model->phone        = $request->mobile;
                    $otp_data = $otp_model->save();
                    $isVerified = 0;
  
                if ($otp_data == TRUE) {

                    /*$response = array(
                        'hasError' => FALSE,
                        'errorCode' => -1,
                        'message' => 'Verification Code sent successfully.',
                        'response' => array(

                            'userId'      => $userSave->id,
                            'phoneNumber' => $request->phoneNumber,
                            'email'       => $request->email,
                            'isVerified'  => $isVerified,
                            'accessToken' => Null
                        )
                    );*/
                    $user = AuthUser::where('id', $userSave->id)->first();
                    $token = Helper::generateJwtToken($user->id);
                    $response = array(
                        'hasError' => FALSE,
                        'errorCode' => -1,
                        'message'  => "Otp Sent Successfully.",
                        'response' => array(
                            'userId'      => $user->id,
                            
                        )
                    );
                    return response()->json($response);
                     
                } else {

                    $response = array(
                        'hasError' => TRUE,
                        'errorCode' => 1001,
                        'message' => 'Oops! we cannot send OTP. Please try again later',
                        'response' => Null
                    );
                    return response()->json($response);
                } 

            }else {

                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'message' => 'Oops! we cannot process your request. Please try again later',
                    'response' => Null
                );
                return response()->json($response);               

            }   
            

        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
        }
    }


    public function userVerify(Request $request){

        try {

            $messages = [

                'userId.required' => 'Please provide your User Id',
                'otp.required'   => 'Please provide your Code',  
            ];

            $validator = Validator::make($request->all(), [

                'userId'    => 'required',
                'otp'      => 'required',
            ], $messages);  
            
            if ($validator->fails()) {
                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'message' => 'Oops! something wrong with data',
                    'response' => NULL
                );
                return response()->json($response);
            }
            $cartCount = 0;
            $otpCheck = Otp::where('user_id',$request->userId)->where('otp',$request->otp)->orderBy('id', 'DESC')->first();

            if($otpCheck){

                //AuthUser::where('phone', $request->userName)->update(['is_otp_verified' => 1]);
                Otp::where('user_id',$request->userId)->delete();
                $userDetails = AuthUser::where('id',$request->userId)->first();
                $token = Helper::generateJwtToken($userDetails->id);
                $cartCount = Cart::where('user_id',$request->userId)->where('status',1)->count();
                if($userDetails){
                    $response = array(
                        'hasError' => FALSE,
                        'errorCode' => -1,
                        'message'  => "Registered Successfully.",
                        'response' => array(
                            'userId'      => $userDetails->id,
                            'firstName'   => $userDetails->name,
                            'lastName'    => $userDetails->last_name,
                            'phoneNumber' => $userDetails->phone,
                            'email'       => $userDetails->email,
                            'accessToken' => $token,
                            'cartCount'   => $cartCount,
                        )
                    );
                    return response()->json($response);
                }
                else{

                    $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'response' => NULL,
                    'message' => 'Oops! No User Found.'
                );
                
                return response()->json($response);
                }    

            } else {

                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'response' => NULL,
                    'message' => 'Oops! wrong OTP provided'
                );
                
                return response()->json($response);
            }

                


        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
        }    

    }

    public function userResendCode(Request $request){

        try{

            $validator = Validator::make($request->all(), [

                'userId' => 'required'

            ]);  
            
            if ($validator->fails()) {

                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'message' => 'Oops! something wrong with data',
                    'response' => NULL
                );

                return response()->json($response);
            }
            $userDetails = AuthUser::where('id',$request->userId)->first();
            $otp = Helper::sendOtp($userDetails->email);
            Otp::where('user_id',$request->userId)->delete();
            $otp_model = new Otp;
            $otp_model->user_id      = $userDetails->id;
            $otp_model->otp          = $otp;
            $otp_model->country_code = $userDetails->country_code;
            $otp_model->phone        = 0;
            $otp_model->email        = $userDetails->email;
            $otp_data = $otp_model->save();
            if ($otp_data == TRUE) {

                $response = array(
                    'hasError' => FALSE,
                    'errorCode' => -1,
                    'message' => 'Verification Code sent successfully.',
                    'response' => array(

                        'userId'  => $userDetails->id,
                        'email'   => $userDetails->email,
                    )
                );
                return response()->json($response);
                     
            } else {

                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'message' => 'Oops! we cannot send OTP. Please try again later',
                    'response' => Null
                );
                return response()->json($response);
            }   

        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
        }

    }

    public function userLogin(Request $request){

        try{

            /*$messages = [
                
                'email.required' => 'Please provide your phoneNumber',
                'password.required' => 'Please provide your password'  

            ];

            $validator = Validator::make(array('email' => $request->email, 'password' => $request->password), [

                'email' => 'required',
                'password'    => 'required'

            ], $messages);  
            
            if ($validator->fails()) {
                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'message' => 'Oops! something wrong with data',
                    'response' => NULL
                );

                return response()->json($response);                
            }*/
            //$user = AuthUser::where('phone', $request->phoneNumber)->first();
            $user = AuthUser::where('phone', $request->userName)->first();
            $otpCheck = Settings::where('key','otp')->first();
            if($user){
                
                /*if($otpCheck->key_value == 1){*/

                    $otp = Helper::sendOtp($user->phone);
                    $otp_model = new Otp;
                    $otp_model->user_id      = $user->id;
                    $otp_model->otp          = $otp;
                    //$otp_model->country_code = $countyCode;
                    $otp_model->phone        = $user->phone;
                    $otp_model->email        = '';
                    $otp_data = $otp_model->save();

                /*} */
                $response = array(
                        'hasError' => FALSE,
                        'errorCode' => -1,
                        'message'  => "Otp Sent Successfully",
                        'response' => array(
                            'userId'      => $user->id
                        )
                    );
                    return response()->json($response);


            }
            else{

                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'message' => 'Invalid credentials Provided',
                    'response' => NULL
                );
                return response()->json($response);
            }

        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
        }    


    }

    public function appData(Request $request) {

        try {

            $cartCount = 0;
            $arealists = DeliveryAreaList::select('id','delivery_area as name')->get();
            if(count($arealists)){
                $arealists = $arealists;
            } else{
                $arealists = null;
            }
            if($request->userId != '-1'){

                $user = AuthUser::where('id',$request->userId)->where('status',1)->first();
                $cartCount = Cart::where('user_id',$request->userId)->where('status',1)->count();
                if($request->lastUpdatedTime == 0){

                    $countries = Country::select('id','name','country_code as countryCode','isd_code as code','flag_url as flagUrl','updated_at','nationality')
                    ->where('status',1)->get();
                    $latest = $countries->last();   
                    $timeStamp = $latest->updated_at->toDateTimeString(); 
                    

                }
                else{

                    $countries = Country::select('id','name','country_code as countryCode','isd_code as code','flag_url as flagUrl','updated_at','nationality')
                    ->where('status',1)->where('updated_at','>',$request->lastUpdatedTime)->get();
                    $latest = $countries->last();                
                    if ($latest) {
                        $timeStamp = $latest->updated_at->toDateTimeString();
                    } else {
                        $timeStamp = $request->lastUpdatedTime;
                    }   
                }
                if (!$user) {

                    $response = array(
                        'hasError' => TRUE,
                        'errorCode' => 1001,
                        'message' => 'Invalid User',
                        'response' => NULL
                    );
                    return response()->json($response);

                }

                $token = Helper::generateJwtToken($request->userId);
            } else {

                $token = '';
                if($request->lastUpdatedTime == 0){

                    $countries = Country::select('id','name','country_code as countryCode','isd_code as code','flag_url as flagUrl','updated_at','nationality')
                    ->where('status',1)->get();
                    $latest = $countries->last();   
                    $timeStamp = $latest->updated_at->toDateTimeString(); 
                    

                }
                else{

                    $countries = Country::select('id','name','country_code as countryCode','isd_code as code','flag_url as flagUrl','updated_at','nationality')
                    ->where('status',1)->where('updated_at','>',$request->lastUpdatedTime)->get();
                    $latest = $countries->last();                
                    if ($latest) {
                        $timeStamp = $latest->updated_at->toDateTimeString();
                    } else {
                        $timeStamp = $request->lastUpdatedTime;
                    }   
                }
            }

            $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "Success",
                'response' => array(
                    'type'        => 1,
                    'loginType'   => 1,
                    'countries'   => $countries,
                    'cartCount'   => $cartCount,
                    'deliveryAreaLists'   => $arealists,
                )
            );
            return response()->json($response);

        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
        }       

    }

    public function forgotPassword(Request $request){

        try {

            $user = AuthUser::where('phone',$request->userName)->first();
                if (!$user) {

                    $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'message' => 'No user found!',
                    'response' => NULL
                    );
                    return response()->json($response);

                }
            $subject       = 'Reset Password';
            $emailtemplate = 'emailtemplates.forgot_password_mail'; 
            $email         = $request->email;
            $password      = Helper::generateRandomString();

            $user->password = Hash::make($password);
            $user->save();
            $data = array('password' => $password,'name' => $user->name);

            $token = Helper::generateJwtToken($user->id);    
            $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "Success",
                'response' => array('text'=>'Reset link sent successfully'),
            );
            return response()->json($response);       
   

            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }

    public function changePassword(Request $request){

        try {

            $user = AuthUser::where('id',$request->userId)->first();
                if (!$user) {

                    $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'message' => 'No user found!!',
                    'response' => NULL
                    );
                    return response()->json($response);

                }
             if (!Hash::check($request->oldPassword, $user->password)) {
                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'message' => 'Old Password is Wrong',
                    'response' => NULL
                    );
                return response()->json($response);
            }
            $user->password = Hash::make($request->password);
            $user->save();
            $token = Helper::generateJwtToken($user->id);
            $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "Success",
                'response' => array(
                            'access-token'=> $token,
                            ),
            );
            return response()->json($response);       
   

            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }

    public function editProfile(Request $request){

        try {

            $user = AuthUser::where('id',$request->userId)->first();
                if (!$user) {

                    $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'message' => 'No user found!!',
                    'response' => NULL
                    );
                    return response()->json($response);

                }
            $mysqlDate = date('Y-m-d', strtotime($request->dob));    
            $user->name        = $request->firstName;
            $user->last_name   = $request->lastName;
            $user->middle_name = $request->middleName;
            $user->country_id  = $request->countryId;
            $user->company     = $request->companyName;
            $user->phone       = $request->phoneNumber;
            $user->gender      = $request->gender;
            $user->dob         = $mysqlDate;
            $user->image       = $request->profileImageUrl;
            $user->country_code = $request->phoneCountryID;
            $user->save();
            $token = Helper::generateJwtToken($request->userId);
            $userSaved = AuthUser::where('id',$request->userId)->first();
            $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "Success",
                'response' => array(
                                'access-token' => $token,
                                'userId'       => $request->userId,
                                'firstName'    => $userSaved->name, 
                                'middleName'   => $userSaved->middle_name,
                                'lastName'     => $userSaved->last_name,
                                'phoneNumber'  => $userSaved->phone,
                                'email'        => $userSaved->email,
                                'companyName'  => $userSaved->company,
                                'countryId'    => $userSaved->country_id,
                                'userType'     => 0,
                                'userId'       => $request->userId,
                                'isVerified'   => $userSaved->is_otp_verified,
                                'profileImageUrl' => $userSaved->image,
                                'gender'       => $userSaved->gender,
                                'dob'          => $userSaved->dob,
                                'phoneCountryID'  => $userSaved->country_code,
                                ),
            );
            return response()->json($response);       
   

            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }


    public function profileImageUpload(Request $request){

        try {

            $user = AuthUser::where('id',$request->userId)->first();
                if (!$user) {

                    $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1001,
                    'message' => 'No user found!!',
                    'response' => NULL
                    );
                    return response()->json($response);

                }
    
            if ($request->has('profileimage')) {

                $image = $request->file('profileimage');
                //$new_name = rand() . '.' . $image->getClientOriginalExtension();
                $new_name = time() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images/profile'), $new_name);                      
                $new_name = $new_name;
            }
            $token = Helper::generateJwtToken($user->id);
            $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "Success",
                'response' => array(
                            'access-token' => $token,
                            'profileimage' => 'images/profile/'.$new_name,
                            ),
            );
            return response()->json($response);       
   

            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }

    public function webContent(Request $request){

        try {

            $content = WebContent::where('type',$request->type)->first();
            /*$lang    = $request->header( 'language' );
            if($lang == 2){
                $webContent = $content->content_ar;
            } else{
                $webContent = $content->content;
            }*/
            $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "Success",
                'response' => array(
                            'data'  => $content->content,
                            ),
            );
            return response()->json($response);    
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }

    public function getProducts(Request $request){

        
        
    }

    public function webContents(Request $request){

        try {


            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }    
    public function notifications(Request $request){

        try {

            $notifications = Notification::select('id as notificationId','title','content','created_at as dateTime','type_id as typeId','type')
                            ->where('user_id',$request->userId)->get();
             $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "Success",
                'response' => $notifications,
            );
            return response()->json($response);                 
            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }

    public function addressListing(Request $request){

        try {

            $address = UserAddress::select('id','name as fullName','strretaddress as deliveryAddress','city','state','landmark','mobile as phoneNumber')
                    ->where('user_id',$request->userId)->get();
             $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "Success",
                'response' => $address,
            );
            return response()->json($response);                 
            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }
    




} // end of class
