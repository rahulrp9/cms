<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User as AuthUser;
use Hash;
use Exception;
use JWTAuth;
use Helper;
use DB;
use Carbon\Carbon;
use App\Model\Admin\Country;
use App\Model\Admin\Otp;
use App\Model\Admin\Settings;
use App\Model\Admin\Rating;
use App\Model\Admin\Shop;
use App\Model\Admin\ShopType;
use App\Model\Admin\Category;
use App\Model\Admin\Offer;
use Mail;
use App\Model\Admin\ProductAddon;
use App\Model\Admin\Product;
use App\Model\Admin\Tag;
use App\Model\Admin\ProductCustomise;
use App\Model\Admin\Cart;
use App\Model\Admin\cartCustom;
use App\Model\Admin\CartAddon;
use App\Model\Admin\orderItem;
use App\Model\Admin\UserAddress;
use App\Model\Admin\Order;
use App\Model\Admin\SizeGroup;
use App\Model\Admin\DeliveryAreaListShop;
use App\Model\Admin\ShopCategory;


class ShopController extends Controller
{

    public function shopList(Request $request){
        try {
            $mainArray = array();
             
            if(!$request->deliveryId || $request->deliveryId == "-1"){


                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1002,
                    'response' => NULL,
                    'message' => 'Missing Delivery area'
                );

                return response()->json($response);
            }
            if($request->deliveryId != '-1'){ 
                $deliveyId = $request->deliveryId;
            } else{
                $deliveyId = 0;
            }
            if($request->categoryId > 0){ 
                $categoryId = $request->categoryId;
            } else{
                $categoryId = 0;
            }    
            $resturants = Shop::where('shops.status',1)
            ->when($deliveyId, function ($query) use ($deliveyId) {
                return $query->leftjoin('delivery_area_list_shops','delivery_area_list_shops.shop_id','=','shops.user_id')
                        ->select('shops.*')
                        ->where('delivery_area_list_shops.delivery_area',$deliveyId);
            })
            ->when($categoryId, function ($query) use ($categoryId) {
                return $query->leftjoin('shop_categories','shop_categories.shop_id','=','shops.user_id')
                        ->where('shop_categories.category_id',$categoryId);
            })
            ->get();
            if($resturants){
                $i = 0;
                foreach ($resturants as $resturant) {
                    
                    $rating  = Rating::where('restaurant_id',$resturant->id)->avg('rating');
                    $ShopType = ShopType::where('id',$resturant->shop_type)->first();
                    $mainArray[$i]['id']        = $resturant->id;
                    $mainArray[$i]['name']      = $resturant->name;
                    $mainArray[$i]['image']     = $resturant->image;
                    $mainArray[$i]['isOpened']  = $resturant->is_opened;
                    $mainArray[$i]['shopType']  = $ShopType->name;
                    $mainArray[$i]['rating']    = round($rating);
                    $mainArray[$i]['minOrder']  = $resturant->min_order;
                    $mainArray[$i]['minTime']  = $resturant->min_time;
                    $location['address']       = $resturant->address;
                    $location['streetName']    = $resturant->street;
                    $location['latitude']      = $resturant->latitude;
                    $location['longitude']     = $resturant->longitude;
                    $mainArray[$i]['location'] = $location;
                    $i++;
                }

            }
            $token = Helper::generateJwtToken($request->userId);
            $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "",
                'response' => $mainArray
            );
            return response()->json($response);
            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }
        
    }

    public function categoryList(Request $request){

        try {
            if(!$request->deliveryId || $request->deliveryId == "-1"){


                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1002,
                    'response' => NULL,
                    'message' => 'Missing Delivery area'
                );

                return response()->json($response);
            }
            $mainArray = array();
            //$resturants = Shop::where('status',1)->where('common_type',3)->get();
            if($request->deliveryId != '-1'){ 
                $deliveyId = $request->deliveryId;
            } else{
                $deliveyId = 0;
            }
            if($request->categoryId > 0){ 
                $categoryId = $request->categoryId;
            } else{
                $categoryId = 0;
            } 
            if($request->deliveryId != '-1'){ 
                /*$resturants = Shop::leftjoin('delivery_area_lists','delivery_area_lists.shop_id','=','shops.id')
                             ->where('status',1)->where('delivery_area_list_shops.delivery_area',$request->deliveryId)
                             //->leftjoin('categories','categories.shop_id','=','shops.id')
                             ->get();*/
                $resturants = Shop::where('shops.status',1)
                ->when($deliveyId, function ($query) use ($deliveyId) {
                    return $query->leftjoin('delivery_area_list_shops','delivery_area_list_shops.shop_id','=','shops.user_id')
                            ->select('shops.*')
                            ->where('delivery_area_list_shops.delivery_area',$deliveyId);
                })
            ->when($categoryId, function ($query) use ($categoryId) {
                return $query->leftjoin('shop_categories','shop_categories.shop_id','=','shops.user_id')
                        ->where('shop_categories.category_id',$categoryId);
            })
            ->get();             
            } else{
                $resturants = Shop::where('status',1)->get();
            }
            if($resturants){
                $i = 0;
                foreach ($resturants as $resturant) {
                    
                    $rating  = Rating::where('restaurant_id',$resturant->id)->avg('rating');
                    $ShopType = ShopType::where('id',$resturant->shop_type)->first();
                    $mainArray[$i]['id']        = $resturant->id;
                    $mainArray[$i]['name']      = $resturant->name;
                    $mainArray[$i]['image']     = $resturant->image;
                    $mainArray[$i]['isOpened']  = $resturant->is_opened;
                    $mainArray[$i]['shopType']  = $ShopType->name;
                    $mainArray[$i]['rating']    = round($rating);
                    $mainArray[$i]['minOrder']  = $resturant->min_order;
                    $mainArray[$i]['minTime']  = $resturant->min_time;
                    $location['address']       = $resturant->address;
                    $location['streetName']    = $resturant->street;
                    $location['latitude']      = $resturant->latitude;
                    $location['longitude']     = $resturant->longitude;
                    $mainArray[$i]['location'] = $location;
                    $i++;
                }

            }
            $categories = Category::select('id','name','image')->where('status',1)->get();
            $offers = Offer::select('id','name','image')->get();
            $token = Helper::generateJwtToken($request->userId);
            $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "",
                'response' => array(
                    'popularShops'   => $mainArray,
                    'category'       => $categories,
                    'offer'          => $offers,
                    'accessToken'    => $token,
                )
            );
            return response()->json($response);

            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }

    public function getProducts(Request $request){

        try {
            $mainArray = [];
            $shopid = Shop::where('id',$request->shopId)->first();
            if(!$shopid){

                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1002,
                    'response' => NULL,
                    'message' => 'Shop Not Found!'
                );

                return response()->json($response);

            }
            $products = Product::select('id','price','image','type','isveg as isVeg','tag','name')->where('status',1)->where('shop_id',$shopid->user_id)->get();
            if($products){
                $i = 0;
                foreach ($products as $product) {
                   $mainArray[$i]['id'] = $product->id;
                   $mainArray[$i]['name'] = $product->name; 
                   $mainArray[$i]['price'] = $product->price;
                   $mainArray[$i]['image'] = $product->image;
                   $mainArray[$i]['type'] = $product->type;
                   $mainArray[$i]['isVeg'] = $product->isVeg;
                   $tag = Tag::where('id',$product->tag)->first();
                   if($tag){
                    $tags['id'] = $tag->id;
                    $tags['name'] = $tag->name;
                    $tags['type'] = $tag->type;
                   }
                   else{
                    $tags = null;
                   }
                   $mainArray[$i]['tag'] = $tags;
                   $customise = array();
                   if($product->type == 2){
                        $customs = ProductCustomise::leftjoin('sizegroups','sizegroups.id','=','products_customise.name')->where('product_id',$product->id)->get();
                        if($customs){
                            $j = 0;
                            foreach ($customs as $custom) {
                               
                                $customise[$j]['id'] = $custom->id;
                                $customise[$j]['name'] = $custom->name;
                                $customise[$j]['addOns'] = ProductAddon::select('id','name')->where('product_id',$product->id)->get();
                                $j++;
                            }

                        } else{
                            $customise = null;
                        }
                   }
                   else{
                        $customise = null;
                   }
                   $mainArray[$i]['customise'] = $customise;
                   $i++;
                }
            }
            $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "",
                'response' => $mainArray
            );
            return response()->json($response);
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }

    public function addCart(Request $request){
        try {
            if(count($request->custom)){

                $checkCustom = $request->custom[0]['id'];
            }
            else{
                $checkCustom = 0;
            }  
            $checkCart = Cart::where(['user_id'=>$request->userId,'shop_id'=>$request->shopId,'product_id'=>$request->productId,'custom_id'=>$checkCustom,'status'=>1])->first();
            if($checkCart){

                $checkCart->quantity = ($checkCart->quantity + 1);
                $checkCart->save();
                //Cart::where('id',$checkCart->id)->update(['quantity' => ($checkCart->quantity + 1)]);
                $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "",
                'response' => array(
                    'productId'   => $request->productId,
                    'quantity'    => $request->quantity,
                    'cartId'      => $checkCart->id,
                )
            );
            return response()->json($response);
            }
            $cart = new Cart;
            $cart->user_id = $request->userId;
            $cart->shop_id = $request->shopId;
            $cart->product_id = $request->productId;
            $cart->quantity = $request->quantity;
            if(count($request->custom)){
                $cart->custom = 1;
                $cart->custom_id = $request->custom[0]['id'];
            } else {
                $cart->custom = 0;
                $cart->custom_id = 0;
            }
            $cart->save();

            if(count($request->custom)){
                foreach ($request->custom as $customs) { 
                    $ccustom = new cartCustom;
                    $ccustom->cart_id  = $cart->id;
                    $ccustom->customise_id = $customs['id'];
                    $ccustom->quantity = $customs['quantity']; 
                    $ccustom->save();

                    if(count($customs['addOns'])){
                        foreach ($customs['addOns'] as $key => $value) {
                            foreach ($value as $val) { 
                                $caddong = new CartAddon;
                                $caddong->cart_id  = $cart->id;
                                $caddong->addon_id = $val;
                                $caddong->save();
                            }
                        }

                    }
                }
                

                
            }
            $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "",
                'response' => array(
                    'productId'   => $request->productId,
                    'quantity'    => $request->quantity,
                    'cartId'      => $cart->id,
                )
            );
            return response()->json($response);    
            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }

    public function updateCart(Request $request){

        try {
            $total = 0;
            if($request->customId == '-1'){

                    $checkCustom = 0;
            } else{
                    $checkCustom = $request->customId;
            }
            if($request->quantity == '0'){

                Cart::where('user_id',$request->userId)->where('product_id',$request->productId)->where('custom_id',$checkCustom)->where('status',1)->delete();
                $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "",
                'response' => array(
                    'productId'   => $request->productId,
                    'quantity'    => $request->quantity,
                    'customId'    => $request->customId,
                    'total'       => number_format((float)$total, 2, '.', ''),
                    'tax'          => 0,
                    'deliveryCharges' => 0,
                    'discount'        => 0,
                    'totalPayable'    => number_format((float)$total, 2, '.', ''),
                    )
                );
                return response()->json($response);

            }
            else{

                $cart = Cart::where('user_id',$request->userId)->where('product_id',$request->productId)->where('custom_id',$checkCustom)
                        ->where('status',1)->first();
                if($cart){
                    $cart->quantity = $request->quantity;
                    $cart->save();

                } else{

                    $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1002,
                    'response' => NULL,
                    'message' => 'No Cart Found!'
                    );

                    return response()->json($response);

                }
            }
            $product = Product::where('id',$request->productId)->first();
            $total = ($product->price * $request->quantity);
            $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "",
                'response' => array(
                    'productId'   => $request->productId,
                    'quantity'    => $request->quantity,
                    'customId'    => $request->customId,
                    'total'       => number_format((float)$total, 2, '.', ''),
                    'tax'          => 0,
                    'deliveryCharges' => 0,
                    'discount'        => 0,
                    'totalPayable'    => number_format((float)$total, 2, '.', ''),
                )
            );
            return response()->json($response);        
            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }
    }

    public function cartListing(Request $request){

        try {
            $total = 0.00;
            $mainArray = array();
            $carts = Cart::where('user_id',$request->userId)->where('status',1)->get();
            if(count($carts)){

                /*$checkOrder = Order::where('user_id',$request->userId)->whereIn('status',[1])->first();
                if($checkOrder){

                    $checkOrder->delete();
                }*/
                $shopcart = Cart::where('user_id',$request->userId)->where('status',1)->first();
                /*$order = new Order;
                $order->user_id = $request->userId;
                $order->shop_id = $shopcart->shop_id;
                //$order->amount  = 100.00;
                $order->payment_status = 0;
                $order->address_id = 0;
                $order->save();*/
                $i = 0;
                foreach ($carts as $cart) {
                    
                    /*$cartitems = new orderItem;
                    $cartitems->order_id = $order->id;
                    $cartitems->cart_id  = $cart->id;
                    $cartitems->save();*/
                    $product = Product::where('id',$cart->product_id)->first();
                    $cartprice = ($product->price * $cart->quantity);
                    $mainArray[$i]['id']    = $product->id;
                    $mainArray[$i]['name']  = $product->name;
                    $mainArray[$i]['productPrice'] = $product->price;
                    $mainArray[$i]['price'] = $cartprice;
                    $total += ($product->price * $cart->quantity);
                    if($cart->custom == 1){
                        $ccustom = cartCustom::where('cart_id',$cart->id)->first();
                        //$mainArray[$i]['customId'] = $ccustom->customise_id;
                        $mainArray[$i]['customId'] = $cart->custom_id;
                        $mainArray[$i]['quantity'] = $cart->quantity;
                        //$mainArray[$i]['quantity'] = 1;
                    } else {
                        $mainArray[$i]['customId'] = -1;
                        $mainArray[$i]['quantity'] = $cart->quantity;
                    }
                    $addons = CartAddon::where('cart_id',$cart->id)->get();
                    $addonArray = array();
                    if($addons){
                        $j = 0;
                        foreach ($addons as $addon) {
                            $addonName = ProductAddon::where('id',$addon->addon_id)->first();
                            $addonArray[$j]['id'] = $addonName->id;
                            $addonArray[$j]['name'] = $addonName->name;
                        }
                        $mainArray[$i]['addOns'] = $addonArray;
                    }
                    else{
                        $mainArray[$i]['addOns'] = null;
                    }
                    $i++;
                }
                //Order::where('id',$order->id)->update(['amount' => $total]);
                $useraddress = UserAddress::where('user_id',$request->userId)->orderBy('id','desc')->first();
                if($useraddress){

                     $userAddressArray['id']      = $useraddress->id;
                     $userAddressArray['phoneNumber']   = $useraddress->mobile;
                     $userAddressArray['zip']     = $useraddress->pincode;
                     $userAddressArray['deliveryAddress'] = $useraddress->strretaddress;
                     $userAddressArray['city']    = $useraddress->city;
                     $userAddressArray['state']   = $useraddress->state;
                     $userAddressArray['type']    = $useraddress->type;   
                     $userAddressArray['fullName']    = $useraddress->name;  
                     $userAddressArray['landmark']    = $useraddress->landmark;  
                }
                else{
                    $userAddressArray = null; 
                }
                $deliveryType['id'] =  1;
                $deliveryType['name'] = 'Home Delivery';
                $deliveryType['type'] = 'Home Delivery';
                $shop = Shop::select('shops.name','shops.image','shops.is_opened','shop_type.name as shoptypename','shops.address','shops.street','shops.latitude','shops.longitude')
                        ->where('shops.id',$shopcart->shop_id)
                        ->leftjoin('shop_type','shop_type.id','=','shops.shop_type')->first();
                $location['address']      = $shop->address;  
                $location['streetName']   = $shop->street;  
                $location['latitude']     = $shop->latitude;  
                $location['longitude']    = $shop->longitude;     
                $response = array(
                'hasError' => FALSE,
                'errorCode' => -1,
                'message'  => "",
                'response' => array(
                    'orderId'         => 0,
                    'products'        => $mainArray,
                    'name'            => $shop->name,
                    'image'           => $shop->image,
                    'isOpened'        => $shop->is_opened,
                    'shopType'        => $shop->shoptypename,
                    'location'        => $location,
                    'note'            => '',
                    'total'           => number_format((float)$total, 2, '.', ''),
                    'tax'             => 0,
                    'deliveryCharges' => 0,
                    'discount'        => 0,
                    'totalPayable'    => number_format((float)$total, 2, '.', ''),
                    'address'         => $userAddressArray,
                    'deliveryType'    => $deliveryType,
                )
            );
            return response()->json($response); 

            } else {

                $response = array(
                    'hasError' => FALSE,
                    'errorCode' => 1002,
                    'response' => NULL,
                    'message' => 'Cart is Empty!'
                    );

                return response()->json($response);
            }

            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }
    }

    public function checkout(Request $request){

        try {
            $total = 0.00;
            $mainArray = array();
            $carts = Cart::where('user_id',$request->userId)->where('status',1)->get();
            if($carts){
                $shopcart = Cart::where('user_id',$request->userId)->where('status',1)->first();
                $order = new Order;
                $order->user_id = $request->userId;
                $order->shop_id = $shopcart->shop_id;
                //$order->amount  = 100.00;
                $order->delivery_type = $request->deliveryType;
                $order->payment_status = 0;
                $order->address_id = $request->addressId;
                $order->save();
                $i = 0;
                foreach ($carts as $cart) {

                    $cartitems = new orderItem;
                    $cartitems->order_id = $order->id;
                    $cartitems->cart_id  = $cart->id;
                    $cartitems->save();
                    $product = Product::where('id',$cart->product_id)->first();
                    $cartprice = ($product->price * $cart->quantity);
                    //$mainArray[$i]['id']    = $product->id;
                    //$mainArray[$i]['name']  = $product->name;
                    //$mainArray[$i]['productPrice'] = $product->price;
                    //$mainArray[$i]['price'] = $cartprice;
                    $total += ($product->price * $cart->quantity);
                    /*if($cart->custom == 1){
                        $ccustom = cartCustom::where('cart_id',$cart->id)->first();
                        //$mainArray[$i]['customId'] = $ccustom->customise_id;
                        $mainArray[$i]['customId'] = $cart->custom_id;
                        $mainArray[$i]['quantity'] = $cart->quantity;
                        //$mainArray[$i]['quantity'] = 1;
                    } else {
                        $mainArray[$i]['customId'] = -1;
                        $mainArray[$i]['quantity'] = $cart->quantity;
                    }*/
                    /*$addons = CartAddon::where('cart_id',$cart->id)->get();
                    $addonArray = array();
                    if($addons){
                        $j = 0;
                        foreach ($addons as $addon) {
                            $addonName = ProductAddon::where('id',$addon->addon_id)->first();
                            $addonArray[$j]['id'] = $addonName->id;
                            $addonArray[$j]['name'] = $addonName->name;
                        }
                        $mainArray[$i]['addOns'] = $addonArray;
                    }
                    else{
                        $mainArray[$i]['addOns'] = null;
                    }*/
                    $i++;
                }
                Order::where('id',$order->id)->update(['amount' => $total]);
                Cart::where('user_id',$request->userId)->update(['status' => 0]);
                /*$useraddress = UserAddress::where('user_id',$request->userId)->orderBy('id','desc')->first();
                if($useraddress){

                   $userAddressArray['id']      = $useraddress->id;
                   $userAddressArray['phoneNumber']   = $useraddress->mobile;
                   $userAddressArray['zip']     = $useraddress->pincode;
                   $userAddressArray['deliveryAddress'] = $useraddress->strretaddress;
                   $userAddressArray['city']    = $useraddress->city;
                   $userAddressArray['state']   = $useraddress->state;
                   $userAddressArray['type']    = $useraddress->type;   
                   $userAddressArray['fullName']    = $useraddress->name;  
                   $userAddressArray['landmark']    = $useraddress->landmark;  
               }
               else{
                $userAddressArray = null; 
            }*/
            /*$deliveryType['id'] =  1;
            $deliveryType['name'] = 'Home Delivery';
            $deliveryType['type'] = 'Home Delivery';
            $shop = Shop::select('shops.name','shops.image','shops.is_opened','shop_type.name as shoptypename','shops.address','shops.street','shops.latitude','shops.longitude')
            ->where('shops.id',$shopcart->shop_id)
            ->leftjoin('shop_type','shop_type.id','=','shops.shop_type')->first();
            $location['address']      = $shop->address;  
            $location['streetName']   = $shop->street;  
            $location['latitude']     = $shop->latitude;  
            $location['longitude']    = $shop->longitude;*/     
            $response = array(
                    'hasError' => FALSE,
                    'errorCode' => -1,
                    'message'  => "",
                    'response' => array(
                        'orderId'       => $order->id,
                        'statusMessage' => 'Ordered Successfully',
                    )
                );
            return response()->json($response); 

        } else {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => 'No Cart Found!'
            );

            return response()->json($response);
        }
            /*$checkOrder = Order::where('id',$request->orderId)->first();
            if($checkOrder){

                $checkOrder->address_id     = $request->addressId;
                $checkOrder->delivery_type  = $request->delivery_type;
                $checkOrder->note           = $request->note;
                $checkOrder->status         = 2;
                $checkOrder->save();
                Cart::where('user_id',$request->userId)->update(['status' => 0]);
                $response = array(
                    'hasError' => FALSE,
                    'errorCode' => -1,
                    'message'  => "",
                    'response' => array(
                        'orderId'       => $request->orderId,
                        'statusMessage' => 'Ordered Successfully',
                    )
                );
                return response()->json($response);

            } else{

                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1002,
                    'response' => NULL,
                    'message' => 'No Order Found!'
                    );

                return response()->json($response);

            }*/
            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }

    public function paymentStatus(Request $request){

        try {

            $checkOrder = Order::where('id',$request->orderId)->first();
            if($checkOrder){

                $checkOrder->payment_status = 1;
                $checkOrder->status         = 3;
                $checkOrder->save();
                Cart::where('user_id',$request->userId)->update(['status' => 0]);
                $response = array(
                    'hasError' => FALSE,
                    'errorCode' => -1,
                    'message'  => "",
                    'response' => array(
                        'orderId'       => $request->orderId,
                        'statusMessage' => 'Ordered Successfully',
                    )
                );
                return response()->json($response);

            } else{

                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1002,
                    'response' => NULL,
                    'message' => 'No Order Found!'
                    );

                return response()->json($response);

            }
            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }

    public function orderList(Request $request){

        try {
            $orderArray = $productsArray = array();
            $checkOrders = Order::where('user_id',$request->userId)->get();
            if(count($checkOrders)){
                $i = 0;
                foreach ($checkOrders as $checkOrder) {
                    $useraddress = UserAddress::where('id',$checkOrder->address_id)->first();
                    if($useraddress){

                         $userAddressArray['id']      = $useraddress->id;
                         $userAddressArray['phoneNumber']   = $useraddress->mobile;
                         $userAddressArray['zip']     = $useraddress->pincode;
                         $userAddressArray['deliveryAddress'] = $useraddress->strretaddress;
                         $userAddressArray['city']    = $useraddress->city;
                         $userAddressArray['state']   = $useraddress->state;
                         $userAddressArray['type']    = $useraddress->type; 
                         $userAddressArray['fullName']    = $useraddress->name;  
                         $userAddressArray['landmark']    = $useraddress->landmark;   
                    }
                    else{
                        $userAddressArray = null; 
                    }
                    /*$shop = Shop::select('shops.name','shops.image','shops.is_opened','shop_type.name as shoptypename','shops.address','shops.street','shops.latitude','shops.longitude')
                        ->where('shops.id',1)
                        ->leftjoin('shop_type','shop_type.id','=','shops.shop_type')->first();*/
                    $shop = Shop::select('shops.name','shops.image','shops.is_opened','shop_type.name as shoptypename','shops.address','shops.street','shops.latitude','shops.longitude')
                        ->where('shops.id',$checkOrder->shop_id)
                        ->leftjoin('shop_type','shop_type.id','=','shops.shop_type')->first();
                        if($shop){
                            $location['address']      = $shop->address;  
                            $location['streetName']   = $shop->street;  
                            $location['latitude']     = $shop->latitude;  
                            $location['longitude']    = $shop->longitude;
                        } else{
                            $location = null;
                        }           
                    $items = orderItem::where('order_id',$checkOrder->id)
                         ->select('products.id','products.name','products.type','products.image','cart.quantity')   
                         ->leftjoin('cart','cart.id','=','order_items.cart_id')
                         ->leftjoin('products','products.id','=','cart.product_id')   
                         ->get();
                    if($items){
                        $j = 0;
                        $finalProduct = '';
                        foreach ($items as $item) {
                           $product = '';
                           $product = $item->name .' * '. $item->quantity;
                           $finalProduct .= $product.',';
                        }
                    } else{

                            $productsArray = null;
                    }    
                    $orderArray[$i]['id']            = $checkOrder->id;
                    $orderArray[$i]['resturant']     = $shop->name;
                    $orderArray[$i]['shopLocation']  = $location;
                    $orderArray[$i]['deliveryAddress']  = $userAddressArray;
                    $orderArray[$i]['status']        = $checkOrder->status;
                    $orderArray[$i]['orderDateTime'] = $checkOrder->updated_at;
                    $orderArray[$i]['total']         = $checkOrder->amount;
                    $orderArray[$i]['products']      = $finalProduct;
                    $i++;

                }

                $response = array(
                    'hasError' => FALSE,
                    'errorCode' => -1,
                    'message'  => "",
                    'response' => $orderArray
                );
                return response()->json($response);

            } else{

                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1002,
                    'response' => NULL,
                    'message' => 'No Orders Found!'
                    );

                return response()->json($response);

            }
            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }

    public function orderDetail(Request $request){

        try {
            $productArray = array();
            $checkOrder = Order::where('id',$request->orderId)->first();
            if($checkOrder){
                $useraddress = UserAddress::where('id',$checkOrder->address_id)->first();
                if($useraddress){

                     $userAddressArray['id']      = $useraddress->id;
                     $userAddressArray['phoneNumber']   = $useraddress->mobile;
                     $userAddressArray['zip']     = $useraddress->pincode;
                     $userAddressArray['deliveryAddress'] = $useraddress->strretaddress;
                     $userAddressArray['city']    = $useraddress->city;
                     $userAddressArray['state']   = $useraddress->state;
                     $userAddressArray['type']    = $useraddress->type; 
                     $userAddressArray['fullName']    = $useraddress->name; 
                     $userAddressArray['landmark']    = $useraddress->landmark;  
                }
                else{
                    $userAddressArray = null; 
                }
                $shop = Shop::select('shops.name','shops.image','shops.is_opened','shop_type.name as shoptypename','shops.address','shops.street','shops.latitude','shops.longitude')
                        ->where('shops.id',$checkOrder->shop_id)
                        ->leftjoin('shop_type','shop_type.id','=','shops.shop_type')->first();
                if($shop){
                            $location['address']      = $shop->address;  
                            $location['streetName']   = $shop->street;  
                            $location['latitude']     = $shop->latitude;  
                            $location['longitude']    = $shop->longitude;
                        } else{
                            $location = null;
                        }         
                $items = orderItem::where('order_id',$request->orderId)
                         ->select('products.id','products.name','products.type','products.image','products.price','cart.quantity')   
                         ->leftjoin('cart','cart.id','=','order_items.cart_id')
                         ->leftjoin('products','products.id','=','cart.product_id')   
                         ->get();
                if($items){
                    $i = 0;
                    foreach ($items as $item) {
                       
                       $productArray[$i]['id']      = $item->id;
                       $productArray[$i]['name']    = $item->name;
                       $productArray[$i]['qty']     = $item->quantity;
                       $productArray[$i]['price']   = $item->price;
                       $i++;
                    }
                } else{

                    $productArray = null;
                }                
                $response = array(
                    'hasError' => FALSE,
                    'errorCode' => -1,
                    'message'  => "",
                    'response' => array(
                        'orderId'            => $request->orderId,
                        'orderType'          => $checkOrder->delivery_type,
                        'deliveryAddress'    => $userAddressArray,
                        'destinationAddress' => $userAddressArray,
                        'restaurantName'     => $shop->name,
                        'orderList'          => $productArray,
                        'orderDateTime'      => $checkOrder->updated_at,
                        'subTotal'           => $checkOrder->amount,
                        'tax'                => 0,
                        'deliveryCharge'     => 0,
                        'totalDiscount'      => 0,
                        'totalPayable'       => $checkOrder->amount,
                        'paymentType'        => 1,
                        'orderStatus'        => $checkOrder->status,
                        'paymentMode'        => 1,
                        'totalPaid'          => $checkOrder->amount,
                        'notes'              => $checkOrder->note,
                        'shopLocation'       => $location,
                    )
                );
                return response()->json($response);

            } else{

                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1002,
                    'response' => NULL,
                    'message' => 'No Order Found!'
                    );

                return response()->json($response);

            }
            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }

    public function productSearch(Request $request){

        try {
            if(!$request->deliveryId || $request->deliveryId == "-1"){


                $response = array(
                    'hasError' => TRUE,
                    'errorCode' => 1002,
                    'response' => NULL,
                    'message' => 'Missing Delivery area'
                );

                return response()->json($response);
            }
            if($request->deliveryId != '-1'){ 
                $deliveyId = $request->deliveryId;
            } else{
                $deliveyId = 0;
            }
            $search    = $request->keyword;
            $shopArray = $productArray = array();
            $products  = Product::where('products.status',1)
                            //->select('id','price','image','type','isveg as isVeg','tag','name')
                            ->when($search, function ($query) use ($search) {
                                return $query->whereRaw("name like '%$search%'");
                            })
                            /*->when($deliveyId, function ($query) use ($deliveyId) {
                                return $query->leftjoin('shops','shops.user_id','=','products.shop_id')
                                        ->leftjoin('delivery_area_list_shops','delivery_area_list_shops.shop_id','=','shops.user_id')
                                        //->select('shops.*')
                                        ->where('delivery_area_list_shops.delivery_area',$deliveyId);
                            })*/
                            ->get();
            $shops     = Shop::where('shops.status',1)
                            ->select('shops.id','shops.name','shops.image','shops.is_opened','shop_type.name as shoptypename','shops.address','shops.street','shops.latitude','shops.longitude','shops.shop_type')
                            ->leftjoin('shop_type','shop_type.id','=','shops.shop_type')
                            ->when($search, function ($query) use ($search) {
                                return $query->whereRaw("shops.name like '%$search%'");
                            })
                            ->when($deliveyId, function ($query) use ($deliveyId) {
                                return $query->leftjoin('delivery_area_list_shops','delivery_area_list_shops.shop_id','=','shops.user_id')
                                        ->select('shops.*')
                                        ->where('delivery_area_list_shops.delivery_area',$deliveyId);
                            })
                            ->get();  
            if(count($shops)){
                $i = 0;
                foreach ($shops as $resturant) {
                    $rating  = Rating::where('restaurant_id',$resturant->id)->avg('rating');
                    $ShopType = ShopType::where('id',$resturant->shop_type)->first();
                    $mainArray[$i]['id']        = $resturant->id;
                    $mainArray[$i]['name']      = $resturant->name;
                    $mainArray[$i]['image']     = $resturant->image;
                    $mainArray[$i]['isOpened']  = $resturant->is_opened;
                    $mainArray[$i]['shopType']  = $ShopType->name;
                    $mainArray[$i]['rating']    = round($rating);
                    $mainArray[$i]['minOrder']  = $resturant->min_order;
                    $mainArray[$i]['minTime']  = $resturant->min_time;
                    $location['address']       = $resturant->address;
                    $location['streetName']    = $resturant->street;
                    $location['latitude']      = $resturant->latitude;
                    $location['longitude']     = $resturant->longitude;
                    $mainArray[$i]['location'] = $location;
                    $i++;
                }
            } else{

                $mainArray = null;
            }
            if(count($products)){
                $i = 0;
                foreach ($products as $product) {
                   $shopDetails = Shop::where('user_id',$product->shop_id)->select('id','name')->first();
                   if($shopDetails) {
                    $shopId   = $shopDetails->id;
                    $shopName = $shopDetails->name;
                   } else{

                   }
                   $mainArray2[$i]['id'] = $product->id;
                   $mainArray2[$i]['name'] = $product->name; 
                   $mainArray2[$i]['price'] = $product->price;
                   $mainArray2[$i]['image'] = $product->image;
                   $mainArray2[$i]['type'] = $product->type;
                   $mainArray2[$i]['isVeg'] = $product->isVeg;
                   $mainArray2[$i]['shopId'] = $shopId;
                   $mainArray2[$i]['shopName'] = $shopName;
                   $tag = Tag::where('id',$product->tag)->first();
                   if($tag){
                    $tags['id'] = $tag->id;
                    $tags['name'] = $tag->name;
                    $tags['type'] = $tag->type;
                   }
                   else{
                    $tags = null;
                   }
                   $mainArray2[$i]['tag'] = $tags;
                   $customise = array();
                   if($product->type == 2){
                        $customs = ProductCustomise::leftjoin('sizegroups','sizegroups.id','=','products_customise.name')->where('product_id',$product->id)->get();
                        if($customs){
                            $j = 0;
                            foreach ($customs as $custom) {
                               
                                $customise[$j]['id'] = $custom->id;
                                $customise[$j]['name'] = $custom->name;
                                $customise[$j]['addOns'] = ProductAddon::select('id','name')->where('product_id',$product->id)->get();
                                $j++;
                            }

                        } else{
                            $customise = null;
                        }
                   }
                   else{
                        $customise = null;
                   }
                   $mainArray2[$i]['customise'] = $customise;
                   $i++;
                }
            } else{

                $mainArray2 = null;
            }                
            $response = array(
                    'hasError' => FALSE,
                    'errorCode' => -1,
                    'message'  => "",
                    'response' => array(
                        'shops'       => $mainArray,
                        'products'    => $mainArray2,
                    )
                );
            return response()->json($response);                               

            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }

    }

    public function addRating(Request $request){

        try {

            $rating = new Rating;
            $rating->user_id = $request->userId;
            $rating->orderid = $request->orderId;
            $rating->rating  = $request->rating;
            $rating->save();
            $response = array(
                    'hasError' => FALSE,
                    'errorCode' => -1,
                    'message'  => "",
                    'response' => "Rating Added",
                );
            return response()->json($response);
            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }
        
    }
    public function addAddress(Request $request){

        try {
            if($request->addressId == '-1'){
                $address = new UserAddress;
                $address->user_id = $request->userId;
                $address->name    = $request->fullName;
                $address->mobile  = $request->phoneNumber;
                $address->strretaddress  = $request->deliveryAddress;
                $address->city    = $request->city;
                $address->landmark  = $request->landmark;
                $address->state     = $request->state;
                $address->save();
            }
            else{

                $address = UserAddress::where('id',$request->addressId)->first();
                $address->user_id = $request->userId;
                $address->name    = $request->fullName;
                $address->mobile  = $request->phoneNumber;
                $address->strretaddress  = $request->deliveryAddress;
                $address->city    = $request->city;
                $address->landmark  = $request->landmark;
                $address->state     = $request->state;
                $address->save();

            }    


            $response = array(
                    'hasError' => FALSE,
                    'errorCode' => -1,
                    'message'  => "",
                    'response' => "Address added successfully",
                );
            return response()->json($response);
            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }
        
    }

    public function removeAddress(Request $request){

        try {
            

            $address = UserAddress::where('id',$request->addressId)->first();
            if($address){

                $address->delete();
            }
              


            $response = array(
                    'hasError' => FALSE,
                    'errorCode' => -1,
                    'message'  => "",
                    'response' => "Address Removed Successfully",
                );
            return response()->json($response);
            
        } catch (Exception $ex) {

            $response = array(
                'hasError' => TRUE,
                'errorCode' => 1002,
                'response' => NULL,
                'message' => $ex->getMessage()
            );

            return response()->json($response);
            
        }
        
    }


} // end of class
