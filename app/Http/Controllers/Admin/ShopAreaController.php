<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB,
    Mail,
    Auth,
    Validator,
    Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Response;
use Config;
use App;
use App\Model\Admin\Shop;
use App\Model\Admin\DeliveryAreaList;
use App\User as AuthUser;
use Hash;
use App\Model\Admin\DeliveryAreaListShop;

class ShopAreaController extends Controller {

    public function index(Request $request) {

        $paginate = 10;
        $areas =  DeliveryAreaListShop::leftjoin('delivery_area_lists','delivery_area_lists.id','=','delivery_area_list_shops.delivery_area')
                  ->select('delivery_area_lists.delivery_area','delivery_area_list_shops.id')  
                  ->where('delivery_area_list_shops.shop_id',Auth::user()->id)->paginate($paginate);
        return view('shoparea.list', compact('areas'));
    }

    public function create() {
        $areas =  DeliveryAreaList::get();
        $area  =  DeliveryAreaListShop::where('shop_id',Auth::user()->id)->pluck('delivery_area')->toArray();
        return view('/shoparea/form',compact('area','areas'));
    } 

    public function store(Request $request) {
        
        if(count($request->delivery_areas)){
            DeliveryAreaListShop::where('shop_id',Auth::user()->id)->delete();
            foreach ($request->delivery_areas as $delivery_area) {
                
                DeliveryAreaListShop::Create(['shop_id' => Auth::user()->id,'delivery_area' => $delivery_area]);
            }
        }    
        
        return Redirect::to('dashboard/shopareas');
    }

    Private function uploadStoreLogo($request) {

        if ($request->hasFile('servicelogo')) {
            $file = $request->file('servicelogo');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $path = public_path() . '/images/offers/';
            $image = '/images/offers/'.$filename;
            $file->move($path, $filename);
        } elseif ($request->has('servicelogo') && $request->servicelogo == '') {
                
                $image = $request->lasthidimg;
        }
        else{
                $image = $request->lasthidimg;
        }
        return $image;    

    }

    public function show($id) {
        $area  = DeliveryAreaList::where('id',$id)->first();
        return view('/shoparea/more', compact('area','areas'));
    }

    public function edit($id) {
        $areas =  DeliveryAreaList::get();
        $area  =  DeliveryAreaListShop::where('shop_id',Auth::user()->id)->pluck('delivery_area')->toArray();
        return view('shoparea.form', compact('area','areas'));
    } 

    public function update(Request $request, $id) {
        $offer = DeliveryAreaList::find($id);
        $offer->update(['shop_id' => 0,'delivery_area' => $request->delivery_area]);
        return Redirect::to('dashboard/shopareas');  
    }

    public function destroy($id) {
        $area = DeliveryAreaListShop::findOrFail($id);
        //$offer->update(['status' => 0]);
        $area->delete();

        return Redirect::to('dashboard/shopareas');  
    }  

    public function checkService(Request $request){

        $service = Servicetype::where('service_name_en',$request->servicename)->first();
        if($service){

            return 1;
        }
        else{

            return 0;
        }
    } 

    public function deleted(){

        $paginate = 10;
        $services =  Servicetype::onlyTrashed()->paginate($paginate);
        return view('/shops/deleted', compact('services'));
    } 

    public function restore($id) {
        $tags = Servicetype::withTrashed()->find($id);
        $tags->restore();
        return Redirect::to('deleted/shops');  
    }     

}
