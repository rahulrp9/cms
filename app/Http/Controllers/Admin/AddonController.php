<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB,
    Mail,
    Auth,
    Validator,
    Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Response;
use Config;
use App;
use App\Model\Admin\Shop;
use App\Model\Admin\Offer;
use App\Model\Admin\Coupon;
use App\User as AuthUser;
use Hash;
use App\Model\Admin\ProductAddon;
use App\Model\Admin\Product;
use App\Model\Admin\Tag;
use App\Model\Admin\ProductCustomise;
use App\Model\Admin\Category;
use App\Model\Admin\SizeGroup;

class AddonController extends Controller {

    public function index(Request $request) {
        $paginate = 10;
        if(Auth::user()->role_id == 1){
            $products =  Product::select('products.id','products.name','products.price','shops.name as shopname') 
                       ->leftjoin('shops','shops.user_id','=','products.shop_id')
                       ->where('products.status',1)
                       ->where('products.isaddon',1)
                       ->groupBy('products.id')
                       ->paginate($paginate);
        } else{
            $products =  Product::select('products.id','products.name','products.price','shops.name as shopname') 
                       ->where('products.shop_id',Auth::user()->id)
                       ->leftjoin('shops','shops.user_id','=','products.shop_id')
                       ->where('products.status',1)
                       ->where('products.isaddon',1)
                       ->paginate($paginate);
        }
        return view('addons.list', compact('products'));
    }

    public function create() {
        /*$mainCategory = array();
        $category = Category::where('parent_id',0)->where('status',1)->get();
        if($category){
            $i = 0;
            foreach ($category as $cat) {
                $sub = Category::where('parent_id',$cat->id)->where('status',1)->get();
                $mainCategory[$i]['name'] = $cat->name;
                $mainCategory[$i]['sub'] = $sub;
                $i++;
            }
        }
        $addons = Product::where('isaddon',1)->where('status',1)->get();
        $sizegroups = SizeGroup::get();*/
        //compact('mainCategory','addons','sizegroups')
        return view('/addons/form');
    } 

    public function store(Request $request) {
        /*$rules     = array('code' => 'unique:coupons,code',
                    'value' => 'required'
                   );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with(['flash_error' => "Mandatory fields missing"]);
        } else {*/
        $logo = $this->uploadStoreLogo($request);
       /* if(isset($request->type)){
            $type = 2;
        } else{
            $type = 1;
        }
        if(isset($request->isaddon)){
            $isaddon = 1;
        } else{
            $isaddon = 0;
        }*/
        $product = Product::Create(['shop_id' => Auth::user()->id,'name' => $request->name,'image' =>$logo,'price'=>$request->value,'isaddon'=>'1','isveg'=>$request->isveg]);
        //''=>$isaddon,
        /*}*/
        return Redirect::to('dashboard/addons');
    }

    Private function uploadStoreLogo($request) {

        if ($request->hasFile('servicelogo')) {
            $file = $request->file('servicelogo');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $path = public_path() . '/images/products/';
            $image = '/images/products/'.$filename;
            $file->move($path, $filename);
        } elseif ($request->has('servicelogo') && $request->servicelogo == '') {
                
                $image = $request->lasthidimg;
        }
        else{
                $image = $request->lasthidimg;
        }
        return $image;    

    }

    public function show($id) {
        $product  = Product::where('id',$id)->first();
        //$pcutomises = ProductCustomise::where('product_id',$id)->get();
        return view('/addons/more', compact('product'));
    }

    public function edit($id) {
        $product  = Product::where('id',$id)->first();
        /*$mainCategory = array();
        $category = Category::where('parent_id',0)->where('status',1)->get();
        if($category){
            $i = 0;
            foreach ($category as $cat) {
                $sub = Category::where('parent_id',$cat->id)->where('status',1)->get();
                $mainCategory[$i]['name'] = $cat->name;
                $mainCategory[$i]['sub'] = $sub;
                $i++;
            }
        }
        $addons = Product::where('isaddon',1)->where('status',1)->get();
        $paddons = ProductAddon::where('product_id',$id)->pluck('name')->toArray();
        $pcutomises = ProductCustomise::where('product_id',$id)->get();
        $sizegroups = SizeGroup::get();*/
        //,'mainCategory','addons','paddons','pcutomises','sizegroups'
        return view('addons.form', compact('product'));
    } 

    public function update(Request $request, $id) {
        $product = Product::find($id);
        $logo = $this->uploadStoreLogo($request);
        /*if(isset($request->type)){
            $type = 2;
        } else{
            $type = 1;
        }
        if(isset($request->isaddon)){
            $isaddon = 1;
        } else{
            $isaddon = 0;
        }*/
        $product->update(['shop_id' => Auth::user()->id,'name' => $request->name,'image' =>$logo,'price'=>$request->value,'isaddon'=>'1','isveg'=>$request->isveg]);
        return Redirect::to('dashboard/addons');  
    }

    public function destroy($id) {
        $coupon = Product::findOrFail($id);
        $coupon->update(['status' => 0]);
        //$shop->delete();

        return Redirect::to('dashboard/addons');  
    }  

    public function checkService(Request $request){

        $service = Servicetype::where('service_name_en',$request->servicename)->first();
        if($service){

            return 1;
        }
        else{

            return 0;
        }
    } 

    public function deleted(){

        $paginate = 10;
        $services =  Servicetype::onlyTrashed()->paginate($paginate);
        return view('/shops/deleted', compact('services'));
    } 

    public function restore($id) {
        $tags = Servicetype::withTrashed()->find($id);
        $tags->restore();
        return Redirect::to('deleted/shops');  
    }     

}
