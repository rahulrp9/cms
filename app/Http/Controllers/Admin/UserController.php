<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB,
    Mail,
    Auth,
    Validator,
    Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Response;
use Config;
use App;
use App\Model\Admin\Shop;
use App\User as AuthUser;
use Hash;

class UserController extends Controller {

    public function index(Request $request) {

        $paginate = 10;
        $users =  AuthUser::where('role_id',0)->paginate($paginate);
        return view('users.list', compact('users'));
    }

    /*public function create() {
        return view('/shops/form');
    } 

    public function store(Request $request) {
        $rules     = array('email' => 'unique:users,email',
                    'name' => 'required',
                    'phone' => 'required',
                    'address' => 'required',
                    'street' => 'required', 
                    'latitude'=> 'required',
                    'longitude'=> 'required');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with(['flash_error' => "Mandatory fields missing"]);
        } else {
        $logo = $this->uploadStoreLogo($request);
        $password = 'qwerty';
        $userSave = new AuthUser();
        $userSave->name         = $request->name;
        $userSave->phone        = $request->phone;
        $userSave->password     = Hash::make($password);
        $userSave->status       = 1;
        $userSave->email        = $request->email;
        $userSave->role_id      = 2;
        $data = $userSave->save();
        Shop::Create(['user_id' => $userSave->id,'name' => $request->name,'image' =>$logo,'address'=>$request->address,'street'=>$request->street,'latitude'=>$request->latitude,'longitude'=>$request->longitude]);
        }
        return Redirect::to('dashboard/shops');
    }

    Private function uploadStoreLogo($request) {

        if ($request->hasFile('servicelogo')) {
            $file = $request->file('servicelogo');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $path = public_path() . '/images/shops/';
            $image = '/images/shops/'.$filename;
            $file->move($path, $filename);
        } elseif ($request->has('servicelogo') && $request->servicelogo == '') {
                
                $image = $request->lasthidimg;
        }
        else{
                $image = $request->lasthidimg;
        }
        return $image;    

    }

    public function show($id) {
        $shop  = Shop::leftjoin('users','users.id','=','shops.user_id')
                 ->select('shops.id','shops.name','shops.address','shops.street','shops.latitude','shops.longitude','users.email','users.phone','shops.image')
                 ->where('shops.id',$id)->first();
        return view('/shops/more', compact('shop'));
    }

    public function edit($id) {
        $shop  = Shop::leftjoin('users','users.id','=','shops.user_id')
                 ->select('shops.id','shops.name','shops.address','shops.street','shops.latitude','shops.longitude','users.email','users.phone','shops.image')
                 ->where('shops.id',$id)->first();
        return view('shops.form', compact('shop'));
    } 

    public function update(Request $request, $id) {
        $shop = Shop::find($id);
        $logo = $this->uploadStoreLogo($request);
        $shop->update(['name' => $request->name,'image' =>$logo,'address'=>$request->address,'street'=>$request->street,'latitude'=>$request->latitude,'longitude'=>$request->longitude]);
        $user = AuthUser::find($shop->user_id);
        $shop->update(['email' => $request->email,'phone'=>$request->phone,'name'=>$request->name]);
        return Redirect::to('dashboard/shops');  
    }

    public function destroy($id) {
        $shop = Shop::findOrFail($id);
        $shop->update(['status' => 0]);
        //$shop->delete();

        return Redirect::to('dashboard/shops');  
    }  

    public function checkService(Request $request){

        $service = Servicetype::where('service_name_en',$request->servicename)->first();
        if($service){

            return 1;
        }
        else{

            return 0;
        }
    } 

    public function deleted(){

        $paginate = 10;
        $services =  Servicetype::onlyTrashed()->paginate($paginate);
        return view('/shops/deleted', compact('services'));
    } 

    public function restore($id) {
        $tags = Servicetype::withTrashed()->find($id);
        $tags->restore();
        return Redirect::to('deleted/shops');  
    } */    

}
