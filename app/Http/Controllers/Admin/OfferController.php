<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB,
    Mail,
    Auth,
    Validator,
    Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Response;
use Config;
use App;
use App\Model\Admin\Shop;
use App\Model\Admin\Offer;
use App\User as AuthUser;
use Hash;

class OfferController extends Controller {

    public function index(Request $request) {

        $paginate = 10;
        $offers =  Offer::where('status',1)->paginate($paginate);
        return view('offers.list', compact('offers'));
    }

    public function create() {
        return view('/offers/form');
    } 

    public function store(Request $request) {
        $rules     = array('name' => 'unique:offers,name',
                    'value' => 'required'
                   );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with(['flash_error' => "Mandatory fields missing"]);
        } else {
        $logo = $this->uploadStoreLogo($request);
        Offer::Create(['shop_id' => Auth::user()->id,'name' => $request->name,'image' =>$logo,'value'=>$request->value]);
        }
        return Redirect::to('dashboard/offers');
    }

    Private function uploadStoreLogo($request) {

        if ($request->hasFile('servicelogo')) {
            $file = $request->file('servicelogo');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $path = public_path() . '/images/offers/';
            $image = '/images/offers/'.$filename;
            $file->move($path, $filename);
        } elseif ($request->has('servicelogo') && $request->servicelogo == '') {
                
                $image = $request->lasthidimg;
        }
        else{
                $image = $request->lasthidimg;
        }
        return $image;    

    }

    public function show($id) {
        $offer  = Offer::where('id',$id)->first();
        return view('/offers/more', compact('offer'));
    }

    public function edit($id) {
        $offer  = Offer::where('id',$id)->first();
        return view('offers.form', compact('offer'));
    } 

    public function update(Request $request, $id) {
        $offer = Offer::find($id);
        $logo = $this->uploadStoreLogo($request);
        $offer->update(['shop_id' => Auth::user()->id,'name' => $request->name,'image' =>$logo,'value'=>$request->value]);
        return Redirect::to('dashboard/offers');  
    }

    public function destroy($id) {
        $offer = Offer::findOrFail($id);
        $offer->update(['status' => 0]);
        //$shop->delete();

        return Redirect::to('dashboard/offers');  
    }  

    public function checkService(Request $request){

        $service = Servicetype::where('service_name_en',$request->servicename)->first();
        if($service){

            return 1;
        }
        else{

            return 0;
        }
    } 

    public function deleted(){

        $paginate = 10;
        $services =  Servicetype::onlyTrashed()->paginate($paginate);
        return view('/shops/deleted', compact('services'));
    } 

    public function restore($id) {
        $tags = Servicetype::withTrashed()->find($id);
        $tags->restore();
        return Redirect::to('deleted/shops');  
    }     

}
