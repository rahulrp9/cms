<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use DB,
    Mail,
    Auth,
    Validator,
    Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Kamaln7\Toastr\Facades\Toastr;
use Response;
use Config;
use App;
use App\Model\Admin\Category;
use App\Models\Specification;
use App\Models\CategorySpecs;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

    public function categories(){
        /*try {
            $paginate = 10;
            $categories = DB::table('categories')
                    ->select('categories.*')
                    ->orderby('id', 'ASC')->paginate($paginate);
            return view('admin/categories', array('categories' => $categories));
        } catch (\Exception $e) {
            Toastr::error('Sorry There Was Some Problem!', $title = null, $options = []);
            return Redirect::to('dashboard');
        }

*/		$paginate = 10;
        $categories = Category::where('parent_id', '=', 0)->where('status',1)->paginate($paginate);
        $allCategories = Category::pluck('name','id')->all();
        //$subcategories = Category::where('parent_id','!=',0)->get();
        return view('categories.index',compact('categories','allCategories'));
    }

    public function getcategoryById($id){
        $result ='';
        $subcategories = Category::where('parent_id','=',$id)->get();
        foreach ($subcategories as $value) {
            $result .= '<li>';
            $result .= '<a href="javascript:void(0);" onclick="javascript:getCategoryById('.$value->id.');">';
            $result .= $value->name;
            $result .= '</a>';
            $result .= '<ul id ="sub_cat_'.$value->id.'">';
            $result .= '</ul>';
            $result .= '</li>';

        }
        return $result;
    }

    public function addcategory($id = ''){
        $specs = '';
        if($id){

            $categories = Category::where('status', '=', '1')->get();
            return view('categories.addcategory',array('categories' => $categories,'catid'=>$id,'specs'=>$specs));
        }else{
            
            $categories = Category::where('status', '=', '1')->get();
            return view('categories.addcategory',array('categories' => $categories,'catid'=>'','specs'=>$specs));
        }



    }

    public function savecategory(Request $request){

        $cat_en = $request->cat_name_en;
        //$cat_ar = Input::get('cat_name_ar');
        $cat_status = $request->cat_status;
        //$des_en = Input::get('des_en');
        //$des_ar = Input::get('des_ar');
        $image  = $request->file('image');
        $parent_id   = $request->pcategory;
        $date 	 = date("Y-m-d H:i:s");
        if($image){
            $filename = time() . $image->getClientOriginalName();
            $path = public_path() . '/images/category/';
            $image->move($path, $filename);
            $image = '/images/category/'.$filename;


        }else{
            $image = '';
        }
        if($parent_id == 0)
        {
            $po = 0;
        }
        else
        {
            $position = Category::where('id','=',$parent_id)->select('categories.position')->first();
            $po = $position->position + 1;
        }
        $newcat = new Category();
        $newcat->name = $cat_en;
        //$newcat->cat_name_ar = $cat_ar;
        //$newcat->description_en = $des_en;
        //$newcat->description_ar = $des_ar;
        $newcat->image 		 = $image;
        //$newcat->color 		 = $color;
        $newcat->parent_id   = $parent_id;
        //$newcat->position    = $po;
        $newcat->shop_id 	 =  Auth::user()->id;
        //$newcat->category_slug = $slug;
        $newcat->status 	 = 1;
        //$newcat->created_at  = $date;
        //$newcat->updated_at  = $date;
        //$newtag->deleted_at = $date;
        $newcat->save();
        $insertid = $newcat->id;


         $recent = 'Category: '.$cat_en.' was Created.';   
         $type = 'Category';  
        return Redirect::to('dashboard/categories');
    }

    public function checkcatname() {

        $tag_en = Input::get('tag_en');
        $tag_ar = Input::get('tag_ar');
        $catid =  Input::get('category_id');
        if($catid){
            $cat_en = Category::where('name',$tag_en)->where('status','1')->where('id','!=',$catid)->first();
            //$cat_ar = Category::where('cat_name_ar',$tag_ar)->where('status','1')->where('id','!=',$catid)->first();
        }
        else{
            $cat_en = Category::where('name',$tag_en)->where('status','1')->first();
            //$cat_ar = Category::where('cat_name_ar',$tag_ar)->where('status','1')->first();
        }         

        if($cat_en) {
            $catn = 1;
        }
        else if($cat_ar) {
            $catn = 2;
        }
        else{
            $catn = 0;
        }
        return $catn;
    }

    public function editcat($id) {
        $specs = '';
        $speca = array();
        $category=Category::findOrFail($id);
        $cats = DB::table('categories')
            ->where(['id' => $id])
            ->first();
        $scat = Category::where('status', '=', '1')->where('parent_id','=',$cats->parent_id)
            ->where('id','!=',$id)
            ->selectRaw('GROUP_CONCAT(categories.id) as gid')
            ->get();


        //print_r($speca);exit('pop');
        return view('categories.editcat', array('cats' => $cats,'specs' => $specs,'specsval' => $speca));
    }

    public function viewcat($id) {
        $category=Category::findOrFail($id);
        /*$idcat = Category::where('id','=',$id)
            ->select('child_count')
            ->first();
        if($idcat){
            $childcount = $idcat->child_count;
        }
        else{

            $childcount = '';
        }*/
        $childcount = '';
        $cats = DB::table('categories')
            ->where(['id' => $id])
            ->first();
        return view('categories.viewcat', array('cats' => $cats,'ccount' =>$childcount,'catid'=>$id));
    }


    public function updatecat(Request $request){

        $cat_en = $request->cat_name_en;
        $image  = $request->file('image');
        $hidimage  = $request->hidimage;
        $id     = $request->id;
        $date 	= date("Y-m-d H:i:s");
        if($image){
            $filename = time() . $image->getClientOriginalName();
            $path = public_path() . '/images/categories/';
            $image->move($path, $filename);
            $image = '/images/categories/'.$filename;


        }else{
            $image = $hidimage;
        }
        $newcat = Category::find($id);
        $newcat->name = $cat_en;
        $newcat->image 		 = $image;
        $newcat->shop_id 	 = Auth::user()->id;
        $newcat->created_at  = $date;
        $newcat->updated_at  = $date;
        $newcat->status 	 = $request->cat_status;
        //$newtag->deleted_at = $date;
        $newcat->save();
        $recent = 'Category: '.$cat_en.' was Updated.';   
        $type = 'Category';  
        return Redirect::to('dashboard/categories');
    }

    public function delcat($id) {
        $category=Category::findOrFail($id);
        $date 	= date("Y-m-d H:i:s");
        try {
            $gettag = Category::find($id);
            $gettag->status = '0';
            $gettag->deleted_at = $date;
            $gettag->save();

            $notification = array(
                'message' => 'Deleted successfully',
                'alert-type' => 'success'
            );
            $recent = 'Category: '.$gettag->name.' was Deleted.';   
            $type = 'Category';  
            return Redirect::to('dashboard/categories')->with($notification);

           // Toastr::success('Successfully Disabled', $title = null, $options = []);
           // return Redirect::to('admin/categories');
        } catch (\Exception $e) {
            //echo $e->getMessage();
           // Toastr::error('Sorry There Was Some Problem!', $title = null, $options = []);
            $notification = array(
                'message' => 'Sorry There Was Some Problem!',
                'alert-type' => 'error'
            );

            // return Redirect::to('admin/categories');
            return Redirect::to('dashboard/categories')->with($notification);
        }
    }

    public function publish(Request $request){

         $prdt = Category::find($request->pdtid);
         if($prdt){

            $prdt->status = '1';
            $prdt->save();
            return 1;
         }else{
            return 0;
         }

    }

    public function unpublish(Request $request){

         $prdt = Category::find($request->pdtid);
         if($prdt){

            $prdt->status = '0';
            $prdt->save();
            return 1;
         }else{
            return 0;
         }

    }

    public function hidden(Request $request){

         $prdt = Category::find($request->pdtid);
         if($prdt){

            $prdt->status = '2';
            $prdt->save();
            return 1;
         }else{
            return 0;
         }

    }

    
}