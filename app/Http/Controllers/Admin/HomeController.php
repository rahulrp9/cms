<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use Hash;
use DB;
use App\Model\Admin\Shop;
use App\Model\Admin\Product;
use App\Model\Admin\Order;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [];

        $data['users'] = User::where('role_id', 0)->count();
        //where('role_id',0)       

        $data['shops'] = Shop::where('status',1)->count();
        $data['products'] = Product::where('status', 1)->count();
        $data['orders'] = Order::where('status', 1)->count();


        return view('dashboard.dashboard', $data);
    }

    public function profile($id)
    {
        $data = [];
        $admins = new User;
        $data['admin'] = $admins::find($id);
        $data['updating'] = true;

        return view('admin.user.show', $data);
    }

    public function update(Request $request, $id)
    {
        $data = [];
        $admins = new User();
        $admin = $admins::find($id);
        // dd($request);
        $this->validate(
            $request,
            [
                'name' => 'required',
                'email' => 'required',
                'password' => 'same:cpassword',
            ]
        );

        $admin->name = $request->input('name');
        $admin->email = $request->input('email');

        if ($request->password != '') {
            if ($request->password  === $request->cpassword) {
                $admin->password = Hash::make($request->password);
            } else {
                Toastr::error('Password OR Confirm is Not Match! Try again', 'Error');
                return back();
            }
        }

        if ($request->file('image')) {

            $image = $request->file('image');
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('backend/images'), $new_name);

            $admin->image = $new_name;
        }
        //exit();
        if ($admin->save()) {
            Toastr::success('Profile updated successfully', 'Success');
            return redirect()->route('admin.index');
        } else {
            Toastr::error('Something went wrong! Try again', 'Error');
            return back();
        }
    }


}
