<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB,
    Mail,
    Auth,
    Validator,
    Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Response;
use Config;
use App;
use App\Model\Admin\Shop;
use App\Model\Admin\Offer;
use App\Model\Admin\Coupon;
use App\User as AuthUser;
use Hash;

class CouponController extends Controller {

    public function index(Request $request) {

        $paginate = 10;
        $coupons =  Coupon::where('status',1)->paginate($paginate);
        return view('coupons.list', compact('coupons'));
    }

    public function create() {
        return view('/coupons/form');
    } 

    public function store(Request $request) {
        $rules     = array('code' => 'unique:coupons,code',
                    'value' => 'required'
                   );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with(['flash_error' => "Mandatory fields missing"]);
        } else {
        $logo = $this->uploadStoreLogo($request);
        Coupon::Create(['shop_id' => Auth::user()->id,'name' => $request->name,'image' =>$logo,'value'=>$request->value,'code'=>$request->code]);
        }
        return Redirect::to('dashboard/coupons');
    }

    Private function uploadStoreLogo($request) {

        if ($request->hasFile('servicelogo')) {
            $file = $request->file('servicelogo');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $path = public_path() . '/images/coupons/';
            $image = '/images/coupons/'.$filename;
            $file->move($path, $filename);
        } elseif ($request->has('servicelogo') && $request->servicelogo == '') {
                
                $image = $request->lasthidimg;
        }
        else{
                $image = $request->lasthidimg;
        }
        return $image;    

    }

    public function show($id) {
        $coupon  = Coupon::where('id',$id)->first();
        return view('/coupons/more', compact('coupon'));
    }

    public function edit($id) {
        $coupon  = Coupon::where('id',$id)->first();
        return view('coupons.form', compact('coupon'));
    } 

    public function update(Request $request, $id) {
        $coupon = Coupon::find($id);
        $logo = $this->uploadStoreLogo($request);
        $coupon->update(['shop_id' => Auth::user()->id,'name' => $request->name,'image' =>$logo,'value'=>$request->value,'code'=>$request->code]);
        return Redirect::to('dashboard/coupons');  
    }

    public function destroy($id) {
        $coupon = Coupon::findOrFail($id);
        $coupon->update(['status' => 0]);
        //$shop->delete();

        return Redirect::to('dashboard/coupons');  
    }  

    public function checkService(Request $request){

        $service = Servicetype::where('service_name_en',$request->servicename)->first();
        if($service){

            return 1;
        }
        else{

            return 0;
        }
    } 

    public function deleted(){

        $paginate = 10;
        $services =  Servicetype::onlyTrashed()->paginate($paginate);
        return view('/shops/deleted', compact('services'));
    } 

    public function restore($id) {
        $tags = Servicetype::withTrashed()->find($id);
        $tags->restore();
        return Redirect::to('deleted/shops');  
    }     

}
