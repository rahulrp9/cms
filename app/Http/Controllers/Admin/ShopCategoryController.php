<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB,
    Mail,
    Auth,
    Validator,
    Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Response;
use Config;
use App;
use App\Model\Admin\Shop;
use App\Model\Admin\Category;
use App\User as AuthUser;
use Hash;
use App\Model\Admin\ShopCategory;

class ShopCategoryController extends Controller {

    public function index(Request $request) {

        $paginate = 10;
        $areas =  ShopCategory::leftjoin('categories','categories.id','=','shop_categories.category_id')
                  ->select('categories.name','categories.image','shop_categories.id')  
                  ->where('shop_categories.shop_id',Auth::user()->id)->paginate($paginate);
        return view('shopcategory.list', compact('areas'));
    }

    public function create() {
        $categories =  Category::where('status',1)->get();
        $category  =  ShopCategory::where('shop_id',Auth::user()->id)->pluck('category_id')->toArray();
        return view('/shopcategory/form',compact('category','categories'));
    } 

    public function store(Request $request) {
        
        if(count($request->categories)){
            ShopCategory::where('shop_id',Auth::user()->id)->delete();
            foreach ($request->categories as $category) {
                
                ShopCategory::Create(['shop_id' => Auth::user()->id,'category_id' => $category]);
            }
        }    
        
        return Redirect::to('dashboard/shopcategories');
    }

    Private function uploadStoreLogo($request) {

        if ($request->hasFile('servicelogo')) {
            $file = $request->file('servicelogo');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $path = public_path() . '/images/offers/';
            $image = '/images/offers/'.$filename;
            $file->move($path, $filename);
        } elseif ($request->has('servicelogo') && $request->servicelogo == '') {
                
                $image = $request->lasthidimg;
        }
        else{
                $image = $request->lasthidimg;
        }
        return $image;    

    }

    public function show($id) {
        $area  = DeliveryAreaList::where('id',$id)->first();
        return view('/shopcategory/more', compact('area','areas'));
    }

    public function edit($id) {
        $categories =  Category::where('status',1)->get();
        $category  =  ShopCategory::where('shop_id',Auth::user()->id)->pluck('category_id')->toArray();
        return view('/shopcategory/form',compact('category','categories'));
    } 

    public function update(Request $request, $id) {
        if(count($request->categories)){
            ShopCategory::where('shop_id',Auth::user()->id)->delete();
            foreach ($request->categories as $category) {
                
                ShopCategory::Create(['shop_id' => Auth::user()->id,'category_id' => $category]);
            }
        } 
        return Redirect::to('dashboard/shopcategories');  
    }

    public function destroy($id) {
        $area = ShopCategory::findOrFail($id);
        //$offer->update(['status' => 0]);
        $area->delete();

        return Redirect::to('dashboard/shopcategories');  
    }  

    public function checkService(Request $request){

        $service = Servicetype::where('service_name_en',$request->servicename)->first();
        if($service){

            return 1;
        }
        else{

            return 0;
        }
    } 

    public function deleted(){

        $paginate = 10;
        $services =  Servicetype::onlyTrashed()->paginate($paginate);
        return view('/shops/deleted', compact('services'));
    } 

    public function restore($id) {
        $tags = Servicetype::withTrashed()->find($id);
        $tags->restore();
        return Redirect::to('deleted/shops');  
    }     

}
