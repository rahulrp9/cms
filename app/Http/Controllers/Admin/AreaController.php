<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB,
    Mail,
    Auth,
    Validator,
    Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Response;
use Config;
use App;
use App\Model\Admin\Shop;
use App\Model\Admin\DeliveryAreaList;
use App\User as AuthUser;
use Hash;
use App\Model\Admin\DeliveryAreaListShop;

class AreaController extends Controller {

    public function index(Request $request) {

        $paginate = 10;
        $areas =  DeliveryAreaList::paginate($paginate);
        return view('area.list', compact('areas'));
    }

    public function create() {
        return view('/area/form');
    } 

    public function store(Request $request) {
        $rules     = array('delivery_area' => 'unique:delivery_area_lists,delivery_area',
                   );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput()->with(['flash_error' => "Mandatory fields missing"]);
        } else {
        DeliveryAreaList::Create(['shop_id' => 0,'delivery_area' => $request->delivery_area]);
        }
        return Redirect::to('dashboard/areas');
    }

    Private function uploadStoreLogo($request) {

        if ($request->hasFile('servicelogo')) {
            $file = $request->file('servicelogo');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $path = public_path() . '/images/offers/';
            $image = '/images/offers/'.$filename;
            $file->move($path, $filename);
        } elseif ($request->has('servicelogo') && $request->servicelogo == '') {
                
                $image = $request->lasthidimg;
        }
        else{
                $image = $request->lasthidimg;
        }
        return $image;    

    }

    public function show($id) {
        $area  = DeliveryAreaList::where('id',$id)->first();
        return view('/area/more', compact('area'));
    }

    public function edit($id) {
        $area  = DeliveryAreaList::where('id',$id)->first();
        return view('area.form', compact('area'));
    } 

    public function update(Request $request, $id) {
        $offer = DeliveryAreaList::find($id);
        $offer->update(['shop_id' => 0,'delivery_area' => $request->delivery_area]);
        return Redirect::to('dashboard/areas');  
    }

    public function destroy($id) {
        $area = DeliveryAreaList::findOrFail($id);
        //$offer->update(['status' => 0]);
        $area->delete();

        return Redirect::to('dashboard/areas');  
    }  

    public function checkService(Request $request){

        $service = Servicetype::where('service_name_en',$request->servicename)->first();
        if($service){

            return 1;
        }
        else{

            return 0;
        }
    } 

    public function deleted(){

        $paginate = 10;
        $services =  Servicetype::onlyTrashed()->paginate($paginate);
        return view('/shops/deleted', compact('services'));
    } 

    public function restore($id) {
        $tags = Servicetype::withTrashed()->find($id);
        $tags->restore();
        return Redirect::to('deleted/shops');  
    }     

}
