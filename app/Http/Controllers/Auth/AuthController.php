<?php

namespace App\Http\Controllers\Auth;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Contracts\Auth\Guard;
use URL;
use Redirect;
use DB;

use App\Model\Cart;
use App\User;
use App\Model\Otp;
use App\Helpers\Helper;
use Hash;
use Exception;

use Carbon\Carbon;


class AuthController extends Controller
{
	public function __construct(Guard $auth)
    {
        $this->auth = $auth;

        //$this->middleware('guest', ['except' => 'getLogout']);
    }
	public function customer_login(Request $request)
    {
        // Check validation
        $phone = $request->phone;
        $validator = Validator::make($request->all(), [
            'phone' => 'required|regex:/[0-9]{10}/|digits:10',            
            ]
        );
        if (!$validator->passes()) {
            $response = array('error'=>'Oops! something wrong with data');
            return response()->json($response);

        } else{ 
             // Get user record
            $user_model = new User;

            $user = User::where('phone', $phone)->first();
            $otp = Otp::where('phone', $phone)->delete();
            $credential = $request->only('phone');

            if($user){               
                
                $data = User::where('phone', $phone)->first();
                $user_id = $data->id;
            } else{
                
                // Inserting data to temporary user table
                $user_model->phone = $phone;
                $user_model->role_id = 0;
                $user_model->password = Hash::make($phone);
                $user_model->is_otp_verified = 1;
                $data = $user_model->save();
                $user_id = $user_model->id; 
            }

            if($data){
                ob_start();

                
                $user_phone = $phone;
                $phone_no = '91'.$user_phone;
                $otp = 1234;                    
                // $this->send_sms($otp, $phone_no);

                ob_end_clean();
                // Sent otp
                $otp_model = new Otp;
                $otp_model->otp = $otp; // need to genrate random number later
                $otp_model->user_id = $user_id;
                $otp_model->phone = $phone;
                $otp_data = $otp_model->save();             
                
                $response = array('success' => 'Otp send Successfully', 'status' => true,'hidden_phone' =>$phone);
                
            } else{
                $response = array('error' => 'Oops! we cannot send OTP. Please try again later', 'status' => false);
            }
            return response()->json($response);
        }  
        
    }
    public function verify(Request $request) 
    {
        // if ($request->ajax()) {
            $clientIP = \Request::ip();
            $otp = $request->input('otp');
            $phone = $request->input('hidden_phone');

            $otp_model = new Otp;
            $user_model = new User;
            $verify = $otp_model->where([['otp',$otp],['phone',$phone]])->first();

            if ($verify) {
                    // Update api token and otp verified status
                $update_token = $user_model->where('phone', $phone)  
                                ->update(['is_otp_verified' => TRUE]);
                
                $user_obj = $user_model->where('phone', $phone)->first(); 
                    
                //update customer ip cart items to cutomer 
                $update_cart_customerIp = Cart::where('customer_ip',$clientIP)->update(['user_id'=>$user_obj->id]); 
                
                // deleting otp for avoiding mismatch
                $otp_model->where('phone', $phone)->delete();

                Auth::login($user_obj);

                $response = array('success' => 'OTP verified successfully', 'status' => true);
            } else {
                $otp_model->where('phone', $phone)->delete();
                $response = array('error' => 'OTP verified failed!Please try again', 'status' => false);
            }     
            return response()->json($response);
        // }
    }
    public function logout(Request $request) {

        Auth::logout();        
        return redirect()->route('index');
    }
}