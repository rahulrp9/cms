<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Admin\Category;
use App\Model\Admin\Products;
use App\Model\Cart;
use App\Model\userDetails;
use App\Model\Admin\Offers;
use App\Model\productOfers;
use App\Model\Order;
use App\Model\OrderDetails;
use App\Model\Payment;
use App\Model\Admin\Company;
use App\Model\Admin\PageContents;
use App\Mail\ConfirmMail as AppConfirmMail;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\User;
use Carbon;
use Auth;
use DB;

class IndexController extends Controller
{
	public function index()
    {        
        return view('index');
    }

}
