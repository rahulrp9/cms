<?php

namespace App\Helpers; 

use DB;
use JWTAuth;
use App\Helpers\Helper;
use Carbon\Carbon;
use App\User;
use App\Model\Admin\UserToken;
use Mail;

class Helper {

	/**
     * To fetch the 
     * @param UserId
     */
    public static function testHelper() {

        return "testHelp";
    }

        /**
     * Functio to generate jwt token for user authentication in API
     * @param user_id 
     * @return token
     * @author Moban
    */
    public static function generateJwtToken($user_id) { 

        $token = JWTAuth::fromUser(User::select('id')->where('id', $user_id)->first());
        UserToken::create(array('user_id' => $user_id,'token' => $token));
        // try {

        //   $token = JWTAuth::getToken();

        //   if ($token) {

        //     JWTAuth::setToken($token)->invalidate();            

        //   } 

        //   $token = JWTAuth::fromUser(User::select('id')->where('id', $user_id)->first());

        // } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

        //     $token = JWTAuth::fromUser(User::select('id')->where('id', $user_id)->first());

        // } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

        //     $token = JWTAuth::fromUser(User::select('id')->where('id', $user_id)->first());
        // }

        return $token;
    } // End of function


    /**
     * Functio to get payment parameters
     * @param transaction_id, amount, description 
     * @return array
     * @author Moban
    */
    
    public static function sendOtp($email = ''){
        
        $subject = 'OTP Verification';
        $emailtemplate = 'emailtemplates.verification_mail';
        //$otp = rand(1350, 4500); 
        $otp = 1234;

        return $otp;
    }
    
    
    public static function getQatarTime($utcdate){

        $changedDate = Carbon::parse($utcdate)->timezone('Asia/Qatar');
        return $changedDate;

    }
  

    public static function generateJwtTokenLogin($user_id) { 

        //$token = JWTAuth::fromUser(User::select('id')->where('id', $user_id)->first());
        
        try {

          $token = JWTAuth::getToken();

          if ($token) {

            JWTAuth::setToken($token)->invalidate();            

          } 

          $token = JWTAuth::fromUser(User::select('id')->where('id', $user_id)->first());

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            $token = JWTAuth::fromUser(User::select('id')->where('id', $user_id)->first());

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            $token = JWTAuth::fromUser(User::select('id')->where('id', $user_id)->first());
        }

        return $token;
    }

    public static function tokenInvalidate($user_id){

        $tokens = UserToken::select('token','id')->where('user_id', '=', $user_id)->get();
        if ($tokens) {

            foreach ($tokens as $token) {
            try { 
                JWTAuth::setToken($token->token)->invalidate();    
            } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

               UserToken::where('id', '=', $token->id)->delete();       
           }
       }

       UserToken::where('user_id', '=', $user_id)->delete();
   }
   $token = JWTAuth::fromUser(User::select('id')->where('id', $user_id)->first());
   return $token;

}

    
    public static function getTimeMinute($time){

        $t = $time;
        $h = floor($t/60) ? floor($t/60) .' hour' : '';
        $m = $t%60 ? $t%60 .' minutes' : '0 minutes';
       return $h.' '.$m;
    } 

    public static function generateRandomString($length = 6) {

      return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);

    }

    public static function getCity($id)
  {
    return \DB::table('cities')->select('name')->where('id', $id)->first();
  }  
    
} // End of class

